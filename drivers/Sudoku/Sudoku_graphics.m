%                      Sudoku_graphics.m
%                  written on Jan. 23, 2005 by
%                        Russell Luke
%                  University of Delaware
%
% DESCRIPTION:  Script driver for viewing results from projection
%               algorithms on various toy problems
%
% INPUT:  
%              method = character string for the algorithm used.
%         true_object = the original image
%                 u_0 = the initial guess
%                   u = the algorithm "fixed point"
%                   S = the support constraint
%                   M = Fourier magnitude data
%                 snr = signal to noise ratio in original image
%              change = the norm square of the change in the
%                              iterates
%              error  = squared set distance error at each
%                              iteration
%              noneg = the norm square of the nonnegativity/support
%                              constraint at each iteration
%              gap  = the norm square of the gap distance, that is
%                     the distance between the projections of the
%                     iterates to the sets
%
% OUTPUT:       graphics
% USAGE: Sudoku_graphics(method_input,method_output)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function success = Sudoku_graphics(method_input, method_output)
              
method=method_input.method;
beta0 = method_input.beta_0;
beta_max = method_input.beta_max;
u_0=method_input.Gegebenes_Sudoku;
% LOESEN_Rueckmodellierung(method_input, method_input.Gegebenes_Sudoku);
u=LOESEN_Rueckmodellierung(method_input, method_output.u1)
u2=LOESEN_Rueckmodellierung(method_input, method_output.u2)
iter = method_output.stats.iter;
change = method_output.stats.change;
if(any(strcmp('time', fieldnames(method_output.stats))))
    time = method_output.stats.time;
else
    time=999
end


figure(900);

    subplot(2,2,1); imagesc((u_0)); axis equal tight; colorbar; title('Given Sudoku'); drawnow; 
    subplot(2,2,2); imagesc((u)); axis equal tight; colorbar; title('Solution'); drawnow; %caxis([4.85,5.35]);
    subplot(2,2,3);   semilogy(change),xlabel('iteration'),ylabel(['log of iterate difference'])
    label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
    title(label)
    if(any(strcmp('diagnostic', fieldnames(method_input))))
        gap  = method_output.stats.gap;
        label = [ 'iteration', ', time = ',num2str(time), 's'];        
        subplot(2,2,4);   semilogy(gap),xlabel(label),ylabel(['log of the gap distance'])
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(label)
    end

success = 1;
