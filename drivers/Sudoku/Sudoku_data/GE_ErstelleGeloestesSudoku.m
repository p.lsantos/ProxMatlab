%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function FertigesSudoku=GE_ErstelleGeloestesSudoku()

%Fkt. zum Erstellen eines gelösten Sudokus
%------------------------------------------------------------------------
%Initialisieren

Sudoku=zeros(9,9);
Quadrat=1;
%------------------------------------------------------------------------


%In einigen Fällen wird ein Sudoku generiert, dass das Setzen der höheren
%Einträge (7,8,9) nicht mehr möglich macht. Somit würden Stellen im Sudoku
%fehlen. Ein Beipsiel dafür findet sich in der obigen Arbeit. Die Fkt.
%GE_MesseDenErfolgBeimErstellen() testet, wie oft ein nicht vollständiges
%Sudoku erzeugt wird.

while(min(min(Sudoku))==0)
    
    Randommatrix=GE_Zufallsmatrix();
    Sudoku=zeros(9,9);
    
    for Eintrag=1:9
        [Sudoku, Randommatrix, moeglich]=...
            GE_SetzeEintraege(Sudoku, Randommatrix, Quadrat, Eintrag);
    end
    
end

FertigesSudoku=Sudoku;
end