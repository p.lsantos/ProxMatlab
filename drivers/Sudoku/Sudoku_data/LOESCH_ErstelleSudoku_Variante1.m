%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function Sudoku1=LOESCH_ErstelleSudoku_Variante1(Sudoku, ...
    AnzahlZuLoeschendeZahlen)

%Diese Funktion löscht eine Zahl nur, wenn dadurch die Eindeutigkeit
%erhalten bleibt
%------------------------------------------------------------------------

AnzahlGeloeschteZahlen=0;
Sudoku1=Sudoku;
Sudoku2=zeros(9,9,2);   %Zweite Ebene gibt an, ob die Zahl getestet wurde

while(AnzahlGeloeschteZahlen<AnzahlZuLoeschendeZahlen)
    
    %Lösche eine mögliche noch nicht getestete Zahl
    [Sudoku2(:,:,1),Sudoku2(:,:,2)]=LOESCH_EntferneEinenEintrag1(...
        Sudoku1, Sudoku2(:,:,2));
    
    
    Eindeutig=EDK_TesteEindeutigkeit_DerReiheNach(Sudoku2);
    
    %Wenn die Eindeutigkeit trotz des Löschens erhalten geblieben ist,
    %akzeptiere das Löschen.
    
    if(Eindeutig==true)
        Sudoku1=Sudoku2(:,:,1);
        AnzahlGeloeschteZahlen=AnzahlGeloeschteZahlen+1;
    end
    
    %Falls keine Zahl mehr gelöscht werden kann, ohne dass das Sudoku
    %mehrdeutig wird, starte Algorithmus neu.
    if(min(min(Sudoku2(:,:,2)))==1  && ...
            AnzahlGeloeschteZahlen<AnzahlZuLoeschendeZahlen)
        
        Sudoku1=LOESCH_ErstelleSudoku_Vergleich1(...
            Sudoku, AnzahlZuLoeschendeZahlen);
        AnzahlGeloeschteZahlen=AnzahlZuLoeschendeZahlen;
    end
end

end