%                  Sudoku_data_processor.m
%                written on Sept 27, 2012 by
%                     Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  
%
% INPUT: input = a data structure
% OUTPUT: input = a data structure
%
% USAGE: input = Sudoku_data_processor(input) 
%
% Data loaded:  
% Nonstandard function calls: GE_ErstelleGeloestesSudoku, 
%      LOESCH_ErstelleSudoku_schnell, LOESEN_Modellierung, 
%      Proj1, Proj2...
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data reader/processor

function input = Sudoku_data_processor(input)

data_ball = input.data_ball;

input.Nx=9;
input.Ny=9;
input.Nz=9;

input.true_solution = GE_ErstelleGeloestesSudoku; 
input.Gegebenes_Sudoku = LOESCH_ErstelleSudoku_schnell(input.true_solution,input.difficulty);
input.norm_data=81; %  this is correct since
                                 % is is calculated in the 
                                 % object domain
input.product_space_dimension = 4;
input.projectors=[['Sudoku_Proj_Z'];...
    ['Sudoku_Proj_S'];...
    ['Sudoku_Proj_Q'];...
    ['Sudoku_Proj_G']];
X_0=feval('LOESEN_Modellierung',input.Gegebenes_Sudoku);
		% Start-Matrix

input.u_0=feval(input.Prox1,input,X_0);
input.u_0=feval(input.Prox2,input,input.u_0);

clear X_0
end



