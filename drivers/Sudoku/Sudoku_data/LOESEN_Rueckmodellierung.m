%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function Matrix=LOESEN_Rueckmodellierung(input,A)

%Diese Funktion erhält ein x_i aus X^4 (die Lösung unseres Sudokus) und
%erstellt daraus wieder ein 9x9 Sudoku-Rätsel

%------------------------------------------------------------------------
Matrix=zeros(9);
%------------------------------------------------------------------------

A=feval(input.Prox2,input,A);
A(:,:,:,1)=Sudoku_Proj_G(input,A(:,:,:,1));

for x=1:9
    for y=1:9
        for z=1:9
            
            if(A(x,y,z,1)~=0)
                Matrix(x,y)=z;
            end
            
        end
    end
end
end
