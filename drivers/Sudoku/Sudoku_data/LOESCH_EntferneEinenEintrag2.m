%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function Sudoku=LOESCH_EntferneEinenEintrag2(Sudoku)

% Dieser Algorithmus entfernt eine Zahl aus dem gelösten Sudoku.
%------------------------------------------------------------------------

ungeloescht=true;

while(ungeloescht)
    
    x=round(9*rand(1)+0.5);
    y=round(9*rand(1)+0.5);
    
    if(Sudoku(x,y)~=0)
        Sudoku(x,y)=0;
        ungeloescht=false;
    end
    
end
