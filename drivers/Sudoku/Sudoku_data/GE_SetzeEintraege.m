%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function [Sudoku2, Random2, moeglich2]=GE_SetzeEintraege(Sudoku, Randommatrix, Quadrat, Eintrag)

%Initialisieren
%---------------------------------------------------------
Sudoku2=Sudoku;
Ergebnisgefunden=false;
Startfeld=round(9*rand(1)+0.5);
Feld=Startfeld;
%---------------------------------------------------------


while(Ergebnisgefunden==false)                                              %Solange das Quadrat nicht vollständig nach Lösungen abgesucht wurde
    
        x=Randommatrix(Feld,2,Quadrat)+(round((Quadrat-5)/3)+1)*3;          %Beginne zufällig in einem Quadrat
        y=Randommatrix(Feld,3,Quadrat)+mod(Quadrat-1,3)*3;

        Sudoku2(x,y)=Eintrag;

        if(GE_RegelVerstos(Sudoku2,x,y)==false && Randommatrix(Feld,4,Quadrat)==0)     %Falls Wert an untersuchte Stelle gesetzt werden darf

            Randommatrix(Feld,4,Quadrat)=1;        %Markiere Feld als besetzt
    
            if(Quadrat~=9)                          %Betrachte Wert vorläufig als richtig und untersuche nächstes Quadrat
                [Sudoku3,Random3,moeglich3]=GE_SetzeEintraege(Sudoku2, Randommatrix, Quadrat+1,Eintrag);
       
                if(moeglich3)
                    moeglich2=true;
                    Sudoku2=Sudoku3;
                    Random2=Random3;
                    Ergebnisgefunden=true;
                else                                %Falls der Wert im folgenen Schwirigkeiten gemacht hat, lösche ihn wieder und untersuche die nächste Stelle bzw. gehe zum Vorherigen Quadrat zurück
                    Sudoku2(x,y)=Sudoku(x,y);
                    Randommatrix(Feld,4,Quadrat)=0;
                    Feld=mod(Feld,9)+1;

                     if(Feld==Startfeld)
                        moeglich2=false;
                        Sudoku2=Sudoku;
                        Random2=Randommatrix;
                        Ergebnisgefunden=true;
                     end
                end
    
            else                    % Falls eine erlaubte Stelle im letzten Quadrat gefunden wurde, akzeptiere die Lösung für den Eintrag
                moeglich2=true;

                Random2=Randommatrix;
                Ergebnisgefunden=true;
            end
    
    
    
    
        else                        %Falls die Stelle verboten ist, schaue die nächste Stelle an oder gehe ein Quadrat zurück
            Feld=mod(Feld,9)+1;
            Sudoku2(x,y)=Sudoku(x,y);

            if(Feld==Startfeld)
                moeglich2=false;
                Sudoku2=Sudoku;
                Random2=Randommatrix;
                Ergebnisgefunden=true;
            end
end
end

end
