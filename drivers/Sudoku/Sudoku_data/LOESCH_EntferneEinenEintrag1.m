%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function [Sudoku, Getestet]=LOESCH_EntferneEinenEintrag1(Sudoku,Getestet)

% Dieser Algorithmus entfernt eine Zahl aus dem gelösten Sudoku. Dies
% geschieht jedoch nur, wenn die Zahl noch nicht als unlöschbar erkannt
% wurde. Die Zahl ist unlöschbar, wenn durch das Löschen die Eindeutigkeit
% des Rätsels verloren geht.
%------------------------------------------------------------------------

ungeloescht=true;

while(ungeloescht)
    
    x=round(9*rand(1)+0.5);
    y=round(9*rand(1)+0.5);
    
    if(Sudoku(x,y)~=0 && Getestet(x,y)~=1)
        Sudoku(x,y)=0;
        Getestet(x,y)=1;
        ungeloescht=false;
    end
    
end
