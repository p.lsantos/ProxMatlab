%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function FertigesSudoku=GE_MessenDerErfolgeBeimErstellen()

%Diese Funktion misst, wie oft bei 10.000 Ausführungen der "Variante 2 zum 
%Lösen" ein gelöstes Sudoku entsteht. Dabei wird beim Generieren nicht
%abgefragt, ob wirklich ein gelöstes Sudoku entstanden ist. Ein Beispiel für
% ein solches nichtgelöstes Sudoku ist in der obigen Arbeit zu finden.
%------------------------------------------------------------------------

Sudoku=zeros(9,9);
Quadrat=1;
%------------------------------------------------------------------------

Nichterfolgreich=0;

for i=1:10000
    
    Randommatrix=GE_Zufallsmatrix();
    Sudoku=zeros(9,9);

    for Eintrag=1:9
        [Sudoku, Randommatrix, moeglich]=GE_SetzeEintraege(...
            Sudoku, Randommatrix, Quadrat, Eintrag);
    end
    
    if(min((Sudoku))==0)
        Nichterfolgreich=Nichterfolgreich+1;
    end
    
end

end