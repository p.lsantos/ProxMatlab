%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function [Ergebnisse]=EDK_SetzeEintraege_DerReiheNach(...
    Sudoku, Quadrat, Eintrag, Ergebnisse)

%Diese Fkt. testet rukursiv für alle leeren Kästchen des Sudokus alle
%Zahlen von 1 bis 9 und sucht nach der Anzahl der Lösungen. Bei mehr als
%einer Lsg. bricht der Algorithmus direkt ab und gibt 2 Ergebnisse zurück.
%Es wäre ebenfalls möglich die gefundenen Lösungen ausgeben zu lassen und
%den Algorithmus als Sudoku-Löser zu verwenden.
%------------------------------------------------------------------------


%Initialisieren
%------------------------------------------------------------------------
Sudoku2=Sudoku;
vorhanden=false;
x=1;
y=1;

%------------------------------------------------------------------------
%Solange nicht alle Kästchen eines Quadrats auf die Möglichkeit den Eintrag
%zu setzen untersucht wurden und solange keine Mehrdeutigkeit des
%Sudokus nachgewiesen wurde, untersuche die Kästchen auf die Möglichkeit
%den Eintrag zu setzen. Falls dies gelungen ist, untersuche das nächste
%Quadrat. Falls bereits das letzte (9.) Quadrat untersucht wurde und eine
%Lösung immernoch möglich ist, untersuche das erste Quadrat für den
%nächsten Eintrag. Falls Eintrag==9 und Quadrat==9 und jede Zahl gesetzt
%werden konnte, ist eine Lösung des Sudokus gefunden worden. Schaue dir
%dann weitere Möglichkeiten an, die Zahlen zu setzen.
%------------------------------------------------------------------------

while(Ergebnisse<2 && (x<3 || y<3) && vorhanden==false)
    
    for a=1:3
        for b=1:3
            
            if(Sudoku(a+(round((Quadrat-5)/3)+1)*3,...
                    b+mod(Quadrat-1,3)*3)==Eintrag)
                
                vorhanden=true;
                
            end
        end
    end
    
    
    
    if(vorhanden==false)
        
        %Falls der entsprechende Eintag noch nicht im Sudoku vorhanden:
        
        for x=1:3
            for y=1:3
                
                x1=x+(round((Quadrat-5)/3)+1)*3;
                y1=y+mod(Quadrat-1,3)*3;
                
                Sudoku2(x1,y1)=Eintrag;
                
                %Untersuche ob der Eintrag an die untersuchte Stelle
                %gesetzt werden darf
                
                if(Sudoku(x1,y1)==0 && GE_RegelVerstos(Sudoku2,x1,y1)==false)
                    if(Quadrat~=9)
                        [Ergebnisse]=EDK_SetzeEintraege_DerReiheNach(...
                            Sudoku2, Quadrat+1, Eintrag, Ergebnisse);
                    else
                        if(Eintrag~=9) [Ergebnisse]=...
                                EDK_SetzeEintraege_DerReiheNach(...
                                Sudoku2, 1, Eintrag+1, Ergebnisse);
                        else
                            Ergebnisse=Ergebnisse+1;
                            break;
                            
                        end
                    end
                    
                end
                
                Sudoku2(x1,y1)=Sudoku(x1,y1);
                
            end
        end
        
        %Falls der Eintrag schon durch das gegebene Sudoku in dem Quadrat
        %vorhanden ist
    else
        
        if(Quadrat~=9)
            [Ergebnisse]=EDK_SetzeEintraege_DerReiheNach(...
                Sudoku2, Quadrat+1, Eintrag, Ergebnisse);
        else
            if(Eintrag~=9) [Ergebnisse]=EDK_SetzeEintraege_DerReiheNach(...
                    Sudoku2, 1, Eintrag+1, Ergebnisse);
            else
                if(x==1 && y==1)
                    Ergebnisse=Ergebnisse+1;
                end
            end
        end
        
    end
end
end


        
            
        
        