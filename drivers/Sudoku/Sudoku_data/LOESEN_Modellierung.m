%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
function Sudoku=LOESEN_Modellierung(Gegebenes_Sudoku)

% Diese Funktion erstellt unsere Startmatrix x_0=( x_0,1 , x_0,2 , x_0,3...
%, x_0,4 ) für die Iteration. Bei der Startmatrix gilt x_0,i=x_0,j für alle
% i,j aus {1,2,3,4}
%------------------------------------------------------------------------

% Starte mit 4 9x9x9- Matrizen voll mit Zufallszahlen zwischen 0 und 1
%------------------------------------------------------------------------
Sudoku=zeros([9 9 9 4]);
Sudoku(:,:,:, 1)=rand([9 9 9 1]);
%------------------------------------------------------------------------

% Wenn das gebene Sudoku einen Eintrag bei (x,y) hat, passe die
% entsprechende Zeile, Spalte, den entspr. Turm und das entspr. Quadrat in
% unserer 9x9x9-Matrix an, indem nicht mehr in Frage kommende Kästchen mit
% einer Null gefüllt werden.
%------------------------------------------------------------------------

for x=1:9
    for y=1:9
        
        if(Gegebenes_Sudoku(x,y)>0)
            
            Sudoku(x,y,:,1)=zeros(1,1,9,1);
            Sudoku(x,:, Gegebenes_Sudoku(x,y) ,1)=zeros(1,9,1,1);
            Sudoku(:,y,Gegebenes_Sudoku(x,y) , 1)=zeros(9,1,1,1);
            
            % Im Folgenden werden die Zahlen 1,2,3 auf 0 ; 4,5,6 auf 1 ; 
            % 7,8,9 auf 2 abgebildet. Dadurch kann über x,y das Quadrat 
            % bestimmt und abgesucht werden.
            
            for k=1:3
                for m=1:3
                    
                    Sudoku( (round((x-5)/3) +1) *3+k, (round((y-5)/3)+1)...
                        *3+m, Gegebenes_Sudoku(x,y) ,1)=0;   
                end
            end
            
            Sudoku(x,y,Gegebenes_Sudoku(x,y), 1)=1;         
        end
        
    end
end

% Klone die Matrix x_0,1 dreimal
for Bed=2:4
    
    Sudoku(:,:,:,Bed)=Sudoku(:,:,:,1);
    
end

end



            
