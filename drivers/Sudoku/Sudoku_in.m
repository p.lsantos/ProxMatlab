%                      Sudoku_in.m
%              written on September 16, 2012 by
%                        Russell Luke
%                 University of Goettingen
%
% DESCRIPTION:  parameter input file for main_ProxToolbox.m
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function input = Sudoku_in(input)

%% We start very general.
%%
%% What type of problem is being solved?  Classification 
%% is according to the geometry:  'Affine', 'Phase', 
%% 'Affine-sparsity',  'Custom'

input.problem_family = 'Sudoku';
input.expert=false; % false = uses  algorithm_wrapper.m

%%==========================================
%% Problem parameters
%%==========================================
%% What is the name of the data file?
input.data_filename = 'Sudoku_data_processor';



%% What type of object are we working with?
%% Options are: 'phase', 'real', 'nonnegative', 'complex'
input.object = 'complex';

%% What type of constraints do we have?
%% Options are: 'support only', 'real and support', 'nonnegative and support',
%%              'amplitude only', 'sparse real', 'sparse complex', and 'hybrid'
%%              'convex'
input.constraint = 'nonconvex';

%% What type of measurements are we working with?
%% Options are: 'single diffraction', 'diversity diffraction', 
%%              'ptychography', 'complex', and 'diversity affine'
input.experiment = 'complex';
input.difficulty = 62;  % this is a number between 1 and 64.  The closer to 
			% 64, the harder the problem, and the longer it takes
			% to generate a uniquely solvable puzzle.

%% What are the dimensions of the measurements?
% given in the Sudoku_data_processor


%%==========================================
%%  Algorithm parameters
%%==========================================
%% Now set some algorithm parameters that the user should be 
%% able to control (without too much damage)


% Point to appropriate projectors.

input.Prox1 = 'P_Parallel'; % projection onto the constraints
input.Prox2='P_Diag'; % projection onto diagonal constraint
% the specific projections in Prox1 are names in the data_Processor

%% Algorithm:
input.method = 'RAAR'; %'AP', 'Cimmino';  
input.numruns=1; % the only time this parameter will
% be different than 1 is when we are
% benchmarking, that is, when algorithm performance statistics 
% are being generated for randomly generated problems/initial 
% values etc.  



%% maximum number of iterations and tolerances
input.MAXIT = 2000;
input.TOL = 1e-9;

%% relaxaton parameters in RAAR, HPR and HAAR
input.beta_0 = 1.0;  % starting relaxation prameter (only used with
% HAAR, HPR and RAAR)
input.beta_max =1.0;   % maximum relaxation prameter (only used with
% HAAR, RAAR, and HPR)
input.beta_switch = 1; % iteration at which beta moves from beta_0 -> beta_max

%% parameter for the data regularization
%% need to discuss how/whether the user should
%% put in information about the noise
input.data_ball = 1e-15;
% the above is the percentage of the gap
% between the measured data and the
% initial guess satisfying the
% qualitative constraints.  For a number
% very close to one, the gap is not expected
% to improve much.  For a number closer to 0
% the gap is expected to improve a lot.
% Ultimately the size of the gap depends
% on the inconsistency of the measurement model
% with the qualitative constraints.

%%==========================================
%% parameters for plotting and diagnostics
%%==========================================
% input.diagnostic = true;
input.verbose = 0; % options are 0 or 1
input.graphics = 1; % whether or not to display figures, options are 0 or 1.
                   % default is 1.
input.anim = 0;  % whether or not to disaply ``real time" reconstructions
                % options are 0=no, 1=yes, 2=make a movie
                % default is 1.
input.animation='none';
input.graphics_display = 'Sudoku_graphics'; % unless specified, a default 
                            % plotting subroutine will generate 
                            % the graphics.  Otherwise, the user
                            % can write their own plotting subroutine

%%======================================================================
%%  Technical/software specific parameters
%%======================================================================
%% Given the parameter values above, the following technical/algorithmic
%% parameters are automatically set.  The user does not need to know 
%% about these details, and so probably these parameters should be set in 
%% a module one level below this one.  

end

