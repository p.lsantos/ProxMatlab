%                   Sudoku_ProjektionZeile.m
%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
% DESCRIPTION:  Projection subroutine onto the row constraints in a
%               sudoku problem
%
% INPUT:       input = a data structure with fields (not used here)
%                 A = array in the domain to be projected
%
% OUTPUT:       backZeileMatrix = the projection
%               
% USAGE: backZeileMatrix=Sudoku_ProjektionZeile(input,A)
%
% Nonstandard Matlab function calls:  none

function backZeileMatrix=Sudoku_ProjektionZeile(input,A)

% Diese Funktion führt die Zeilenprojektion an einer ihr übergebenen
% 9x9x9-Matrix aus
%------------------------------------------------------------------------

backZeileMatrix=zeros([9 9 9]);
for y=1:9
    for z=1:9
        
        %----------------------------------------------------------------
        %Bestimme den max. Eintrag
        %Erzeuge Null-Vektor und setze an die Stelle des max. Eintrags 1
        
        
        [MAX,Koordinate]=max(A(:,y,z));
        vector=zeros([9 1 1]);
        vector(Koordinate)=1;
        backZeileMatrix(:,y,z)=vector;
        
    end
end
clear A input
return
