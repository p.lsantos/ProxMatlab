function MatrixB=LOESEN_ProjektionB(A)

% Diese Funktion stellt die Projektion P_B dar
%------------------------------------------------------------------------

MatrixB(:,:,:,1)=1/4*(A(:,:,:,1)+A(:,:,:,2)+A(:,:,:,3)+A(:,:,:,4));

for i=2:4
    MatrixB(:,:,:,i)=MatrixB(:,:,:,i-1);
end

end
