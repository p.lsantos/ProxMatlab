%                   Sudoku_ProjektionQuadrat.m
%                written on July 26, 2012 by 
%                   Alexander Stalljahnn
%                 University of Goettingen
%
% DESCRIPTION:  Projection subroutine onto the box constraints in a
%               sudoku problem
%
% INPUT:       input = a data structure with fields (not used here)
%                 A = array in the domain to be projected
%
% OUTPUT:       backQuadratMatrix = the projection
%               
% USAGE: backQuadratMatrix=Sudoku_ProjektionQuadrat(input,A)
%
% Nonstandard Matlab function calls:  none
function backQuadratMatrix=Sudoku_ProjektionQuadrat(input, A)

% Diese Funktion führt die Quadratprojektion an einer ihr übergebenen
% 9x9x9-Matrix aus
%------------------------------------------------------------------------

backQuadratMatrix=zeros([9 9 9]);
for z=1:9
    for x=1:3:9
        for y=1:3:9
            
            %Bestimme erst die Zeile in der das Max. steht
            %Bestimme dann die Spalte in der das Max. steht
            
            [max_value_vec,y_koor_max_value_vec]=max(A(x:(x+2),y:(y+2),z));
            [max_value,x_koor_max_value]=max(max(A(x:(x+2),y:(y+2),z)));
            
            Quadrat=zeros(3);
            Quadrat(y_koor_max_value_vec(x_koor_max_value),x_koor_max_value)=1;
            backQuadratMatrix(x:(x+2),y:(y+2),z)=Quadrat;
        end
    end
end
clear A input
return
