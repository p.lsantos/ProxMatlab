%                            Custom.m
%                  written on May 23, 2012 by 
%                         Russell Luke
%       Inst. Fuer Numerische und Angewandte Mathematik
%                    Universitaet Gottingen
%
%
%
% This module sets the projectors based on the problem type
% For the problem family 'Custom' it doesn't really do anything
function [input, output] = Custom(input)

% for problem family 'Custom' we assume that the user has 
% named the projectors in either the parameter_filename 
% or the data_filename files (and, of course,  written 
% the projectors)
% apparently `exist' doesn't allow you to check elements of 
% data structures...:(
% if(~exist('input.product_space_dimension','var'))
%    input.product_space_dimension = 1;
% end
% if you are only working with two sets but 
% want to do averaged projections
% (= alternating projections on the product space)
% or RAAR on the product space (=swarming), then
% you will want to change product_space_dimension=2
% and adjust your input files and projectors accordingly. 
% you could also do this within the data processor

input.iter=0;
input.TOL2 = 1e-15; 
u_1 = feval(input.Prox1,input,input.u_0);
u_2 = feval(input.Prox2,input,u_1);
% estimate the gap in the relevant metric
tmp_gap=0;
if(input.Ny==1)||(input.Nx==1)
    tmp_gap = (feval('norm',u_1-u_2,'fro')/(input.norm_data))^2;
else
    for j=1:input.product_space_dimension
        % compute (||P_Sx-P_Mx||/normM)^2:
        tmp_gap = tmp_gap+(feval('norm',u_1(:,:,j)-u_2(:,:,j),'fro')/(input.norm_data))^2;
    end
end
clear u_1 u_2
gap_0=sqrt(tmp_gap);

% sets the set fattening to be a percentage of the
% initial gap to the unfattened set with 
% respect to the relevant metric (KL or L2), 
% that percentage given by
% input.data_ball input by the user.
input.data_ball=input.data_ball*gap_0;
% the second tolerance relative to the oder of 
% magnitude of the metric
input.TOL2 = input.data_ball*1e-15; 


%%===================================================================
%% 
%%   Run the algorithm!!!
%% 
%%===================================================================

output=feval('algorithm_wrapper',input);

iter = output.stats.iter;
stats=output.stats;
if(any(strcmp('time', fieldnames(output.stats))))
    time = output.stats.time;
    label = ['Took ',num2str(iter),' iterations and ', num2str(time), ' seconds.'];
    disp(label)
else
    label = ['Took ',num2str(iter),' iterations'];
    disp(label)
end
if(any(strcmp('diagnostic', fieldnames(input))))&& (strcmp(input.method,'RAAR'))
    tmp=min(stats.shadow_change);
else
    tmp=min(stats.change);
end
if(tmp>input.TOL)
    % label = ['Tolerance ',num2str(TOL), ' not achieved by iteration ', num2str(iter(j))];
    label = ['MAXIT=', num2str(input.MAXIT),' exceeded'];
    disp(label)
end
disp(' ')

%%===================================================================
%%
%%  now write output in form that comforms with standard format
%%  assumed in main_HohageLuke
%%
%%===================================================================

%%===================================================================
%% 
%%   Graphics (could also do this from within main_HohageLuke)
%% 
%%===================================================================
if(input.graphics==1)
    success = feval(input.graphics_display, input,output);
end
%%=========================================================================
%% 
%%   write results to file (could also do this from within main_HohageLuke)
%% 
%%=========================================================================

end
