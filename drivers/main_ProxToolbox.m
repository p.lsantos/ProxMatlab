%                      main_ProxToolbox.m
%                  written on May 23, 2012 by 
%                         Russell Luke
%       Inst. Fuer Numerische und Angewandte Mathematik
%                    Universitaet Gottingen
%
% DESCRIPTION: This is the main ProxToolbox module that calls all dependencies based
%              on the input file *_in.m.  Sets the paths used in the
%              toolbox realtive to the location of main_ProxToolbox.
%
% INPUT: input_files: data structures that set the parameters for problem
%        instances.  Unless you are benchmarking, there will usually only be one
%        input file.
%
% OUTPUT:  input:  the expanded data structures passed in to the processor. 
%          output: data structure, results of the algorithms
%          'keep_log':  if the input data structure has a keep_log=true
%          field, then the output will be written to a log file in the 
%          ../OutputData/ directory with a timestamp name. 
% 
% USAGE: [output, input] = main_ProxToolbox(input_files)
%
% PROXTOOLBOX FUNCTION CALLS: 
%           input_files: this is a list of parameter input files, called as functions, but really 
%                        these are just scripts. 
%           input.data_filename:   this is a call to the data processor 
%           input.problem_family:  the main_ProxToolbox passes to problem_family 
%                                  for standardized setup of fixed problem families, like
%                                  phase retreival (Phase.m) or computed tomography (ART.m)
%
%
%
function [output,input] = main_ProxToolbox(input_files)

% the parameter_filename will be a file in drivers/input_files
% 
[datasets,stringlength]=size(input_files);

close all;

%% Set Paths: (the order in which the path is added
%%    sets the priority in case there are multiple files
%%    with the same name.  Loading the SAMSARA path first
%%    ensures that all defaults in SAMSARA have lower
%%    priority that user defined OPTIONS installed in the 
%%    input_files directory below.  
% SAMSARA_directory = '../../../SAMSARA';
% addpath(SAMSARA_directory);
addpath('../Utilities','../ProxOperators','../Algorithms', '../InputData')
  %    '../OutputData',...  (some place where you save output data
addpath('Ptychography','Ptychography/ProxOperators','Ptychography/Data_processors',...
         'Ptychography/Data_processors/Robin', 'Ptychography/demos')
addpath('Misc', 'Misc/Misc_data', 'Misc/ProxOperators')
addpath('Phase','Phase/ProxOperators','Phase/demos','Phase/Data_processors')
addpath(   'Sudoku','Sudoku/ProxOperators','Sudoku/Sudoku_data')
 addpath(   'Sparsity','Sparsity/ProxOperators','Sparsity/Sparsity_data')
addpath(   'CT','CT/ProxOperators','CT/Data_processors','CT/demos')
addpath('Sensor_location','Sensor_location/ProxOperators','Sensor_location/Data')

%% read parameter file
input.dummy='cogito';
if datasets==1
    input = feval(input_files);
else
    input = feval(strtrim(input_files(1,:)), input);
end

% except for benchmarking purposes, numruns=1;
numruns=input.numruns;
for j=1:numruns % a loop for doing multiple runs (usually from random
    label = ['-------- RUN ', num2str(j), ' --------'];
    disp(label)
    input.run_number = j;   
    for jj=1:datasets
        input.dummy='cogito';
        if (jj==1)&& (j==1)
            % already loaded
        elseif (datasets==1)
            input = feval(input_files); % wipes old input clean.
        else
            input = feval(strtrim(input_files(jj,:)), input); % amends old input 
        end
        
        %%===================================================
        %% initialize the operator and get data
        %%===================================================
        if jj==1
            %% Load and process user data: the data file input.data_filename
            %% will be creaeted/edited by the user.  If it has to be in a certain
            %% format, i.e. conforming to our reference data structure and array
            %%  conventions, then
            %% at this point it would be very helpful to build a GUI interface
            %% allowing the user to create a properly formatted data input file.
            %% Would need to work with the users to design this (RL).
            input = feval(input.data_filename,input);
            
            % show the new parameter structures, completed by default values
        else
            % data already there
        end
        if input.verbose>=1
            input
        end
        
        %% perform iteration
        % I combined all the parameters and data into a single data structure for flexibility.
        % If the problem_family is 'Custom', then the user is
        % responsible for writing and naming the prox mappings Prox1 and
        % Prox2 (and Prox3 etc if more than 2)
        [input, output] = feval(input.problem_family,input);
        % we will need to standardize the output.  Below is just a placeholder
        % for a type of output.  For benchmarking this will need to be
        % collected in an array indexed by the numruns counter
        % stats(j)=output.stats;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% save results:  if no keep_log in the input file, nothing will be saved
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if any(strcmp('keep_log',fieldnames(input))) 
            if input.keep_log==1
                directory = ['../OutputData/' datestr(now,'yymmdd') input.problem_family];
                if ~exist(directory,'dir')
                    mkdir(directory);
                end
                % disp(['Saving input and output to ', directory])
                % fields={'Masks', 'rt_data', 'data', 'data_sq'}; input_save=rmfield(input, fields);
                % save([directory '/' input_files(jj,:) '_run' num2str(j) '.input.mat'],'input_save');
                fields={'u1', 'u2', 'u'}; output_save=rmfield(output, fields); 
                save([directory '/' input_files(jj,:) '_run' num2str(j) '.output.mat'],'output_save');
            end
        end
        % The following is for benchmarking.  The user will probably want
        % to customize this.
        if input.verbose>=1
            iter=output.stats.iter;
            benchmark.iter(datasets*j+jj-1)=iter;
            if(any(strcmp('time', fieldnames(output.stats))))
                time = output.stats.time;
                benchmark.time(datasets*j+jj-1)=time;
            else
            end            
            benchmark.end_change(datasets*j+jj-1)=output.stats.change(iter);
            tmp=output.stats.change(2:iter)./output.stats.change(1:iter-1);
            rate=sum(tmp(iter-10:iter-1))/10; % if algorithm exits in fewer than
                                              % 10 iterations, this will
                                              % throw an error because it
                                              % is taking an average over
                                              % the last 10 iterations.
                                              % Adjust accordingly.
            benchmark.rate(datasets*j+jj-1)=rate;
            benchmark.a_posteriori_error(j)=rate/(1-rate)*output.stats.change(iter);
            if(any(strcmp('diagnostic', fieldnames(input))))                
                if any(strcmp('truth',fieldnames(input)))&&any(strcmp('Relerrs', fieldnames(output.stats)))
                    benchmark.true_error(datasets*j+jj-1)=output.stats.Relerrs(iter);
                end
            end
            benchmark
        end
    end
    if(j<numruns) % clean out the input file and start fresh on the next run
        clear input
        input.dummy='cogito';
        input = feval(strtrim(input_files(1,:)), input);    
    end
end
% if any(strcmp('benchmark',fieldnames(input)))
%             if input.benchmark==1;
%                 directory = ['../OutputData/' datestr(now,'yymmdd') input.problem_family];
%                 if ~exist(directory,'dir')
%                     mkdir(directory);
%                 end
%                 disp(['Saving benchmarking data to ', directory])
%                 save([directory '/' input_files '.input.mat'],'input');
%                 save([directory '/' input_files '.benchmark.mat'],'benchmark');
%             end
% end



