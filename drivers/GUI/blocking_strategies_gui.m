function varargout = blocking_strategies_gui(varargin)
% BLOCKING_STRATEGIES_GUI MATLAB code for blocking_strategies_gui.fig
%      BLOCKING_STRATEGIES_GUI, by itself, creates a new BLOCKING_STRATEGIES_GUI or raises the existing
%      singleton*.
%
%      H = BLOCKING_STRATEGIES_GUI returns the handle to a new BLOCKING_STRATEGIES_GUI or the handle to
%      the existing singleton*.
%
%      BLOCKING_STRATEGIES_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BLOCKING_STRATEGIES_GUI.M with the given input arguments.
%
%      BLOCKING_STRATEGIES_GUI('Property','Value',...) creates a new BLOCKING_STRATEGIES_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before blocking_strategies_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to blocking_strategies_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help blocking_strategies_gui

% Last Modified by GUIDE v2.5 07-Jun-2015 11:51:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @blocking_strategies_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @blocking_strategies_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before blocking_strategies_gui is made visible.
function blocking_strategies_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to blocking_strategies_gui (see VARARGIN)

% Choose default command line output for blocking_strategies_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes blocking_strategies_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = blocking_strategies_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in blocking_switch.
function blocking_switch_Callback(hObject, eventdata, handles)
% hObject    handle to blocking_switch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns blocking_switch contents as cell array
%        contents{get(hObject,'Value')} returns selected item from blocking_switch


% --- Executes during object creation, after setting all properties.
function blocking_switch_CreateFcn(hObject, eventdata, handles)
% hObject    handle to blocking_switch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in blocking_scheme.
function blocking_scheme_Callback(hObject, eventdata, handles)
% hObject    handle to blocking_scheme (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns blocking_scheme contents as cell array
%        contents{get(hObject,'Value')} returns selected item from blocking_scheme


% --- Executes during object creation, after setting all properties.
function blocking_scheme_CreateFcn(hObject, eventdata, handles)
% hObject    handle to blocking_scheme (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in between_blocks_scheme.
function between_blocks_scheme_Callback(hObject, eventdata, handles)
% hObject    handle to between_blocks_scheme (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns between_blocks_scheme contents as cell array
%        contents{get(hObject,'Value')} returns selected item from between_blocks_scheme


% --- Executes during object creation, after setting all properties.
function between_blocks_scheme_CreateFcn(hObject, eventdata, handles)
% hObject    handle to between_blocks_scheme (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in within_blocks_scheme.
function within_blocks_scheme_Callback(hObject, eventdata, handles)
% hObject    handle to within_blocks_scheme (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns within_blocks_scheme contents as cell array
%        contents{get(hObject,'Value')} returns selected item from within_blocks_scheme


% --- Executes during object creation, after setting all properties.
function within_blocks_scheme_CreateFcn(hObject, eventdata, handles)
% hObject    handle to within_blocks_scheme (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function block_MAXIT_Callback(hObject, eventdata, handles)
% hObject    handle to block_MAXIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of block_MAXIT as text
%        str2double(get(hObject,'String')) returns contents of block_MAXIT as a double


% --- Executes during object creation, after setting all properties.
function block_MAXIT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to block_MAXIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in block_verbose.
function block_verbose_Callback(hObject, eventdata, handles)
% hObject    handle to block_verbose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns block_verbose contents as cell array
%        contents{get(hObject,'Value')} returns selected item from block_verbose


% --- Executes during object creation, after setting all properties.
function block_verbose_CreateFcn(hObject, eventdata, handles)
% hObject    handle to block_verbose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in block_anim.
function block_anim_Callback(hObject, eventdata, handles)
% hObject    handle to block_anim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns block_anim contents as cell array
%        contents{get(hObject,'Value')} returns selected item from block_anim


% --- Executes during object creation, after setting all properties.
function block_anim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to block_anim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

input = getappdata(0,'input');

list = get(handles.blocking_switch,'String');
input.blocking_switch = list(get(handles.blocking_switch,'Value'));
input.blocking_switch = char(input.blocking_switch);
list = get(handles.blocking_scheme,'String');
input.blocking_scheme = list(get(handles.blocking_scheme,'Value'));
input.blocking_scheme = char(input.blocking_scheme);
list = get(handles.between_blocks_scheme,'String');
input.between_blocks_scheme = list(get(handles.between_blocks_scheme,'Value'));
input.between_blocks_scheme = char(input.between_blocks_scheme);
list = get(handles.within_blocks_scheme,'String');
input.within_blocks_scheme = list(get(handles.within_blocks_scheme,'Value'));
input.within_blocks_scheme = char(input.within_blocks_scheme);
if strcmp(input.blocking_scheme,'divide') || strcmp(input.blocking_scheme,'orthogonal')
    input.block_rows = inputdlg('Number of rows used','Number',1,{''});
    input.block_rows = str2double(input.block_rows);
    input.block_cols = inputdlg('Number of columns used','Number',1,{''});
    input.block_cols = str2double(input.block_cols);
end
input.block_MAXIT = get(handles.block_MAXIT,'String');
input.block_MAXIT = str2double(input.block_MAXIT);
list = get(handles.block_verbose,'String');
input.block_verbose = list(get(handles.block_verbose,'Value'));
if strcmp(input.block_verbose,'true')
    input.block_verbose = 1;
else
    input.block_verbose = 0;
end
list = get(handles.block_anim,'String');
input.block_anim = list(get(handles.block_anim,'Value'));
if strcmp(input.block_anim,'true')
    input.block_anim = 1;
else
    input.block_anim = 0;
end

setappdata(0,'input',input); 

close(gcf);
