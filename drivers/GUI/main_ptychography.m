function varargout = main_ptychography(varargin)
% MAIN_PTYCHOGRAPHY MATLAB code for main_ptychography.fig
%      MAIN_PTYCHOGRAPHY, by itself, creates a new MAIN_PTYCHOGRAPHY or raises the existing
%      singleton*.
%
%      H = MAIN_PTYCHOGRAPHY returns the handle to a new MAIN_PTYCHOGRAPHY or the handle to
%      the existing singleton*.
%
%      MAIN_PTYCHOGRAPHY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN_PTYCHOGRAPHY.M with the given input arguments.
%
%      MAIN_PTYCHOGRAPHY('Property','Value',...) creates a new MAIN_PTYCHOGRAPHY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_ptychography_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_ptychography_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main_ptychography

% Last Modified by GUIDE v2.5 06-Jun-2015 16:17:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_ptychography_OpeningFcn, ...
                   'gui_OutputFcn',  @main_ptychography_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main_ptychography is made visible.
function main_ptychography_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main_ptychography (see VARARGIN)

% Choose default command line output for main_ptychography
handles.output = hObject;
handles.warmup_check = hObject;
handles.warmup_check = 0;
handles.blocking_strategies_check = hObject;
handles.blocking_strategies_check = 0;
handles.input_data_check = hObject;
handles.input_data_check = 0;
handles.use_generates_data_check = hObject;
handles.use_generates_data_check = 0;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes main_ptychography wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = main_ptychography_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in start_button.
function start_button_Callback(hObject, eventdata, handles)
% hObject    handle to start_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.use_generates_data_check || handles.warmup_check || handles.blocking_strategies_check || handles.input_data_check
    input = getappdata(0,'input'); 
end

if ~handles.use_generates_data_check && ~handles.input_data_check
    warndlg('You forget input data!')
    return
end

% muss initialisiert sein, warum??
if ~handles.blocking_strategies_check
    input.block_rows = 0;
    input.block_cols = 0;
    input.blocking_switch = 0;
    input.blocking_scheme = 0;
    input.between_blocks_scheme = 0;
    input.within_blocks_scheme = 0;
    input.block_MAXIT = 0;
    input.block_verbose = 0;
    input.block_anim = 0;
end

input.data_filename = get(handles.data_filename,'String');
input.data_filename = char(input.data_filename);
list = get(handles.object,'String');
input.object = list(get(handles.object,'Value'));
input.object = char(input.object);
list = get(handles.constraint,'String');
input.constraint = list(get(handles.constraint,'Value'));
input.constraint = char(input.constraint);
list = get(handles.experiment,'String');
input.experiment = list(get(handles.experiment,'Value'));
input.experiment = char(input.experiment);
list = get(handles.switch_object_support_constraint,'String');
input.switch_object_support_constraint = list(get(handles.switch_object_support_constraint,'Value'));
input.switch_object_support_constraint = char(input.switch_object_support_constraint);

list = get(handles.noise,'String');
input.noise = list(get(handles.noise,'Value'));
input.noise = char(input.noise);
if strcmp(input.noise,'poisson')
    input.poissonfactor = inputdlg('Poissonfactor','Poissonfactor',1,{'1'});
    input.poissonfactor = str2double(input.poissonfactor);
end
list = get(handles.switch_probemask,'String');
input.switch_probemask = list(get(handles.switch_probemask,'Value'));
if strcmp(input.switch_probemask,'true')
    input.switch_probemask = 1;
    input.probe_mask_gamma = inputdlg('Probemask Gamma','Probemask Gamma',1,{'0.5'});
    input.probe_mask_gamma = str2double(input.probe_mask_gamma);
else
    input.switch_probemask = 0;
end
input.rmsfraction = get(handles.rmsfraction,'String');
input.rmsfraction = str2double(input.rmsfraction);
list = get(handles.scan_type,'String');
input.scan_type = list(get(handles.scan_type,'Value'));
input.scan_type = char(input.scan_type);
list = get(handles.probe_guess_type,'String');
input.probe_guess_type = list(get(handles.probe_guess_type,'Value'));
input.probe_guess_type = char(input.probe_guess_type);
list = get(handles.object_guess_type,'String');
input.object_guess_type = list(get(handles.object_guess_type,'Value'));
input.object_guess_type = char(input.object_guess_type);

list = get(handles.method,'String');
input.method = list(get(handles.method,'Value'));
input.method = char(input.method);
if strcmp(input.method,'RAAR') || strcmp(input.method,'HPR') || strcmp(input.method,'HAAR')
    setappdata(0,'input',input); 
    uiwait(relaxation_parameters_in_RAAR_HPR_HAAR)
    input = getappdata(0,'input');
end
input.numruns = get(handles.numruns,'String');
input.numruns = str2double(input.numruns);
input.MAXIT = get(handles.MAXIT,'String');
input.MAXIT = str2double(input.MAXIT);
input.TOL = get(handles.TOL,'String');
input.TOL = str2double(input.TOL);
input.TOL2 = get(handles.TOL2,'String');
input.TOL2 = str2double(input.TOL2);
list = get(handles.ptychography_prox,'String');
input.ptychography_prox = list(get(handles.ptychography_prox,'Value'));
input.ptychography_prox = char(input.ptychography_prox);
if strcmp(input.ptychography_prox,'Rodenburg')
    input.RodenburgInnerIt = inputdlg('Rodenburg_Inner','Rodenburg_Inner',1,{'1'});
    input.RodenburgInnerIt = str2double(input.RodenburgInnerIt);
end
list = get(handles.verbose,'String');
input.verbose = list(get(handles.verbose,'Value'));
if strcmp(input.verbose,'on')
    input.verbose = 1;
else
    input.verbose = 0;
end
list = get(handles.graphics,'String');
input.graphics = list(get(handles.graphics,'Value'));
if strcmp(input.graphics,'on')
    input.graphics = 1;
    if ~isfield(input,'beta_0')
        input.beta_0 = 1;
        input.beta_max = 1;
    end
else
    input.graphics = 0;
end
list = get(handles.anim,'String');
input.anim = list(get(handles.anim,'Value'));
if strcmp(input.anim,'on')
    input.anim = 1;
elseif strcmp(input.anim,'off')
    input.anim = 0;
else
    input.anim = 2;
end
input.graphics_display = get(handles.graphics_display,'String');
input.graphics_display = char(input.graphics_display);
input.anim_pause = get(handles.anim_pause,'String');
input.anim_pause = str2double(input.anim_pause);
list = get(handles.plot,'String');
input.plot = list(get(handles.plot,'Value'));
input.plot = char(input.plot);
list = get(handles.animate,'String');
input.animate = list(get(handles.animate,'Value'));
input.animate = char(input.animate);

list = get(handles.error_type,'String');
input.error_type = list(get(handles.error_type,'Value'));
input.error_type = char(input.error_type);

setappdata(0,'input',input); 

close(gcf)

cd ..
cd drivers
main_ProxToolbox('input_in');


% --- Executes on button press in warm_up_gui.
function warm_up_gui_Callback(hObject, eventdata, handles)
% hObject    handle to warm_up_gui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.warmup_check = 1;
guidata(hObject, handles);

warm_up_gui;


% --- Executes on button press in use_generated_data_gui.
function use_generated_data_gui_Callback(hObject, eventdata, handles)
% hObject    handle to use_generated_data_gui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% data_file = uigetfile;

current_path = pwd;
cd ..

cd drivers\Ptychography\Ptychography_data\InputData;
[FileName,PathName] = uigetfile('.mat');
if ~(FileName == 0)
    input = getappdata(0,'input');
    if handles.input_data_check
        handles.input_data_check = 0;
        guidata(hObject, handles);
        input = rmfield(input,'sim_data_type');
        input = rmfield(input,'Nx');
        input = rmfield(input,'Ny');
        input = rmfield(input,'scan_stepsize');
        input = rmfield(input,'nx');
        input = rmfield(input,'ny');
        input = rmfield(input,'sample_area_centre');
        input = rmfield(input,'sample_fourier_area');
    end
    splitted_FileName = strsplit(FileName,'.m');
    input.datafile = splitted_FileName(1);
    input.datafile = char(input.datafile);
    splitted_PathName = strsplit(PathName,'\');
    input.data_dir = strcat(splitted_PathName(end-2),'/',splitted_PathName(end-1),'/');
    input.data_dir = char(input.data_dir);
    input.home_dir = '../';
    setappdata(0,'input',input); 
    handles.use_generates_data_check = 1;
    guidata(hObject, handles);
end
cd(current_path);

% --- Executes on button press in generate_simulated_data_gui.
function generate_simulated_data_gui_Callback(hObject, eventdata, handles)
% hObject    handle to generate_simulated_data_gui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.use_generates_data_check
    handles.use_generates_data_check = 0;
    guidata(hObject, handles);
    input = getappdata(0,'input');
    input = rmfield(input,'datafile');
    input = rmfield(input,'data_dir');
    input = rmfield(input,'home_dir');
    setappdata(0,'input',input); 
end

handles.input_data_check = 1;
guidata(hObject, handles);

generate_input_data_gui;


% --- Executes on button press in blocking_strategies_gui.
function blocking_strategies_gui_Callback(hObject, eventdata, handles)
% hObject    handle to blocking_strategies_gui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.blocking_strategies_check = 1;
guidata(hObject, handles);

blocking_strategies_gui;


% --- Executes on selection change in error_type.
function error_type_Callback(hObject, eventdata, handles)
% hObject    handle to error_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns error_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from error_type


% --- Executes during object creation, after setting all properties.
function error_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to error_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in plot.
function plot_Callback(hObject, eventdata, handles)
% hObject    handle to plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns plot contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot


% --- Executes during object creation, after setting all properties.
function plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in animate.
function animate_Callback(hObject, eventdata, handles)
% hObject    handle to animate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns animate contents as cell array
%        contents{get(hObject,'Value')} returns selected item from animate


% --- Executes during object creation, after setting all properties.
function animate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to animate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in verbose.
function verbose_Callback(hObject, eventdata, handles)
% hObject    handle to verbose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns verbose contents as cell array
%        contents{get(hObject,'Value')} returns selected item from verbose


% --- Executes during object creation, after setting all properties.
function verbose_CreateFcn(hObject, eventdata, handles)
% hObject    handle to verbose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in graphics.
function graphics_Callback(hObject, eventdata, handles)
% hObject    handle to graphics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns graphics contents as cell array
%        contents{get(hObject,'Value')} returns selected item from graphics


% --- Executes during object creation, after setting all properties.
function graphics_CreateFcn(hObject, eventdata, handles)
% hObject    handle to graphics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in anim.
function anim_Callback(hObject, eventdata, handles)
% hObject    handle to anim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns anim contents as cell array
%        contents{get(hObject,'Value')} returns selected item from anim


% --- Executes during object creation, after setting all properties.
function anim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to anim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function graphics_display_Callback(hObject, eventdata, handles)
% hObject    handle to graphics_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of graphics_display as text
%        str2double(get(hObject,'String')) returns contents of graphics_display as a double


% --- Executes during object creation, after setting all properties.
function graphics_display_CreateFcn(hObject, eventdata, handles)
% hObject    handle to graphics_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function anim_pause_Callback(hObject, eventdata, handles)
% hObject    handle to anim_pause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of anim_pause as text
%        str2double(get(hObject,'String')) returns contents of anim_pause as a double


% --- Executes during object creation, after setting all properties.
function anim_pause_CreateFcn(hObject, eventdata, handles)
% hObject    handle to anim_pause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in method.
function method_Callback(hObject, eventdata, handles)
% hObject    handle to method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from method


% --- Executes during object creation, after setting all properties.
function method_CreateFcn(hObject, eventdata, handles)
% hObject    handle to method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function numruns_Callback(hObject, eventdata, handles)
% hObject    handle to numruns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numruns as text
%        str2double(get(hObject,'String')) returns contents of numruns as a double


% --- Executes during object creation, after setting all properties.
function numruns_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numruns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MAXIT_Callback(hObject, eventdata, handles)
% hObject    handle to MAXIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MAXIT as text
%        str2double(get(hObject,'String')) returns contents of MAXIT as a double


% --- Executes during object creation, after setting all properties.
function MAXIT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MAXIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ptychography_prox.
function ptychography_prox_Callback(hObject, eventdata, handles)
% hObject    handle to ptychography_prox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ptychography_prox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ptychography_prox


% --- Executes during object creation, after setting all properties.
function ptychography_prox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ptychography_prox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function rmsfraction_Callback(hObject, eventdata, handles)
% hObject    handle to rmsfraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rmsfraction as text
%        str2double(get(hObject,'String')) returns contents of rmsfraction as a double


% --- Executes during object creation, after setting all properties.
function rmsfraction_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rmsfraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in scan_type.
function scan_type_Callback(hObject, eventdata, handles)
% hObject    handle to scan_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns scan_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from scan_type


% --- Executes during object creation, after setting all properties.
function scan_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scan_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in switch_probemask.
function switch_probemask_Callback(hObject, eventdata, handles)
% hObject    handle to switch_probemask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns switch_probemask contents as cell array
%        contents{get(hObject,'Value')} returns selected item from switch_probemask


% --- Executes during object creation, after setting all properties.
function switch_probemask_CreateFcn(hObject, eventdata, handles)
% hObject    handle to switch_probemask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in noise.
function noise_Callback(hObject, eventdata, handles)
% hObject    handle to noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns noise contents as cell array
%        contents{get(hObject,'Value')} returns selected item from noise


% --- Executes during object creation, after setting all properties.
function noise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in probe_guess_type.
function probe_guess_type_Callback(hObject, eventdata, handles)
% hObject    handle to probe_guess_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns probe_guess_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from probe_guess_type


% --- Executes during object creation, after setting all properties.
function probe_guess_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to probe_guess_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in object_guess_type.
function object_guess_type_Callback(hObject, eventdata, handles)
% hObject    handle to object_guess_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns object_guess_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from object_guess_type


% --- Executes during object creation, after setting all properties.
function object_guess_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to object_guess_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in experiment.
function experiment_Callback(hObject, eventdata, handles)
% hObject    handle to experiment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns experiment contents as cell array
%        contents{get(hObject,'Value')} returns selected item from experiment


% --- Executes during object creation, after setting all properties.
function experiment_CreateFcn(hObject, eventdata, handles)
% hObject    handle to experiment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in constraint.
function constraint_Callback(hObject, eventdata, handles)
% hObject    handle to constraint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns constraint contents as cell array
%        contents{get(hObject,'Value')} returns selected item from constraint


% --- Executes during object creation, after setting all properties.
function constraint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to constraint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in object.
function object_Callback(hObject, eventdata, handles)
% hObject    handle to object (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns object contents as cell array
%        contents{get(hObject,'Value')} returns selected item from object


% --- Executes during object creation, after setting all properties.
function object_CreateFcn(hObject, eventdata, handles)
% hObject    handle to object (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function data_filename_Callback(hObject, eventdata, handles)
% hObject    handle to data_filename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of data_filename as text
%        str2double(get(hObject,'String')) returns contents of data_filename as a double


% --- Executes during object creation, after setting all properties.
function data_filename_CreateFcn(hObject, eventdata, handles)
% hObject    handle to data_filename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in switch_object_support_constraint.
function switch_object_support_constraint_Callback(hObject, eventdata, handles)
% hObject    handle to switch_object_support_constraint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns switch_object_support_constraint contents as cell array
%        contents{get(hObject,'Value')} returns selected item from switch_object_support_constraint


% --- Executes during object creation, after setting all properties.
function switch_object_support_constraint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to switch_object_support_constraint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TOL_Callback(hObject, eventdata, handles)
% hObject    handle to TOL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TOL as text
%        str2double(get(hObject,'String')) returns contents of TOL as a double


% --- Executes during object creation, after setting all properties.
function TOL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TOL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TOL2_Callback(hObject, eventdata, handles)
% hObject    handle to TOL2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TOL2 as text
%        str2double(get(hObject,'String')) returns contents of TOL2 as a double


% --- Executes during object creation, after setting all properties.
function TOL2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TOL2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
