function varargout = generate_input_data_gui(varargin)
% GENERATE_INPUT_DATA_GUI MATLAB code for generate_input_data_gui.fig
%      GENERATE_INPUT_DATA_GUI, by itself, creates a new GENERATE_INPUT_DATA_GUI or raises the existing
%      singleton*.
%
%      H = GENERATE_INPUT_DATA_GUI returns the handle to a new GENERATE_INPUT_DATA_GUI or the handle to
%      the existing singleton*.
%
%      GENERATE_INPUT_DATA_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GENERATE_INPUT_DATA_GUI.M with the given input arguments.
%
%      GENERATE_INPUT_DATA_GUI('Property','Value',...) creates a new GENERATE_INPUT_DATA_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before generate_input_data_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to generate_input_data_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help generate_input_data_gui

% Last Modified by GUIDE v2.5 07-Jun-2015 11:54:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @generate_input_data_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @generate_input_data_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before generate_input_data_gui is made visible.
function generate_input_data_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to generate_input_data_gui (see VARARGIN)

% Choose default command line output for generate_input_data_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes generate_input_data_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = generate_input_data_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function sim_data_type_Callback(hObject, eventdata, handles)
% hObject    handle to sim_data_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sim_data_type as text
%        str2double(get(hObject,'String')) returns contents of sim_data_type as a double


% --- Executes during object creation, after setting all properties.
function sim_data_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sim_data_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Nx_Callback(hObject, eventdata, handles)
% hObject    handle to Nx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Nx as text
%        str2double(get(hObject,'String')) returns contents of Nx as a double


% --- Executes during object creation, after setting all properties.
function Nx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Nx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Ny_Callback(hObject, eventdata, handles)
% hObject    handle to Ny (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Ny as text
%        str2double(get(hObject,'String')) returns contents of Ny as a double


% --- Executes during object creation, after setting all properties.
function Ny_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ny (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function scan_stepsize_Callback(hObject, eventdata, handles)
% hObject    handle to scan_stepsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of scan_stepsize as text
%        str2double(get(hObject,'String')) returns contents of scan_stepsize as a double


% --- Executes during object creation, after setting all properties.
function scan_stepsize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scan_stepsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nix_Callback(hObject, eventdata, handles)
% hObject    handle to nix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nix as text
%        str2double(get(hObject,'String')) returns contents of nix as a double


% --- Executes during object creation, after setting all properties.
function nix_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function niy_Callback(hObject, eventdata, handles)
% hObject    handle to niy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of niy as text
%        str2double(get(hObject,'String')) returns contents of niy as a double


% --- Executes during object creation, after setting all properties.
function niy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to niy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sample_area_centre_Callback(hObject, eventdata, handles)
% hObject    handle to sample_area_centre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sample_area_centre as text
%        str2double(get(hObject,'String')) returns contents of sample_area_centre as a double


% --- Executes during object creation, after setting all properties.
function sample_area_centre_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sample_area_centre (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sample_area_centre_1_Callback(hObject, eventdata, handles)
% hObject    handle to sample_area_centre_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sample_area_centre_1 as text
%        str2double(get(hObject,'String')) returns contents of sample_area_centre_1 as a double


% --- Executes during object creation, after setting all properties.
function sample_area_centre_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sample_area_centre_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sample_area_centre_2_Callback(hObject, eventdata, handles)
% hObject    handle to sample_area_centre_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sample_area_centre_2 as text
%        str2double(get(hObject,'String')) returns contents of sample_area_centre_2 as a double


% --- Executes during object creation, after setting all properties.
function sample_area_centre_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sample_area_centre_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sample_fourier_area_Callback(hObject, eventdata, handles)
% hObject    handle to sample_fourier_area (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sample_fourier_area as text
%        str2double(get(hObject,'String')) returns contents of sample_fourier_area as a double


% --- Executes during object creation, after setting all properties.
function sample_fourier_area_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sample_fourier_area (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

input = getappdata(0,'input');

input.sim_data_type = get(handles.sim_data_type,'String');
input.sim_data_type = char(input.sim_data_type);
input.Nx = get(handles.Nx,'String');
input.Nx = str2double(input.Nx);
input.Ny = get(handles.Ny,'String');
input.Ny = str2double(input.Ny);
input.scan_stepsize = get(handles.scan_stepsize,'String');
input.scan_stepsize = str2double(input.scan_stepsize);
input.nx = get(handles.nix,'String');
input.nx = str2double(input.nx);
input.ny = get(handles.niy,'String');
input.ny = str2double(input.ny);
sample_area_centre_1 = get(handles.sample_area_centre_1,'String');
sample_area_centre_2 = get(handles.sample_area_centre_2,'String');
sample_area_centre_1 = str2double(sample_area_centre_1);
sample_area_centre_2 = str2double(sample_area_centre_2);
input.sample_area_centre = [sample_area_centre_1 sample_area_centre_2];
input.sample_fourier_area = get(handles.sample_fourier_area,'String');
input.sample_fourier_area = str2double(input.sample_fourier_area);

setappdata(0,'input',input); 

close(gcf);
