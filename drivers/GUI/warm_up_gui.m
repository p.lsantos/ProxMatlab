function varargout = warm_up_gui(varargin)
% WARM_UP_GUI MATLAB code for warm_up_gui.fig
%      WARM_UP_GUI, by itself, creates a new WARM_UP_GUI or raises the existing
%      singleton*.
%
%      H = WARM_UP_GUI returns the handle to a new WARM_UP_GUI or the handle to
%      the existing singleton*.
%
%      WARM_UP_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WARM_UP_GUI.M with the given input arguments.
%
%      WARM_UP_GUI('Property','Value',...) creates a new WARM_UP_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before warm_up_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to warm_up_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help warm_up_gui

% Last Modified by GUIDE v2.5 15-May-2015 15:59:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @warm_up_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @warm_up_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before warm_up_gui is made visible.
function warm_up_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to warm_up_gui (see VARARGIN)

% Choose default command line output for warm_up_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes warm_up_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = warm_up_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

input = getappdata(0,'input');

list = get(handles.warmup,'String');
input.warmup = list(get(handles.warmup,'Value'));
if strcmp(input.warmup,'true')
    input.warmup = 1;
else
    input.warmup = 0;
end
list = get(handles.warmup_method,'String');
input.warmup_method = list(get(handles.warmup_method,'Value'));
input.warmup_method = char(input.warmup_method);
input.warmup_MAXIT = get(handles.warmup_MAXIT,'String');
input.warmup_MAXIT = str2double(input.warmup_MAXIT);

setappdata(0,'input',input); 

close(gcf);

% --- Executes on selection change in warmup.
function warmup_Callback(hObject, eventdata, handles)
% hObject    handle to warmup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns warmup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from warmup


% --- Executes during object creation, after setting all properties.
function warmup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to warmup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in warmup_method.
function warmup_method_Callback(hObject, eventdata, handles)
% hObject    handle to warmup_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns warmup_method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from warmup_method


% --- Executes during object creation, after setting all properties.
function warmup_method_CreateFcn(hObject, eventdata, handles)
% hObject    handle to warmup_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function warmup_MAXIT_Callback(hObject, eventdata, handles)
% hObject    handle to warmup_MAXIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of warmup_MAXIT as text
%        str2double(get(hObject,'String')) returns contents of warmup_MAXIT as a double


% --- Executes during object creation, after setting all properties.
function warmup_MAXIT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to warmup_MAXIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
