function varargout = main_sudoku(varargin)
% MAIN_SUDOKU MATLAB code for main_sudoku.fig
%      MAIN_SUDOKU, by itself, creates a new MAIN_SUDOKU or raises the existing
%      singleton*.
%
%      H = MAIN_SUDOKU returns the handle to a new MAIN_SUDOKU or the handle to
%      the existing singleton*.
%
%      MAIN_SUDOKU('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN_SUDOKU.M with the given input arguments.
%
%      MAIN_SUDOKU('Property','Value',...) creates a new MAIN_SUDOKU or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_sudoku_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_sudoku_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main_sudoku

% Last Modified by GUIDE v2.5 07-Jun-2015 11:44:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_sudoku_OpeningFcn, ...
                   'gui_OutputFcn',  @main_sudoku_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main_sudoku is made visible.
function main_sudoku_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main_sudoku (see VARARGIN)

% Choose default command line output for main_sudoku
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes main_sudoku wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = main_sudoku_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in experiment.
function experiment_Callback(hObject, eventdata, handles)
% hObject    handle to experiment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns experiment contents as cell array
%        contents{get(hObject,'Value')} returns selected item from experiment


% --- Executes during object creation, after setting all properties.
function experiment_CreateFcn(hObject, eventdata, handles)
% hObject    handle to experiment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in constraint.
function constraint_Callback(hObject, eventdata, handles)
% hObject    handle to constraint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns constraint contents as cell array
%        contents{get(hObject,'Value')} returns selected item from constraint


% --- Executes during object creation, after setting all properties.
function constraint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to constraint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in object.
function object_Callback(hObject, eventdata, handles)
% hObject    handle to object (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns object contents as cell array
%        contents{get(hObject,'Value')} returns selected item from object


% --- Executes during object creation, after setting all properties.
function object_CreateFcn(hObject, eventdata, handles)
% hObject    handle to object (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function data_filename_Callback(hObject, eventdata, handles)
% hObject    handle to data_filename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of data_filename as text
%        str2double(get(hObject,'String')) returns contents of data_filename as a double


% --- Executes during object creation, after setting all properties.
function data_filename_CreateFcn(hObject, eventdata, handles)
% hObject    handle to data_filename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in switch_object_support_constraint.
function switch_object_support_constraint_Callback(hObject, eventdata, handles)
% hObject    handle to switch_object_support_constraint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns switch_object_support_constraint contents as cell array
%        contents{get(hObject,'Value')} returns selected item from switch_object_support_constraint


% --- Executes during object creation, after setting all properties.
function switch_object_support_constraint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to switch_object_support_constraint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in plot.
function plot_Callback(hObject, eventdata, handles)
% hObject    handle to plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns plot contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot


% --- Executes during object creation, after setting all properties.
function plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in animate.
function animate_Callback(hObject, eventdata, handles)
% hObject    handle to animate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns animate contents as cell array
%        contents{get(hObject,'Value')} returns selected item from animate


% --- Executes during object creation, after setting all properties.
function animate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to animate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in verbose.
function verbose_Callback(hObject, eventdata, handles)
% hObject    handle to verbose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns verbose contents as cell array
%        contents{get(hObject,'Value')} returns selected item from verbose


% --- Executes during object creation, after setting all properties.
function verbose_CreateFcn(hObject, eventdata, handles)
% hObject    handle to verbose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in graphics.
function graphics_Callback(hObject, eventdata, handles)
% hObject    handle to graphics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns graphics contents as cell array
%        contents{get(hObject,'Value')} returns selected item from graphics


% --- Executes during object creation, after setting all properties.
function graphics_CreateFcn(hObject, eventdata, handles)
% hObject    handle to graphics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in anim.
function anim_Callback(hObject, eventdata, handles)
% hObject    handle to anim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns anim contents as cell array
%        contents{get(hObject,'Value')} returns selected item from anim


% --- Executes during object creation, after setting all properties.
function anim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to anim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function graphics_display_Callback(hObject, eventdata, handles)
% hObject    handle to graphics_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of graphics_display as text
%        str2double(get(hObject,'String')) returns contents of graphics_display as a double


% --- Executes during object creation, after setting all properties.
function graphics_display_CreateFcn(hObject, eventdata, handles)
% hObject    handle to graphics_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function anim_pause_Callback(hObject, eventdata, handles)
% hObject    handle to anim_pause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of anim_pause as text
%        str2double(get(hObject,'String')) returns contents of anim_pause as a double


% --- Executes during object creation, after setting all properties.
function anim_pause_CreateFcn(hObject, eventdata, handles)
% hObject    handle to anim_pause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in method.
function method_Callback(hObject, eventdata, handles)
% hObject    handle to method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from method


% --- Executes during object creation, after setting all properties.
function method_CreateFcn(hObject, eventdata, handles)
% hObject    handle to method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function numruns_Callback(hObject, eventdata, handles)
% hObject    handle to numruns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numruns as text
%        str2double(get(hObject,'String')) returns contents of numruns as a double


% --- Executes during object creation, after setting all properties.
function numruns_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numruns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MAXIT_Callback(hObject, eventdata, handles)
% hObject    handle to MAXIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MAXIT as text
%        str2double(get(hObject,'String')) returns contents of MAXIT as a double


% --- Executes during object creation, after setting all properties.
function MAXIT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MAXIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TOL_Callback(hObject, eventdata, handles)
% hObject    handle to TOL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TOL as text
%        str2double(get(hObject,'String')) returns contents of TOL as a double


% --- Executes during object creation, after setting all properties.
function TOL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TOL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in projection.
function projection_Callback(hObject, eventdata, handles)
% hObject    handle to projection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on button press in start_button.
function start_button_Callback(hObject, eventdata, handles)
% hObject    handle to start_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

input = getappdata(0,'input');
input.problem_family = 'Custom';

input.data_filename = get(handles.data_filename,'String');
input.data_filename = char(input.data_filename);
list = get(handles.object,'String');
input.object = list(get(handles.object,'Value'));
input.object = char(input.object);
list = get(handles.constraint,'String');
input.constraint = list(get(handles.constraint,'Value'));
input.constraint = char(input.constraint);
list = get(handles.experiment,'String');
input.experiment = list(get(handles.experiment,'Value'));
input.experiment = char(input.experiment);
input.data_ball = get(handles.data_ball,'String');
input.data_ball = char(input.data_ball);

list = get(handles.method,'String');
input.method = list(get(handles.method,'Value'));
input.method = char(input.method);
input.numruns = get(handles.numruns,'String');
input.numruns = str2double(input.numruns);
input.MAXIT = get(handles.MAXIT,'String');
input.MAXIT = str2double(input.MAXIT);
input.TOL = get(handles.TOL,'String');
input.TOL = str2double(input.TOL);

if strcmp(input.method,'RAAR') || strcmp(input.method,'HPR') || strcmp(input.method,'HAAR')
    setappdata(0,'input',input); 
    uiwait(relaxation_parameters_in_RAAR_HPR_HAAR)
    input = getappdata(0,'input');
end

input.Proj1 = get(handles.Proj1,'String');
input.Proj1 = char(input.Proj1);
input.Proj2 = get(handles.Proj2,'String');
input.Proj2 = char(input.Proj2);

list = get(handles.verbose,'String');
input.verbose = list(get(handles.verbose,'Value'));
if strcmp(input.verbose,'on')
    input.verbose = 1;
else
    input.verbose = 0;
end
list = get(handles.graphics,'String');
input.graphics = list(get(handles.graphics,'Value'));
if strcmp(input.graphics,'on')
    input.graphics = 1;
else
    input.graphics = 0;
end
list = get(handles.anim,'String');
input.anim = list(get(handles.anim,'Value'));
% input.animation????
input.animation = 'none';
if strcmp(input.anim,'on')
    input.anim = 1;
elseif strcmp(input.anim,'off')
    input.anim = 0;
else
    input.anim = 2;
end
input.graphics_display = get(handles.graphics_display,'String');
input.graphics_display = char(input.graphics_display);
input.anim_pause = get(handles.anim_pause,'String');
input.anim_pause = str2double(input.anim_pause);

close(gcf)

setappdata(0,'input',input); 

cd ..
cd drivers
main_ProxToolbox('input_in');



function Proj1_Callback(hObject, eventdata, handles)
% hObject    handle to Proj1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Proj1 as text
%        str2double(get(hObject,'String')) returns contents of Proj1 as a double


% --- Executes during object creation, after setting all properties.
function Proj1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Proj1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Proj2_Callback(hObject, eventdata, handles)
% hObject    handle to Proj2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Proj2 as text
%        str2double(get(hObject,'String')) returns contents of Proj2 as a double


% --- Executes during object creation, after setting all properties.
function Proj2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Proj2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function data_ball_Callback(hObject, eventdata, handles)
% hObject    handle to data_ball (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of data_ball as text
%        str2double(get(hObject,'String')) returns contents of data_ball as a double


% --- Executes during object creation, after setting all properties.
function data_ball_CreateFcn(hObject, eventdata, handles)
% hObject    handle to data_ball (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
