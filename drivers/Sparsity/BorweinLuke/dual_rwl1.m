%  Dual subgradient descent method  
%  for minimizing the l1 norm
%  subject to a linear constraint.  This 
%  strategy was first proposed in 
%  ``Duality and Convex Porgramming" by
%  J. M. Borwein and D. R. Luke, 
%  to appear in THE HANDBOOK OF IMAGING
%  edited by Otmar Scherzer.    This algorithm
%  together with analysis appeared in 
%  
%  ``Entropic Regularization of the l_0 Function '', 
%  J. M. Borwein and D. R. Luke, in 
%  Fixed-Point Algorithms for Inverse Problems in Science 
%  and Engineering, 
%  edited by H. Bauschke, R. Burachik, P. Combettes, V. Elser, R. Luke, and H. Wolkowicz 
%  Springer Verlag (2011). 
% 
%  For preprints go to 
%
% http://num.math.uni-goettingen.de/~r.luke/publications
% 
% 
%  Russell Luke
%  Universitaet Goettingen and 
%  University of Delaware
%  Oct. 28, 2009
%
%


% n = 128^2;                            % Size of signal
input.Nx=256^2;
input.Ny=1;
input.Sparsity=.005; % input.Sparsity is a 
% percentage of nonzero entries.  0% is fully sparse, 1.0 is dense.
input.sample_rate=1/8; %

[b,M, u_true] = SparseStoch1D(input);
% [b,M, u_true] = Stoch1D(input.Nx);
% name = 'siemens_star';
% [b,M] = xray(name);
% add symmetric noise
b=b.*M;
% add noise to the data:  this is experimental... no theory yet
% eta = fft(rand(size(b))-.5).*M;
% eta=eta/norm(eta,'fro')*norm(b,'fro').*M;
b_true=b;
% b=b+eta*.3;
JJM=(M~=0);
bhmm=b(JJM);
[n,m]=size(b);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithm 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
b_star = feval('ifft',b);
if(norm(imag(b_star))<=1e-14)
    b_star=real(b_star);
    real_flag=1;
end

% gamma=.002;
gamma=sum(M);
if(real_flag)
    L=1/gamma*ones(n,m);  % dual interval length
else
    L=1/gamma*ones(n,m)+1i*1/gamma*ones(n,m);
end
epsilon=0; % 500000000;
% initial guess
[lambda,J]=max(abs(b_star));
% L(J)=0;
y0=zeros(n,m);

MAXIT=400;
TOL=1e-9;
iter=1;
nr=ones(MAXIT,1);
v  = zeros(n,m);
step=zeros(MAXIT,1);
step(iter)=1;
av_inner_iter=0;
t=cputime;
JJp=[]; JJm=[]; J=[];
A=[];
while (step(iter)>TOL && iter< MAXIT)
    iter=iter+1
    x = feval('ifft',y0);
    % now, choose v in subdifferential of f^* at x via 
    % alternating projections
    % initial guess:
    % Project x onto feasible region [-L,L] (necessary
    % in the instance where the feasible region shrinks) and
    % find points close to the boundary
    % of the feasible region:
    % v0  = zeros(n,1);
    % v0(JJp) = 1./L(JJp)-gamma;
    % v0(JJm) = -(1./L(JJm)-gamma);
    v0=v;
    if(real_flag)
        x=real(x);
        if (~isempty(J))
            if(x(J)-L(J)>-TOL)
                JJp=[JJp;J];
            elseif(x(J)+L(J)<TOL)
                JJm=[JJm;J];
            end
            JJ=[JJp;JJm];
            JJ=sort(JJ);
            Jidx=find(JJ==J);
            %%%% rescale the problem:  
			if(real_flag)
				% L(J) = L(J);
				%%%% optimal rescaling sketch...not tested
				% y0= (rescaling./L).*y0;
				% x = feval('ifft',y0);
				% L = L./L*rescaling;
				%%%% greedy rescaling: 
				L(J)=0;
				y0=0*y0;
				x=zeros(n,m);
			else
				% not tested:
				y0 = (1./(abs(real(v))+gamma)./real(L))*(real(y0))+i*1./(abs(imag(v))+gamma)./imag(L)*imag(y0);
				x = feval('ifft',y0);
				L = 1./(abs(real(v))+gamma)+i*1./(abs(imag(v))+gamma);
				L(J)=0;
				y0=0*y0;
			end
    
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%% compute the projection onto the normal cone
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % below is a ``direct" solution to the
            % problem.  Surprizingly, this does worse than
            % the iterative method for n=256^2 and above.
            % I would have expected the exchange in efficiency to
            % occurr later than 256^2.  Part of the problem could
            % be the loop for building the matrix A.
            % P_N_C direct----->
            %             ej=zeros(n,1); ej(J)=1;
            %             aj=feval('fft',ej);
            %             A=[A(:,1:Jidx-1),aj(JJM),A(:,Jidx:end)];
            %             At=A';
            %             btmp=At*bhmm;
            %             AtA=At*A;
            %             vtmp = AtA\btmp;
            %             v=zeros(n,1); v(JJ)=real(vtmp);
            % <----P_N_C direct
            % commented out below is an iterative solution to
            % the problem.  To run this instead, comment out the 
            % code above between P_N_C direct-----> and <----P_N_C direct
            % above.
            % P_N_C iterative----->
            [v,inner_iter] = feval('choose_sdfstar',b,M,v0,JJp,JJm,10*TOL,MAXIT);
            % inner_iter is only for the iterative projection methods
            % inner_iter;
            av_inner_iter = av_inner_iter+inner_iter;
            % <----P_N_C iterative
        end
    else
        JJpr = find((real(x)-real(L))>-TOL);
        JJmr = find(real(x)+real(L)<TOL);
        JJpi = find((imag(x)-imag(L))>-TOL);
        JJmi = find(imag(x)+imag(L)<TOL);
        [v,inner_iter] = feval('choose_sdfstarc',b,M,v0,JJpr,JJpi,JJmr,JJmi,TOL,MAXIT);
        % inner_iter is only for the iterative projection methods
        % inner_iter;
        av_inner_iter = av_inner_iter+inner_iter;
    end
    % now select the steplength parameter to be the largest 
    % feasible step
    Av=M.*feval('fft',v);
    r=b-Av;
    nr(iter)=norm(r);
    z=feval('ifft',r);
    if(real_flag)
        z=real(z);
        tmp = 1e+15*ones(n,m);
        JJ=find(z>TOL);
        tmp(JJ)=(L(JJ)-x(JJ))./z(JJ);
        JJ=find(z<-TOL);
        tmp(JJ)=-(L(JJ)+x(JJ))./z(JJ);
        tmp(JJm)=1e+15;
        tmp(JJp)=1e+15;
        [lambda,J] = min(tmp);
    else
        tmpr = 1e+15*ones(n,m);
        tmpi = 1e+15*ones(n,m);
        JJ=find(real(z)>TOL);
        tmpr(JJ)=(real(L(JJ))-real(x(JJ)))./real(z(JJ));
        JJ=find(real(z)<-TOL);
        tmpr(JJ)=-(real(L(JJ))+real(x(JJ)))./real(z(JJ));
        tmpr(JJmr)=1e+15;
        tmpr(JJpr)=1e+15;
        JJ=find(imag(z)>TOL);
        tmpi(JJ)=(imag(L(JJ))-imag(x(JJ)))./imag(z(JJ));
        JJ=find(imag(z)<-TOL);
        tmpi(JJ)=-(imag(L(JJ))+imag(x(JJ)))./imag(z(JJ));
        tmpi(JJmi)=1e+15;
        tmpi(JJpi)=1e+15;
        lambda = min(min(min(tmpr)), min(min(tmpi)));
    end

    if (lambda==1e+15)
        step(iter)=nr(iter);
        ynew=y0+r;
        y0=ynew;
        break
    end
    step(iter)=lambda*nr(iter);
    ynew=y0+lambda*r;
    y0=ynew;
end
compute_time=cputime-t
iter;
av_inner_iter=av_inner_iter/iter
x=feval('ifft',y0);
% figure(999)
% plot(L), axis('tight')
% title('Optimal Greedy Weights','FontSize', 16)
% xlabel('Signal length','FontSize', 16)
% % ylabel(['L = ',num2str(L)],'FontSize', 16)
% set(gca,'FontSize',16);

% Now calculate u from the dual solution y0
% via alternating projections.
% Take as initial guess to be b_star projected
% onto the set of funtions with sign and support 
% indicated by y0.
JJp = find((x-L)>-TOL);
JJm = find(x+L<TOL);
u0 =  zeros(n,1);
% u0(JJp) = 1./L(JJp)-gamma;
% u0(JJm) = -(1./L(JJm)-gamma);

[u,inner_iter] = feval('choose_sdfstar',b,M,v0,JJp,JJm,10*TOL,MAXIT);
u=choose_sdfstar(b,M,v,JJp,JJm,10*TOL, MAXIT);

figure(1000), clf
subplot(2,2,2), 
  u_mat=reshape(u,sqrt(n),sqrt(n));
  imagesc(abs(u_mat)), colorbar
  title('recovered signal')
subplot(2,2,4), 
  u_true_mat=reshape(u_true,sqrt(n),sqrt(n));
  imagesc(abs(u_true_mat)), colorbar
  title('true signal')
subplot(2,2,1)
plot(u_true-real(v))
title('true - recovered','FontSize', 16)
ylabel('error = |true - recovered|','FontSize', 16)
xlabel('signal coefficients','FontSize', 16)
% ylabel(['L = ',num2str(L)],'FontSize', 16)
set(gca,'FontSize',16);
subplot(2,2,3)
semilogy(step, '*')
ylabel('||step||^2 ','FontSize', 16)
xlabel('iteration','FontSize', 16)
set(gca,'FontSize',16);
if (gamma<1)
    subplot(2,2,2)
    plot(1./L-gamma - abs(u_true))
    xlabel('(1./L-\gamma) - |true signal|','FontSize', 16)
    set(gca,'FontSize',16);

    figure(1001), plot(1./L-gamma - abs(u_true))
end
