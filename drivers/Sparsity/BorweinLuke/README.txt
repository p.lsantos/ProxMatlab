 Dual subgradient descent method  
 for minimizing the l1 norm
 subject to a linear constraint.  This 
 strategy was first proposed in 
 ``Duality and Convex Porgramming" by
 J. M. Borwein and D. R. Luke, 
 to appear in THE HANDBOOK OF IMAGING
 edited by Otmar Scherzer.    This algorithm
 together with analysis appeared in 
 
 ``Entropic Regularization of the l_0 Function '', 
 J. M. Borwein and D. R. Luke, in 
 Fixed-Point Algorithms for Inverse Problems in Science 
 and Engineering, 
 edited by H. Bauschke, R. Burachik, P. Combettes, V. Elser, R. Luke, and H. Wolkowicz 
 Springer Verlag (2011). 

 TO RUN THE ALGORITHM TYPE:
 
 dual_rwl1
 
 at the Matlab prompt.

 
 For preprints go to 

http://num.math.uni-goettingen.de/~r.luke/publications


 Russell Luke
 Universitaet Goettingen and 
 University of Delaware
 Oct. 28, 2009


