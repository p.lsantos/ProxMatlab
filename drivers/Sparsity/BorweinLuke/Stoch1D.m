% Input:
%   n = signal size

function [b,M,u_true]=Stoch1D(n)

Sparsity = -.495; % the closer this is to -.5 the more sparse the signal
Sample= -0.29; % -.13 resutls in a sampling rate of about n/8. -.03 gives a 
%       sampling rate of about n/4.
%       There's a bit of a cheat in Candes' example:  he samples n/8 Fourier
%       coefficients at random, but since the Fourier transform is of a real
%       signal, there is hidden symmetry.  So if he samples n/8 coefficients he
%       really gets about twice that number of samples due to symmetry in the
%       Fourier transform. 
%------------------------------------
% Set up toy problem:
% Candes' example, sort of
%------------------------------------
rand('twister',13); % sets the random seed to the default
support=round(rand(n,1)+Sparsity);
% support=floor(support);
randn('state',27);
u_true = support.*randn(n,1);
% seed=clock;
% seed=floor(seed(6)*1000);
rand('twister',533); % sets the random seed to the default
M=round(rand(n,1)+Sample);
% M(1,1)=1; % always sample the zero frequency?
% symmetrize samples to keep object real
M(2:end/2)=(floor(.5*(M(2:end/2)+M(end:-1:end/2+2)))); 
M(end:-1:end/2+2)=M(2:end/2);
M(end/2+1)=1;

% define linear operators
b=(M.*feval('fft',u_true));

