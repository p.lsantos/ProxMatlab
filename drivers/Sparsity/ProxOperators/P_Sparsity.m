%                      P_Sparsity.m
%             written on August 20, 2012 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               support constraints
%
% INPUT:        input = data structure with .supp_ampl a vector of indeces of the nonzero elements of the array.
%
%               u = array to be projected
%
% OUTPUT:       p_Sparsity    = the projection 
%               
% USAGE: p_Sparsity = P_Sparsity(input,u)
%
% Nonstandard Matlab function calls:  

function p_Sparsity = P_Sparsity(input,u)

p_Sparsity=0*u;
[tmp,I]=sort(abs(u),'descend');
p_Sparsity(I(1:input.s))=u(I(1:input.s));


