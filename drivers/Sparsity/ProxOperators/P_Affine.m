%                      P_Affine.m
%             written on August 15, 2014 by 
%                   Patrick Neumann
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               an affine subspace
%
% INPUT:        input = data structure containing the pseudoinverse of a
%               matrix determining the affine subspace
%
%               u = array to be projected
%
% OUTPUT:       p_Affine    = the projection 
%               
% USAGE:        p_Affine    = P_Affine(input,u)
%
% Nonstandard Matlab function calls:  

function p_Affine = P_Affine(input,u)
p  = input.pinvp;
M2 = input.pident;
p_Affine = u-M2*u+p;