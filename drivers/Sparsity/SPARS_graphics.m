%                      SPARS_graphics.m
%                  written on 29. Mai, 2012 by
%                        Russell Luke
%                  Universität Göttingen
%
% DESCRIPTION:  Script driver for viewing results from projection
%               algorithms on various toy problems
%
% INPUT:  
%              method_input/output = data structures
%
% OUTPUT:       graphics
% USAGE: SPARS_graphics(method_input,method_output)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function success = SPARS_graphics(method_input, method_output)
              
method=method_input.method;
beta0 = method_input.beta_0;
beta_max = method_input.beta_max;
u_true = method_input.truth;
u_0 = method_input.u_0;
u = method_output.u1(:,:,1);
% If there is a timer in the algorithm:
time = method_output.stats.time;

iter = method_output.stats.iter;
change = method_output.stats.change;
if(any(strcmp('time', fieldnames(method_output.stats))))
    time = method_output.stats.time;
else
    time=999
end


if(any(strcmp('diagnostic', fieldnames(method_input))))&& (strcmp(method,'RAAR'))
    shadow_change=min(method_output.stats.shadow_change);
else
end


figure(900);

subplot(3,2,1); plot(u); title('reconstructed signal'); drawnow; axis('tight')
%figure(901);
subplot(3,2,2); plot(u-u_true); title('u-u_{true}'); drawnow;

%figure(902);
subplot(3,2,3);semilogy(change),xlabel('iteration'),ylabel(['change in iterates'])
% subplot(3,2,3);semilogy(change),xlabel('iteration'),ylabel(['change in iterates']); ylim([1e-15 1e6]);
label = ['Algorithm: ',method,' CPU-time: ', num2str(time), 's'];
title(label)
%figure(903);
if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap  = method_output.stats.gap;
    label = [ 'iteration', ', time = ',num2str(time), 's'];
    subplot(3,2,4);   semilogy(gap),xlabel(label),ylabel(['log of the gap distance'])
    label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
    title(label)
end

if(any(strcmp('diagnostic', fieldnames(method_input))))&& (strcmp(method,'RAAR'))
    subplot(3,2,5);   semilogy(shadow_change),xlabel('iteration'),ylabel(['change in the shadows'])
    title(label)
end

Ny=sqrt(method_input.Ny);
U=reshape(u,Ny,Ny);    
subplot(3,2,6);  
%figure(905)
      imagesc(abs(U)),xlabel('reconstructed signal')
      % label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
      title(label)

success = 1;


