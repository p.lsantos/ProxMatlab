%                      Sensor_location.m
%             written on Aug 11, 2016 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen

%
%
% This module sets the projectors based on the problem type
% within the problem family 'Sensor_location'
function output = Sensor_location(input)

%%===============================================================
%% Rewrite fields in our representation:  this bit needs work to 
%%   standardise, particularly for the object domain constraints
%%===============================================================
if(strcmp(input.instance, 'JWST'))
    %% reshape and rename the data 
    input.data_sq = input.data;
    input.data = input.rt_data;
    tmp=size(input.data);
    if(tmp(1)~=input.Nx)
	input.data_sq=reshape(input.data_sq,input.Nx,input.Ny);
	% the projection algorithms work with the square root of the measurement:
	input.data=reshape(input.data,input.Nx,input.Ny);
    end
    input.norm_data=input.norm_rt_data;

    if(any(strcmp('Nz',fieldnames(input))))
    else
	input.Nz = 1;
    end
    % set the animation program:
    input.animation='Phase_animation';
end

%% If input.formulation is does not exist, i.e. not specified in 
%% the *_in.m file, use the product space as the default.
if(any(strcmp('formulation',fieldnames(input))))
    formulation = input.formulation;
else
    formulation = 'product space';
end


%% Set the projectors and inputs based on the types of constraints and 
%% experiments
if(~strcmp(input.instance, 'JWST'))
    input.Proj1 = 'Project_on_diagonal';
    input.Proj2 = 'Project_on_balls'; % 
else
    input.Proj1 = 'Project_on_JWST_diagonal';
    input.Proj2 = 'Project_on_JWST_balls'; % 
end    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  translate variables into more general names
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% nothing necessary

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Set parameters and initialization if use has not
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(~any(strcmp('receiver_data',fieldnames(input))))
    A=0; % default receiver location
else
    A = input.receiver_data;
end

if(~any(strcmp('TOL2',fieldnames(input))))
    input.TOL2 = 1e-15; 
end

%%===================================================================
%% 
%%   Run the algorithm!!!
%% 
%%===================================================================

output=feval(input.method,input);

iter = output.stats.iter;
stats=output.stats;
t=stats.time;
label = ['Took ',num2str(iter),' iterations and ',num2str(t), ' seconds.'];
disp(label)
if strcmp(input.method,'RAAR')
    tmp=min(stats.shadow_change);
else
    tmp=min(stats.change);
end
if(tmp>input.TOL)
    % label = ['Tolerance ',num2str(TOL), ' not achieved by iteration ', num2str(iter(j))];
    label = ['MAXIT=', num2str(input.MAXIT),' exceeded'];
    disp(label)
end
disp(' ')

%%===================================================================
%%
%%  now write output in form that comforms with standard format
%%  assumed in main_ProxToolbox
%%
%%===================================================================

%%===================================================================
%% 
%%   Graphics (could also do this from within main_ProxToolbox)
%% 
%%===================================================================
if(input.graphics==1)
    success = feval(input.graphics_display, input,output);
end
%%=========================================================================
%% 
%%   write results to file (could also do this from within main_ProxToolbox)
%% 
%%=========================================================================

end
