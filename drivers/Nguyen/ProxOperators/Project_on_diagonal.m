%                Project_on_diagonal.m
%              written on 9th September 2016 by
%                        Russell Luke  (from Haifa!)
%                University of Gottingen
%
% DESCRIPTION:  The projection of a vector on the ``block diagonal'' subspace.
%
%

function ud = Project_on_diagonal(method_input, u)

   K = method_input.product_space_dimension; % the size of the product space
   m = method_input.Ny; % the row-dim of the prod. space elements 
   n = method_input.Nx; % the column-dim of the prod. space elements 
   p = method_input.Nz;
   if(m==1) % product space of points 
       tmp = sum(u,2);
       tmp=tmp/K;
       ud = tmp*ones(1,K);
   elseif(n==1) % product space of points
       tmp = sum(u);
       tmp=tmp/K;
       ud = ones(K,1)*tmp;
   elseif(m==K) % product space of row vectors
	ud = repmat(sum(u, 1)./K,K,1);
   elseif(n==K) % product space of column vectors
	ud = repmat(sum(u, 2)./K,K,1);
   elseif(p==1) % product space a 3D array of 2D matrices
       tmp=zeros(n,m);
       for k=1:K
           tmp = tmp+u(:,:,k);
       end
       tmp=tmp/K;
       ud = repmat(tmp,[1,1,K]);
   else
       % the depth-dim of the prod. space elements (for 3-D matrices)     
       tmp=zeros(n,m,p);
       for k=1:K
           tmp = tmp+u(:,:,:,k);
       end
       tmp=tmp/K;
       ud = repmat(tmp,[1,1,1,K]);
   end
end
