
%                   sensor_location_in
%              written on August 8, 2016 by
%             Russell Luke & Nguyen Hieu Thao
%                 University of Goettingen
%
% DESCRIPTION:  parameter input file for main_ProxToolbox.m
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This is the input file that the user sees/modifies.  It should be simple, 
%% avoid jargon or acronyms, and should be a model for a menu-driven GUI

function prbl = sensor_location_in()

%% We start very general.
%% What type of problem is being solved?  Classification 
%% is according to the geometry:  Affine, Cone, Convex, 
%% Phase, Affine-sparsity, Nonlinear-sparsity, Sudoku

prbl.problem_family = 'Sensor_location';
prbl.instance = 'simple random';  % options include:  `simple file', 'JWST'

%%==========================================
%% Problem parameters
%%==========================================
%% What is the name of the data file?
prbl.data_filename = 'sensor_location_processor';


%% What type of object are we working with?
%% Options are: 'real', 'complex'
prbl.object = 'real';

%% What type of constraints do we have?
%% Options are: 'diagonal_and_balls'
prbl.constraint = 'diagonal_and_balls';

%% What are the sizes of the arrays (not the physical dimensions of the problem)?
sensors = 10;
dimension = 2;
prbl.Nx = dimension;
prbl.Ny = sensors;
prbl.Nz = 1; 
prbl.phys_boundary=10000;
prbl.radius_bound = 100;

%% What are the noise characteristics (Poisson or Gaussian or none)?
prbl.noise='Gaussian';

%%==========================================
%%  Algorithm parameters
%%==========================================
%% Now set some algorithm parameters that the user should be 
%% able to control (without too much damage)

%% Algorithm:
prbl.method = 'ADMMPlus';%'AP';  
prbl.stepsize = 1.5;
prbl.numruns = 1; % the only time this parameter will
% be different than 1 is when we are
% benchmarking...not something a normal user
% would be doing.


%% The following are parameters specific to RAAR, HPR, and HAAR that the 
%% user should be able to set/modify.  Surely
%% there will be other algorithm specific parameters that a user might 
%% want to play with.  Don't know how best 
%% to do this.  Thinking of a GUI interface, we could hard code all the 
%%  parameters the user might encounter and have the menu options change
%% depending on the value of the prbl.method field. 
%% do different things depending on the chosen algorithm:
    %% maximum number of iterations and tolerances
    prbl.MAXIT = 500;
    prbl.TOL = 1e-10;


%%==========================================
%% parameters for plotting and diagnostics
%%==========================================
prbl.verbose = 1; % options are 0 or 1
prbl.graphics = 1; % whether or not to display figures, options are 0 or 1.
                   % default is 1.
prbl.anim = 0;  % whether or not to disaply ``real time" reconstructions
                % options are 0=no, 1=yes, 2=make a movie
                % default is 1.
prbl.graphics_display = 'sensor_graphics'; % unless specified, a default 
                            % plotting subroutine will generate 
                            % the graphics.  Otherwise, the user
                            % can write their own plotting subroutine

%%======================================================================
%%  Technical/software specific parameters
%%======================================================================
%% Given the parameter values above, the following technical/algorithmic
%% parameters are automatically set.  The user does not need to know 
%% about these details, and so probably these parameters should be set in 
%% a module one level below this one.  

end

