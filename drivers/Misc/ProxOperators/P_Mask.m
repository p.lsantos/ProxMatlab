%                      P_Mask.m
%             written on October 6, 2011 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  
%
% INPUT:       input = a data structure with the following fields: 
%                .Maskhat = nonegative real FOURIER DOMAIN mask
%                .Mhat_0 = energy in the zero mode
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_M    = the projection IN THE PHYSICAL (time) DOMAIN
%               
% USAGE: p_M = P_Mask(input,u)
%
% Nonstandard Matlab function calls:  MagProj

function p_M = P_Mask(input,u)


% load('data/siemens_star_mask.mat')
% ZeroPad(I_mask);
tmp = feval('fft2',u);
p_M = (input.Maskhat).*tmp;
p_M(1)=input.Mhat_0; % matching the zero frequency keeps the energy 
                     % bounded below. 
p_M = feval('ifft2',p_M);
clear tmp u input
return


