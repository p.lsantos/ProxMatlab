%  Normal Cone projection subroutine of the
%  dual subgradient descent method  
%  for minimizing the l1 norm
%  subject to a linear constraint.  This 
%  strategy was first proposed in 
%  ``Duality and Convex Porgramming" by
%  J. M. Borwein and D. R. Luke, 
%  to appear in THE HANDBOOK OF IMAGING
%  edited by Otmar Scherzer.  This algorithm
%  together with analysis is in preparation for 
%  submission to the book FIXED-POINT ALGORITHMS 
%  FOR INVERSE PROBLEMS IN SCIENCE AND ENGINEERING,
%  part of the Springer Verlag series Optimization 
%  and Its Applications.
%  For preprints go to 
%
% http://num.math.uni-goettingen.de/~r.luke/publications
% 
%  Russell Luke
%  Universitaet Goettingen and 
%  University of Delaware
%  Oct. 28, 2009
%
function v = P_A(input,v0)

v=0.*v0;
v(input.JJp) = max(v0(input.JJp), 0);
v(input.JJm) = min(v0(input.JJm),0);

clear v0
return