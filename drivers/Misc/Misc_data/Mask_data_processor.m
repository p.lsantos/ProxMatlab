% data reader/processor

function input = Mask_data_processor(input)


% load -ascii Maskhat.mat
L=2;
points=70;
x=-L/2:L/points:L/2-L/points;
centerx=x(points/2+1);
centery=centerx;
Maskhat=ones(points,points);
for j=1:points
    for i=points/2+1:points
        if((abs(atan(x(j)/x(i)))<=pi/4)&& ((x(j)-centery).^2+(x(i)-centerx).^2>(1/25))&&((x(j)-centery).^2+(x(i)-centerx).^2<1))
            Maskhat(j,i)=0;
            Maskhat(j, 1+points-i)=0;
        end
    end
end
stepup=ceil(log2(input.Nx/points));
for j=1:stepup
  Maskhat=OnesPad(Maskhat);
end
Maskhat=fftshift(Maskhat);

points = input.Nx;
G=Gaussian(points,10,[points/2+1,points/2+1]);
Aperture=zeros(points,points);
I=G>=G(points,points/2+1);
Aperture(I)=1;

input.Maskhat = Maskhat;
input.supp_ampl = find(Maskhat~=0);
input.Mhat_0 = sum(sum(Aperture));
input.support = Aperture;
figure(88)
imagesc(Aperture);
input.support_idx=find(Aperture>0);
input.u_0=Aperture;
input.product_space_dimension = 1;


clear Maskhat Aperture G I x
end




