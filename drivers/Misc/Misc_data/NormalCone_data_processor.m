%                  NormalCone_data_processor.m
%                written on May 27, 2009 by
%                     Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  %
% INPUT: 
%
% OUTPUT:
%
% USAGE: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function input = NormalCone_data_processor(input)

% load toy data
    disp(['Loading data file NormalCone.mat '])
    load NormalCone.mat

input.M=M;
input.b=b;
input.JJp=JJp;
input.JJm=JJm;
input.u_0=ifft(b);
input.product_space_dimension=1;
clear M b JJp JJm
end

