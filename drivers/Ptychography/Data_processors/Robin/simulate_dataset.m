addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/main/tools/pie');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/main/tools/');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/psi_routines');

clear;
close all;
startup;

%Save the data?
P.save_data = false;

current_path = pwd;

if P.save_data == true
    disp(['Current path is: ' pwd]);
    answer = input('Are you in the correct directory for saving (y/n)? ','s');
    if ~isequal(answer,'y')
        error('Apply your necessary changes and start again.')
    end
else
    disp('Data is not saved.');
end


%% Parameter declaration

P.datatype = 'sim';

%desired number of pixels
Nx = 2048/4;
P.Nx = Nx;
Ny = 2048/4;
P.Ny = Ny;
framesize = [Ny,Nx];

%energy in keV
E = 1.2;
P.E = E;

%wavelength in microns
lambda = 12.394/E*1E-10;   
k = 2*pi/lambda;
P.lambda = lambda;
P.k = k;

%simulated propagation distances
z12 = 0.6;
P.z12 = z12;
z01 = 1*1e-3;
P.z01 = z01;

%pinhole radius
R = 1*1e-6;
P.R = R;

%pinhole area
A_p = pi*R^2;

%fresnel number
F = 4*R^2/(lambda*z01)

%fluence (in one second)
F_ph = 1e12/(5e-6)^2;

% total incoming intensity through pinhole (counts)
I_0 = 1e8;%F_ph*A_p
P.I_0 = I_0;

% step size of PIE scan
pie_step_x = 0.25*1e-6;
pie_step_y = 0.25*1e-6;
pie_step = [pie_step_y pie_step_x];

P.pie_step_x = pie_step_x;
P.pie_step_y = pie_step_y;
P.pie_step = pie_step;

%number of pie positions
nx = 5; %horizontal
ny = 5; %vertical
P.nx = nx;
P.ny = ny;

if ~isequal(nx,ny)
    error('Quadratic PIE scan supported only for now.');
end
%NOTE: actual sidelength of meshscan is equal to nx and ny

%total number of PIE points
N_pie = nx*ny;
P.N_pie = N_pie;

%%  Prepare coordinate systems etc.

%detector plane (E2) ------------------------------------------------------

%pixel dimensions
d2x = 13.5*1E-6;
P.d2x = d2x;
d2y = 13.5*1E-6;
P.d2y = d2y;

%oversampling radtio
sigma = lambda*z12/(d2x*2*R)

D2x = Nx*d2x;
P.D2x = D2x;
D2y = Ny*d2y;
P.D2y = D2y;

% coordinate system (points at centers of the pixels)
[X_2,Y_2] = meshgrid(d2x/2 + [0:1:Nx-1]*d2x - Nx/2*d2x,d2y/2 + [0:1:Ny-1]*d2y - Ny/2*d2y);

% highest q-angle with respect to previous plane
q2x_max = 4*pi/lambda*sin(0.5*atan(D2x/(2*z12)))*cos(0.5*atan(D2x/(2*z12)));
P.q2x_max = q2x_max;
q2y_max = 4*pi/lambda*sin(0.5*atan(D2y/(2*z12)))*cos(0.5*atan(D2y/(2*z12)));
P.q2y_max = q2y_max;

% coordinate system in Fourier space
dq2x = 2*q2x_max/Nx;
P.dq2x = dq2x;
dq2y = 2*q2y_max/Ny;
P.dq2y = dq2y;
[Q2_x,Q2_y] = meshgrid(-q2x_max + dq2x/2 + [0:1:Nx-1]*dq2x,-q2y_max + dq2y/2 + [0:1:Ny-1]*dq2y);

%specimen plane (E1) / probe plane (as long as direct propagation is used) -
%checked with Pierres program (19.02.09, KG)

%the resolution is set to be the pixel size in the object plane
d1x = pi/q2x_max;
P.d1x = d1x;
d1y = pi/q2y_max;
P.d1y = d1y;

D1x = Nx*d1x;
P.D1x = D1x;
D1y = Ny*d1y;
P.D1y = D1y;

% coordinate system (points at centers of the pixels)
[X_1,Y_1] = meshgrid(d1x/2 + [0:1:Nx-1]*d1x - Nx/2*d1x,d1y/2 + [0:1:Ny-1]*d1y - Ny/2*d1y);

% highest q-angle with respect to previous plane
q1x_max = 4*pi/lambda*sin(0.5*atan(D1x/(2*z01)))*cos(0.5*atan(D1x/(2*z01)));
P.q1x_max = q1x_max;
q1y_max = 4*pi/lambda*sin(0.5*atan(D1y/(2*z01)))*cos(0.5*atan(D1y/(2*z01)));
P.q1y_max = q1y_max;

% coordinate system in Fourier space
dq1x = 2*q1x_max/Nx;
P.dq1x = dq1x;
dq1y = 2*q1y_max/Ny;
P.dq1y = dq1y;
[Q1_x,Q1_y] = meshgrid(-q1x_max + dq1x/2 + [0:1:Nx-1]*dq1x,-q1y_max + dq1y/2 + [0:1:Ny-1]*dq1y);

%probe plane (E0) / if far-field propagation is used

%the resolution is set to be the pixel size in the object plane
d0x = pi/q1x_max;
P.d0x = d0x;
d0y = pi/q1y_max;
P.d0y = d0y;

D0x = Nx*d0x;
P.D0x = D0x;
D0y = Ny*d0y;
P.D0y = D0y;

% coordinate system (points at centers of the pixels)
[X_0,Y_0] = meshgrid(d0x/2 + [0:1:Nx-1]*d0x - Nx/2*d0x,d0y/2 + [0:1:Ny-1]*d0y - Ny/2*d0y);


%% setting up PIE matrix
% positions matrix (will contain (x,y)-coordinates of positions in pixel units of object plane)

% nominal positions
positions = zeros([N_pie,2]);
k = 1;
for n_slow = 0:ny-1
    for n_fast = 0:nx-1
        positions(k,2) = pie_step(2) * n_fast/d1x; % horizontal axis
        positions(k,1) = pie_step(1) * n_slow/d1y; % vertical axis
        k = k+1;
    end
end
positions(:,1) = positions(:,1) - min(positions(:,1));
positions(:,2) = positions(:,2) - min(positions(:,2));
positions = round(positions);

P.positions = positions;

% Adjust objetc size accordingly
object_size = [Ny,Nx] + max(positions,[],1);
P.object_size = object_size;
if ~isequal(object_size(1),object_size(2))
    error('Only quadratic object frame supported right now.')
end

%extended coordinate system for object plane
[X_obj,Y_obj] = meshgrid(P.d1x/2 + [0:1:P.object_size(2)-1]*P.d1x - P.Nx/2*P.d1x,P.d1y/2 + [0:1:P.object_size(1)-1]*P.d1y - P.Ny/2*P.d1y);

%valid pixel mask
I_valid = ones(P.Ny,P.Nx,ny,nx);


%% Simulate Probe

if F > 0.5 
    propagation_method = 'nf'
else
    propagation_method = 'ff'
end

switch propagation_method

    case 'nf'
        
        %calculate probe
        probe_true = circ(X_1,Y_1,0,0,R);

        % propagation from pinhole to sample plane
        kappa = sqrt(1-fftshift(Q2_x.^2+Q2_y.^2)/P.k^2);
        P.kappa = kappa;
        Probe_true = ifft2(fft2(probe_true).*exp(i*P.k*z01*(kappa-1)));

        %normalization to I_0
        Probe_true = sqrt(I_0/(numel(Probe_true))) * Probe_true/norm_avg(Probe_true);
        P.Probe_true = Probe_true;

    case 'ff'
        
        %calculate probe
        probe_true = circ(X_0,Y_0,0,0,R);
        Probe_true = dist_obj_prop(probe_true,X_0,Y_0,X_1,Y_1,k,lambda,z01);
        % 
        %normalization to I_0
        Probe_true = sqrt(I_0/(numel(Probe_true))) * Probe_true/norm_avg(Probe_true);
        P.Probe_true = Probe_true;
end

Probe_true_hsv = complex_plot(Probe_true);

figure; imagesc(squeeze(X_1(1,:)),squeeze(Y_1(:,1)),hsv2rgb(Probe_true_hsv)); axis equal; axis tight;
title(['\sigma = ', num2str(sigma), ', F = ', num2str(F)]); xlabel('x [\mum]'); ylabel('y [\mum]');

figure; plot(squeeze(X_1(1,:)), abs(Probe_true(Ny/2,:)).^2);

%% Simulate object

%contrast possibilities

%phase is measured in units of pi

P.contrast = 'Ta500nm';

switch P.contrast
    case 'cell'
        T = 1-0.0813;
        A = 1-T;
        phase = 1/pi;
    case 'Ta500nm'
        A = 0.6629;
        T = 1-A;
        phase = 2.6/pi; %in units of pi
end

P.phantom = 'siemens';

switch P.phantom
    
    case 'image'
        
        phantom_path = '/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/matlab/main/phantoms/';
        P.image_name = 'rd_phantom.png';
        P.image_name = 'NTT.tif';
        addpath(phantom_path);
                
        % resoze image, mean over all rgb colors, grayscale between 0 and 1
        pattern = mat2gray(mean(double(imresize(imread(P.image_name),object_size)),3));
        
        % adjusted so that maximum amplitude difference is "A"
        datamp = 1 - A * squeeze(pattern);
        % adjusted so that maximum phase difference is "phase"
        datphase = phase * (2*pi.*squeeze(pattern)-pi)/2;
        % should be alright now (KG, 02.03.2009)
        
        object_true = datamp.*exp(sqrt(-1)*datphase);
    
    case 'siemens'

        STAR = star(floor(Nx/2),25,object_size(1));
        datamp = STAR*T;
        datphase = pi*STAR*phase;
        object_true = datamp.*exp(sqrt(-1)*datphase);

end

figure; imagesc(squeeze(X_1(1,:)),squeeze(Y_1(:,1)),angle(object_true)); axis equal; axis tight; colormap jet; colorbar
xlabel('x [\mum]'); ylabel('y [\mum]');


%% Simulate diffraction patterns

I_exp = zeros(P.Ny,P.Nx,P.ny,P.nx);

figure;
pos = 1;
for n_slow = 1:P.ny
    for n_fast = 1:P.nx

    Indy = P.positions(pos,1) + (1:P.Ny);
    Indx = P.positions(pos,2) + (1:P.Nx); 

    psi = Probe_true.*object_true(Indy,Indx);

    I_exp(:,:,n_slow,n_fast) = abs(fftshift(FFT2(psi))).^2;
    
    % generating experimental pattern with Poissonian noise
    % the factor 1E12 is to correct the matlab internal scaling for
    % generating poissonian noise
    % important: I has to be of type double

    if max(max(I_exp(:,:,n_slow,n_fast))) > 1e12
        error('Poissonian noise generator works only for values < 10^12.')
    else
        I_exp(:,:,n_slow,n_fast) = 1E12*imnoise(floor(I_exp(:,:,n_slow,n_fast))*1E-12,'poisson');
    end

    imagesc(squeeze(Q2_x(1,:)*1e-6/pi),squeeze(Q2_y(:,1)*1e-6/pi),log10(I_exp(:,:,n_slow,n_fast))); axis equal; axis tight; colormap jet; colorbar;
    xlabel('f_x [\mum^{-1}]'); ylabel('f_y [\mum^{-1}]'); title(['I_{max} = ',num2str(max(max(I_exp(:,:,n_slow,n_fast))))]);
    drawnow;
    
    pos = pos + 1; 
    end    
end

%numel(Probe_true)*norm_avg(sqrt(I_exp(:,:,1,1)))^2

P.I_exp = I_exp;
P.I_valid = I_valid;
P.object_true = object_true;

%% Down-binning of the exactly-simulated diffraction patterns

binning_factor = 4;
cropping_factor = 2;

%downbinned number of pixels
Nx = 2048/(binning_factor*cropping_factor);
P.Nx = Nx;
Ny = 2048/(binning_factor*cropping_factor);
P.Ny = Ny;
framesize = [Ny,Nx];

%detector plane (E2) ------------------------------------------------------

%pixel dimensions
d2x = 13.5*1E-6*binning_factor;
P.d2x = d2x;
d2y = 13.5*1E-6*binning_factor;
P.d2y = d2y;

%oversampling radtio
sigma = lambda*z12/(d2x*2*R)

D2x = Nx*d2x;
P.D2x = D2x;
D2y = Ny*d2y;
P.D2y = D2y;

% coordinate system (points at centers of the pixels)
[X_2,Y_2] = meshgrid(d2x/2 + [0:1:Nx-1]*d2x - Nx/2*d2x,d2y/2 + [0:1:Ny-1]*d2y - Ny/2*d2y);

% highest q-angle with respect to previous plane
q2x_max = 4*pi/lambda*sin(0.5*atan(D2x/(2*z12)))*cos(0.5*atan(D2x/(2*z12)));
P.q2x_max = q2x_max;
q2y_max = 4*pi/lambda*sin(0.5*atan(D2y/(2*z12)))*cos(0.5*atan(D2y/(2*z12)));
P.q2y_max = q2y_max;

% coordinate system in Fourier space
dq2x = 2*q2x_max/Nx;
P.dq2x = dq2x;
dq2y = 2*q2y_max/Ny;
P.dq2y = dq2y;
[Q2_x,Q2_y] = meshgrid(-q2x_max + dq2x/2 + [0:1:Nx-1]*dq2x,-q2y_max + dq2y/2 + [0:1:Ny-1]*dq2y);

%specimen plane (E1) / probe plane (as long as direct propagation is used) -
%checked with Pierres program (19.02.09, KG)

%the resolution is set to be the pixel size in the object plane
d1x = pi/q2x_max;
P.d1x = d1x;
d1y = pi/q2y_max;
P.d1y = d1y;

D1x = Nx*d1x;
P.D1x = D1x;
D1y = Ny*d1y;
P.D1y = D1y;

% coordinate system (points at centers of the pixels)
[X_1,Y_1] = meshgrid(d1x/2 + [0:1:Nx-1]*d1x - Nx/2*d1x,d1y/2 + [0:1:Ny-1]*d1y - Ny/2*d1y);

% highest q-angle with respect to previous plane
q1x_max = 4*pi/lambda*sin(0.5*atan(D1x/(2*z01)))*cos(0.5*atan(D1x/(2*z01)));
P.q1x_max = q1x_max;
q1y_max = 4*pi/lambda*sin(0.5*atan(D1y/(2*z01)))*cos(0.5*atan(D1y/(2*z01)));
P.q1y_max = q1y_max;

% coordinate system in Fourier space
dq1x = 2*q1x_max/Nx;
P.dq1x = dq1x;
dq1y = 2*q1y_max/Ny;
P.dq1y = dq1y;
[Q1_x,Q1_y] = meshgrid(-q1x_max + dq1x/2 + [0:1:Nx-1]*dq1x,-q1y_max + dq1y/2 + [0:1:Ny-1]*dq1y);

%probe plane (E0) / if far-field propagation is used

%the resolution is set to be the pixel size in the object plane
d0x = pi/q1x_max;
P.d0x = d0x;
d0y = pi/q1y_max;
P.d0y = d0y;

D0x = Nx*d0x;
P.D0x = D0x;
D0y = Ny*d0y;
P.D0y = D0y;

% coordinate system (points at centers of the pixels)
[X_0,Y_0] = meshgrid(d0x/2 + [0:1:Nx-1]*d0x - Nx/2*d0x,d0y/2 + [0:1:Ny-1]*d0y - Ny/2*d0y);


% setting up PIE matrix
% positions matrix (will contain (x,y)-coordinates of positions in pixel units of object plane)

% nominal positions
positions = zeros([N_pie,2]);
k = 1;
for n_slow = 0:ny-1
    for n_fast = 0:nx-1
        positions(k,2) = pie_step(2) * n_fast/d1x; % horizontal axis
        positions(k,1) = pie_step(1) * n_slow/d1y; % vertical axis
        k = k+1;
    end
end
positions(:,1) = positions(:,1) - min(positions(:,1));
positions(:,2) = positions(:,2) - min(positions(:,2));
positions = round(positions);

P.positions = positions;

% Adjust objetc size accordingly
object_size = [Ny,Nx] + max(positions,[],1);
P.object_size = object_size;
if ~isequal(object_size(1),object_size(2))
    error('Only quadratic object frame supported right now.')
end

%extended coordinate system for object plane
[X_obj,Y_obj] = meshgrid(P.d1x/2 + [0:1:P.object_size(2)-1]*P.d1x - P.Nx/2*P.d1x,P.d1y/2 + [0:1:P.object_size(1)-1]*P.d1y - P.Ny/2*P.d1y);

%valid pixel mask
I_valid = ones(P.Ny,P.Nx,ny,nx);

% Interpolate Probe

I_exp_new = binning(I_exp(:,:,1,1),binning_factor);
S = size(I_exp_new);
cy = S(1)/2+1;
cx = S(2)/2+1;
I_exp_new = I_exp_new(cy-Ny/2:cy+Ny/2-1,cx-Nx/2:cx+Nx/2-1);

I_exp_new = zeros(P.Ny,P.Nx,P.ny,P.nx);

ind = 1;
disp('Interpolating...');
for n_slow = 1:P.ny
    for n_fast = 1:P.nx

        dummy = binning(I_exp(:,:,n_slow,n_fast),binning_factor);
        S = size(dummy);
        cy = S(1)/2+1;
        cx = S(2)/2+1;
        I_exp_new(:,:,n_slow,n_fast) = dummy(cy-Ny/2:cy+Ny/2-1,cx-Nx/2:cx+Nx/2-1);
        
        ind = ind+1;
        if mod(ind,5) == 0
            fprintf('%4.2f per cent completed.\n',ind/N_pie*100);
        end
    end
end

P.I_exp = I_exp_new;
P.I_valid = I_valid;
P.object_true = [];
P.Probe_true = [];

%% Store simulation

if P.save_data == true
    
    fprintf('Now storing the data.\n');

    save_dir = [datestr(now,30) '_simulation_', P.phantom, '_', P.contrast];
    mkdir(save_dir);
    cd(save_dir);
    
    save(['simulation_', P.phantom, '_', P.contrast, '_', num2str(P.Nx),'x',num2str(P.Ny),'.mat'],'P');

    diary parameters.txt
    P 
    diary off;
    
    cd /home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/matlab/main/tools/pie/;
    copyfile('simulate_dataset.m',[current_path, '/', save_dir ]);
    cd(current_path);
    
end
