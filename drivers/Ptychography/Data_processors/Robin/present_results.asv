function val = present_results(data_file,z01,phase_bounds,defoc_series,save_plots)

addpath('C:\Dokumente und Einstellungen\spec_user\Eigene Dateien\Messzeiten\0909_Bessy\matlab\main\tools');
addpath('C:\Dokumente und Einstellungen\spec_user\Eigene Dateien\Messzeiten\0909_Bessy\matlab\main\tools\pie');
addpath('C:\Dokumente und Einstellungen\spec_user\Eigene Dateien\Messzeiten\0909_Bessy\matlab\psi_routines');
addpath('C:\Dokumente und Einstellungen\spec_user\Eigene Dateien\Messzeiten\0909_Bessy\matlab\psi_routines\matlab_pierre\all\');


% load experimental data
close all;
startup;

disp('Loading datafile.');
load(data_file);
disp('Finished loading data.');
disp('Preparing plots.');

P.z01 = z01;

if save_plots
    mkdir('result_plots');
    cd('result_plots');
end

%% Generate coordinate systems

%detector plane (E2) ------------------------------------------------------

% coordinate system (points at centers of the pixels)
[X_2,Y_2] = meshgrid(P.d2x/2 + [0:1:P.Nx-1]*P.d2x - P.Nx/2*P.d2x,P.d2y/2 + [0:1:P.Ny-1]*P.d2y - P.Ny/2*P.d2y);

% coordinate system in Fourier space
[Q2_x,Q2_y] = meshgrid(-P.q2x_max + P.dq2x/2 + [0:1:P.Nx-1]*P.dq2x,-P.q2y_max + P.dq2y/2 + [0:1:P.Ny-1]*P.dq2y);

%specimen plane (E1)/ probe plane (as long as direct propagation is used) -

% coordinate system (points at centers of the pixels)
[X_1,Y_1] = meshgrid(P.d1x/2 + [0:1:P.Nx-1]*P.d1x - P.Nx/2*P.d1x,P.d1y/2 + [0:1:P.Ny-1]*P.d1y - P.Ny/2*P.d1y);

% coordinate system in Fourier space
[Q1_x,Q1_y] = meshgrid(-P.q1x_max + P.dq1x/2 + [0:1:P.Nx-1]*P.dq1x,-P.q1y_max + P.dq1y/2 + [0:1:P.Ny-1]*P.dq1y);

%extended coordinate system for object plane
[X_obj,Y_obj] = meshgrid(P.d1x/2 + [0:1:P.object_size(2)-1]*P.d1x - P.Nx/2*P.d1x, P.d1y/2 + [0:1:P.object_size(1)-1]*P.d1y - P.Ny/2*P.d1y);

% Fresnel number
F = (2*P.R)^2/(P.lambda*P.z01);
P.F = F;


%% Fiddling around with the experimental data a bit (normalization etc.)

% maximum average intensity in a single experimental frame
I_max_avg = max(max(sum(sum(P.I_exp,1),2),[],3),[],4) / (P.Nx*P.Ny);

% generate cell array of normalized intensities 
% normalizeation such, that for I = I_max (as matrix) the average pixel
% intensity is 1 and total is Nx*Ny, for all other I the values are lower
Amp_exp_norm = cell(P.Nx*P.Ny,1);

% average of squared(??) autocorrelation function of each dataset
auto = zeros(P.Ny,P.Nx);

% center coordinates (assuming data is already centered)
cx=floor(size(P.I_exp,1)/2);
cy=floor(size(P.I_exp,2)/2);

k=1;
for n_slow = 1:P.ny
    for n_fast = 1:P.nx
        dat = P.I_exp(:,:,n_slow,n_fast);
        auto = auto + FFT2(fftshift(dat));
        Amp_exp_norm{k} = sqrt(dat/I_max_avg);
        %no Amp_exp_norm can be larger than 1
        k = k+1;
    end
end


%% Plot final result

if isequal(P.datatype,'exp')

    %Object (combined)
    objecthsv = zeros(P.object_size(1),P.object_size(2),3);
    objecthsv(:,:,1) = (angle(P.object.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
    objecthsv(:,:,2) = ones(size(P.object,1),size(P.object,2));
    objecthsv(:,:,3) = abs(P.object)- (0.5+0.5*sign(abs(P.object)-1)).*(abs(P.object)-1);
    % This is equivalent to function 
    % f(x) = abs(x)     for abs(x) <= 1 and
    % f(x) = 1          for abs(x) > 1
    figure;
    imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,hsv2rgb(objecthsv)); axis xy; axis equal; axis tight; colormap hsv;
    xlabel('\mum'); ylabel('\mum');
    title(['Object']);
    if save_plots
        figurename = 'complex_object';
        saveas(gcf, figurename, 'fig');
    end
    
    %Probe (combined)
    probehsv = zeros(P.Ny,P.Nx,3);
    probehsv(:,:,1) = (angle(P.Probe.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
    probehsv(:,:,2) = ones(size(P.Probe,1),size(P.Probe,2));
    probehsv(:,:,3) = abs(P.Probe)/max(max(abs(P.Probe)));
    figure;
    imagesc(squeeze(X_1(1,:))*1e6,squeeze(Y_1(:,1))*1e6,hsv2rgb(probehsv)); axis xy; axis equal; axis tight;
    xlabel('\mum'); ylabel('\mum');
    title(['Probe']);
    if save_plots
        figurename = 'complex_probe';
        saveas(gcf, figurename, 'fig');
    end
    
    %Object (phase)
    figure;
    h = imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,angle(P.object)); axis xy; axis equal; axis tight;
    colormap hsv; colorbar; caxis(phase_bounds); xlabel('\mum'); ylabel('\mum');
    title(['Probe']);
    if save_plots
        figurename = 'phase_object';
        saveas(gcf, figurename, 'fig');
    end
    

elseif isequal(P.datatype,'sim')
    
    %Object (combined)
    objecthsv = zeros(P.object_size(1),P.object_size(2),3);
    objecthsv(:,:,1) = (angle(P.object.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
    objecthsv(:,:,2) = ones(size(P.object,1),size(P.object,2));
    objecthsv(:,:,3) = abs(P.object)- (0.5+0.5*sign(abs(P.object)-1)).*(abs(P.object)-1);
    % This is equivalent to function 
    % f(x) = abs(x)     for abs(x) <= 1 and
    % f(x) = 1          for abs(x) > 1
    figure;
    subplot(1,2,1);
    imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,hsv2rgb(objecthsv)); axis xy; axis equal; axis tight; colormap hsv;
    xlabel('\mum'); ylabel('\mum');
    title(['Object']);
    objecthsv = zeros(P.object_size(1),P.object_size(2),3);
    objecthsv(:,:,1) = (angle(P.object_true.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
    objecthsv(:,:,2) = ones(size(P.object,1),size(P.object,2));
    objecthsv(:,:,3) = abs(P.object_true)- (0.5+0.5*sign(abs(P.object_true)-1)).*(abs(P.object_true)-1);
    % This is equivalent to function 
    % f(x) = abs(x)     for abs(x) <= 1 and
    % f(x) = 1          for abs(x) > 1
    subplot(1,2,2);
    imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,hsv2rgb(objecthsv)); axis xy; axis equal; axis tight; colormap hsv;
    xlabel('\mum'); ylabel('\mum');
    title(['Object (original)']);
    if save_plots
        figurename = 'complex_object';
        saveas(gcf, figurename, 'fig');
    end
    

    %Probe (combined)
    probehsv = zeros(P.Ny,P.Nx,3);
    probehsv(:,:,1) = (angle(P.Probe.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
    probehsv(:,:,2) = ones(size(P.Probe,1),size(P.Probe,2));
    probehsv(:,:,3) = abs(P.Probe)/max(max(abs(P.Probe)));
    figure;
    subplot(1,2,1);
    imagesc(squeeze(X_1(1,:))*1e6,squeeze(Y_1(:,1))*1e6,hsv2rgb(probehsv)); axis xy; axis equal; axis tight;
    xlabel('\mum'); ylabel('\mum');
    title(['Probe']);
    probehsv = zeros(P.Ny,P.Nx,3);
    probehsv(:,:,1) = (angle(P.Probe_true.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
    probehsv(:,:,2) = ones(size(P.Probe,1),size(P.Probe,2));
    probehsv(:,:,3) = abs(P.Probe_true)/max(max(abs(P.Probe_true)));
    subplot(1,2,2);
    imagesc(squeeze(X_1(1,:))*1e6,squeeze(Y_1(:,1))*1e6,hsv2rgb(probehsv)); axis xy; axis equal; axis tight;
    xlabel('\mum'); ylabel('\mum');
    title(['Probe (original)']);
    if save_plots
        figurename = 'complex_probe';
        saveas(gcf, figurename, 'fig');
    end
    

    %Object (phase)
    figure;
    subplot(1,2,1);
    h = imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,angle(P.object)); axis xy; axis equal; axis tight;
    colormap hsv; colorbar; caxis(phase_bounds); xlabel('\mum'); ylabel('\mum');
    title(['Object (phase)']);
    subplot(1,2,2);
    h = imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,angle(P.object_true)); axis xy; axis equal; axis tight;
    colormap hsv; colorbar; caxis(phase_bounds); xlabel('\mum'); ylabel('\mum');
    title(['Object, original (phase)']);
    if save_plots
        figurename = 'phase_object';
        saveas(gcf, figurename, 'fig');
    end
    

end    

Indy = P.positions(10,1) + (1:P.Ny);
Indx = P.positions(10,2) + (1:P.Nx); 
psi = P.object(Indy,Indx).*P.Probe;

figure;
subplot(1,2,2);
I_norm = Amp_exp_norm{1}.^2; MIN = log10(min(min(I_norm(find(I_norm > 0)))));
MAX = log10(max(max(Amp_exp_norm{1}.^2)));
imagesc(squeeze(Q2_x(1,:)*1e-6/(2*pi)),squeeze(Q2_y(:,1)*1e-6/(2*pi)),log10((Amp_exp_norm{1}.^2))); axis xy; axis equal; axis tight;
colormap jet; caxis([MIN MAX]); colorbar; xlabel('f_x [\mum^{-1}]'); ylabel('f_y [\mum^{-1}]');
title(['Measured/simulated diffraction pattern at first position']);
subplot(1,2,1);
imagesc(squeeze(Q2_x(1,:)*1e-6/(2*pi)),squeeze(Q2_y(:,1)*1e-6/(2*pi)),log10(fftshift(abs(FFT2(psi))).^2)); axis xy; axis equal; axis tight;
colormap jet; caxis([MIN MAX]); colorbar; xlabel('f_x [\mum^{-1}]'); ylabel('f_y [\mum^{-1}]');
title(['Reconstructed diffraction pattern at first position']);
if save_plots
    figurename = 'diffraction_pattern';
    saveas(gcf, figurename, 'fig');
end
    

figure;
subplot(1,2,1);
semilogy(P.err);
title(['DM-distance, end value is ', num2str(P.err(end))]);
subplot(1,2,2);
plot(P.chi);
title(['chi, end value is ' num2str(P.chi(end))]);
if save_plots
    figurename = 'error';
    saveas(gcf, figurename, 'fig');
end
    
%% Defocus series

if defoc_series

    disp('Calculating defocus series.');

    Z = [-2e-3:2e-5:1e-3];
    Probe_defoc = zeros(P.Ny,P.Nx,length(Z));
    kappa = sqrt(P.k^2-(Q2_x.^2+Q2_y.^2));

    for k = 1:length(Z)
        %fprintf('Z =  %d mm\n',Z(k)*1000);
        Probe_defoc(:,:,k) = single(IFFT2(FFT2(P.Probe).*fftshift(exp(i*kappa*Z(k)))));
        Probe_defoc(:,:,k) = single(Probe_defoc(:,:,k)./norm_avg(Probe_defoc(:,:,k)));
    end

    %mkdir('defocus_series');
    %cd ('defocus_series');

    figure;
    %set(gcf,'PaperOrientation','portrait');
    %set(gcf, 'PaperPositionMode','auto');

    Probe_crosssection = squeeze(Probe_defoc(P.Ny/2,:,:));

    for k = 1:length(Z)

        probehsv = zeros(P.Ny,P.Nx,3);
        probehsv(:,:,1) = ones(P.Ny,P.Nx);%.*(angle(Probe_defoc(:,:,k).*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
        probehsv(:,:,2) = ones(P.Ny,P.Nx);
        probehsv(:,:,3) = abs(Probe_defoc(:,:,k))/max(max(abs(Probe_defoc(:,:,k))));
    %    probehsv(:,:,3) = abs(Probe_defoc(:,:,k))- (0.5+0.5*sign(abs(Probe_defoc(:,:,k))-1)).*(abs(Probe_defoc(:,:,k))-1);
        subplot(1,2,1); imagesc(1e6*squeeze(X_1(1,:)),1e6*squeeze(Y_1(:,1)),hsv2rgb(probehsv)); axis xy; axis equal; axis tight; xlabel('x [\mum]'); ylabel('y [\mum]');

        dummy = Probe_crosssection;
        dummy(:,k) = max(max(dummy));

        probehsv = zeros(P.Nx,length(Z),3);
        probehsv(:,:,1) = ones(P.Nx,length(Z));%.*(angle(Probe_crosssection.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
        probehsv(:,:,2) = ones(P.Nx,length(Z));
        probehsv(:,:,3) = abs(dummy)/max(max(abs(dummy)));    

        subplot(1,2,2); imagesc(Z*1000, squeeze(X_1(1,:))*1e6,hsv2rgb(probehsv)); axis xy; xlabel('z [mm]'); ylabel('x [\mum]'); axis equal; axis tight; ylim([-2,2]); 
        drawnow;

       %filename = [num2str(k,'%0.3i'), '.jpg'];

       %orient rotated;
       %print ('-djpeg100', filename);


    end

    %cd('..');

    % cross section
    figure;
    imagesc(Z*1000, squeeze(X_1(1,:))*1e6,hsv2rgb(probehsv)); axis xy; colormap gray; colorbar;
    if save_plots
        figurename = 'cross_section';
        saveas(gcf, figurename, 'fig');
    end
    
    % probe at aperture position

    [dummy,k] = min(abs(Z+P.z01)); 

    probehsv = zeros(P.Ny,P.Nx,3);
    probehsv(:,:,1) = (angle(Probe_defoc(:,:,k).*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
    probehsv(:,:,2) = ones(P.Ny,P.Nx);
    probehsv(:,:,3) = abs(Probe_defoc(:,:,k))/max(max(abs(Probe_defoc(:,:,k))));
    %probehsv(:,:,3) = abs(Probe_defoc(:,:,k))- (0.5+0.5*sign(abs(Probe_defoc(:,:,k))-1)).*(abs(Probe_defoc(:,:,k))-1); 
    figure; imagesc(1e6*squeeze(X_1(1,:)),1e6*squeeze(Y_1(:,1)),hsv2rgb(probehsv)); axis xy; axis equal; axis tight; xlabel('x [\mum]'); ylabel('y [\mum]');
    if save_plots
        figurename = 'complex_probe_aperture';
        saveas(gcf, figurename, 'fig');
    end    
    
end

%%
% Create a new figure, and position it
%    fig1 = figure;
%    winsize = get(fig1,'Position');
%    winsize(1:2) = [0 0];
% Create movie file with required parameters
%    fps= 10;
%    outfile = sprintf('%s',[datafile(1:end-4),'.avi'])
%    mov = avifile(outfile,'fps',fps,'quality',100);
% Ensure each frame is of the same size
%    set(fig1,'NextPlot','replacechildren');
    
%for pos = 1:P.N_pie
%    Indy = P.positions(pos,1) + (1:P.Ny);
%    Indx = P.positions(pos,2) + (1:P.Nx);
    
%    psi = P.Probe.*P.object(Indy,Indx);
%    imagesc(squeeze(Q2_x(1,:)),squeeze(Q2_y(:,1)),log10(abs(fftshift(FFT2(psi))).^2));
%    caxis([-5 3]); axis equal; axis image; axis tight; colormap jet; drawnow;
    % put this plot in a movieframe
    % In case plot title and axes area are needed
    %     F = getframe(fig1,winsize);
    % For clean plot without title and axes
%    F = getframe;
%    mov = addframe(mov,F);
%end

% save movie
%    mov = close(mov);

%cd('..')



val = 1;