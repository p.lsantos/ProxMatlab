%function val = present_results(data_file,z01,phase_bounds,defoc_series,save_plots)

addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/main/tools/pie');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/main/tools/');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/psi_routines');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/psi_routines/matlab_pierre/all/');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/export_fig');

clear; close all; startup;

%% Loading data

rootpath = '/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/data/20100426T225905_data_091002_142319/20100430T050403_reconstruction_091002_142319__960x960_tau_2/';
data_file = 'reconstruction_091002_142319__960x960.mat';
cd(rootpath);

fig_ind = 0;
z01 = 1.4e-3;
phase_bounds = [-pi pi];
defoc_series = 1;
save_analysis = 1;

disp('Loading datafile.');
load(data_file);
disp('Finished loading data.');
disp('Preparing plots.');


%% Generate coordinate systems etc.

%detector plane (E2) ------------------------------------------------------

% coordinate system in Fourier space
[Q2_x,Q2_y] = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.dq2x,((1:P.Ny)-floor(P.Ny/2)-1)*P.dq2y);

%specimen plane (E1)/ probe plane (as long as direct propagation is used) -

% coordinate system (points at centers of the pixels)
[X_1,Y_1] = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.d1x,((1:P.Ny)-floor(P.Ny/2)-1)*P.d1y);

%extended coordinate system for object plane
[X_obj,Y_obj] = meshgrid((1:P.object_size(2))*P.d1x,(1:P.object_size(1))*P.d1y);

% maximum average intensity in a single experimental frame
I_max_avg = max(max(sum(sum(P.I_exp,1),2),[],3),[],4) / (P.Nx*P.Ny);

% generate cell array of normalized intensities 
% normalizeation such, that for I = I_max (as matrix) the average pixel
% intensity is 1 and total is Nx*Ny, for all other I the values are lower
Amp_exp_norm = cell(P.Nx*P.Ny,1);

k=1;
for n_slow = 1:P.ny
    for n_fast = 1:P.nx
        dat = P.I_exp(:,:,n_slow,n_fast);
        Amp_exp_norm{k} = sqrt(dat/I_max_avg);
        %no Amp_exp_norm can be larger than 1
        k = k+1;
    end
end

%% object reconstruction

% threshold for combined plotting
th = 1;

%Object (combined)
objecthsv = zeros(P.object_size(1),P.object_size(2),3);
objecthsv(:,:,1) = (angle(P.object.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
objecthsv(:,:,2) = ones(size(P.object,1),size(P.object,2));
objecthsv(:,:,3) = (abs(P.object)- (1+sign(abs(P.object)-th))/2 .* (abs(P.object)-th))/th-1e-5;
% This is equivalent to function 
% f(x) = abs(x)     for abs(x) <= th and
% f(x) = th          for abs(x) > th
fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','none','units','normalized','position',[.1 .1 .8 .8]);
imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,hsv2rgb(objecthsv)); axis xy; axis equal; 
axis tight; colormap hsv; zoom(1.5); xlabel('\mum'); ylabel('\mum');
%ylim([9.5 17]); xlim([9.5 18.5])

%%
%Object (amplitude)
fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','none','units','normalized','position',[.1 .1 .8 .8]);
imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,abs(P.object)); axis xy equal tight; 
colormap bone; caxis([0 1]); zoom(1.5); xlabel('\mum'); ylabel('\mum'); 
%ylim([9.5 17]); xlim([9.5 18.5])

%Object (phase)
fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','none','units','normalized','position',[.1 .1 .8 .8]);
imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,angle((P.object))); axis xy equal tight; 
colormap hsv; zoom(1.5); xlabel('\mum'); ylabel('\mum');
%ylim([9.5 17]); xlim([9.5 18.5])

%% Probe

probe_mask = circ(X_1,Y_1,0,0,14.96*1e-6);
Probe = P.Probe.*probe_mask;

%Probe (combined)
probehsv = zeros(P.Ny,P.Nx,3);
probehsv(:,:,1) = (angle(Probe.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
probehsv(:,:,2) = ones(size(Probe,1),size(P.Probe,2));
probehsv(:,:,3) = abs(Probe)/max(max(abs(Probe)));
fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','white','units','normalized','position',[.1 .1 .8 .8]);
imagesc(squeeze(X_1(1,:))*1e6,squeeze(Y_1(:,1))*1e6,hsv2rgb(probehsv)); axis xy; axis equal; axis tight;
xlabel('\mum'); ylabel('\mum'); drawnow;
%%
% Defocus series
disp('Calculating defocus series.');

Z = (-1.2e-3:0.005e-3:-0.9e-3);

Probe_defoc = zeros(P.Ny,P.Nx,length(Z));
kappa = sqrt(P.k^2-ifftshift(Q2_x.^2+Q2_y.^2));

for ind = 1:length(Z)
    Probe_defoc(:,:,ind) = ifft2(fft2(Probe).*exp(1i*kappa*Z(ind)));
end

%%
%for ind = 1:10
fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','black','units','normalized','position',[.1 .1 .5 .5]);

for ind = 1:length(Z)
   imagesc(abs(Probe_defoc(:,:,ind))); axis xy; axis equal; axis tight; %set(gca,'visible','off');
   zoom(2); title(['z_{01} = ' num2str(Z(ind)*1e3,'%3.2f') ' mm'],'color','w'); colormap copper; drawnow; pause(0.02);
end
%end
%%
% probe at aperture position

probe_mask_2 = circ(X_1,Y_1,0,0,2*P.R);
z = -1.06e-3;
Probe_ap = ifft2(fft2(Probe).*exp(1i*kappa*z));

probehsv = zeros(P.Ny,P.Nx,3);
probehsv(:,:,1) = (angle(Probe_ap*exp(1i*0.5))+pi)./(2*pi);
probehsv(:,:,2) = ones(P.Ny,P.Nx);
probehsv(:,:,3) = abs(Probe_ap)/max(max(abs(Probe_ap)));

fig_ind = fig_ind+1; figure(fig_ind);
%set(gcf,'color','none','units','normalized','position',[.1 .1 .5 .5]);
imagesc(1e6*squeeze(X_1(1,:)),1e6*squeeze(Y_1(:,1)),hsv2rgb(probehsv)); axis xy equal tight; zoom(4); 

line(linspace(0.5,1.5,10),linspace(-1.36,-1.36,10),'LineWidth',6,'Color','w');

%% Fourier space reconstruction

%linear position
n = 1;

Indy = P.positions(n,1) + (1:P.Ny);
Indx = P.positions(n,2) + (1:P.Nx); 
psi = P.object(Indy,Indx).*P.Probe;

I_norm = Amp_exp_norm{n}.^2; 
MIN = log10(min(min(I_norm(I_norm > 0)))); 
MAX = log10(max(max(Amp_exp_norm{n}.^2)));

fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','black','units','normalized','position',[.1 .3 0.8 0.4]);

subplot(1,2,1);
imagesc(squeeze(Q2_x(1,:)*1e-6/(2*pi)),squeeze(Q2_y(:,1)*1e-6/(2*pi)),log10(fftshift(abs(FFT2(psi))).^2)); axis xy; axis equal; axis tight;
colormap jet; caxis([MIN MAX]); 
colorbar; xlabel('f_x [\mum^{-1}]'); ylabel('f_y [\mum^{-1}]');
title(['Reconstructed diffraction pattern at first position']);

subplot(1,2,2);
imagesc(squeeze(Q2_x(1,:)*1e-6/(2*pi)),squeeze(Q2_y(:,1)*1e-6/(2*pi)),log10(Amp_exp_norm{n}.^2)); axis xy; axis equal; axis tight;
colormap jet; caxis([MIN MAX]); 
colorbar; xlabel('f_x [\mum^{-1}]'); ylabel('f_y [\mum^{-1}]');
title(['Measured/simulated diffraction pattern at first position']);
    
% errors
fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','black','units','normalized','position',[.1 .3 0.8 0.4]);
subplot(1,2,1);
semilogy(P.err);
title(['DM-distance, end value is ', num2str(P.err(end))]);
subplot(1,2,2);
plot(P.chi);
title(['chi, end value is ' num2str(P.chi(end))]);
    
%%

if save_analysis
   mkdir ANALYSIS;
   cd ANALYSIS;
   for ind = 1:fig_ind
      figure(ind);
      eval(['saveas(gcf,''' num2str(ind) ''',''fig'')']);
      set(gcf,'Color','none');
      eval(['export_fig ' num2str(ind) '.pdf -pdf']);
   end
   close all;
end
