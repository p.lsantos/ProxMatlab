function a = binning(A,b)

%A: matrix
%b: binning factor (b >= 1)

S = size(A);

if b < 1
    error('Only downbinning is possible here.');
end

if length(S) < 2 || length(S) > 2
    error('Matrix must be of dimension 2.');
end

for kk=1:length(S)
    if abs(mod(S(kk),b)) > 0
        error('At least one dimension not factorizable by the binning factor.');
    end
end

% allocate memory for new matrix
a = zeros(S(1)/b,S(2)/b);


for ii=1:S(1)/b
    for jj=1:S(2)/b        
        
        % pointer to index of upper left element within area of old matrix
        % that will be summed up to a single element of the new matrix
        II = (ii-1)*b + 1;
        JJ = (jj-1)*b + 1;
        %anotate elements of new matrix
        a(ii,jj) = sum(sum(A(II:II+b-1,JJ:JJ+b-1)));      
        
    end
end