
function positions = ptycho_prep_pos(p)




positions = [];

switch p.scan_type
    
    case 'round_roi'
        dx_spec = p.dx_spec;
        %dy_spec = p.dy_spec;
        rmax = sqrt((p.lx/2)^2 + (p.ly/2)^2);
        nr = 1 + floor(rmax/p.dr);
        for ir=1:nr+1
            rr = ir*p.dr;
            dth = 2*pi / (p.nth*ir);
            for ith=0:p.nth*ir-1
                th = ith*dth;
                xy = rr * [sin(th), cos(th)];
                if( abs(xy(1)) >= p.ly/2 || (abs(xy(2)) > p.lx/2) )
                    continue
                end
                positions(end+1,:) = xy ./dx_spec; %#ok<AGROW>
%                 positions(end+1,2) = xy(1) ./dx_spec; %#ok<AGROW>
%                 positions(end+1,1) = xy(2) ./dy_spec; %#ok<AGROW>
            end
        end
    case 'custom'
        positions = p.positions;
        positions(:,2) = positions(:,2)/p.d1x; % horizontal axis
        positions(:,1) = positions(:,1)/p.d1y; % vertical axis
    otherwise
        
        ind = 1;
        for n_slow = 0:(p.ny-1)
            for n_fast = 0:(p.nx-1)
                positions(ind,2) = p.pie_step(2) * n_fast/p.d1x; % horizontal axis
                positions(ind,1) = p.pie_step(1) * n_slow/p.d1y; % vertical axis
                ind = ind+1;
            end
        end
        
end

positions(:,1) = positions(:,1) - min(positions(:,1));
positions(:,2) = positions(:,2) - min(positions(:,2));
positions = round(positions);

end
