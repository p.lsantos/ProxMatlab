function I_hsv = colorwheel(N)

[X,Y] = meshgrid(linspace(-1,1,N),linspace(-1,1,N));
[THETA,RHO] = cart2pol(X,Y);

I_hsv = ones(N,N,3);

I_hsv(:,:,1) = (THETA + pi)/(2*pi);
I_hsv(:,:,3) = 1 - max(0,1-RHO);

figure;
imagesc(linspace(-1,1,N),linspace(-1,1,N),hsv2rgb(I_hsv)); axis equal; axis tight;

hold on;
% auxiliary grid
h = ellipse(1,1,1);
set(h,'Color','w');
set(h,'Linestyle','--');
set(h,'Linewidth',2);

%h = ellipse(0.75,0.75,0.75);
%set(h,'Color','w');
%set(h,'Linestyle','--');
%set(h,'Linewidth',2);

h = ellipse(0.5,0.5,0.5);
set(h,'Color','w');
set(h,'Linestyle','--');
set(h,'Linewidth',2);

%h = ellipse(0.25,0.25,0.25);
%set(h,'Color','w');
%set(h,'Linestyle','--');
%set(h,'Linewidth',2);

h = line(linspace(0,0,100),linspace(-1,1,100));
set(h,'Color','w');
set(h,'Linestyle','--');
set(h,'Linewidth',2);

h = line(linspace(-1,1,100),linspace(0,0,100));
set(h,'Color','w');
set(h,'Linestyle','--');
set(h,'Linewidth',2);

axis xy;
set(gca,'xtick',[])
set(gca,'ytick',[])


