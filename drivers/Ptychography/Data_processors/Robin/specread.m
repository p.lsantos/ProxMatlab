function val = specread(filename,data_var)

%SPECREAD reads a spec-file that contains A SINGLE SCAN (!) and names the
%variables according to the header of the spec-file
%The input-file name has to be of the form '#.spec' where '#' stands for
%the corresponding scan number.
%The names of the variables are HGL_#, for example when HGL is a motor
%name with certain values represented by a column.

%The Output of the function is the measured data that was requested in the
%cell array of strings data_var

inputfile = fopen(filename);
prefix = filename(1:length(filename)-5);
suffix = '.spec';
scannumber = str2num(prefix);

while 1
    current_line = fgets(inputfile);
    if ~isspace(current_line(1)) 
        if isequal(char(current_line(2)),'L')
            %---- Cutting of header header #L_...
            [token,reminder] = strtok(current_line);
            current_line = reminder;
            %---- first variable of header
            [token,reminder] = strtok(current_line);
            current_line = reminder;
            header = cell(1,1);
            header = {token};
            while length(reminder)>0
                [token,reminder] = strtok(current_line);
                current_line = reminder;
                header_var = deblank(token);
                if ~isequal(header_var,[])
                    header = [header, {header_var}];
                end
            end
            break
        end
    end
end

N = length(header);

%--- this little section is taken from the ILL matlab routine "specdata.m"
data=[];
r=fgets(inputfile);
while (max(size(r))>2 & r(1)~='#')
   a=sscanf(r,'%f');
   data=[data ; a'];
   r=fgets(inputfile);
end
fclose(inputfile);
%---- end

[N,M] = size(data);

%for k=1:N
%    assignin('caller',header{k},data(:,k));
%end

data_out = zeros(N,length(data_var));

k = 0;
for m=1:length(data_var)
    for n=1:length(header)
        if isequal(header(n),data_var(m))
            data_out(:,m) = data(:,n);
            k = k+1;
        end
    end
end
if k<length(data_var)
    disp('Not all variables are contained in file or one or more are missspelled.')
end

val = data_out;

%disp('Measured variables of Scan');
%disp(num2str(scannumber));
%disp('are:');
%disp(header);