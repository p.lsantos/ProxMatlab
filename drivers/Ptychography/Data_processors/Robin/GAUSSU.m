%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Berechnet die el. Feldkomponente eines Gauss'schen Strahles
%Als Eingabe Position auf der z-Achse (Z) xy-Ebene (RHO)
%benutzte Wellenlaenge (lambda) und Waist Radius (wnull)

%           by R.Wilke 05.05.2009

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Einige berechnete Parameter:
% W: beam radius
% R: radius of curvature
% znull: Raygleigh range
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [U,GAUSS] = GAUSSU(RHO,Z,lambda,wnull)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               Berechne zuerst die benoetigten Parameter
%

k = 2*pi/lambda;
Anull = 1 ;
znull = wnull^2*pi/lambda;
W = wnull*sqrt(1+Z.^2/znull^2);

TMP=(Z==0);             %Problem falls Z irgendwo Null
R=Z;                    %d.h. nicht durch Null teilen
R(TMP)=Inf;
R(~TMP) = Z(~TMP).*(1+znull^2./Z(~TMP).^2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               Berechne komplette Feldamplitude U (Rho, Z)
%

U = Anull*(1./R+sqrt(-1)*2./(k*W.^2)).*exp(sqrt(-1)*k*Z).* ...
    exp((sqrt(-1)*k./(2*R)-1./W.^2).*RHO.^2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               alternativ Darstellung zu obigen U
%

% zeta = atan(Z/znull)

%U = Anull*sqrt(-1)*2./(k*wnull*W).*exp(-sqrt(-1)*zeta).* ...
%    exp(sqrt(-1)*k*Z).*exp((sqrt(-1)*k./(2*R)-1./W.^2).*RHO.^2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               speichere alle Parameter in structure 'Gauss'
%               fuer eventuelle uebergabe
%



GAUSS.k = k;
GAUSS.lambda = lambda;
GAUSS.Anull = Anull;
GAUSS.Wnull = wnull;
GAUSS.Znull = znull;
GAUSS.W     = W;
GAUSS.R     = R;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        von RHO unabhaengiger Anteil des Feldes U = Zpart.*RHOpart:
%        Zpart
%


GAUSS.Zpart = Anull*(1./R+sqrt(-1)*2./(k*W.^2)).*exp(sqrt(-1)*k*Z);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%         von RHO abhaengiger Anteil des Feldes U = Zpart.*RHOpart:
%         RHOpart
%

GAUSS.RHOpart = exp((sqrt(-1)*k./(2*R)-1./W.^2).*RHO.^2);
end

