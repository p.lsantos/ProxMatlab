%function val =
%present_results(data_file,z01,phase_bounds,defoc_series,save_plots)

addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/main/tools/pie');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/main/tools/');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/psi_routines');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/psi_routines/matlab_pierre/all/');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/export_fig');

clear; close all; startup;

%% Loading data

rootpath = '/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/data_new/20100531T154930_data_091002_142319/20100601T045747_reconstruction_091002_142319__960x960_tau_1.5/';
data_file = 'reconstruction_091002_142319__960x960.mat';
cd(rootpath);

fig_ind = 0;
z01 = 1.4e-3;
phase_bounds = [-pi pi];
defoc_series = 1;
save_analysis = 1;

disp('Loading datafile.');
load(data_file);
disp('Finished loading data.');
disp('Preparing plots.');


%% Generate coordinate systems etc.

%detector plane (E2) ------------------------------------------------------

% coordinate system in Fourier space
[Q2_x,Q2_y] = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.dq2x,((1:P.Ny)-floor(P.Ny/2)-1)*P.dq2y);

%specimen plane (E1)/ probe plane (as long as direct propagation is used) -

% coordinate system (points at centers of the pixels)
[X_1,Y_1] = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.d1x,((1:P.Ny)-floor(P.Ny/2)-1)*P.d1y);

%extended coordinate system for object plane
[X_obj,Y_obj] = meshgrid((1:P.object_size(2))*P.d1x,(1:P.object_size(1))*P.d1y);

% maximum average intensity in a single experimental frame
I_max_avg = max(max(sum(sum(P.I_exp,1),2),[],3),[],4) / (P.Nx*P.Ny);

kappa = sqrt(P.k^2-ifftshift(Q2_x.^2+Q2_y.^2));

% generate cell array of normalized intensities 
% normalizeation such, that for I = I_max (as matrix) the average pixel
% intensity is 1 and total is Nx*Ny, for all other I the values are lower
Amp_exp_norm = cell(P.nx*P.ny,1);

ind=1;
for n_slow = 1:P.ny
    for n_fast = 1:P.nx
        dat = P.I_exp(:,:,n_slow,n_fast);
        Amp_exp_norm{ind} = sqrt(dat/I_max_avg);
        %no Amp_exp_norm can be larger than 1
        ind = ind+1;
    end
end

%% object reconstruction

object = ((P.object));

%fig_ind = fig_ind+1; figure(fig_ind);
%plot(squeeze(X_1(1,:))*1e6,abs(P.Probe(floor(P.Ny/2)+1,:))/max(abs(P.Probe(floor(P.Ny/2)+1,:))))
HWHM_x = 1.4;
%fig_ind = fig_ind+1; figure(fig_ind);
%plot(squeeze(Y_1(:,1))'*1e6,abs(P.Probe(:,floor(P.Nx/2)+1))/max(abs(P.Probe(:,floor(P.Nx/2)+1))))
HWHM_y = 1.47;

x1 = (floor(P.Nx/2))*P.d1x*1e6 - HWHM_x;
x2 = (floor(P.Nx/2))*P.d1x*1e6 + (P.nx-1)*P.pie_step_x*1e6 + HWHM_x;
y1 = (floor(P.Ny/2))*P.d1y*1e6 - HWHM_y;
y2 = (floor(P.Ny/2))*P.d1y*1e6 + (P.ny-1)*P.pie_step_y*1e6 + HWHM_y;

% threshold for combined plotting
th = 1;

fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','none','units','normalized','position',[.1 .1 .8 .8]);
imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,hsv2rgb(im2hsv(object,th))); axis xy equal tight; colormap hsv;
%xlim([x1 x2]);ylim([y1 y2]);


%%
% disp('Please select a first region of interest for calculation of mean.')
% [selection,xi1,yi1] = roipoly;    
% SELECTION = object(selection > 0);
% SELECTION_abs = abs(SELECTION);
% SELECTION_phase = angle(SELECTION);
% hold on;
% plot(xi1,yi1,'w','LineWidth',2);
% 
% fprintf('Mean amplitude of selection is %3.2f.\n',mean(mean(SELECTION_abs)));
% fprintf('Mean phase of selection is %3.2f.\n',mean(mean(SELECTION_phase)));
% 
% disp('Please select a first region of interest for calculation of mean.')
% [selection,xi2,yi2] = roipoly;    
% SELECTION = object(selection > 0);
% SELECTION_abs = abs(SELECTION);
% SELECTION_phase = angle(SELECTION);
% hold on;
% plot(xi2,yi2,'w','LineWidth',2);
% 
% fprintf('Mean amplitude of selection is %3.2f.\n',mean(mean(SELECTION_abs)));
% fprintf('Mean phase of selection is %3.2f.\n',mean(mean(SELECTION_phase)));

%% Object (amplitude)

fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','none','units','normalized','position',[.1 .1 .8 .8]);
imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,abs(object)); axis xy equal tight; 
set(gca,'xtick',[]); set(gca,'ytick',[]);
colormap bone; colorbar;
%xlim([x1 x2]);ylim([y1 y2]); caxis([0 th])

x1_phase = 13-7;
y1_phase = 10-5.8;
x2_phase = 18-7;
y2_phase = 15-5.8;

rectangle('Position',[x1_phase,y1_phase,abs(x2_phase-x1_phase),abs(y2_phase-y1_phase)],'Linewidth',3,'EdgeColor','w');

%% resolution

% ind_profile = 246;
% 
% abs_object = fliplr(abs(P.object));
% imagesc(abs_object); axis equal tight; 
% 
% y_profile = abs_object(ind_profile,220:280)';
% x_profile = (1:length(y_profile))'*P.d1x*1e6-1.36;
% 
% plot(x_profile,y_profile);
% 
% str_erf = 'phi_0*erf((x-x0)/(sqrt(2)*sigma)) + xi_0';
% start = [0.1,0,0,0.05];
% 
% outliers = excludedata(x_profile,y_profile,'domain',[-0.25 0.1]);
% mymodel = fittype(str_erf,'independent','x','coefficients',{'phi_0','x0','xi_0','sigma'});
% opts = fitoptions('method','NonlinearLeastSquares','StartPoint',start,'Exclude',outliers);
% 
% [fresult,gof,out] = fit(x_profile,y_profile,mymodel,opts);
% 
% fprintf('Resolution (FWHM) is %f nm.\n ',1e3*2*sqrt(2*log(2))*fresult.sigma);
% 
% hold on;
% plot(fresult)

%% Object (phase)
fig_ind = fig_ind+1; figure(fig_ind); 
set(gcf,'color','none','units','normalized','position',[.1 .1 .4 .4]);
imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,angle(object)); 
axis xy equal tight; colormap hsv; colorbar;set(gca,'xtick',[]); set(gca,'ytick',[]);
%xlim([x1_phase x2_phase]);ylim([y1_phase y2_phase]);

%% Probe

%Probe (combined)
probehsv = zeros(P.Ny,P.Nx,3);
probehsv(:,:,1) = (angle(P.Probe.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
probehsv(:,:,2) = ones(size(P.Probe,1),size(P.Probe,2));
probehsv(:,:,3) = abs(P.Probe)/max(max(abs(P.Probe)));
%fig_ind = fig_ind+1; figure(fig_ind);
%set(gcf,'color','none','units','normalized','position',[.1 .1 .3 .5]);
%imagesc(squeeze(X_1(1,:))*1e6,squeeze(Y_1(:,1))*1e6,hsv2rgb(probehsv)); axis xy equal tight;
%xlabel('\mum'); ylabel('\mum'); drawnow;

%%
%Defocus series
disp('Calculating defocus series.');

Z = (-1.2e-3:0.005e-3:-0.8e-3);

Probe_defoc = zeros(P.Ny,P.Nx,length(Z));

for ind = 1:length(Z)
    Probe_defoc(:,:,ind) = ifft2(fft2(P.Probe).*exp(1i*kappa*Z(ind)));
end

%%
%for ind = 1:10
fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','black','units','normalized','position',[.1 .1 .5 .5]);

for ind = 1:length(Z)
   imagesc(abs(Probe_defoc(:,:,ind))); axis xy equal tight; %set(gca,'visible','off');
   zoom(2); title(['z_{01} = ' num2str(Z(ind)*1e3,'%3.2f') ' mm'],'color','w'); colormap copper; drawnow; pause(0.02);
end
%end
%%
% probe at aperture position

z = -1.08e-3;
%Probe_ap = ifft2(fft2(P.Probe).*exp(1i*kappa*z));
Probe_ap = prop_nf(P.Probe,P.lambda,z,P.d1x,P.d1y);

probehsv = zeros(P.Ny,P.Nx,3);
probehsv(:,:,1) = (angle(Probe_ap*exp(-1i*1))+pi)./(2*pi);
probehsv(:,:,2) = ones(P.Ny,P.Nx);
probehsv(:,:,3) = abs(Probe_ap)/max(max(abs(Probe_ap)));

fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','none','units','normalized','position',[.1 .1 .5 .5]);
imagesc(1e6*squeeze(X_1(1,:)),1e6*squeeze(Y_1(:,1)),hsv2rgb(probehsv)); axis xy equal tight; 
set(gca,'xtick',[]); set(gca,'ytick',[]); xlim([-1.5 1.5]); ylim([-1.5 1.5])
line(linspace(0.3,1.3,10),linspace(-1.3,-1.3,10),'LineWidth',10,'Color','w');

%% Fourier space reconstruction

%linear position
n = 1;

Indy = P.positions(n,1) + (1:P.Ny);
Indx = P.positions(n,2) + (1:P.Nx); 
psi = P.object(Indy,Indx).*P.Probe;

I_norm = Amp_exp_norm{n}.^2; 
MIN = log10(min(min(I_norm(I_norm > 0)))); 
MAX = log10(max(max(Amp_exp_norm{n}.^2)));

fig_ind = fig_ind+1; figure(fig_ind);
set(gcf,'color','black','units','normalized','position',[.1 .3 0.8 0.4]);

subplot(1,2,1);
imagesc(squeeze(Q2_x(1,:)*1e-6/(2*pi)),squeeze(Q2_y(:,1)*1e-6/(2*pi)),log10(fftshift(abs(FFT2(psi))).^2)); axis xy; axis equal; axis tight;
colormap jet; caxis([MIN MAX]); 
colorbar; xlabel('f_x [\mum^{-1}]'); ylabel('f_y [\mum^{-1}]');
title(['Reconstructed diffraction pattern at first position']);

subplot(1,2,2);
imagesc(squeeze(Q2_x(1,:)*1e-6/(2*pi)),squeeze(Q2_y(:,1)*1e-6/(2*pi)),log10(Amp_exp_norm{n}.^2)); axis xy; axis equal; axis tight;
colormap jet; caxis([MIN MAX]); 
colorbar; xlabel('f_x [\mum^{-1}]'); ylabel('f_y [\mum^{-1}]');
title(['Measured/simulated diffraction pattern at first position']);
    
% errors
fig_ind = fig_ind+1; figure(fig_ind);
%set(gcf,'color','black','units','normalized','position',[.1 .3 0.8 0.4]);
subplot(1,2,1);
semilogy(P.err);
title(['DM-distance, end value is ', num2str(P.err(end))]);
subplot(1,2,2);
plot(P.chi);
title(['chi, end value is ' num2str(P.chi(end))]);

%%
I_max_avg = max(max(sum(sum(P.I_exp,1),2),[],3),[],4) / (P.Nx*P.Ny);
fig_ind = fig_ind+1; figure(fig_ind);
%set(gcf,'color','none','units','normalized','position',[.1 .1 0.4 0.4]);
imagesc(squeeze(Q2_x(1,:)*1e-6/(2*pi)),squeeze(Q2_y(:,1)*1e-6/(2*pi)),log10(Amp_exp_norm{n}.^2*I_max_avg)); axis xy; axis equal; axis tight; colormap jet;
set(gca,'xtick',[]); set(gca,'ytick',[]);colorbar; 
    
%%

if save_analysis
   mkdir ANALYSIS_paper;
   cd ANALYSIS_paper;
   current_path = pwd;
   for ind = 1:fig_ind
      figure(ind);
      eval(['saveas(gcf,''' num2str(ind) ''',''fig'')']);
      set(gcf,'Color','none');
      eval(['export_fig ' num2str(ind) '.pdf -pdf']);
   end
   close all;
   cd('/home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/main/tools/');
   copyfile('analyze_results_navicula.m',current_path);
   cd(current_path);
end
