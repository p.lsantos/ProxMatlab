%function val = recon_movie(data_file)

addpath /home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/psi_routines/matlab_pierre/all/
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/main/tools');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/main/tools/pie');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/psi_routines');


% load experimental data
%close all;
%startup;


%disp('Loading datafile.');
%load(data_file);
%disp('Finished loading data.');
disp('Preparing plots.');

%% Generate coordinate systems

%detector plane (E2) ------------------------------------------------------

% coordinate system (points at centers of the pixels)
[X_2,Y_2] = meshgrid(P.d2x/2 + [0:1:P.Nx-1]*P.d2x - P.Nx/2*P.d2x,P.d2y/2 + [0:1:P.Ny-1]*P.d2y - P.Ny/2*P.d2y);

% coordinate system in Fourier space
[Q2_x,Q2_y] = meshgrid(-P.q2x_max + P.dq2x/2 + [0:1:P.Nx-1]*P.dq2x,-P.q2y_max + P.dq2y/2 + [0:1:P.Ny-1]*P.dq2y);

%specimen plane (E1)/ probe plane (as long as direct propagation is used) -

% coordinate system (points at centers of the pixels)
[X_1,Y_1] = meshgrid(P.d1x/2 + [0:1:P.Nx-1]*P.d1x - P.Nx/2*P.d1x,P.d1y/2 + [0:1:P.Ny-1]*P.d1y - P.Ny/2*P.d1y);

% coordinate system in Fourier space
[Q1_x,Q1_y] = meshgrid(-P.q1x_max + P.dq1x/2 + [0:1:P.Nx-1]*P.dq1x,-P.q1y_max + P.dq1y/2 + [0:1:P.Ny-1]*P.dq1y);

%extended coordinate system for object plane
[X_obj,Y_obj] = meshgrid(P.d1x/2 + [0:1:P.object_size(2)-1]*P.d1x - P.Nx/2*P.d1x,P.d1y/2 + [0:1:P.object_size(1)-1]*P.d1y - P.Ny/2*P.d1y);

% Fresnel number
F = (2*P.R)^2/(P.lambda*P.z01);
P.F = F;

%% plotting

%
%Create a new figure, and position it
   fig1 = figure;
   winsize = get(fig1,'Position');
   winsize(1:2) = [0 0];
%Create movie file with required parameters
   fps= 15;
   outfile = 'test.avi';
   mov = avifile(outfile,'fps',fps,'quality',100);
%Ensure each frame is of the same size
   set(fig1,'NextPlot','replacechildren');


for it = 1:P.N_it
    imagesc(P.images(1:end,1:end,it)); axis xy equal tight; colormap copper;
    text(135,205,['N = ',num2str(it)],'FontSize',20);
    caxis([-0.26 0.3]); drawnow; %pause(0.05);
    
    %put this plot in a movieframe
    %In case plot title and axes area are needed
        %F = getframe(fig1,winsize);
    %For clean plot without title and axes
    F = getframe;
    mov = addframe(mov,F);
end

%save movie
mov = close(mov);
