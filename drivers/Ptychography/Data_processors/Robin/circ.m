function val = circ(X,Y,x0,y0,R);

% R   radius of circular aperture
val = heaviside(-((X-x0).^2+(Y-y0).^2-R^2));