function psi_z = dist_obj_prop(psi_0,X_0,Y_0,X_z,Y_z,k,lambda,z)

psi_0_dist = exp(1i*pi/(lambda*z)*(X_0.^2 + Y_0.^2)).*psi_0;
R = sqrt(X_z.^2 + Y_z.^2 + z^2);
%psi_z = -sqrt(-1)*exp(sqrt(-1)*k*R)./(lambda*R) .* exp(i*pi/(lambda*z)*(X_z.^2 + Y_z.^2)) .* ifftshift(FFT2(fftshift(psi_0_dist)));
psi_z = exp(1i*pi/(lambda*z)*(X_z.^2 + Y_z.^2)) .* fftshift(FFT2(ifftshift(psi_0_dist)));