function [val1,val2] = present_results_tau_analysis(data_file)

addpath /home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/psi_routines/matlab_pierre/all/
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/main/tools');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/main/tools/pie');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/psi_routines');


% load experimental data
close all;
startup;

disp('Loading datafile.');
load(data_file);
disp('Finished loading data.');


%% Generate coordinate systems

%detector plane (E2) ------------------------------------------------------

% coordinate system (points at centers of the pixels)
[X_2,Y_2] = meshgrid(P.d2x/2 + [0:1:P.Nx-1]*P.d2x - P.Nx/2*P.d2x,P.d2y/2 + [0:1:P.Ny-1]*P.d2y - P.Ny/2*P.d2y);

% coordinate system in Fourier space
[Q2_x,Q2_y] = meshgrid(-P.q2x_max + P.dq2x/2 + [0:1:P.Nx-1]*P.dq2x,-P.q2y_max + P.dq2y/2 + [0:1:P.Ny-1]*P.dq2y);

%specimen plane (E1)/ probe plane (as long as direct propagation is used) -

% coordinate system (points at centers of the pixels)
[X_1,Y_1] = meshgrid(P.d1x/2 + [0:1:P.Nx-1]*P.d1x - P.Nx/2*P.d1x,P.d1y/2 + [0:1:P.Ny-1]*P.d1y - P.Ny/2*P.d1y);

% coordinate system in Fourier space
[Q1_x,Q1_y] = meshgrid(-P.q1x_max + P.dq1x/2 + [0:1:P.Nx-1]*P.dq1x,-P.q1y_max + P.dq1y/2 + [0:1:P.Ny-1]*P.dq1y);

%extended coordinate system for object plane
[X_obj,Y_obj] = meshgrid(P.d1x/2 + [0:1:P.object_size(2)-1]*P.d1x - P.Nx/2*P.d1x, P.d1y/2 + [0:1:P.object_size(1)-1]*P.d1y - P.Ny/2*P.d1y);

% Fresnel number
F = (2*P.R)^2/(P.lambda*P.z01);
P.F = F;


%% Fiddling around with the experimental data a bit (normalization etc.)

% maximum average intensity in a single experimental frame
I_max_avg = max(max(sum(sum(P.I_exp,1),2),[],3),[],4) / (P.Nx*P.Ny);

% average of squared(??) autocorrelation function of each dataset
auto = zeros(P.Ny,P.Nx);

% center coordinates (assuming data is already centered)
cx=floor(size(P.I_exp,1)/2);
cy=floor(size(P.I_exp,2)/2);

k=1;
sigma_recon = 0;
sigma_exp = 0;
for n_slow = 1:P.ny
    for n_fast = 1:P.nx

        Indy = P.positions(k,1) + (1:P.Ny);
        Indx = P.positions(k,2) + (1:P.Nx); 

        
        P.I_valid(:,:,n_slow,n_fast) = pshift(double(P.I_valid(:,:,n_slow,n_fast)),[cx cy]);
        % is this always done in the used simulations/reconstructions????
        dat = P.I_valid(:,:,n_slow,n_fast).*pshift(P.I_exp(:,:,n_slow,n_fast),[cx cy]);
        Amp_exp_norm = fftshift(sqrt(dat/I_max_avg));
        
        ind_nonzero = find(Amp_exp_norm > 0);
        
        psi = P.Probe.*P.object(Indy,Indx);
        PSI = abs(fftshift(FFT2(psi))).^2;
        
        sigma_recon = sigma_recon + mean2(abs(Amp_exp_norm(ind_nonzero).^2 - PSI(ind_nonzero)));
        sigma_exp = sigma_exp + mean2(Amp_exp_norm(ind_nonzero));
      
        k = k+1;
    end
end

SIGMA_recon = sigma_recon/(P.ny*P.nx)
SIGMA_exp = sigma_exp/(P.ny*P.nx)

val1 = SIGMA_recon;
val2 = P.chi(end);