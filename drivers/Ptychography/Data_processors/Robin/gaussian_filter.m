function Im = gaussian_filter(im, sigma)

S = size(im);

[u,v] = meshgrid(ifftshift(((1:S(2))-floor(S(2)/2)-1)),ifftshift(((1:S(1))-floor(S(1)/2)-1)));
H = exp(-(u.^2 + v.^2)/(2*sigma^2));

Im = real(ifft2(fft2(im).*H));


