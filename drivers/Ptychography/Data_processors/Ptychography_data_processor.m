%              Ptychography_data_processor.m
%               written on 15th January 2014
%                       Matthew Tam
%                      CARMA Centre
%                  University of Newcastle
%           last modified on 29th January 2014
%
%
% DESCRIPTION: This is the data processor for ptychography data. It can be
%              used with a ptychography data file (if one is specified).
%              Alternatively, the the data is simulated using the code
%              based on "sim_ptchography_data" written by r.w. on
%              19.09.2011.
%
% INPUT:       P, a data structure
% OUTPUT:      P, a data structure
%
% USAGE: input = Ptychography_data_processor(input) 
%
% Nonstandard function calls: ExtractROI
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function P = Ptychography_data_processor( P )

%% Does the user have the data files?
addpath('../InputData/Ptychography')


%% Has a data file been specified?
if ~strcmp( P.datafile, '' ) % If so, load the data file.
    [fid, ~]=fopen([P.datafile,'.mat']);
    if fid<0
        disp('*************************************************************************')
        disp('* INPUT DATA MISSING.  Please download the ptychography input data from *')
        disp('* http://vaopt.math.uni-goettingen.de/data/Ptychography.tar.gz          *')
        disp('* Save and unpack the Ptychography.tar.gz datafile in the               *')
        disp('*    ProxMatlab/InputData/Ptychography subdirectory                     *')
        disp('*************************************************************************')
    end
    fclose(fid);
    
    %% MKT: We need to make sure "P" is not overwritten.
    % warning('Check that your input data conforms to the specifications...')
    Q = load(P.datafile);
    Q = Q.P;
    fnames = fieldnames(Q);
    for fn=1:length(fnames)
        if ~strcmp(char(fnames(fn)),'data_dir') %~sum(ismember({'data_dir'},fnames(fn)))
            P.(char(fnames(fn))) = Q.(char(fnames(fn)));
        end
    end
    if strcmp(P.datafile, 'data_S07025_data_800x800')
        QQ = load([P.data_dir 'reconstruction_data_S07025_data_800x800.mat'],'P');
        QQ = QQ.P;
        P.sample_plane = ones(P.object_size);
        P.sample_plane(1:end-3,1:end-2) = QQ.object;
        P.probe = QQ.Probe; %ones(size(P.I_exp(:,:,1)));
        %clear P.probe
        %P.probe_guess_type = ''
        P.trans_min_true = 0.7;
        P.trans_max_true = 1.0;
        %P.fmask = ones([800 800 323]);
        clear Q QQ
    elseif strcmp(P.datafile, 'data_NTT_01_26210_192x192')
        QQ = load([P.data_dir 'reconstruction_data_NTT_01_26210_192x192.mat'],'P');
        QQ = QQ.P;
        positions = load ([P.data_dir 'positions_NTT_01_26210']);
        positions=positions.positions;
        %         positions = P.positions;
        % taken from robins custom case
        positions(:,2) = positions(:,2)/P.d1x; % horizontal axis
        positions(:,1) = positions(:,1)/P.d1y; % vertical axis
        positions(:,1) = positions(:,1) - min(positions(:,1));
        positions(:,2) = positions(:,2) - min(positions(:,2));
        P.positions = round(positions);
        %
        P.sample_plane = ones(P.object_size);
        P.sample_plane = QQ.object;
        P.probe=QQ.Probe;
        %
        %     % Adjust object size accordingly
        %     object_size = [P.Ny,P.Nx] + max(P.positions,[],1);
        %     P.object_size = object_size;
        %         P.probe = QQ.Probe; %ones(size(P.I_exp(:,:,1)));
        %         P.object = QQ.object;
        P.trans_min_true = 0;
        P.trans_max_true = 1;
        clear Q QQ
    elseif strcmp(P.datafile, 'data_RWRD11_02_07175_192x192')
        QQ = load([P.data_dir 'data_RWRD11_02_07175_192x192'],'P');
        QQ = QQ.P;
        positions = load ([P.data_dir 'positions_RWRD11_02_07175']);
        positions=positions.positions;
        %         positions = P.positions;
        % taken from robins custom case
        positions(:,2) = positions(:,2)/P.d1x; % horizontal axis
        positions(:,1) = positions(:,1)/P.d1y; % vertical axis
        positions(:,1) = positions(:,1) - min(positions(:,1));
        positions(:,2) = positions(:,2) - min(positions(:,2));
        P.positions = round(positions);
        %
        %         P.sample_plane = QQ.object;
        P.probe=ones(size(P.I_exp(:,:,1)));
        %
        % Adjust object size accordingly
        object_size = [P.Ny,P.Nx] + max(P.positions,[],1);
        P.object_size = object_size;
        P.sample_plane = ones(P.object_size);
        %         P.probe = QQ.Probe; %ones(size(P.I_exp(:,:,1)));
        %         P.object = QQ.ect;
        P.trans_min_true = 0;
        P.trans_max_true = 1;
        clear Q QQ
    else
    end
    % The following reduces the size of the problem by removing some of
    % the data so that you can debug on your laptop.
    %      keepPos = sum(((P.positions)<30),2)==2;
    %      P.positions = P.positions(keepPos,:);
    %      P.N_pie = sum(keepPos);
    %      P.I_exp = P.I_exp(:,:,keepPos);
    %      P.fmask = P.fmask(:,:,keepPos);
    %      P.ny = sum(keepPos);
    %      P.nx = 1;
    %      sum(keepPos)
    
else % Else, we simulate a ptychography experiment.
    % Requires the following fields be set in the input file
    % (ie. 'Ptychography_in.m').
    %   P.Nx = 64; %dector size in pixels along x-axis.
    %   P.Ny = 64; %dector size in pixels along y-axis.
    %   P.scan_stepsize = 3.5e-7; %size of the step between ptychographic images.
    %   P.nx = 20; %number of images taken along x-axis.
    %   P.ny = 20; %number of images taken along y-axis.
    %   P.sample_area_centre = [3/8 3/8]; %point in [0,1]*[0,1], the relative coordinate of the centre of the area to be sampled.
    %   P.sample_fourier_area = 0.3;
    
    scan_stepsize = P.scan_stepsize;
    
    %% Physical parameters of the experiment.
    % X-ray engergy [keV]
    P.E = 6.2;
    % Wavelength [m]
    P.lambda = 12.3984/P.E*1E-10;
    % Wavenumber [m^-1]
    P.k = 2*pi/P.lambda;
    
    % Pixel dimensions: [m]
    P.d2x = 172e-6;
    P.d2y = 172e-6;
    
    % Origin of cordinate system
    P.origin.x0 = P.Nx/2+1;
    P.origin.y0 = P.Ny/2+1;
    
    % Position(s) of sample along optical axis
    % distance focus <-> sample [m]
    P.z01 = .5e-4;
    
    % distance focus <-> detector [m]
    P.z02 = 7.2;
    
    % distance sample <-> detector [m]
    P.z12 = P.z02-P.z01;
    
    P.d1y =P.lambda*P.z12/(P.Ny*P.d2y);
    P.d1x =P.lambda*P.z12/(P.Nx*P.d2x);
    
    P.dq2x = 2*pi/(P.Nx*P.d1x);
    P.dq2y = 2*pi/(P.Ny*P.d1y);
    
    
    %% Scan parameters.
    
    % The scan type. Options are 'round_roi', or 'raster'
    P.scan_type = 'raster';
    
    % The size of the `step' between ptychographic images.
    P.pie_step_x  = scan_stepsize;
    P.pie_step_y  = scan_stepsize;
    P.pie_step    = [P.pie_step_y P.pie_step_x];
    
    % Actual sidelength of meshscan, length of fast axis
    P.N = P.nx;
    
    % Setting up PIE matrix
    % Positions matrix (will contain (x,y)-coordinates of positions in pixel
    % units of object plane)
    % P.scan_type = 'round_roi';
    % P.round_roi.lx = 5E-6;
    % P.round_roi.ly = 5E-6;
    % P.round_roi.nth = 5;
    % P.round_roi.dr = 0.5E-6;
    % P.dx_spec = P.d1x;
    
    % Nominal positions
    positions = [];
    
    switch P.scan_type
        case 'round_roi'
            dx_spec = P.dx_spec;
            rmax = sqrt((P.round_roi.lx/2)^2 + (P.round_roi.ly/2)^2);
            nr = 1 + floor(rmax/P.round_roi.dr);
            for ir=1:nr+1
                rr = ir*P.round_roi.dr;
                dth = 2*pi / (P.round_roi.nth*ir);
                for ith=0:P.round_roi.nth*ir-1
                    th = ith*dth;
                    xy = rr * [sin(th), cos(th)];
                    if( abs(xy(1)) >= P.round_roi.ly/2 || (abs(xy(2)) > P.round_roi.lx/2) )
                        continue
                    end
                    
                    positions(end+1,:) = xy ./dx_spec;
                end
            end
            P.ny = 1;
            P.nx = size(positions,1);
        otherwise %i.e., P.scan_type = 'raster';
            ind = 1;
            for n_slow = 0:(P.ny-1)
                for n_fast = 0:(P.nx-1)
                    positions(ind,2) = P.pie_step(2) * n_fast/P.d1x; % horizontal axis
                    positions(ind,1) = P.pie_step(1) * n_slow/P.d1y; % vertical axis
                    ind = ind+1;
                end
            end
            
    end
    
    % Total length
    P.N_pie = P.nx*P.ny;
    positions(:,1) = positions(:,1) - min(positions(:,1));
    positions(:,2) = positions(:,2) - min(positions(:,2));
    positions = round(positions);
    
    P.positions = positions;
    
    % Adjust object size accordingly
    object_size = [P.Ny,P.Nx] + max(P.positions,[],1);
    P.object_size = object_size;
    
    if strcmp(P.sim_data_type,'siemensstar')
        [fid, ~]=fopen('siemensstar.mat');
        if fid<0
            disp('*************************************************************************')
            disp('* INPUT DATA MISSING.  Please download the ptychography input data from *')
            disp('* http://vaopt.math.uni-goettingen.de/data/Ptychography.tar.gz          *')
            disp('* Save and unpack the Ptychography.tar.gz datafile in the               *')
            disp('*    ProxMatlab/InputData/Ptychography subdirectory                     *')
            disp('*************************************************************************')
        else
            load('siemensstar.mat','siemensstar');
        end
        fclose(fid);
        
        %% Load the image to be probed.
        tmp = siemensstar > 0.4;
        siemensstar = siemensstar / sum(sum(siemensstar(tmp)))*numel(siemensstar(tmp));
        
        % Define pixeldimensions of sample:
        % Approx. 200/28 nm per pixel for siemensstar
        dim_star = 200e-9/28;
        scan_pixelsteps = round(scan_stepsize/dim_star);
        
        %% Resolution parameters.
        %  Dimensions of variable sample_plane necessary for function ExtractRoi
        dim.A.i = dim_star;
        dim.A.j = dim_star;
        
        % Fourier relation for dimensions in sample plane necessary for
        % function ExtractRoi
        dim.B.j = P.sample_fourier_area * P.d1y;
        dim.B.i = P.sample_fourier_area * P.d1x;
        
        % Choose the area of the image to sample. For ease, we just use it's %center.
        Rx   = 1024 * P.sample_area_centre(1); %%
        Ry   = 700  * P.sample_area_centre(2);
        pA.i = round(Ry);
        pA.j = round(Rx);
        
        % Size of field of view necessary for function ExtractRoi
        B.j = P.object_size(1);
        B.i = P.object_size(2);
        
        %%  Get sample with right pixeldimensions
        Siemensstar = ExtractROI(siemensstar,pA,dim,B);
        
        %% Give X-ray properties to testpattern
        %  Define thickness of star
        T = .5E-6;
        
        % Load X-ray parameters for E = 6.2 keV and Tantalum
        load('xray_prop_Ta_6200eV','delta','beta')
        
        phi = P.k *delta*T;
        mu  = 2*P.k*beta;
        sample_plane = exp(-mu*T*Siemensstar/2).*exp(sqrt(-1)*Siemensstar*phi);
    elseif strcmp(P.sim_data_type,'gaenseliesel')
        %% Gaenseliesel object
        [fid, ~]=fopen('gaenseliesel.png');
        if fid<0
            disp('*************************************************************************')
            disp('* INPUT DATA MISSING.  Please download the ptychography input data from *')
            disp('* http://vaopt.math.uni-goettingen.de/data/Ptychography.tar.gz          *')
            disp('* Save and unpack the Ptychography.tar.gz datafile in the               *')
            disp('*    ProxMatlab/InputData/Ptychography subdirectory                     *')
            disp('*************************************************************************')
        else
            g = imread('gaenseliesel.png');
        end
        fclose(fid);
        g = double(g);
        g = g / max(max(g));
        g = 0.8*g + 0.1;                 %scale to be in [0.1,0.9]
        g = abs(g).*(cos(g)+1i*sin(g));
        sample_plane = g;
        P.object_size = [128 128];
    else
        error('Error: Object no specified for simulated data.')
    end
    
    %% Illumination functions.
    
    % Create a set of illumination functions
    % FWHM of gaussian illumination [m]
    FWHM = .5E-6;
    p.w0 =  FWHM/(2*sqrt(log(2)));
    
    % Corresponding rayleigh range of gaussian beam
    z0 = p.w0^2*pi/P.lambda;
    
    % Get coordinate system in sample plane
    
    x = ((1:P.Nx)-(floor(P.Nx/2)+1))*P.d1x;
    y = ((1:P.Ny)-(floor(P.Ny/2)+1))*P.d1y;
    [X,Y] = meshgrid(x,-y);
    
    % Get illumination in sample plane
    
    [U] = GAUSSU(sqrt(X.^2+Y.^2),P.z01,P.lambda,p.w0);
    
    %% Scan along xyz-directions.
    P.I_exp=zeros(P.Ny,P.Nx,P.ny,P.nx);
    P.probe = zeros(P.Ny,P.Nx);
    
    % Transfer parameters from P to p for function dpgenerate
    p.d2x = P.d2x;
    p.d2y = P.d2y;
    p.origin = P.origin;
    p.E  = P.E;
    p.z02 = P.z02;
    p.z01 = P.z01;
    
    % Generate the data.
    ind = 1;
    for akk = 1:P.ny
        for bkk =1:P.nx
            Indy = P.positions(ind,1) + (1:P.Ny);
            Indx = P.positions(ind,2) + (1:P.Nx);
            
            % Extract roi from sample_plane
            P.sample = sample_plane(Indy,Indx);
            % Illumination function
            P.probe = U;
            % Calculate exit surface wave
            P.esw = P.probe .* P.sample;
            % Calculate coherent diffraction pattern
            P.I_exp(:,:,akk,bkk) = abs(rot90(circshift(fft2(P.esw),[P.origin.x0-1 P.origin.y0-1]),-2)).^2;
            
            ind = ind+1;
        end
    end
    
    P.sample_plane = sample_plane;

    % ture object and illumination
    P.true_object = sample_plane;
    P.true_probe = P.probe;
    P.truth=true;
    
    P.datatype = 'exp'; % why is this set to 'exp'? DRL 22.08.17
    P.file_prefix = 'SimData';
    
    % Interval for object constraints.
    P.trans_min_true = 0;
    P.trans_max_true = 1;
end




%%=========================================================================
%%           Process the data (based on Paer's data processor).
%%=========================================================================

%  Average total intensity in experimental diffraction pattern
I_avg = mean(mean(sum(sum(P.I_exp,1),2),3),4);
fprintf('Average intensity of single frame is %3.2e photons.\n',I_avg);

%  Maximum average intensity in a single experimental frame
I_max_avg = max(max(sum(sum(P.I_exp,1),2),[],3),[],4) / (P.Nx*P.Ny);

%  Generate cell array of normalized intensities
%  Normalizeation such, that for I = I_max (as matrix) the average pixel
%  intensity is 1 and total is Nx*Ny, for all other I the values are lower
P.Amp_exp_norm = zeros(size(P.I_exp,1),size(P.I_exp,2),P.N_pie);
%fmask = ones(size(P.I_exp,1),size(P.I_exp,2),P.N_pie);

% Specimen plane (E1)/ probe plane (as long as direct propagation is used) -
[X_1,Y_1] = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.d1x,((1:P.Ny)-floor(P.Ny/2)-1)*P.d1y);

% % Extended coordinate system for object plane
% [X_obj,Y_obj] = meshgrid((1:P.object_size(2))*P.d1x,(1:P.object_size(1))*P.d1y);

if ~isfield(P,'probe_mask_gamma')
    P.probe_mask_gamma = 1;
end

%  Relative radius of ideal circular probe mask with respect to radius of
%  pinhole used for initial simulation of the probe function
P.R        = 2.2E-6;
beta_sam   = 0.9*P.Nx*P.d1x/2/P.R*P.probe_mask_gamma;
P.beta_sam = beta_sam;

if P.switch_probemask
    P.Probe_mask = circ(X_1,Y_1,0,0,beta_sam*P.R);
else
    P.Probe_mask = ones(size(P.probe));
end

%  Generate noise
switch P.noise
    case 'poisson'
        fprintf('Adding Poisson noise to measurement data...\n')
        % figure(700)
        for i=1:P.N_pie
            %             subplot(1,2,1)
            %                 imagesc(log10(P.I_exp(:,:,i)));
            %                 title('Without noise')
            % subplot(1,2,1)
            %    imagesc(log10(P.I_exp(:,:,i)));
            %    title('Without noise')
            P.I_exp(:,:,i) = arrayfun(@(u) (u + 10^(PoissonRan(P.poissonfactor))), P.I_exp(:,:,i));
            % fprintf('Noise added to frame %i of %i \n', i, P.N_pie);
            % subplot(1,2,2)
            %    imagesc(log10(P.I_exp(:,:,i)));
            %    title('With noise')
            % pause(0.01)
        end
        % close 700
    otherwise
        fprintf('No noise added to measurement data...\n')
end


cx = floor(size(P.I_exp,2)/2);
cy = floor(size(P.I_exp,1)/2);
%  Removal of residual noise in the data is done here.
ind=1;
for n_slow = 1:P.ny
    for n_fast = 1:P.nx
        %check if semi-transparent central stop was used. scale intensity
        %accordingly
        
        if isfield(P,'bs_mask')
            dat = P.I_exp(:,:,n_slow,n_fast);
            dat(logical(P.bs_mask)) = dat(logical(P.bs_mask))*P.bs_fact;
            P.I_exp(:,:,n_slow,n_fast) = dat;
        end
        % Save time and get experimental data into matlab fft2 non-centered
        % system
        
        dat=rot90(rot90(circshift(P.I_exp(:,:,n_slow,n_fast),-[cy cx])));
        %dat = P.I_exp(:,:,n_slow,n_fast);
        
        % Normalization such that average intensity of data is on the order
        % of 1
        P.Amp_exp_norm(:,:,ind) = sqrt(dat/I_max_avg);
        
        %check for pixelmask
        if isfield(P,'fmask')
            P.fmask(:,:,ind) = rot90(rot90(circshift(double(P.fmask(:,:,n_slow,n_fast)),-[cy cx])));
        end
        ind = ind+1;
    end
end

guess_value = 1000;

%  Inital guess for the probe.
switch P.probe_guess_type
    case 'exact'
        P.probe_guess = P.probe;
    case 'exact_amp'
        P.probe_guess = abs(P.probe); %1*(abs(P.probe)>0.75);
    case 'gauss'
        [X_1,Y_1] = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.d1x,((1:P.Ny)-floor(P.Ny/2)-1)*P.d1y);
        P.probe_guess = GAUSSU(sqrt(X_1.^2+Y_1.^2),P.z01,P.lambda,.7E-6/(2*sqrt(log(2))));
    case 'circle'
        P.probe_guess = zeros(size(P.probe));
        probe_size = size(P.probe);
        
        %guess_value = max(max(max(P.Amp_exp_norm))); %1000;
        
        radius = 13/256 * size(P.probe,1);
        x_c = probe_size(1)/2;
        y_c = probe_size(2)/2;
        for i=1:probe_size(1)
            for j=1:probe_size(2)
                if(sqrt((i - x_c)^2 + (j - y_c)^2) < radius)
                    P.probe_guess(i,j) = guess_value;
                end
            end
        end
        
        %P.probe_guess = P.probe_guess * 1.01 * max(max(abs(P.probe)));
    case 'ellipse'
        probe_size    = size(P.probe);
        P.probe_guess = zeros(probe_size);
        
        a = 1/6 * size(P.probe,1); %major axis
        b = a/2; %minor axis
        ang = 0;
        
        [yy xx] = meshgrid( (1:probe_size(1)) - round(probe_size(1)/2), (1:probe_size(2)) - round(probe_size(2)/2) );
        
        if ang ~= 0
            P.probe_guess = 1 * imrotate(((xx/a).^2+(yy/b).^2<=1),ang,'nearest','crop');
        else
            P.probe_guess = 1 * ((xx/a).^2+(yy/b).^2<=1) + 0;
        end
        
        figure
        imagesc(P.probe_guess);
        figure
        imagesc(abs(P.probe));
        pause(10);
    case 'ellipse_phase'
        probe_size    = size(P.probe);
        P.probe_guess = zeros(probe_size);
        
        a = 1/6 * size(P.probe,1); %major axis
        b = a/2; %minor axis
        ang = 0;
        
        [yy xx] = meshgrid( (1:probe_size(1)) - round(probe_size(1)/2), (1:probe_size(2)) - round(probe_size(2)/2) );
        
        if ang ~= 0
            P.probe_guess = 1 * imrotate(((xx/a).^2+(yy/b).^2<=1),ang,'nearest','crop');
        else
            P.probe_guess = 1 * ((xx/a).^2+(yy/b).^2<=1) + 0;
        end
        
        P.probe_guess = 10 * P.probe_guess .* P.probe ./ (abs(P.probe)+1e10);
        
        %         figure
        %         imagesc(P.probe_guess);
        %         figure
        %         imagesc(abs(P.probe));
    case 'robinInitial'
        a = .2E-6;
        b = .2E-6;
        alpha = 0;
        [X_1,Y_1]     = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.d1x,((1:P.Ny)-floor(P.Ny/2)-1)*P.d1y);
        P.probe_guess = abs(imrotate(ellipsis(X_1,Y_1,0,0,a,b),alpha,'crop'));
        
        
        P.probe_guess = prop_nf_pa(P.probe_guess,P.lambda,P.z01,P.d1x,P.d1y);
        P.probe_guess = P.probe_guess / norm_avg(P.probe_guess);
        
    case 'robinGaussian'
        FWHM          = 1.0000e-07;
        [X_1,Y_1]     = meshgrid(((1:P.Nx)-floor(P.Nx/2)-1)*P.d1x,((1:P.Ny)-floor(P.Ny/2)-1)*P.d1y);
        P.probe_guess = GAUSSU(sqrt(X_1.^2+Y_1.^2),P.z01,P.lambda,FWHM/(2*sqrt(log(2)))) / 80;
        
        phaseShift    = P.probe(floor(P.Nx/2)-1,floor(P.Ny/2)-1) / abs(P.probe(floor(P.Nx/2)-1,floor(P.Ny/2)-1)) / P.probe_guess(floor(P.Nx/2)-1,floor(P.Ny/2)-1);
        P.probe_guess = P.probe_guess * phaseShift;
        
        
    case 'ones'
        P.probe_guess = ones(size(P.probe));
    case 'exact_perturbed'
        noise         =  sqrt(2) * (rand(size(P.probe))-0.5) + 1i* (rand(size(P.probe))-0.5);
        P.probe_guess = P.probe + 5 * noise;
    case 'exactAmp_randPhase'
        P.probe_guess = abs(P.probe) .* (rand(size(P.probe))-0.5) + 1i* (rand(size(P.probe))-0.5);
    otherwise
        error('probe guess string not recognized...')
end

%  Initial guess for the object.
switch P.object_guess_type
    case 'exact'
        P.object_guess = P.sample_plane;
    case 'ones'
        P.object_guess = ones(size(P.sample_plane)) * (1/guess_value);
    case 'constant'
        P.object_guess = ones(size(P.sample_plane)) * (1/guess_value);
    case 'exact_perturbed'
        P.object_guess = P.sample_plane + 2.0e-1 * (rand(size(P.sample_plane))-0.5);
    case 'random'
        rand('seed',P.run_number);
        P.object_guess = (1/guess_value) * (rand(P.object_size)) .* exp(1i*(2*pi*rand(P.object_size) - pi)/2);
    otherwise
        error('object guess string not recognized...')
end

% Legacy variables.
P.product_space_dimension = P.N_pie;
P.norm_data = 1;

% cfactor for probe
cfact = 0.0001*P.ny*P.nx;
P.cfact = cfact;





% Computer object support constraint (using intial probe guess).
%if P.switch_object_support_constraint
if strcmp(P.probe_guess_type,'exact')
    probe_guess = zeros(size(P.probe));
    probe_size = size(P.probe);
    
    %guess_value = max(max(max(P.Amp_exp_norm))); %1000;
    
    radius = 13/256 * size(P.probe,1);
    x_c = probe_size(1)/2;
    y_c = probe_size(2)/2;
    for i=1:probe_size(1)
        for j=1:probe_size(2)
            if(sqrt((i - x_c)^2 + (j - y_c)^2) < radius)
                probe_guess(i,j) = guess_value;
            end
        end
    end
else
    probe_guess = P.probe_guess;
end
low = 0.1;
object_support = zeros(size(P.sample_plane));
for pos=1:P.N_pie
    Indy = P.positions(pos,1) + (1:P.Ny);
    Indx = P.positions(pos,2) + (1:P.Nx);
    if strcmp(P.probe_guess_type,'robinGaussian')
        object_support(Indy,Indx) = object_support(Indy,Indx) + (P.Probe_mask);
    else
        object_support(Indy,Indx) = object_support(Indy,Indx) + ((conj(probe_guess).*probe_guess)>low);
    end
end
P.object_support = (object_support > 0);
%P.object_support = ones(size(object_support));

P.object_guess = P.object_guess .* P.object_support;
P.sample_plane = P.sample_plane .* P.object_support;

if P.switch_probemask
    P.probe = P.probe .* P.Probe_mask;
end


if P.switch_object_support_constraint
    P.object_guess = P.object_guess .* P.object_support;
end
if P.switch_probemask
    P.probe = P.probe .* P.Probe_mask;
end

% Generate initial guess for exit wave functions
datasize = [P.Ny,P.Nx,P.ny*P.nx];
phi      = ones(datasize);

for pos=1:P.N_pie
    Indy = P.positions(pos,1) + (1:P.Ny);
    Indx = P.positions(pos,2) + (1:P.Nx);
    phi(:,:,pos) = P.probe_guess.*P.object_guess(Indy, Indx);
end



% Set the initial iterate.
P.u_0 = cPtychography(phi, P.object_guess, P.probe_guess);


