function psi_hsv = complex_plot(psi)

psi_hsv = zeros(size(psi,1),size(psi,2),3);
psi_hsv(:,:,1) = (angle(psi.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
psi_hsv(:,:,2) = ones(size(psi,1),size(psi,2));
psi_hsv(:,:,3) = abs(psi)/max(max(abs(psi)));