function val = FFT2(X);

val = fft2(X)/(sqrt(numel(X)));