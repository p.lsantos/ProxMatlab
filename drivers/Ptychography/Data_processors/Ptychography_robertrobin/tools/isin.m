function val = isin(x,I)

s = size(I);

if length(s) <= 2
    if sum(s(find(s == 1))) >= 1
        if sum(isinf(1./(I-x))) > 0
            val = true;
        else
            val = false;
        end
    else
        error('ISIN implemented for one-dimensional arrays only right now.');
    end
else
    error('ISIN implemented for one-dimensional arrays only right now.');
end