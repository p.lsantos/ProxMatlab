function val = norm_avg(X);

val = sqrt(1/numel(X)*sum(sum(conj(X).*X)));