function val = ellipsis(X,Y,x0,y0,a,b)

% R   radius of circular aperture
val = heaviside(-(((X-x0)/a).^2+((Y-y0)/b).^2-1));