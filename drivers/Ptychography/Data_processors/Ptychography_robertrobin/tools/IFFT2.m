function val = IFFT2(X);

val = ifft2(X)*sqrt(numel(X));