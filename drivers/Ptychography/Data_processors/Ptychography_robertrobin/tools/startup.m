%clear;
%close all;
set(0,'DefaultFigurePaperType','A4');
set(0,'DefaultFigurePaperOrientation','landscape');
set(0,'DefaultFigurePaperPositionMode','manual');
set(0,'DefaultFigurePaperUnits','normalized');
set(0,'DefaultFigurePaperPosition', [-0.0 -0.0 1. 1.]);

set(0, ...
    'defaultaxesfontsize',   12, ...
    'defaultaxeslinewidth',   1.5, ...
    'defaultlinelinewidth',   2., ...
    'defaultpatchlinewidth',  1.5, ...
    'defaultaxesfontname', 'Arial', ...
    'defaultaxesbox', 'on');

set(0,'DefaultAxesColorOrder',[0 0 0]);
%set(0,'DefaultAxesLineStyleOrder','-|-.|--|:');
set(0,'DefaultAxesLineStyleOrder',{'-', ':', '-.','-o','-*','-.','-+','--'});
set(0,'DefaultLineMarkerFaceColor','w');
set(0,'DefaultLineMarkerSize',5);
