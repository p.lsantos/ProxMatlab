% Author K. Giewekemeyer
% Original file from sftp://klaus@login.roentgen.physik.uni-goettingen.de/
% home/klaus/Documents/Uni/Daten/Messzeiten/0909_BESSY/analysis_at_home/matlab/main/tools
% last modified on 07.07.2010.
% last modified and checked 13.10.2010.
%
% prop_nf_pa propagates a 2d-wave field within paraxial approximation into the
% optical near field. Sufficient sampling of the phase-chirp function
% is checked with a near-field criterion which is only valid within the 
% paraxial approximation.
% The validity of the small-angle approximation is enforced. If it is not
% obeyed, the function returns an error.
% If sampling of the phase chirp function is not sufficient, the field is 
% embedded into a wider field of fiew, filling new pixels, so that a 
% continuous boundary conditions are obeyed.
% If the field has been enlarged prior to propagation, it is cut back 
% to the original size after propagation. Note that this cropping generally 
% destroys the conservation of the total fluence in the array.

function Im = prop_nf_pa_force(im,lambda,Delta_z,dx,dy)

% dx, dy: pixel widths in horizontal and vertical direction 
% Delta_z: propagation distance
% lambda: wavelength
% im: incident 2d wave field

[Ny, Nx] = size(im);
k = 2*pi/lambda;

% Enforce paraxial approximation, according to a sufficient criterion
% due to Goodman, Introduction to Fourier Optics, 2005, p.68. Note that
% this criterion can be overly restrictive.
alpha_th = (4/pi*lambda/Delta_z)^(1/4);
alpha = max(atan(Nx*dx/(2*Delta_z)),atan(Ny*dy/(2*Delta_z)));
% if alpha > alpha_th
%  error('Small-angle approximation is violated.');
% end

% Sampling criterion for phase chirp.
px = abs(lambda*Delta_z/(Nx*dx^2));
py = abs(lambda*Delta_z/(Ny*dy^2));
f = max(px,py);

% beta = 1 ensures Nyquist sampling. Higher beta's make result better, but
% computationally more expensive.
beta = 1;

if f > 1
    fprintf('Probe-FOV is enlarged by factor %3.2f for propagation.\n',beta*f);
    
    % enlarged array sizes
    Nx_l = ceil(beta*f)*Nx;
    dqx = 2*pi/(Nx_l*dx);
    Ny_l = ceil(beta*f)*Ny;
    dqy = 2*pi/(Ny_l*dy);
    % If sampling criterion was obeyed in one direction, result gets even
    % better.
    
    % Again enforce paraxial approximation.
    alpha_th = (4/pi*lambda/Delta_z)^(1/4);
    alpha = max(atan(Nx_l*dx/(2*Delta_z)),atan(Ny_l*dy/(2*Delta_z)));
%     if alpha > alpha_th
%      error('Small-angle approximation is violated.');
%     end

    [Qx,Qy] = meshgrid(((1:Nx_l)-floor(Nx_l/2)-1)*dqx,((1:Ny_l)-floor(Ny_l/2)-1)*dqy);
    % paraxial propagator
    kappa = -1/2*ifftshift(Qx.^2+Qy.^2)/k;
    
    % center in new array
    CX = floor(Nx_l/2)+1;
    CY = floor(Ny_l/2)+1;
        
    % Embedding of image into larger field.
    im_l = zeros(Ny_l,Nx_l);
    % Filling up new pixels with boundary values (continuous boundary
    % conditions).
    % bottom rows
    for ind = 1:CY-floor(Ny/2)-1
       im_l(ind,CX-floor(Nx/2):CX+ceil(Nx/2)-1) = im(1,:);
    end
    % top rows
    for ind = CY+ceil(Ny/2):Ny_l
       im_l(ind,CX-floor(Nx/2):CX+ceil(Nx/2)-1) = im(end,:);
    end
    % left columns
    for ind = 1:CX-floor(Nx/2)-1
        im_l(CY-floor(Ny/2):CY+ceil(Ny/2)-1,ind) = im(:,1);
    end
    % right columns
    for ind = CX+ceil(Nx/2):Nx_l
        im_l(CY-floor(Ny/2):CY+ceil(Ny/2)-1,ind) = im(:,end);
    end
    % bottom left edge
    im_l(1:CY-floor(Ny/2)-1,1:CX-floor(Nx/2)-1) = im(1,1);
    % bottom right edge
    im_l(1:CY-floor(Ny/2)-1,CX+ceil(Nx/2):end) = im(1,end);
    % top left edge
    im_l(CY+ceil(Ny/2):Ny_l,1:CX-floor(Nx/2)-1) = im(end,1);
    % top right edge
    im_l(CY+ceil(Ny/2):Ny_l,CX+ceil(Nx/2):end) = im(end,end);
    
    % center: actual field
    im_l(CY-floor(Ny/2):CY+ceil(Ny/2)-1,CX-floor(Nx/2):CX+ceil(Nx/2)-1) = im;
    
    % propagation
    Im_l = fftshift(ifft2(fft2(ifftshift(im_l)).*exp(1i*kappa*Delta_z)));
    % decrease field of view
    Im = Im_l(CY-floor(Ny/2):CY+ceil(Ny/2)-1,CX-floor(Nx/2):CX+ceil(Nx/2)-1);

else
    
    dqx = 2*pi/(Nx*dx);
    dqy = 2*pi/(Ny*dy);
    
    [Qx,Qy] = meshgrid(((1:Nx)-floor(Nx/2)-1)*dqx,((1:Ny)-floor(Ny/2)-1)*dqy);
    kappa = -1/2*ifftshift(Qx.^2+Qy.^2)/k;

    Im = fftshift(ifft2(fft2(ifftshift(im)).*exp(1i*kappa*Delta_z)));
    
end