function val = radial_integ(I_exp,center_x, center_y, square_length, r_start, r_step, r_max, deg_range, start_angle, no_of_segments);

%every section gets a range of angular and a radial indices on a polar grid
%corresponding to the polar coordinates [theta rho]

% create an array of the (x,y) coordinates relative to the beam center and
% convert it to polar coordinates
[ x, y ] = meshgrid( (1:square_length)-center_x, (1:square_length)-center_y );
[ theta, rho ] = cart2pol( x, y );
theta = (theta/pi +1) * 180.0;

% prepare circular masks of the integer width r_step (in pixel)
r = r_start:r_step:r_max;

% cell containing all mask values in the end
masks_r = cell( length(r), no_of_segments );
% matrix containing all sums of masks specefied in masks_r
mask_r_sum = zeros( length(r), no_of_segments );
% cell containing degree values of angular segments (over the full radial range)
seg_inds = cell(no_of_segments,1);


for ind_seg = 1:no_of_segments
    seg_from = deg_range/no_of_segments * (ind_seg-1) + start_angle;
    seg_to = deg_range/no_of_segments * ind_seg + start_angle;
    % linear indices of current angular segment in [1 no_of_segments]
    ind_curr = find( ((theta > seg_from) & (theta <= seg_to)) );
    % store indices of current angular segment in element of seg_ind
    seg_inds{ind_seg} = ind_curr;
end

for ind_r=1:length(r)
    % linear indices of current radial segment in [1 length(r)]
    r_inds = find( (rho >= r(ind_r)) & (rho < r(ind_r)+r_step) );
    for ind_seg = 1:no_of_segments
        %intersection of linear indices of angular and radial sections
        masks_r{ind_r, ind_seg} = intersect( r_inds, seg_inds{ind_seg} );
        % calculate the normalization value (number of pixels within the mask)
        mask_r_sum(ind_r, ind_seg) = length( masks_r{ind_r, ind_seg} );
    end
end

for ind_r = 1:length(r)
    for ind_seg = 1:no_of_segments
        I_exp(masks_r{ind_r,ind_seg}) = 0;
    end
end

I_exp(masks_r{1,1}) = 0;
imagesc(log10(I_exp));
axis equal; axis tight; drawnow;

val = 1;






