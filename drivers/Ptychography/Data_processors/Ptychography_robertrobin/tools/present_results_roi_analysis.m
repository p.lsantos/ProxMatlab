function val = present_results_roi_analysis(data_file,phase_bounds)

addpath /home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/psi_routines/matlab_pierre/all/
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/main/tools');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/main/tools/pie');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/psi_routines');


% load experimental data
close all;
startup;

disp('Loading datafile.');
load(data_file);
disp('Finished loading data.');
disp('Preparing plots.');

%% Generate coordinate systems

%detector plane (E2) ------------------------------------------------------

% coordinate system (points at centers of the pixels)
[X_2,Y_2] = meshgrid(P.d2x/2 + [0:1:P.Nx-1]*P.d2x - P.Nx/2*P.d2x,P.d2y/2 + [0:1:P.Ny-1]*P.d2y - P.Ny/2*P.d2y);

% coordinate system in Fourier space
[Q2_x,Q2_y] = meshgrid(-P.q2x_max + P.dq2x/2 + [0:1:P.Nx-1]*P.dq2x,-P.q2y_max + P.dq2y/2 + [0:1:P.Ny-1]*P.dq2y);

%specimen plane (E1)/ probe plane (as long as direct propagation is used) -

% coordinate system (points at centers of the pixels)
[X_1,Y_1] = meshgrid(P.d1x/2 + [0:1:P.Nx-1]*P.d1x - P.Nx/2*P.d1x,P.d1y/2 + [0:1:P.Ny-1]*P.d1y - P.Ny/2*P.d1y);

% coordinate system in Fourier space
[Q1_x,Q1_y] = meshgrid(-P.q1x_max + P.dq1x/2 + [0:1:P.Nx-1]*P.dq1x,-P.q1y_max + P.dq1y/2 + [0:1:P.Ny-1]*P.dq1y);

%extended coordinate system for object plane
[X_obj,Y_obj] = meshgrid(P.d1x/2 + [0:1:P.object_size(2)-1]*P.d1x - P.Nx/2*P.d1x, P.d1y/2 + [0:1:P.object_size(1)-1]*P.d1y - P.Ny/2*P.d1y);

% Fresnel number
F = (2*P.R)^2/(P.lambda*P.z01);
P.F = F;


%% Fiddling around with the experimental data a bit (normalization etc.)

% maximum average intensity in a single experimental frame
I_max_avg = max(max(sum(sum(P.I_exp,1),2),[],3),[],4) / (P.Nx*P.Ny);

% generate cell array of normalized intensities 
% normalizeation such, that for I = I_max (as matrix) the average pixel
% intensity is 1 and total is Nx*Ny, for all other I the values are lower
Amp_exp_norm = cell(P.Nx*P.Ny,1);

% average of squared(??) autocorrelation function of each dataset
auto = zeros(P.Ny,P.Nx);

% center coordinates (assuming data is already centered)
cx=floor(size(P.I_exp,1)/2);
cy=floor(size(P.I_exp,2)/2);

k=1;
for n_slow = 1:P.ny
    for n_fast = 1:P.nx
        P.I_valid(:,:,n_slow,n_fast) = pshift(double(P.I_valid(:,:,n_slow,n_fast)),[cx cy]);
        dat = pshift(P.I_exp(:,:,n_slow,n_fast),[cx cy]);
        auto = auto + FFT2(fftshift(dat));
        Amp_exp_norm{k} = sqrt(dat/I_max_avg);
        %no Amp_exp_norm can be larger than 1
        k = k+1;
    end
end


%% Plot final result

%Object (combined)
objecthsv = zeros(P.object_size(1),P.object_size(2),3);
objecthsv(:,:,1) = (angle(P.object.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
objecthsv(:,:,2) = ones(size(P.object,1),size(P.object,2));
objecthsv(:,:,3) = abs(P.object)- (0.5+0.5*sign(abs(P.object)-1)).*(abs(P.object)-1);
% This is equivalent to function 
% f(x) = abs(x)     for abs(x) <= 1 and
% f(x) = 1          for abs(x) > 1
figure(1);
imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,hsv2rgb(objecthsv)); axis xy; axis equal; axis tight; colormap hsv;
xlabel('\mum'); ylabel('\mum');
title(['Object']);

%Probe (combined)
probehsv = zeros(P.Ny,P.Nx,3);
probehsv(:,:,1) = (angle(P.Probe.*exp(sqrt(-1).*1*pi))+pi)./(2*pi);
probehsv(:,:,2) = ones(size(P.Probe,1),size(P.Probe,2));
probehsv(:,:,3) = abs(P.Probe)/max(max(abs(P.Probe)));
figure;
imagesc(squeeze(X_1(1,:))*1e6,squeeze(Y_1(:,1))*1e6,hsv2rgb(probehsv)); axis xy; axis equal; axis tight;
xlabel('\mum'); ylabel('\mum');
title(['Probe']);

%Object (phase)
figure(2);
h = imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,angle(P.object)); axis xy; axis equal; axis tight;
colormap hsv; colorbar; caxis(phase_bounds); xlabel('\mum'); ylabel('\mum');
title(['Probe']);

disp('Please select a first region of interest for calculation of mean.')
[selection,xi1,yi1] = roipoly;    
SELECTION = P.object(find(selection > 0));
SELECTION_abs = abs(SELECTION);
SELECTION_phase = angle(SELECTION);
hold on;
plot(xi1,yi1,'w','LineWidth',2);

fprintf('Mean amplitude of selection is %3.2f.\n',mean(mean(SELECTION_abs)));
fprintf('Mean phase of selection is %3.2f.\n',mean(mean(SELECTION_phase)));

disp('Please select a first region of interest for calculation of mean.')
[selection,xi2,yi2] = roipoly;    
SELECTION = P.object(find(selection > 0));
SELECTION_abs = abs(SELECTION);
SELECTION_phase = angle(SELECTION);
hold on;
plot(xi2,yi2,'w','LineWidth',2);

fprintf('Mean amplitude of selection is %3.2f.\n',mean(mean(SELECTION_abs)));
fprintf('Mean phase of selection is %3.2f.\n',mean(mean(SELECTION_phase)));


%Object (amplitude)
figure(3);
h = imagesc(squeeze(X_obj(1,:))*1e6,squeeze(Y_obj(:,1))*1e6,abs(P.object)); axis xy; axis equal; axis tight;
colormap gray; colorbar; caxis(phase_bounds); xlabel('\mum'); ylabel('\mum');
title(['Probe']);
hold on;
plot(xi1,yi1,'w','LineWidth',2);
plot(xi2,yi2,'w','LineWidth',2);

figure(1);
hold on;
plot(xi1,yi1,'w','LineWidth',2);
plot(xi2,yi2,'w','LineWidth',2);

val = 1;