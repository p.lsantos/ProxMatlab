function [b, p, varargout] = rmphaseramp(a, varargin)
%
% Function [A1, P] = RMPHASERAMP(A)
%
% A1 = RMPHASERAMP(A)
% Removes the phase ramp from the image A.
%
% [A1, P] = RMPHASERAMP(A)
% Returns in addition the phase array
% such that A1 = A.* P.
%
% ... = RMPHASERAMP(A, 'abs')
% Uses the absolute value of A as a weight.
%
% ... = RMPHASERAMP(A, 'roi')
% Prompts user to select a polygonal mask to define flat phase.

useweight = false;
if nargin > 1
   if size(varargin{1}) == size(a)
      weight = varargin{1};
   elseif strcmp(varargin{1}, 'abs')
      weight = abs(a);
   elseif strcmp(varargin{1}, 'roi')
      weight = make_mask(a);
   elseif strcmp(varargin{1}, 'mask')
      weight = varargin{2};
   else
      error('Unknown second parameter');
   end
   useweight = true;
end

ph = exp(1i*angle(a));
[gx, gy] = gradient(ph);
gx = -real(1i*gx./ph);
gy = -real(1i*gy./ph);

if useweight
    nrm = sum(sum(weight));
    agx = sum(sum(gx.*weight)) / nrm;
    agy = sum(sum(gy.*weight)) / nrm;
else
    agx = mean(mean(gx));
    agy = mean(mean(gy));
end

sz = size(a);
[xx,yy] = meshgrid(1:sz(2),1:sz(1));
p = exp(-1i*(agx*xx + agy*yy));
b = a .* p;

if nargout == 3
    if useweight
        varargout{1} = weight;
    else
        varargout{1} = [];
    end
end

function bw = make_mask(a)

    figure;
    imagesc(angle(rmphaseramp(a)));
    colormap(hsv(256));
    bw = roipoly;
    close;

