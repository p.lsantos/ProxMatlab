function val = kullback_leibler(a,b)


if sum(sum(find(b == 0))) > 0
    error('KB distance not defined.')
else
    val = 0 + sum(sum(a(find(a.*b>0)).*log2(a(find(a.*b>0))./b(find(a.*b>0))) - a(find(a.*b>0)) + b(find(a.*b>0)) )) + ...
          sum(sum(b(find(a == 0))));
end