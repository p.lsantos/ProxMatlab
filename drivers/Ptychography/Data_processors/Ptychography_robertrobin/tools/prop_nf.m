function Im = prop_nf(im,lambda,Delta_z,dx,dy)

[Ny, Nx] = size(im);
k = 2*pi/lambda;

%check small-angle approximation
alpha = max(Nx*dx/Delta_z,Ny*dy/Delta_z);
r = 1-sin(alpha)/alpha;
%fprintf('1-sin(D/z)/(D/z) = %3.2e.\n',r);
if  r > 0.01
    error('Small-angle approximation is violated.');
end

% determine, if numerical propagation from aperture to sample-plane is
% problematic
px = abs(lambda*Delta_z/(Nx*dx^2));
py = abs(lambda*Delta_z/(Ny*dy^2));
f = max(px,py);

beta = 1;

if f > 1
    %fprintf('Caution: Probe-FOV is enlarged by factor %3.2f for propagation.\n',beta*f);
    
    Nx_l = round(beta*f)*Nx;
    dqx = 2*pi/(Nx_l*dx);
    Ny_l = round(beta*f)*Ny;
    dqy = 2*pi/(Ny_l*dy);
    
    %check small-angle approximation again
    alpha = max(Nx_l*dx/Delta_z,Ny_l*dy/Delta_z);
    r = 1-sin(alpha)/alpha; 
    %fprintf('1-sin(D/z)/(D/z) = %3.2e.\n',r);
    if  r > 0.01
       error('Small-angle approximation is violated.');
    end

    [Qx,Qy] = meshgrid(((1:Nx_l)-floor(Nx_l/2)-1)*dqx,((1:Ny_l)-floor(Ny_l/2)-1)*dqy);
    kappa = sqrt(k^2-ifftshift(Qx.^2+Qy.^2));
    
    CX = floor(Nx_l/2)+1;
    CY = floor(Ny_l/2)+1;
    
    if max([max(abs(im(:,1))),max(abs(im(:,Nx))),max(abs(im(1,:))),max(abs(im(Ny,:)))]) > 0.01*max(max(abs(im)))
        fprintf('Cropping artifacts might be large.\n');
    end    
    
    %embedding of image into larger field
    im_l = zeros(Ny_l,Nx_l);
    im_l(CY-floor(Ny/2):CY+ceil(Ny/2)-1,CX-floor(Nx/2):CX+ceil(Nx/2)-1) = im;
    
    Im_l = fftshift(ifft2(fft2(ifftshift(im_l)).*exp(1i*kappa*Delta_z)));
    Im = Im_l(CY-floor(Ny/2):CY+ceil(Ny/2)-1,CX-floor(Nx/2):CX+ceil(Nx/2)-1);

else
    
    dqx = 2*pi/(Nx*dx);
    dqy = 2*pi/(Ny*dy);
    
    [Qx,Qy] = meshgrid(((1:Nx)-floor(Nx/2)-1)*dqx,((1:Ny)-floor(Ny/2)-1)*dqy);
    kappa = sqrt(k^2-ifftshift(Qx.^2+Qy.^2));

    Im = fftshift(ifft2(fft2(ifftshift(im)).*exp(1i*kappa*Delta_z)));
    
end
