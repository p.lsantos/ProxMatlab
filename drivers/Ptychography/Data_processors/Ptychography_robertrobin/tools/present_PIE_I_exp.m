function val = present_PIE_I_exp(data_file)

addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/main/tools');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/main/tools/pie');
addpath('/home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/psi_routines');
addpath /home/klaus/Documents/Uni/Daten/Messzeiten/0811_cSAXS/Data/matlab/psi_routines/matlab_pierre/all/

% load experimental data
close all;
startup;


disp('Loading datafile.');
load(data_file);
disp('Finished loading data.');
disp('Preparing plots.');

%% Generate coordinate systems

%detector plane (E2) ------------------------------------------------------

% coordinate system (points at centers of the pixels)
[X_2,Y_2] = meshgrid(P.d2x/2 + [0:1:P.Nx-1]*P.d2x - P.Nx/2*P.d2x,P.d2y/2 + [0:1:P.Ny-1]*P.d2y - P.Ny/2*P.d2y);

% coordinate system in Fourier space
[Q2_x,Q2_y] = meshgrid(-P.q2x_max + P.dq2x/2 + [0:1:P.Nx-1]*P.dq2x,-P.q2y_max + P.dq2y/2 + [0:1:P.Ny-1]*P.dq2y);

%specimen plane (E1)/ probe plane (as long as direct propagation is used) -

% coordinate system (points at centers of the pixels)
[X_1,Y_1] = meshgrid(P.d1x/2 + [0:1:P.Nx-1]*P.d1x - P.Nx/2*P.d1x,P.d1y/2 + [0:1:P.Ny-1]*P.d1y - P.Ny/2*P.d1y);

% coordinate system in Fourier space
[Q1_x,Q1_y] = meshgrid(-P.q1x_max + P.dq1x/2 + [0:1:P.Nx-1]*P.dq1x,-P.q1y_max + P.dq1y/2 + [0:1:P.Ny-1]*P.dq1y);

%extended coordinate system for object plane
[X_obj,Y_obj] = meshgrid(P.d1x/2 + [0:1:P.object_size(2)-1]*P.d1x - P.Nx/2*P.d1x,P.d1y/2 + [0:1:P.object_size(1)-1]*P.d1y - P.Ny/2*P.d1y);



%% Fiddling around with the experimental data a bit (normalization etc.)

% maximum average intensity in a single experimental frame
I_max_avg = max(max(sum(sum(P.I_exp,1),2),[],3),[],4) / (P.Nx*P.Ny);

% generate cell array of normalized intensities 
% normalizeation such, that for I = I_max (as matrix) the average pixel
% intensity is 1 and total is Nx*Ny, for all other I the values are lower
Amp_exp_norm = cell(P.Nx*P.Ny,1);

% average of squared(??) autocorrelation function of each dataset
auto = zeros(P.Ny,P.Nx);

% center coordinates (assuming data is already centered)
cx=floor(size(P.I_exp,1)/2);
cy=floor(size(P.I_exp,2)/2);

k=1;
for n_slow = 1:P.ny
    for n_fast = 1:P.nx
        P.I_valid(:,:,n_slow,n_fast) = pshift(double(P.I_valid(:,:,n_slow,n_fast)),[cx cy]);
        dat = pshift(P.I_exp(:,:,n_slow,n_fast),[cx cy]);
        auto = auto + FFT2(fftshift(dat));
        Amp_exp_norm{k} = sqrt(dat/I_max_avg);
        %not Amp_exp_norm can be larger than 1
        k = k+1;
    end
end


%% plotting scan files
mkdir('I_exp_movie');
cd('I_exp_movie');



%set(0,'Units','normalized');
%get(0,'ScreenSize');
figure('Position',[1 1 800 800]);

set(0,'defaultaxesfontsize',   16);
set(gcf,'PaperOrientation','portrait');
set(gcf, 'PaperPositionMode','auto');



k = 1;
for pos = 1:P.N_pie;
   Indy = P.positions(pos,1) + (1:P.Ny);
   Indx = P.positions(pos,2) + (1:P.Nx);
   
   %imagesc(squeeze(Q2_x(1,:)*1e-6/pi),squeeze(Q2_y(:,1)*1e-6/pi),log10(fftshift(Amp_exp_norm{k}.^2))); axis equal; axis tight; axis xy; colormap jet; xlabel('f_x [\mum^{-1}]'); ylabel('f_y [\mum^{-1}]');
   imagesc(log10(fftshift(Amp_exp_norm{k}.^2))); axis equal; axis tight; axis xy; colormap jet;
   set(gca,'xtick',[]); set(gca,'ytick',[]);
   drawnow;

   filename = [num2str(k,'%0.3i'), '.jpg'];
   
   %orient rotated;
   print ('-djpeg100', filename);

   %put this plot in a movieframe
    %In case plot title and axes area are needed
        %F = getframe(fig1,winsize);
    %For clean plot without title and axes
   %F = getframe;
   %mov = addframe(mov,F);
   k = k+1;
end

cd('..');

%save movie
%   mov = close(mov);


val = 1;