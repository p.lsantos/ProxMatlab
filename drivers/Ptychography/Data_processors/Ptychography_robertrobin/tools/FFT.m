function val = FFT(X);

val = fft(X)/(sqrt(numel(X)));