function no  = getNextFileNo(base_dir,dig,type_str,file_end)
% this functions looks into the base_dir folder and compares all Folders of
% the type type_str_xxxxxx where xxxxxx is the SIM no. It takes the last no and
% returns the next one

%
tmp = dir([base_dir,'*.',file_end]);
N = length(tmp);
M = length(type_str);
No = [];
for akk = 1:N
    name = tmp(akk).name;
        ln = length(name);
        limit_ln =(length(type_str)+dig+length(file_end)+1);
        if ~isempty(findstr(name,type_str)) && ln==limit_ln
        No(end+1) = str2num(name(M+1:M+dig));
        end
end

if isempty(No)
    no =1;
else
    no = max(No)+1;
end
end

