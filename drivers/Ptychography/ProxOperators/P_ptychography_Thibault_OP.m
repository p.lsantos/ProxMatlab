%             P_ptychography_Thibault_OP.m
%                written by Paer(?) on ??
%           last modified 22nd February 2014
%                     Matthew Tam
%                    CARMA Centre
%               University of Newcastle
%  
%
% DESCRIPTION:  The simualtaneous probe and object update for Thibualt et
%               al algorithm.
%
% INPUT:       input = a data structure with fields:
%              u     = array to be updated
%
% OUTPUT:      u     = the updated array.
%
% USAGE: u = P_ptychography_Thibault_OP(input,u)
%
% Nonstandard Matlab function calls:

function u = P_ptychography_Thibault_OP(input,u)

phi    = u.phi;
object = u.object;
probe  = u.probe;

itstop = 5; %Prescribed number of inner-iterations. Should probably be moved to the input file.

dummy_obj_ones = ones(input.object_size);

this_Probe = probe;
this_object = object;

% Solve the equation system
prch0 = 0;
for i=1:itstop
    
    % new object
    % -------------------------------------------------------------
    obj_enum = 1e-8*dummy_obj_ones;
    obj_denom = 1e-8*dummy_obj_ones;
    I_Probe = abs(this_Probe).^2;
    c_Probe = conj(this_Probe);
    for pos=1:input.N_pie
        Indy = input.positions(pos,1) + (1:input.Ny);
        Indx = input.positions(pos,2) + (1:input.Nx);
        
        obj_enum(Indy,Indx) = obj_enum(Indy,Indx) + c_Probe.*phi(:,:,pos);
        obj_denom(Indy,Indx) = obj_denom(Indy,Indx) + I_Probe;
    end
    this_object = obj_enum./obj_denom;
    
    if input.switch_object_support_constraint
        this_object = this_object .* input.object_support;
    end
    
    % Prohibit values larger 1
    trans_max_true = input.trans_max_true;
    trans_min_true = input.trans_min_true;
    abs_object = abs(this_object);
    high = (abs_object > trans_max_true);
    low = (abs_object < trans_min_true);
    this_object = (1-low).*(1-high).*this_object + (low*trans_min_true + high*trans_max_true).*this_object./(abs_object+1e-30);
    
    % -------------------------------------------------------------
    % New probe only if iteration number is high enough
    % -------------------------------------------------------------
    
    
    Probe_enum = input.cfact*this_Probe;
    Probe_denom = input.cfact;
    abs_obj = abs(this_object).^2;
    conj_obj = conj(this_object);
    for pos=1:input.N_pie
        Indy = input.positions(pos,1) + (1:input.Ny);
        Indx = input.positions(pos,2) + (1:input.Nx);
        
        Probe_enum = Probe_enum + phi(:,:,pos).*conj_obj(Indy,Indx);
        Probe_denom = Probe_denom + abs_obj(Indy, Indx);
    end
    
    %Probe_old = this_Probe;
    this_Probe = Probe_enum./Probe_denom;
    
    if input.switch_probemask
        %Mask in sample plane
        Probe_norm = norm_avg(this_Probe);
        this_Probe = this_Probe.*input.Probe_mask;
        this_Probe = this_Probe .* Probe_norm/norm_avg(this_Probe);
    end
    

    
%     Check if we have found a 'solution' or not.
%     prch = norm((this_Probe - Probe_old));
%     if prch0 == 0
%         prch0 = prch;
%     elseif prch < 0.1*prch0 % comparison of probe change relative to first change in nested loop
%         break
%     end
    
end

%datasize = [input.Ny,input.Nx,input.ny*input.nx];
%phi = ones(datasize);


if(strcmp(input.ptychography_prox,'Thibault'))
    for pos=1:input.N_pie
        Indy = input.positions(pos,1) + (1:input.Ny);
        Indx = input.positions(pos,2) + (1:input.Nx);
        phi(:,:,pos) = this_Probe.*this_object(Indy,Indx);
    end
    u.phi = phi;
end

u.probe = this_Probe;
u.object = this_object;

% For debugging.
%     figure(435);
%     imagesc(hsv2rgb(im2hsv(probe./max(max(abs(probe))), 1)));
%     pause(0.1);

end

