%             P_ptychography_Thibault_O.m
%                written by Paer(?) on ??
%           last modified 22nd February 2014
%                     Matthew Tam
%                    CARMA Centre
%               University of Newcastle
%
% DESCRIPTION:  Projection subroutine onto the diagonal
%               of a product space
%
% DESCRIPTION:  The object update for Thibualt et al algorithm, with probe
%                fixed.
%
% INPUT:       input = a data structure with fields:
%              u     = array to be updated
%
% OUTPUT:      u     = the updated array.
%
% USAGE: u = P_ptychography_Thibault_O(input,u)
%
% Nonstandard Matlab function calls:

function u = P_ptychography_Thibault_O(input,u)

phi    = u.phi;
object = u.object;
probe  = u.probe;



itstop = 3;

dummy_obj_ones = ones(input.object_size);

this_Probe = probe;
this_object = object;

% Solve the equation system
prch0 = 0;
for i=1:itstop
    
    % new object
    % -------------------------------------------------------------
    obj_enum = 1e-30*dummy_obj_ones;
    obj_denom = 1e-30*dummy_obj_ones;
    I_Probe = abs(this_Probe).^2;
    c_Probe = conj(this_Probe);
    for pos=1:input.N_pie
        Indy = input.positions(pos,1) + (1:input.Ny);
        Indx = input.positions(pos,2) + (1:input.Nx);
        
        obj_enum(Indy,Indx) = obj_enum(Indy,Indx) + c_Probe.*phi(:,:,pos);
        obj_denom(Indy,Indx) = obj_denom(Indy,Indx) + I_Probe;
    end
    this_object = obj_enum ./ obj_denom;
    
    
    if input.switch_object_support_constraint
        this_object = this_object .* input.object_support;
    end
    
    % Prohibit values larger 1
    trans_max_true = input.trans_max_true;
    trans_min_true = input.trans_min_true;
    abs_object = abs(this_object);
    high = (abs_object > trans_max_true);
    low = (abs_object < trans_min_true);
    this_object = (1-low).*(1-high).*this_object + (low*trans_min_true + high*trans_max_true).*this_object./(abs_object+1e-30);
        
    Probe_old = this_Probe;
    
    if input.switch_probemask
        %Mask in sample plane
        %Probe_norm = norm_avg(this_Probe);
        this_Probe = this_Probe.*input.Probe_mask;
        %this_Probe = this_Probe .* Probe_norm/norm_avg(this_Probe);
    end
    
    
end

%datasize = [input.Ny,input.Nx,input.ny*input.nx];
%phi = ones(datasize);



if(strcmp(input.ptychography_prox,'Thibault'))
    for pos=1:input.N_pie
        Indy = input.positions(pos,1) + (1:input.Ny);
        Indx = input.positions(pos,2) + (1:input.Nx);
        phi(:,:,pos) = this_Probe.*this_object(Indy,Indx);
    end
    u.phi = phi;
end


u.phi = phi;
u.object = this_object;

end

