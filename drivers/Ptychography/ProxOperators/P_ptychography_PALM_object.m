%                P_ptchography_PALM_object.m
%              written on 16th January 2014
%                       Matthew Tam
%                      CARMA Centre
%                  University of Newcastle
%            last modified on 29th January 2014
%
% DESCRIPTION:  The object update for the PALM algorithm.
%
%

function u = P_ptychography_PALM_object(input,u)

xSum   = zeros(size(u.object));
phiSum = zeros(size(u.object));
%supportCount = zeros(size(u.object));
%countSum = zeros(size(u.object));
modSqProbe = conj(u.probe) .* u.probe;
conjProbe  = conj(u.probe);

for xPos=1:input.N_pie
    Indy = input.positions(xPos,1) + (1:input.Ny);
    Indx = input.positions(xPos,2) + (1:input.Nx);
    xSum(Indy,Indx) = xSum(Indy,Indx) + modSqProbe;
    phiSum(Indy,Indx) = phiSum(Indy,Indx) + conjProbe .* u.phi(:,:,xPos);
    %supportCount(Indy,Indx) = supportCount(Indy,Indx) + ((conj(input.probe_guess).*input.probe_guess)>0.1);  
    %countSum(Indy,Indx) = countSum(Indy,Indx) + ones(size(u.probe));
end

if ~isfield(input,'overrelax')
   input.overrelax=1;
end
xSumDenom = input.overrelax*max(max(xSum));
u.object = u.object - (xSum .* u.object - phiSum ) / (xSumDenom + 1e-30);

% Apply the support constraint.
%u.object  = u.object .* (supportCount>1);
%u.object = u.object .* input.object_support;

% Enforce that the object has values in [minTrue,maxTrue]
maxTrue = 1;
minTrue = 0;
absObject = abs(u.object);
high = (absObject > maxTrue);
low  = (absObject < minTrue);
u.object = (1-low) .* (1-high) .* u.object + (low * minTrue + high * maxTrue) .* u.object ./ (absObject + 1e-30);

end

