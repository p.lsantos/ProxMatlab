%               P_ptychography_PHeBIE_probe_ptwise.m
%              written on 16th January 2014
%                       Matthew Tam
%                      CARMA Centre
%                  University of Newcastle
%              last modified on 23rd July 2014
%
% DESCRIPTION:  The probe update for the PHeBIE algorithm.
%
%

function u = P_ptychography_PHeBIE_probe_ptwise(input,u)

ySum   = zeros(size(u.probe));
phiSum = zeros(size(u.probe));
conjObject = conj(u.object);

for yPos=1:input.N_pie
    Indy = input.positions(yPos,1) + (1:input.Ny);
    Indx = input.positions(yPos,2) + (1:input.Nx);
    ySum = ySum + abs(u.object(Indy,Indx)).^2;
    phiSum = phiSum + conjObject(Indy,Indx) .* u.phi(:,:,yPos);
end

if ~isfield(input,'overrelax')
   input.overrelax=1;
end
ySumDenom = input.overrelax * max(ySum,1e-30);
u.probe = u.probe - (ySum .* u.probe - phiSum) ./ ySumDenom;

% Mask in the sample plane?
if input.switch_probemask
    u.probe = u.probe .* input.Probe_mask;
end

% Make sure that the probe is bounded by some large number.
absProbe = abs(u.probe);
high = (absProbe > 10e30);
% Do only if needed.
if max(max(high)) == 1
    u.object = (1-high) .* u.object + (high * 10e30) .* u.probe ./ (absProbe + 1e-30);
end

end

