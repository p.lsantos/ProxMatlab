%                P_ptchography_PHeBIE_object.m
%              written on 16th January 2014
%                       Matthew Tam
%                      CARMA Centre
%                  University of Newcastle
%            last modified on 11th June 2014
%
% DESCRIPTION:  The object update for the PHeBIE algorithm.
%
%

function u = P_ptychography_PHeBIE_object(input,u)

xSum   = zeros(size(u.object));
phiSum = zeros(size(u.object));
modSqProbe = conj(u.probe) .* u.probe;
conjProbe  = conj(u.probe);

for xPos=1:input.N_pie
    Indy = input.positions(xPos,1) + (1:input.Ny);
    Indx = input.positions(xPos,2) + (1:input.Nx);
    xSum(Indy,Indx) = xSum(Indy,Indx) + modSqProbe;
    phiSum(Indy,Indx) = phiSum(Indy,Indx) + conjProbe .* u.phi(:,:,xPos);
end

if ~isfield(input,'overrelax')
   input.overrelax=1;
end
xSumDenom = input.overrelax * max(max(max(xSum),1e-30));
u.object = u.object - (xSum .* u.object - phiSum ) / xSumDenom;

%Apply the support constraint.
if input.switch_object_support_constraint
    u.object = u.object .* input.object_support;
end

% Enforce that the object has values in [minTrue,maxTrue]
maxTrue = input.trans_max_true;
minTrue = input.trans_min_true;
absObject = abs(u.object);
high = (absObject > maxTrue);
low  = (absObject < minTrue);
u.object = (1-low) .* (1-high) .* u.object + (low * minTrue + high * maxTrue) .* u.object ./ (absObject + 1e-30);

end

