%                 P_ptychography_PALM_probe.m
%              written on 16th January 2014
%                       Matthew Tam
%                      CARMA Centre
%                  University of Newcastle
%             last modified on 29th January 2014
%
% DESCRIPTION:  The probe update for the PALM algorithm.
%
%

function u = P_ptychography_PALM_probe(input,u)

ySum   = zeros(size(u.probe));
phiSum = zeros(size(u.probe));
conjObject = conj(u.object);

for yPos=1:input.N_pie
    Indy = input.positions(yPos,1) + (1:input.Ny);
    Indx = input.positions(yPos,2) + (1:input.Nx);
    ySum = ySum + abs(u.object(Indy,Indx)).^2;
    phiSum = phiSum + conjObject(Indy,Indx) .* u.phi(:,:,yPos);
end

if ~isfield(input,'overrelax')
   input.overrelax=1;
end
ySumDenom = input.overrelax*max(max(ySum));
u.probe = u.probe - (ySum .* u.probe - phiSum) / (ySumDenom + 1e-30);

% Mask in the sample plane?
if input.switch_probemask
    u.probe = u.probe .* input.Probe_mask;
end

end

