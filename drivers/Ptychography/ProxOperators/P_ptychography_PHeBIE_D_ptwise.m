%               P_ptychography_PHeBIE_D_ptwise.m
%              written on 16th April 2015
%                       Russell Luke
%                 Universitaet Goettingen
%
% DESCRIPTION:  The approximate projection onto the set D given by Eq(4.16) in Hesse-Luke-Sabach-Tam (2015)
%
%

function u = P_ptychography_PHeBIE_D_ptwise(input,u)


if ~isfield(input,'maxit_inner')
   input.maxit_inner = 1;
end
for jj=1:input.maxit_inner
   u = feval('P_ptychography_PHeBIE_probe_ptwise',input,u);
   u = feval('P_ptychography_PHeBIE_object_ptwise',input,u);
end


for pos=1:input.N_pie
    Indy = input.positions(pos,1) + (1:input.Ny);
    Indx = input.positions(pos,2) + (1:input.Nx);
    u.phi(:,:,pos) = u.probe.*u.object(Indy,Indx);
end

