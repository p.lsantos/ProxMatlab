%                 P_ptchography_PHeBIE_phi.m
%              written on 16th Janurary 2014
%                       Matthew Tam
%                      CARMA Centre
%                  University of Newcastle
%              last modified 11th June 2014
%
% DESCRIPTION:  The phi update for the PHeBIE algorithm.
%
%

function u = P_ptychography_PHeBIE_phi(input,u)

fnorm = sqrt(input.Ny*input.Nx);
for pos=1:input.N_pie
    Indy = input.positions(pos,1) + (1:input.Ny);
    Indx = input.positions(pos,2) + (1:input.Nx);
    newPhi = u.probe .* u.object(Indy,Indx);
    oldPhiHat = feval('fft2',newPhi) / fnorm;
    phiHat = feval('MagProj',input.Amp_exp_norm(:,:,pos),oldPhiHat);
    if isfield(input,'fmask') %Mask in the Fourier domain.
        phiHat = phiHat .* input.fmask(:,:,pos) + oldPhiHat .* (input.fmask(:,:,pos)==0);
    end
    u.phi(:,:,pos) = feval('ifft2',phiHat) * fnorm;
end

end

