%                  Ptychography_graphics.m
%              written on 15th January 2014
%                       Matthew Tam
%                      CARMA Centre
%                  University of Newcastle
%
% DESCRIPTION:  Script driver for viewing results from ptychography.
%
% INPUT:        input/output = data structures
%
% OUTPUT:       graphics
%
% USAGE:        Ptychography_graphics(input,output)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function success = Ptychography_graphics( input, output )


method=input.method;
if(any(strcmp('beta_0',fieldnames(input))))
  beta0 = input.beta_0;
  beta_max = input.beta_max;
elseif(strcmp(input.method,'ADMMPlus'))
    beta0=input.stepsize;
    beta_max=beta0;
else
    beta0=1;
    beta_max=1;
end

if isfield(output,'u')
    u = output.u; %u is phi (legacy reasons)
elseif isfield(output,'u1')
    u = output.u1;
end
probe  = u.probe;
object = u.object;

if(any(strcmp('stats', fieldnames(output))))
    iter = output.stats.iter;
    change = output.stats.change;
    if(any(strcmp('time', fieldnames(output.stats))))
        time = output.stats.time;
    else
        time=999;
    end
end

%% Choose the appropriate plotting routine, depending on whether the 
%% algorithm has terminated. (i.e., Is this 'final plot' or 'animation' ?)

if isfield(output,'u1') % i.e., Algorithm has terminated.
    %% Final plot routine.
    
    method   = input.method;
    beta0    = input.beta_0;
    beta_max = input.beta_max;
    u_0      = input.u_0;
    u        = output.u1(:,:,1);
    iter     = output.stats.iter;
    change   = output.stats.change;
    if isfield(output.stats,'gap')
        gap      = output.stats.gap;
    end
    if isfield(output.stats,'customError')
        rmserrorobj     = output.stats.customError(:,1);
        rmserrorprobe   = output.stats.customError(:,2);
        changephi       = output.stats.customError(:,3);
        R_factor        = output.stats.customError(:,4);
        objectiveValue  = output.stats.customError(:,5);
    end
    
    if(any(strcmp('truth',fieldnames(input))))
        figure(1)
        subplot(2,2,1)
        imagesc(hsv2rgb(im2hsv(input.true_probe./max(max(abs(input.true_probe))), 1)));
        title('True probe amplitude');
        
        subplot(2,2,3)
        imagesc(log10(abs(input.true_object)));
        colormap gray;
        axis equal tight;
        colorbar;
        title('True object amplitude');
        
        subplot(2,2,4)
        imagesc(angle(input.true_object));
        colormap gray;
        axis equal tight;
        colorbar;
        title('True object phase');
    end
    figure(2)
    subplot(2,2,1)
    imagesc(hsv2rgb(im2hsv(u_0.probe./max(max(abs(u_0.probe))), 1)));
    title('Initial guess: probe amplitude');
    
    subplot(2,2,3)
    imagesc(log10(abs(u_0.object)));
    colormap gray;
    axis equal tight;
    colorbar;
    title('Initial guess: object amplitude');
    
    subplot(2,2,4)
    imagesc(angle(u_0.object));
    colormap gray;
    axis equal tight;
    colorbar;
    title('Initial guess: object phase');
    
    
    % Object amplitude.
    figure(600)
    subplot(1,2,1)
    ampObject = log10(abs(object));
    %ampObject(ampObject<0.5) = ampObject(ceil(end/2),ceil(end/2));
    imagesc(ampObject);
    colormap gray;
    axis equal tight;
    colorbar;
    title('Best object approximation, amplitude');
    
    % Object phase.
    subplot(1,2,2)
    imagesc(angle(object));
    colormap gray;
    axis equal tight;
    colorbar;
    title('Best object approximation, phase');
    
    % Probe.
    figure(602)
    subplot(1,2,1)
    imagesc(hsv2rgb(im2hsv(probe./max(max(abs(probe))), 1)));
    title('Best probe approximation amplitude');
    axis equal tight;
    
    % Objective function value
    if isfield(output.stats,'customError')
        figure(603)
        semilogy(objectiveValue),xlabel('iteration'),ylabel('bbjective function value')
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['Objective value, ' label])
    end
    
    figure(604)
    colormap gray;
    meas = zeros(size(input.sample_plane));
    for pos=1:input.N_pie
        Indy = input.positions(pos,1) + (1:input.Ny);
        Indx = input.positions(pos,2) + (1:input.Nx);
        meas(Indy, Indx) = meas(Indy, Indx)...
            + ones(length(Indy), length(Indx)).*input.Probe_mask;
    end
    imagesc(meas);
    title('Number of measurements, pixelwise')
    colorbar;
    
    figure(700)
    semilogy(change),xlabel('iteration'),ylabel('||u^{k+1}-u^k||^2');
    label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
    title(['Change in iterates, ' label])
    
    if isfield(output.stats,'customError')    
        figure(605)
        subplot(1,2,1)
        semilogy(rmserrorobj),xlabel('iteration'),ylabel('RMS-Error, Object','Interpreter','LaTex')
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['RMS Error Object, ' label])
        subplot(1,2,2)
        colormap gray;
        %newmeas = zeros(size(meas));
        %newmeas(meas > input.N_pie*(1/2)) = meas(meas > input.N_pie*(input.rmsfraction));
        imagesc(input.object_support);
        title('Area over which error is measured.')
        axis equal tight
        colorbar;
        
        figure(606)
        semilogy(rmserrorprobe),xlabel('iteration'),ylabel('RMS-Error, Probe','Interpreter','LaTex')
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['RMS Error Probe, ' label])
        
        figure(607)
        semilogy(changephi),xlabel('iteration'),ylabel(['$\| \Phi_{k+1}-\Phi_{k} \|$'],'Interpreter', 'LaTex')
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['change in phi, ' label])
        
        figure(608)
        plot(R_factor),xlabel('iteration'),ylabel('R-factor');
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(['R-factor, ' label])
        
    end
    success=1;
    
    
else % i.e., Algorithm is still running, and we would like to animate.
    %% Animation routine.
    iter   = input.iter;
    if isfield(output,'change')
        change = output.change;
    end
    
    if(input.anim)       
        [X_obj,Y_obj] = meshgrid((1:input.object_size(2))*input.d1x,(1:input.object_size(1))*input.d1y);       
        if strcmp(input.datatype,'exp')
            % Reconstructed object amplitude.
            figure(100)
            subplot(2,3,1)
            imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),log10(abs(object)));
            colormap(hsv);
            title(['Object Ampl., it  ' num2str(iter)]);
            drawnow
            % Reconstructed object phase.
            subplot(2,3,4)
            imagesc(squeeze(X_obj(1,:)),squeeze(Y_obj(:,1)),angle(object));
            title(['Object Phase, it ' num2str(iter)]);
            drawnow
            
            subplot(2,3,3)
            imagesc(angle(input.sample_plane));
            hold on
            scatter(input.positions(:,2)+floor(input.Nx/2+1),...
                input.positions(:,1)+floor(input.Ny/2+1),...
                (2*input.R/input.d1x)*ones(1,size(input.positions,1)),'o','black');
            hold off
            title('Measurements');
            drawnow
            
            subplot(2,3,6)
            meas = zeros(size(input.sample_plane));
            for pos=1:input.N_pie
                Indy = input.positions(pos,1) + (1:input.Ny);
                Indx = input.positions(pos,2) + (1:input.Nx);
                meas(Indy, Indx) = meas(Indy, Indx)...
                    + ones(length(Indy), length(Indx)).*input.Probe_mask;
            end
            imagesc(meas);
            drawnow
            
            % Probe related.
            subplot(2,3,2)
            imagesc(hsv2rgb(im2hsv(probe./max(max(abs(probe))), 1)));
            title(['Probe function, it  ' num2str(iter)]);
            drawnow
            
            % Plot objective function values.
            if input.MAXIT>1 && isfield(output,'change')
                figure(200)
                if strcmp(input.method(end-3:end),'PHeBIE')
                    semilogy(change),xlabel('iteration'),ylabel('change')
                    label = [''];
                    title(['||u^{k+1}-u^k||^2, ' label])
                    xlim([1 min(ceil(iter/50)*50,input.MAXIT)]);
                else
                    subplot(1,2,1)
                    semilogy(change),xlabel('iteration'),ylabel(['$\| x_{k+1}-x_{k} \|$'],'Interpreter', 'LaTex')
                    label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
                    title(['change, ' label])
                    if isfield(output,'stats')&&isfield(output.stats,'gap')
                        subplot(1,2,2)
                        semilogy(gap),xlabel('iteration'),ylabel('$\sum_j \|x_k - \Pi_M x_k\| + \sum_j \|x_k - \Pi_N x_k\|$','Interpreter','LaTex')
                       label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
                        title(['gap, ' label])
                    end
                end
                drawnow
            end            
        end
        success=1;
    end
end

function im = c2image(a, varargin)
% FUNCTION IM = C2IMAGE(A)
%
% Returns a RGB image of complex array A where
% the phase is mapped to hue, and the amplitude
% is mapped to brightness.

absa = abs(a);
phasea = angle(a);

% (optional second argument can switch between various plotting modes)
abs_range = [];
if nargin==2
    m = varargin{1};
elseif nargin==3
    m = varargin{1};
    abs_range = varargin{2};
else
    m = 1;
end

if isempty(abs_range)
    nabsa = absa/max(max(absa));
else
    nabsa = (absa - abs_range(1))/(abs_range(2) - abs_range(1));
    nabsa(nabsa < 0) = 0;
    nabsa(nabsa > 1) = 1;
end

switch m
    case 1
        im_hsv = zeros([size(a) 3]);
        im_hsv(:,:,1) = mod(phasea,2*pi)/(2*pi);
        im_hsv(:,:,2) = 1;
        im_hsv(:,:,3) = nabsa;
        im = hsv2rgb(im_hsv);
    case 2
        im_hsv = ones([size(a) 3]);
        im_hsv(:,:,1) = mod(phasea,2*pi)/(2*pi);
        im_hsv(:,:,2) = nabsa;
        im = hsv2rgb(im_hsv);
end

end

%For debugging.
if input.anim_pause > 0
    pause(input.anim_pause)
end
end

