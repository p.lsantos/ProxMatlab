%                    blocking_meta.m
%            written on 29th January 2014 by 
%                     Matthew Tam
%                    CARMA Centre
%               University of Newcastle
%           last modified on 10th February 2014
%
% DESCRIPTION: A meta-algorithm used if a blocking scheme is applied.
% Althought this is probably not the most efficient implementation of
% blocking schemes, it can be applied independent of the algorithm.
%
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = blocking_meta(method_input)
%
% Nonstandard Matlab function calls:  Proj1, Proj2, ..., Projm
function method_output = blocking_meta( method_input )

% Local variables.
MAXIT = method_input.MAXIT;
TOL   = method_input.TOL;
u     = method_input.u_0;

if(isempty(method_input.anim))
    anim = 0;           % numeric graphics toggle.
else
    anim = method_input.anim;
end

% Preallocate the error monitors:
change    = zeros(1,MAXIT);
change(1) = 999;
gap       = change;
if isfield(method_input,'error_type') && strcmp(method_input.error_type,'custom')
    numCustomErrors = size(computeErrors(u,u,method_input),2);
    customError = zeros(MAXIT,numCustomErrors);
    for i=1:numCustomErrors
        customError(1,i) = 999;
    end
end

iter = 1;
method_input.iter = iter;

if(anim>=1)
    if(anim==2)
        set(904,'DoubleBuffer','on');
        set(gca,...
            'NextPlot','replace','Visible','off')
        method_input.mov = avifile('RAARmovie.avi');
    end
    method_output.u = u;
    method_output.change = change;
    method_input = feval(method_input.animation, method_input,method_output);
end


inBlock   = method_input.in_block;
numBlocks = max(inBlock);

tmp_u = u;

    
%% Preallocate data-structures for the sub-problems.
subProblemInput = method_input;
subProblemInput.MAXIT   = method_input.block_MAXIT;
subProblemInput.verbose = method_input.block_verbose;
subProblemInput.anim    = method_input.block_anim;
if (subProblemInput.verbose) || (subProblemInput.anim)
    subProblemInput.ignore_error = false; 
else
    subProblemInput.ignore_error = true; %i.e., don't compute errors in sub-problems.
end
    
%  If sub-sub-problems required then preallocate.
if strcmp(method_input.within_blocks_scheme,'sequential')
    subSubProblemInput = method_input;
    subSubProblemInput.MAXIT   = 1; %method_input.block_MAXIT;
    subSubProblemInput.verbose = method_input.block_verbose;
    subSubProblemInput.anim    = method_input.block_anim;
    if (subSubProblemInput.verbose) || (subSubProblemInput.anim)
        subSubProblemInput.ignore_error = false;
    else
        subSubProblemInput.ignore_error = true; %i.e., don't compute errors in sub-problems.
    end
end

%% The main loop.
while((iter<=MAXIT)&&(change(iter)>=TOL))
    if method_input.verbose==1
        iter = iter+1
    else
        iter = iter+1;
    end
    method_input.iter=iter;
    
    if strcmp(method_input.between_blocks_scheme,'averaged')
            newObject = zeros(size(u.object));
            newProbe  = zeros(size(u.probe));
    end
    
    
    for b=1:numBlocks
        u = tmp_u;
        %% Create the sub-problem for the current block.
        %  Get the indices for the current block.
        B = (inBlock==b);
        
        % Get the views for the current block.
        subProblemInput.u_0          = cPtychography(u.phi(:,:,B),u.object,u.probe);
        subProblemInput.positions    = method_input.positions(B,:);
        subProblemInput.Amp_exp_norm = method_input.Amp_exp_norm(:,:,B);
        subProblemInput.N_pie        = sum(B);
        subProblemInput.product_space_dimension = sum(B);
        
        %% Run the algorithm on the current block.
        switch method_input.within_blocks_scheme
            case 'none'
                subProblemOutput = feval(method_input.method,subProblemInput);
        
            case 'sequential'
                % Solve the current block a special scheme (i.e., solve a
                % sub-sub-problem).
                views_in_block = 1:method_input.N_pie;
                views_in_block = views_in_block(B); %views in the current block.
                
                subSubProblemOutput.u_final.object = u.object;
                subSubProblemOutput.u_final.probe  = u.probe;
                %% Loop through the sub-sub-problems.
                for v=1:length(views_in_block)
                    view = views_in_block(v);
                    
                    subSubProblemInput.u_0                 = cPtychography(u.phi(:,:,view),subSubProblemOutput.u_final.object,subSubProblemOutput.u_final.probe);
                    subSubProblemInput.positions           = method_input.positions(view,:);
                    subSubProblemInput.Amp_exp_norm        = method_input.Amp_exp_norm(:,:,view);
                    subSubProblemInput.N_pie               = 1;
                    subSubProblemInput.product_space_dimension = 1;
                    
                    subSubProblemOutput = feval(method_input.method,subSubProblemInput);
                    
                    subProblemOutput.u_final.phi(:,:,v) = subSubProblemOutput.u_final.phi;
                end
                subProblemOutput.u_final.object = subSubProblemOutput.u_final.object;
                subProblemOutput.u_final.probe  = subSubProblemOutput.u_final.probe;
        end
        
        %  At this point in the code the phi, object and probe
        %  coresponding to the sub-problem should be stored as:
        %              subProblemOutput.u_final
        
        %% Update varaibles, according to the method used.
        switch method_input.between_blocks_scheme
            case 'sequential'
                tmp_u.phi(:,:,B) = subProblemOutput.u_final.phi;
                tmp_u.object     = subProblemOutput.u_final.object;
                tmp_u.probe      = subProblemOutput.u_final.probe;
            case 'averaged'
                tmp_u.phi(:,:,B) = subProblemOutput.u_final.phi;
                newObject        = newObject + subProblemOutput.u_final.object;
                newProbe         = newProbe + subProblemOutput.u_final.probe;
        end
        
    end %blocks
    
    %% Update master problem varaibles (if neeeded).
    if strcmp(method_input.between_blocks_scheme,'averaged')
        tmp_u.object = newObject / numBlocks;
        tmp_u.probe  = newProbe  / numBlocks;
    end
    
    %  At this point in the most recent phi, object and probe should be
    %  stored as:   tmp_u
    
    
    
    %% Compute statistics.
    change(iter) = objective(tmp_u,method_input); %value of objective function.
    gap(iter)    = 0;
    if isfield(method_input,'error_type') && strcmp(method_input.error_type,'custom')
        % Use a custom method to compute the errors. The method should be
        % implemented in the iteration class data structure and named
        % ``computeErrors". We the following specification:
        %     'otherErrors', a vector containing other errors of intrest.
        otherErrors = computeErrors(u,tmp_u,method_input);
        customError(iter,:) = otherErrors;
    end
    
    
    % Plot animation.
    if((anim>=1)&&(mod(iter,2)==0))
        method_output.u=tmp_u;
        method_output.change = change;
        method_input=feval(method_input.animation, method_input,method_output);
        pause(0.2)
    end
    
    
    % Update with the current iterate.
    u=tmp_u;
end

%% Collect the output of the algorithm.
method_output.u_final = u;
method_output.u       = u;

tmp = u;
if(method_input.Ny==1)
    method_output.u1 = tmp(:,1);
elseif(method_input.Nx==1)
    method_output.u1 = tmp(1,:);
else
    method_output.u1 = tmp(:,:,1);
end
change = change(2:iter);
gap    = gap(2:iter);

method_output.stats.iter   = iter;
method_output.stats.change = change;
method_output.stats.gap    = gap;

if isfield(method_input,'error_type') && strcmp(method_input.error_type,'custom')
    method_output.stats.customError = customError;
end

if(anim==2)
    method_input.mov = close(method_input.mov);
end
