global max_warmup_it
global max_it
global repetitions

max_warmup_it = 20
max_it = 300
repetitions = 1




input_files={'Ptychography_in_PHeBIE_poisson';...
    'Ptychography_in_Rodenburg_poisson';...
    'Ptychography_in_Thibault_poisson';...
    'Ptychography_in_Thibault_AP_poisson';...
    'Ptychography_in_PHeBIEptwise_noNoise'};

   
%for poissonfactor=[10 20 30 40 50]    
for poissonfactor=[5 10 15 20 25]    
    for i=1:size(input_files)
        input_file = strcat( input_files{i}, num2str(poissonfactor,'%02d') )
        diary(['../OutputData/' datestr(now,'yymmdd') input_file 'Log.txt'])
        [output,input] = main_ProxToolbox(char(input_file));
        diary off
    end
end
   



