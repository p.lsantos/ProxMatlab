% This is the input file that the user sees/modifies.  It should be simple, 
% avoid jargon or acronyms, and should be a model for a menu-driven GUI

function prbl = Ptychography_in()
%% Problem type
%  What type of problem is being solved?  Classification 
%  is according to the geometry:  Affine, Cone, Convex, 
%  Phase, Affine-sparsity, Nonlinear-sparsity, Sudoku
prbl.problem_family = 'Ptychography';

%%==========================================
%% Problem parameters
%%==========================================
%  What is the name of the data file?
prbl.data_filename = 'Ptychography_data_processor';

%  What type of object are we working with?
%  Options are: 'phase', 'real', 'nonnegative', 'complex'
prbl.object     = 'complex';

%  What type of constraints do we have?
%  Options are: 'support only', 'real and support', 'nonnegative and support',
%              'amplitude only', 'sparse real', 'sparse complex', and 'hybrid'.
prbl.constraint = 'amplitude only';

%  What type of measurements are we working with?
%  Options are: 'single diffraction', 'diversity diffraction', 
%              'ptychography', and 'complex'
prbl.experiment = 'ptychography';

%  Next we move to things that most of our users will know 
%  better than we will.  Some of these may be overwritten in the 
%  data processor file which the user will most likely write. 

%%==========================================
%% Input datafile. 
%%==========================================
%  To use a simulated data set prbl.datafile=''.
prbl.data_dir = ['Ptychography_data/InputData/'];
prbl.datafile = '';%'data_S07025_data_800x800'; %use an empty string (ie. '') for simulated data.
prbl.home_dir = '../';

%  The following parameters are used to generate simualted ptychographic
%  data. For convienence, we include a small test data set and all of the
%  data sets from Paer's master thesis (although we may like to move these
%  settings somewhere else?).

%  Small test data
prbl.sim_data_type = 'siemensstar';
prbl.Nx = 64; %dector size in pixels along x-axis.
prbl.Ny = 64; %dector size in pixels along y-axis.
prbl.scan_stepsize = 3.5e-7; %size of the step between ptychographic images.
prbl.nx = 20; %number of images taken along x-axis.
prbl.ny = 20; %number of images taken along y-axis.
prbl.sample_area_centre = [5/8 3/8]; %point in [0,1]*[0,1], the relative coordinate of the centre of the area to be sampled.
prbl.sample_fourier_area = 0.3;

% %  Paer Data Set 1
% prbl.sim_data_type = 'siemensstar';
% P_in.Nx = 256; 
% P_in.Ny = 256; 
% P_in.scan_stepsize = 1e-6;
% P_in.nx = 5;
% P_in.ny = 5;
% prbl.sample_area_centre = [1/2 1/2];
% prbl.sample_fourier_area = 1;

% %  Paer Data Set 2
% prbl.sim_data_type = 'siemensstar';
% P_in.Nx = 256; 
% P_in.Ny = 256; 
% P_in.scan_stepsize = 5e-7; 
% P_in.nx = 10; 
% P_in.ny = 10; 
% prbl.sample_area_centre = [1/2 1/2]; 
% prbl.sample_fourier_area = 1;

% %  Paer Data Set 3
% prbl.sim_data_type = 'siemensstar';
% P_in.Nx = 256; 
% P_in.Ny = 256; 
% P_in.scan_stepsize = 3.75e-7; 
% P_in.nx = 15; 
% P_in.ny = 15; 
% prbl.sample_area_centre = [1/2 1/2]; 
% prbl.sample_fourier_area = 1;

% %  Paer Data Set 4
% prbl.sim_data_type = 'siemensstar';
% P_in.Nx = 256;
% P_in.Ny = 256;
% P_in.scan_stepsize = 2.5e-7; 
% P_in.nx = 20;
% P_in.ny = 20;
% prbl.sample_area_centre = [1/2 1/2];
% prbl.sample_fourier_area = 1;

% %  Paer Data Set 5
% prbl.sim_data_type = 'siemensstar';
% P_in.Nx = 256; 
% P_in.Ny = 256; 
% P_in.scan_stepsize = 5e-7; 
% P_in.nx = 5; 
% P_in.ny = 5; 
% prbl.sample_area_centre = [1/2 1/2]; 
% prbl.sample_fourier_area = 1;

% %  Paer Data Set 6
% prbl.sim_data_type = 'siemensstar';
% P_in.Nx = 256; 
% P_in.Ny = 256; 
% P_in.scan_stepsize = 5e-7; 
% P_in.nx = 15; 
% P_in.ny = 15; 
% prbl.sample_area_centre = [1/2 1/2]; 
% prbl.sample_fourier_area = 1;

% %  Paer Data Set 7
% prbl.sim_data_type = 'siemensstar';
% P_in.Nx = 256; 
% P_in.Ny = 256; 
% P_in.scan_stepsize = 5e-7; 
% P_in.nx = 20; 
% P_in.ny = 20; 
% prbl.sample_area_centre = [1/2 1/2];
% prbl.sample_fourier_area = 1;

% %  Paer Data Set "SimData_parFine.m"
% prbl.sim_data_type = 'siemensstar';
% prbl.Nx = 256;
% prbl.Ny = 256;
% prbl.scan_stepsize = 1.0e-7; 
% prbl.nx = 20; 
% prbl.ny = 20; 
% prbl.sample_area_centre = [1/2 1/2]; 
% prbl.sample_fourier_area = 1;

% Gaenseliesel
prbl.sim_data_type = 'gaenseliesel';
prbl.Nx = 64; %dector size in pixels along x-axis.
prbl.Ny = 64; %dector size in pixels along y-axis.
prbl.scan_stepsize = 3.5e-7; %size of the step between ptychographic images.
prbl.nx = 25; %number of images taken along x-axis.
prbl.ny = 25; %number of images taken along y-axis.
prbl.sample_area_centre = [1/2 1/2]; %point in [0,1]*[0,1], the relative coordinate of the centre of the area to be sampled.

%% What are the noise characteristics?
%  Options are 'poisson', and 'none'.
prbl.noise = 'poisson';
prbl.poissonfactor = 0.5; %only used in poisson noise generation.


%% Should the plotting iterate over all diffraction patterns?
prbl.plot = true;
prbl.animate = false;
%prbl.probe_recon_iter = 5;
prbl.switch_probemask = true; %if this is 'true' then the probemask is applied.
% This variable controls what pixels should contribute to the overall
% error. The rms-fraction is the fraction of pixels (reg. maximum number of
% measurements) that will be used. E.g.: Max Meas = 100, rmsfraction = 1/2
% -> all pixels where over 50 measurements have been made will contribute
% to the error measurement.
prbl.rmsfraction = 1/2;
prbl.probe_mask_gamma = 1; %This multiplicative factor effects how restrictive the probe mask is.

prbl.scan_type = '';  % possible values 'round_roi' or leave '' for raster

prbl.switch_object_support_constraint = true; %apply the support constraint on the object?

%%==========================================
%% Algorithm parameters
%%==========================================
%  How should the probe be initialized?
%  Options are: 'exact', 'exact_amp', 'gauss', 'circle',  'ellipse', 'ones', and 'exact_perturbed'.
prbl.probe_guess_type = 'circle';
%  How should the object be initialized?
%  Options are: 'exact', 'ones, and 'exact_perturbed'.
prbl.object_guess_type = 'random'; 

global max_warmup_it
global max_it
global repetitions
%  Warm-up algorithm parameters.
%  (any other required parameter(s) are copied from the main algorithm).
prbl.warmup        = true; %is a warm-up algorithm to be used?
prbl.warmup_method = 'RAAR_PALM';
prbl.warmup_MAXIT  = max_warmup_it;

%  Main algorithm parameters
prbl.method  = 'RAAR_PALM'; %Use 'PALM', 'Nesterov_PALM', or 'RAAR'.
prbl.numruns = repetitions;  %only used for benchmarking.
prbl.MAXIT   = max_it; %maximum number of iterations
prbl.TOL     = -1;
prbl.TOL2    = 1e-4;

% Set the prox-operators depending on the algorithm to be used.
% Options are: 'PALM', 'Thibault' (Difference-map), 'Thibault_AP' and 'Rodenburg'.
prbl.ptychography_prox = 'Thibault';
prbl.overrelax = 1+1e-30; %Set as a number bigger than 1.

%% Parameters relating to blocking strategies.
prbl.blocking_switch = false;          % Use a blocking scheme?

% How should blocks be decided? Options are: 
%  'one'         (all views in one block)
%  'single_view' (one view per block)
%  'greedy'      (fill blocks without overlap using a greedy algorithm)
%  'divide'      (divide views into (dy)-adic (quad)rants, as specified by block_rows/cols)
%  'split'       (each block contains at most one view from each of the (quad)rants in the 'divide' strategy.)
prbl.blocking_scheme = 'divide';           

prbl.between_blocks_scheme = 'sequential';   % Scheme between blocks? Options are: 
                                           % 'sequential' (default) ,or 'averaged'.
prbl.within_blocks_scheme  = 'sequential'; %  Scheme within blocks? Options are:
                                           % 'none', or 'sequential'.
prbl.block_rows = 2;    % The number of rows/columns used. Only applies if
prbl.block_cols = 2;    % 'divide' or 'orthogonal' blocking schemes are used.
prbl.block_MAXIT   = 1; % Maximum number of iteration used in each sub-problem.
prbl.block_verbose = 0; % Sub-problems verbose?
prbl.block_anim    = 0; % Sub-problem animations?

%% Aditional parameters required for specific algorithms.
%  The number of iteration in the inner loop if Rodenburg is used.
prbl.RodenburgInnerIt = 1;

%  Relaxaton parameters used in RAAR, HPR and HAAR
prbl.beta_0      = 1;  % starting relaxation prameter (only used with
                       % HAAR, HPR and RAAR)
prbl.beta_max    = 1;  % maximum relaxation prameter (only used with
                       % HAAR, RAAR, and HPR)
prbl.beta_switch = 30; % iteration at which beta moves from beta_0 -> beta_max

%%==========================================
%% parameters for plotting and diagnostics
%%==========================================
prbl.verbose  = 0; % options are 0 or 1
prbl.graphics = 0; % whether or not to display figures, options are 0 or 1.
                   % default is 1.
prbl.anim     = 0; % whether or not to disaply ``real time" reconstructions
                   % options are 0=no, 1=yes, 2=make a movie
                   % default is 1.
prbl.graphics_display = 'Ptychography_graphics_nodisplay'; % options are:
                                                 % 'Ptychography_graphics'(for GUI MATLAB use),  and
                                                 % 'Ptychography_graphics_nodisplay'(for use when MATLAB is run from the command line)
                                                 % NB. MATLAB gives a segmentation fault if the LaTeX interpreter is used together with the "-nodisplay" flag. 
prbl.anim_pause = -1; %The about of time (sec) to pause on each animation drawn.
                            
%%======================================================================
%%  Technical/software specific parameters
%%======================================================================
%  Given the parameter values above, the following technical/algorithmic
%  parameters are automatically set.  The user does not need to know 
%  about these details, and so probably these parameters should be set in 
%  a module one level below this one.  

%  How is the error to be calculated? 
%  Options are: 'default', or 'custom'. If 'custom' is used the error
%  function 'customError' must be defined in the data structure (i.e., the
%  class of the iterate.)
prbl.error_type = 'custom';

end
