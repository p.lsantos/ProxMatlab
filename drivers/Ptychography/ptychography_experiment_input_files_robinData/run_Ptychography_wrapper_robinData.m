global max_warmup_it
global max_it
global repetitions

max_warmup_it = 20
max_it = 300
repetitions = 5




for poissonFact=0:1:0

    poissonFact
    
    if poissonFact == 0
       input_files={'Ptychography_in_PALM_robinData';...
           'Ptychography_in_Nesterov_PALM_robinData';...
           'Ptychography_in_Rodenburg_robinData';...
           'Ptychography_in_Thibault_AP_robinData';...
           'Ptychography_in_Thibault_robinData'};
    else
       input_files={['Ptychography_in_PALM_poisson' num2str(poissonFact)];...
           ['Ptychography_in_Nesterov_PALM_poisson' num2str(poissonFact)];...
           ['Ptychography_in_Rodenburg_poisson' num2str(poissonFact)];...
           ['Ptychography_in_Thibault_AP_poisson' num2str(poissonFact)];...
           ['Ptychography_in_Thibault_poisson' num2str(poissonFact)]};
    end

   
    
    for i=5%1:size(input_files)
        input_file = input_files{i}
        diary(['../OutputData/' datestr(now,'yymmdd') input_file 'Log.txt'])
        [output,input] = main_ProxToolbox(char(input_file));
        diary off
    end
   
end



