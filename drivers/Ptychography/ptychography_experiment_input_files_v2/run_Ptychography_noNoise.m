global max_warmup_it
global max_it
global repetitions

max_warmup_it = 2
max_it = 10
repetitions = 1




input_files={'Ptychography_in_PHeBIE_noNoise';...
    'Ptychography_in_Rodenburg_noNoise';...
    'Ptychography_in_Thibault_noNoise';...
    'Ptychography_in_Thibault_AP_noNoise';...
    'Ptychography_in_Thibault_RAAR_noNoise'};

   
    
for i=1:size(input_files)
    input_file = input_files{i}
    diary(['../OutputData/' datestr(now,'yymmdd') input_file 'Log.txt'])
    [output,input] = main_ProxToolbox(char(input_file));
    diary off
end
   



