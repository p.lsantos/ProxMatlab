directory = ['../../../OutputData/PtychographyArticle'];
% directory = ['/scratch/ProxToolbox/OutputData/clipped0810/140512Ptychography'];
addpath(directory)
addpath('../ProxOperators','../Ptychography_data', '../',...
        '../Ptychography_data/InputData','../Ptychography_data/Robin',...
        '../../../Utilities')

close all
clear all


<<<<<<< .mine
input_files={'Ptychography_in_PHeBIE_mask';...
    'Ptychography_in_PHeBIEptwise_mask';...
    'Ptychography_in_DRlPHeBIE09_mask';...
    'Ptychography_in_DRlPHeBIE07_mask';...
    'Ptychography_in_DRlPHeBIE05_mask';...
    'Ptychography_in_DM_mask';...
    'Ptychography_in_Rodenburg_nomask';...
=======
input_files={'Ptychography_in_PHeBIE';...
    'Ptychography_in_PHeBIEptwise';...
    'Ptychography_in_DM';...
    'Ptychography_in_Rodenburg';...
>>>>>>> .r1776
    };
<<<<<<< .mine
theMethods = {'PHeBIE-I' 'PHeBIE-II' 'DRlPHeBIE09' 'DRlPHeBIE07' 'DRlPHeBIE05' 'Thibault'  'Rodenburg'};
whattodo=[2 3 4 6 7];
theMethods(whattodo)

theColour = [[0 0 .5]; [1 0 0]; [.5 0 .5]; [0 1 1]; [0 .5 .5]; [.5 .5 0]; [1 1 0]];
% theColour = [[0 0 128]; [128 0 0]; [255 0 0]; [100 0 0]; [128 0 128]; [255 0 255]; [0 255 255]];
=======
theMethods = {'PHeBIE-I' 'PHeBIE-II' 'Thibault' 'Rodenburg' };
theColour = [[0 0 .5]; [1 0 0]; [0 1 1]; [0 .5 .5]];
>>>>>>> .r1776
cPtychography(1, 1, 1);
<<<<<<< .mine
=======
whattodo=[ 1 2 3 4];
>>>>>>> .r1776
 stringlength=32;
 datasets=7;
<<<<<<< .mine
 linetype={':o' '-' '-.' '--' ':+ ' ':d' ':x'};
=======
 linetype={':o' '-' '-.' '--'};
>>>>>>> .r1776
 figure(1), axes('FontSize', 14), hold on 
 figure(2), axes('FontSize', 14), hold on
 figure(3), axes('FontSize', 14), hold on
 figure(4), axes('FontSize', 14), hold on
 k=0;
 for j=whattodo
     k=k+1;
%     parameter_filename=input_files(j:datasets:stringlength*datasets);
    parameter_filename=input_files{j};
    currentmethod=theMethods{j};
%     jj=length(parameter_filename);
%     while strcmp(parameter_filename(jj),' ')
%         jj=jj-1;
%     end
%     parameter_filename=parameter_filename(1:jj);
    load([parameter_filename '_run1.output.mat'])
    load([parameter_filename '_run1.input.mat'])
    clear statset
    statset(:,1:5)=output.stats.customError(2:end,:);
    statset(:,6)=output.stats.change(2:end)';
    objectset=output.u_final.object;
    probeset=output.u_final.probe;
    
        probeFit  = dftregistration(fft2(input.probe), fft2(probeset));           
        rowShift = probeFit(3);
        colShift = probeFit(4);
        shiftedObject = circshift(objectset, [rowShift colShift]);
        shiftedprobe=circshift(probeset, [rowShift colShift]);
        clear objectset probeset
        objectset=shiftedObject(92:end-92,92:end-92);
        probeset=shiftedprobe;
        
  
    cpu(j)=output.cputime;
    figure(1), semilogy(statset(:,4), 'Color',theColour(j,:), 'LineWidth', 2) 
    figure(2), semilogy(statset(:,6), 'Color',theColour(j,:), 'LineWidth', 2) 
    figure(815), axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
    figure(816), axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
    figure(815), colormap('gray'), 
        subplot(2,ceil(max(size(whattodo))/2),k),   
        ampObject = (abs(objectset));
        %ampObject(ampObject<0.5) = ampObject(ceil(end/2),ceil(end/2));
        imagesc(ampObject), axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
        % imagesc(abs(objectset)), axis('off'), caxis([-.01, .01]), 
        title(currentmethod),  xlabel(currentmethod)
    figure(816),colormap('gray'),
        subplot(2,ceil(max(size(whattodo))/2),k),
        imagesc(angle(objectset)), axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
        % imagesc(abs(objectset)), axis('off'), caxis([-.01, .01]), 
        title(currentmethod), xlabel(currentmethod)
    figure(817)
        subplot(2,ceil(max(size(whattodo))/2),k),
%         axes('FontSize', 14), colormap('gray'); 
        imagesc(hsv2rgb(im2hsv(probeset./max(max(abs(probeset))), 1)));
        axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
        title(currentmethod),  xlabel(currentmethod)
 end
 
lgnd1=[theMethods{1} ': ' num2str(cpu(1)) 's']; 
lgnd2=[theMethods{2} ': ' num2str(cpu(2)) 's']; 
lgnd6=[theMethods{6} ': ' num2str(cpu(6)) 's']; 
lgnd6=[theMethods{7} ': ' num2str(cpu(7)) 's']; 
% theMethodswithtime=theMethods;
for i=whattodo
   theMedhodswithtime{i}=[theMethods{i} ': ' num2str(cpu(i)) 's'];
end
legende=theMedhodswithtime(whattodo);
<<<<<<< .mine
figure(1), legend(legende),set(gca,'yscale','log'); legend boxoff; title('rms error object','FontSize', 16), 
xlabel('iteration','FontSize', 14), hold off

figure(2), legend(legende),set(gca,'yscale','log'); legend boxoff; title('rms error probe','FontSize', 16), 
xlabel('iteration','FontSize', 14),  hold off

figure(3), legend(legende),set(gca,'yscale','log'); legend boxoff; title('change \Phi','FontSize', 16), 
xlabel('iteration','FontSize', 14), hold off

figure(4), legend(legende),set(gca,'yscale','log'); legend boxoff; title('Residual error','FontSize', 16), 
=======
figure(1), legend(legende),set(gca,'yscale','log'); legend boxoff; title('Residual error','FontSize', 16), 
>>>>>>> .r1776
ylabel('R-factor','FontSize', 14), xlabel('iteration','FontSize', 14), hold off
<<<<<<< .mine

figure(5), legend(legende),set(gca,'yscale','log'); legend boxoff; title('Objective Value', 'FontSize', 16), 
ylabel('Objective Value','FontSize', 14), xlabel('iteration','FontSize', 14), hold off
=======
figure(2), legend(legende)
figure(2), xlabel('iteration','FontSize', 14),ylabel('norm iterate change','FontSize', 14),
title('Critical Point Convergence', 'FontSize', 16), hold off
>>>>>>> .r1776

figure(6), legend(legende), set(gca,'yscale','log'); legend boxoff; title('Critical Point Convergence', 'FontSize', 16),
xlabel('iteration','FontSize', 14),ylabel('log of stepsize','FontSize', 14), hold off
 
 if false
    % Save graphics to file.
    figs = get(0,'children');
    directory = ['/scratch/ProxToolbox/OutputData'];
    if ~exist(directory,'dir')
        mkdir(directory);
    end
    for i=1:length(figs)
%         figure_filename = [directory '/' input.ptychography_prox 'beta' num2str(input.beta_max) 'figure' num2str(i)];

        figure_filename = [directory '/' 'figure' num2str(i)];
        saveas(figs(i), figure_filename, 'fig');
        saveas(figs(i), figure_filename, 'png');
        saveas(figs(i), figure_filename, 'epsc');
    end
 end
