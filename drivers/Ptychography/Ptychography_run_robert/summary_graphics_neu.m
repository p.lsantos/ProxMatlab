directory = ['./'];
% directory = ['/scratch/ProxToolbox/OutputData/clipped0810/140512Ptychography'];
addpath(directory)
addpath('../ProxOperators','../Ptychography_data', '../',...
        '../Ptychography_data/InputData','../Ptychography_data/Robin',...
        '../../../Utilities')

close all
clear all


input_files={'Ptychography_in_RAAR09';...
    'Ptychography_in_RAAR07';...
    'Ptychography_in_DM';...
    'Ptychography_in_RAAR05';...
    'Ptychography_in_Rodenburg';...
    'Ptychography_in_RAARauto';...
    'Ptychography_in_ThibaultAP';...
    'Ptychography_in_DM_fattened';...
    'Ptychography_in_PHeBIE';...
    };
<<<<<<< .mine
theMethods = {'DR_\lambdae \lambda=0.9' 'DR_\lambda \lambda=0.7' 'Douglas-Rachford' 'DR_\lambda \lambda=0.5' 'Rodenburg' 'DR_\lambda' 'Alternierende Projektionen' 'not in yet' 'PHeBIE'};
=======
theMethods = {'DR_\lambdae \lambda=0.9' 'DR_\lambda \lambda=0.7' 'Thibault' 'DR_\lambda \lambda=0.5' 'Rodenburg' 'DR_\lambda auto' 'Thibault-AP' 'not in yet' 'PHeBIE'};
>>>>>>> .r1490

cPtychography(1, 1, 1);
<<<<<<< .mine
whattodo=[ 3 7 6  5  ];
=======
whattodo=[ 3 5 7 9];
>>>>>>> .r1490
 stringlength=32;
 datasets=7;
 linetype=['b   '; '-- c'; '-.r '; ':m+ '; '-- c'; '. g ' ;'   b'];
 figure(1), axes('FontSize', 14), hold on 
 figure(2), axes('FontSize', 14), hold on
 figure(3), axes('FontSize', 14), hold on
 figure(4), axes('FontSize', 14), hold on
 figure(5), axes('FontSize', 14), hold on
 figure(6), axes('FontSize', 14), hold on
 k=0;
 for j=whattodo
     k=k+1;
%     parameter_filename=input_files(j:datasets:stringlength*datasets);
    parameter_filename=input_files{j};
    currentmethod=theMethods{j};
%     jj=length(parameter_filename);
%     while strcmp(parameter_filename(jj),' ')
%         jj=jj-1;
%     end
%     parameter_filename=parameter_filename(1:jj);
    load([parameter_filename '_run1.output.mat'])
    load([parameter_filename '_run1.input.mat'])
    statset(:,1:5)=output.stats.customError(2:end,:);
    statset(:,6)=output.stats.change(2:end)';
    objectset=output.u_final.object;
    probeset=output.u_final.probe;
    
        probeFit  = dftregistration(fft2(input.probe), fft2(probeset));           
        rowShift = probeFit(3);
        colShift = probeFit(4);
        shiftedObject = circshift(objectset, [rowShift colShift]);
        shiftedprobe=circshift(probeset, [rowShift colShift]);
        clear objectset probeset
        objectset=shiftedObject(92:end-92,92:end-92);
        probeset=shiftedprobe;
        
  
    cpu(j)=output.cputime;
    figure(1), semilogy(statset(:,1), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(2), semilogy(statset(:,2), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(3), semilogy(statset(:,3), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(4), semilogy(statset(:,4), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(5), semilogy(statset(:,5), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(6), loglog(statset(:,6), linetype(j:datasets:datasets*4), 'LineWidth', 2) 
    figure(815), axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
    figure(816), axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
    figure(815), colormap('gray'), 
        subplot(2,floor(max(size(whattodo))/2),k),   
        ampObject = (abs(objectset));
        %ampObject(ampObject<0.5) = ampObject(ceil(end/2),ceil(end/2));
        imagesc(ampObject), axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
        % imagesc(abs(objectset)), axis('off'), caxis([-.01, .01]), 
        title(currentmethod),  xlabel(currentmethod)
    figure(816),colormap('gray'),
        subplot(2,floor(max(size(whattodo))/2),k),
        imagesc(angle(objectset)), axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
        % imagesc(abs(objectset)), axis('off'), caxis([-.01, .01]), 
        title(currentmethod), xlabel(currentmethod)
    figure(817)
        subplot(2,floor(max(size(whattodo))/2),k),
%         axes('FontSize', 14), colormap('gray'); 
        imagesc(hsv2rgb(im2hsv(probeset./max(max(abs(probeset))), 1)));
        axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
        title(currentmethod),  xlabel(currentmethod)
%         axis equal tight, colorbar('FontSize', 14);
        % imagesc(abs(probeset(:,:,j))), axis('off'), caxis([0, 2000]),      
 end
<<<<<<< .mine
  figure(995), colormap('gray'), 
%         subplot(2,floor(max(size(whattodo))/2)+1,k+1),   
        ampObject = (abs(input.sample_plane(92:end-92,92:end-92)));
        %ampObject(ampObject<0.5) = ampObject(ceil(end/2),ceil(end/2));
        imagesc(ampObject), axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
        % imagesc(abs(objectset)), axis('off'), caxis([-.01, .01]), 
        title('Objekt (Magnitude)'),  xlabel(currentmethod)
  figure(996), colormap('gray'),
%         subplot(2,floor(max(size(whattodo))/2)+1,k+1),
        imagesc(angle(input.sample_plane(92:end-92,92:end-92))), axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
        % imagesc(abs(objectset)), axis('off'), caxis([-.01, .01]), 
        title('Objekt (Phase)'), xlabel(currentmethod)
   figure(997)
%         subplot(2,floor(max(size(whattodo))/2)+1,k+1),
%         axes('FontSize', 14), colormap('gray'); 
        imagesc(hsv2rgb(im2hsv(input.probe./max(max(abs(input.probe))), 1)));
        axis xy, axis equal tight, colorbar('FontSize', 14)
        axis('off')
        title('Beleuchtungsfunktion'),  xlabel(currentmethod)
%         axis equal tight, colorbar('FontSize', 14);
        % imagesc(abs(probeset(:,:,j))), axis('off'), caxis([0, 2000]),
=======
%   figure(815), colormap('gray'), 
%         subplot(2,floor(max(size(whattodo))/2)+1,k+1),   
%         ampObject = (abs(input.sample_plane(92:end-92,92:end-92)));
%         %ampObject(ampObject<0.5) = ampObject(ceil(end/2),ceil(end/2));
%         imagesc(ampObject), axis xy, axis equal tight, colorbar('FontSize', 14)
%         axis('off')
%         % imagesc(abs(objectset)), axis('off'), caxis([-.01, .01]), 
%         title('Reference'),  xlabel(currentmethod)
%   figure(816), colormap('gray'),
%         subplot(2,floor(max(size(whattodo))/2)+1,k+1),
%         imagesc(angle(input.sample_plane(92:end-92,92:end-92))), axis xy, axis equal tight, colorbar('FontSize', 14)
%         axis('off')
%         % imagesc(abs(objectset)), axis('off'), caxis([-.01, .01]), 
%         title('Reference'), xlabel(currentmethod)
%    figure(817)
%         subplot(2,floor(max(size(whattodo))/2)+1,k+1),
% %         axes('FontSize', 14), colormap('gray'); 
%         imagesc(hsv2rgb(im2hsv(input.probe./max(max(abs(input.probe))), 1)));
%         axis xy, axis equal tight, colorbar('FontSize', 14)
%         axis('off')
%         title('Reference'),  xlabel(currentmethod)
% %         axis equal tight, colorbar('FontSize', 14);
%         % imagesc(abs(probeset(:,:,j))), axis('off'), caxis([0, 2000]),
>>>>>>> .r1490
        
lgnd1=[theMethods{1} ': ' num2str(cpu(1)) 's']; 
lgnd2=[theMethods{2} ': ' num2str(cpu(2)) 's']; 
lgnd3=[theMethods{3} ': ' num2str(cpu(3)) 's']; 
lgnd4=[theMethods{4} ': ' num2str(cpu(4)) 's']; 
lgnd5=[theMethods{5} ': ' num2str(cpu(5)) 's']; 
lgnd6=[theMethods{6} ': ' num2str(cpu(6)) 's']; 
lgnd7=[theMethods{7} ': ' num2str(cpu(7)) 's']; 
theMethodswithtime=theMethods;
for i=whattodo
   theMedhodswithtime{i}=[theMethods{i} ': ' num2str(cpu(i)) 's'];
end
legende=theMethodswithtime(whattodo);
figure(1), legend(legende),set(gca,'yscale','log'); legend boxoff; title('rms error object'), hold off
figure(2), legend(legende),set(gca,'yscale','log'); legend boxoff; title('rms error probe'), hold off
figure(3), legend(legende),set(gca,'yscale','log'); legend boxoff; title('change \Phi'), hold off
figure(4), legend(legende),set(gca,'yscale','log'); legend boxoff; title('residual error'), hold off
figure(5), legend(legende),set(gca,'yscale','log'); legend boxoff; title('Objective Value'), ylabel('Objective Value'), hold off
figure(6), legend(legende)
figure(6), xlabel('iteration','FontSize', 14),ylabel('norm iterate change','FontSize', 14),
title('Critical Point Convergence', 'FontSize', 16), hold off

% figure(1), legend(lgnd1, lgnd2, lgnd3, lgnd4, lgnd5, lgnd6, lgnd7),  title('rms error object'), hold off
% figure(2), legend(lgnd1, lgnd2, lgnd3, lgnd4, lgnd5, lgnd6, lgnd7),  title('rms error probe'), hold off
% figure(3), legend(lgnd1, lgnd2, lgnd3, lgnd4, lgnd5, lgnd6, lgnd7),  title('change \Phi'), hold off
% figure(4), legend(lgnd1, lgnd2, lgnd3, lgnd4, lgnd5, lgnd6, lgnd7),  title('R-factor'), hold off
% figure(5), legend(lgnd1, lgnd2, lgnd3, lgnd4, lgnd5, lgnd6, lgnd7),  title('Objective Value'), ylabel('Objective Value'), hold off
% figure(6), legend(lgnd1, lgnd2, lgnd3, lgnd4, lgnd5, lgnd6, lgnd7)  
% figure(6), xlabel('iteration'),ylabel('norm iterate change'), title('Critical Point Convergence'), hold off
%     figure(100), axes('FontSize', 14), colormap('gray'), 
%         subplot(1,2,1)
%         ampObject = log10(abs(u.object));
%         %ampObject(ampObject<0.5) = ampObject(ceil(end/2),ceil(end/2));
%         imagesc(ampObject), axis equal tight, colorbar('FontSize', 14)
%         % imagesc(abs(objectset(:,:,j))), axis('off'), caxis([-.01, .01]), 
%         title('Best object approximation, amplitude '),  xlabel(currentmethod)
%         subplot(1,2,2)
%         imagesc(angle(u.object)), axis equal tight, colorbar('FontSize', 14)
%         % imagesc(abs(objectset(:,:,j))), axis('off'), caxis([-.01, .01]), 
%         title('Best object approximation, phase '), xlabel(currentmethod)
% 
%     figure(101), axes('FontSize', 14), colormap, 
%         imagesc(hsv2rgb(im2hsv(u.probe./max(max(abs(u.probe))), 1)));
%         title('Best probe approximation'),  xlabel(currentmethod)
%         axis equal tight, colorbar('FontSize', 14);
%         % imagesc(abs(probeset(:,:,j))), axis('off'), caxis([0, 2000]),      
 
 if true
    % Save graphics to file.
    figs = get(0,'children');
    directory = ['/scratch/ProxToolbox/OutputData/RobertDefense'];
    if ~exist(directory,'dir')
        mkdir(directory);
    end
    for i=1:length(figs)
%         figure_filename = [directory '/' input.ptychography_prox 'beta' num2str(input.beta_max) 'figure' num2str(i)];

        figure_filename = [directory '/' 'figure' num2str(i)];
        saveas(figs(i), figure_filename, 'fig');
        saveas(figs(i), figure_filename, 'png');
        saveas(figs(i), figure_filename, 'epsc');
    end
 end