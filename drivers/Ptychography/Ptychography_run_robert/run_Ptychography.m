global max_warmup_it
global max_it
global repetitions

max_warmup_it = 20;
max_it = 500;
repetitions = 1;




input_files={'Ptychography_in_PHeBIE_nomask';...
    'Ptychography_in_PHeBIEptwise_nomask';...
    'Ptychography_in_DRlPHeBIE09_nomask';...
    'Ptychography_in_DRlPHeBIE09_mask';...
    'Ptychography_in_DRlPHeBIE07_mask';...
    'Ptychography_in_DRlPHeBIE05_mask';...
    'Ptychography_in_DM_mask';...
    'Ptychography_in_Rodenburg_mask';...
    'Ptychography_in_PHeBIEptwise_mask';...
    };
theMethods = {'PHeBIE_nomask' 'PHeBIEptwise_nomask' 'DRlPHeBIE09_nomask'  'DRlPHeBIE09_mask'  'DRlPHeBIE07_mask' 'DRlPHeBIE05_mask'  'DM_mask'  'Rodenburg_mask' 'PHeBIEptwise_mask' };
whattodo=[ 6 7 8 9];
theMethods(whattodo)
cd ../../
for i=whattodo%1:size(input_files)
    input_file = input_files{i}
    directory = ['../OutputData/' datestr(now,'yymmdd')];
    if ~exist(directory,'dir')
        mkdir(directory);
    end
    diary([directory '/' input_file 'Log.txt'])
    [output,input] = main_ProxToolbox(char(input_file));
    diary off
    % if true
    % Save graphics to file.
    % figs = get(0,'children');
    % directory = ['/scratch/ProxToolbox/OutputData/' datestr(now,'yymmdd') input.problem_family];
    % if ~exist(directory,'dir')
    %    mkdir(directory);
    % end
    % for i=1:length(figs)
%         figure_filename = [directory '/' input.ptychography_prox 'beta' num2str(input.beta_max) 'figure' num2str(i)];

    %    figure_filename = [directory '/' input_file 'figure' num2str(i)];
    %    saveas(figs(i), figure_filename, 'fig');
    %    saveas(figs(i), figure_filename, 'png');
    % end
% end
end
   



%  %% Make some summary figures figures
%  resultsDirectory = directory; %['..//OutputData/' datestr(now,'yymmdd') 'Ptychography/'];
%  summaryDirectory = ['../OutputData/' datestr(now,'yymmdd') '/PtychographySummary/'];
%  if ~exist(summaryDirectory,'dir')
%      mkdir(summaryDirectory);
%  end
%  
%  
%  close all;
%  
%  % R-factor
%  figNum=2;
%  figure(10), axes('FontSize', 14);
%  for m=whattodo
%      input_file = input_files{m};
%      thisFig = open([resultsDirectory input_file 'figure' num2str(figNum) '.fig']);
%      ax = get(gcf,'Children');
%      bx = get(ax(1),'Children');
%      ay = get(bx,'YData');
%      close(thisFig);
%      figure(10);
%      hold on;
%      plot(ay,'Color',theColour(m,:), 'Linetype',theLinetype(m) );
%  end
%  legend(theMethods(whattodo));
%  title('R-factor against Iterations');
%  xlabel('Iterations');
%  ylabel('R-factor');
%  set(gca,'yscale','log');
%  hold off;
%  saveas(gca,[summaryDirectory 'Rfactor'],'fig');
%  saveas(gca,[summaryDirectory 'Rfactor'],'png');
%  % R-factor
%  figNum=4;
%  figure(11);
%  for m=whattodo
%      input_file = input_files{m};
%      thisFig = open([resultsDirectory input_file 'figure' num2str(figNum) '.fig']);
%      ax = get(gcf,'Children');
%      bx = get(ax(1),'Children');
%      ay = get(bx,'YData');
%      close(thisFig);
%      figure(11);
%      hold on;
%      plot(ay,'Color',theColour(m,:), 'Linetype',theLinetype(m) );
%  end
%  legend(theMethods(whattodo));
%  title('RMS-factor Probe against Iterations');
%  xlabel('Iterations');
%  ylabel('R-factor');
%  set(gca,'yscale','log');
%  hold off;
%  saveas(gca,[summaryDirectory 'RMSProbe'],'fig');
%  saveas(gca,[summaryDirectory 'RRSProbe'],'png');
%  % RMS Object-factor
%  figNum=5;
%  figure(15);
%  for m=whattodo
%      input_file = input_files{m};
%      thisFig = open([resultsDirectory input_file 'figure' num2str(figNum) '.fig']);
%      ax = get(gcf,'Children');
%      bx = get(ax(3),'Children');
%      ay = get(bx,'YData');
%      close(thisFig);
%      figure(15);
%      hold on;
%      plot(ay,'Color',theColour(m,:), 'Linetype',theLinetype(m) );
%  end
%  legend(theMethods(whattodo));
%  title('RMS-factor Object against Iterations');
%  xlabel('Iterations');
%  ylabel('R-factor');
%  set(gca,'yscale','log');
%  hold off;
%  saveas(gca,[summaryDirectory 'RMSObjectfactor'],'fig');
%  saveas(gca,[summaryDirectory 'RMSObjectfactor'],'png');
%  % Change in Phi
%  figNum=3;
%  figure(16);
%  for m=whattodo
%      input_file = input_files{m};
%      thisFig = open([resultsDirectory input_file 'figure' num2str(figNum) '.fig']);
%      ax = get(gcf,'Children');
%      bx = get(ax(1),'Children');
%      ay = get(bx,'YData');
%      close(thisFig);
%      figure(16);
%      hold on;
%      plot(ay,'Color',theColour(m,:), 'Linetype',theLinetype(m) );
%  end
%  legend(theMethods(whattodo));
%  title('Change in Phi');
%  xlabel('Iterations');
%  ylabel('norm Phi');
%  set(gca,'yscale','log');
%  hold off;
%  saveas(gca,[summaryDirectory 'ChangeinPhi'],'fig');
%  saveas(gca,[summaryDirectory 'ChangeinPhi'],'png');
%  
