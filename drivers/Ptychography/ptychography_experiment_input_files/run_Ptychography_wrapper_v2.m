global max_warmup_it
global max_it
global repetitions

max_warmup_it = 20
max_it = 1000
repetitions = 5




for poissonFact=0:2:5

    poissonFact
    
    if poissonFact == 0
       input_files={'Ptychography_in_PALM_noNoise';...
           'Ptychography_in_Nesterov_PALM_noNoise';...
           'Ptychography_in_Rodenburg_noNoise';...
           'Ptychography_in_Thibault_AP_noNoise';...
           'Ptychography_in_Thibault_noNoise'};
    else
       input_files={['Ptychography_in_PALM_poisson' num2str(poissonFact)];...
           ['Ptychography_in_Nesterov_PALM_poisson' num2str(poissonFact)];...
           ['Ptychography_in_Rodenburg_poisson' num2str(poissonFact)];...
           ['Ptychography_in_Thibault_AP_poisson' num2str(poissonFact)];...
           ['Ptychography_in_Thibault_poisson' num2str(poissonFact)]};
    end

   
    
    for i=1:size(input_files)
        input_file = input_files{i}
        diary(['../OutputData/' datestr(now,'yymmdd') input_file 'Log.txt'])
        [output,input] = main_ProxToolbox(char(input_file));
        diary off
    end
   
end



