%              GetPtychographyBlocks.m
%          written on 28th January 2014 by 
%                   Matthew Tam
%                  CARMA Centre
%             University of Newcastle
%          last modified 10th February 2014
%
% DESCRIPTION: Generate blocks for the ptychography problem using a
%              specified strategy.
%
% INPUT:  method_input, a data structure
%
% OUTPUT: inBlock, a integer vector of length M whose p-th entry is the
%         block of the p-th image.
%
% USAGE: inBlock = GetPtychographyBlocks( input )
%
% Nonstandard Matlab function calls:  

function inBlock = GetPtychographyBlocks( input )
rows = input.block_rows;
cols = input.block_cols;


%% Initialise book-keeping variables.
inBlock = zeros(input.N_pie,1);
placed = zeros(input.N_pie,1);

%% Used the initial probe estimate to estimate the object support.
probeSupport = conj(input.probe) .* input.probe;
probeSupport = probeSupport > 0.01;
shiftedProbeSupports = zeros([size(input.sample_plane) input.N_pie]);
objectSupport = zeros(size(input.sample_plane));
for j=1:input.N_pie
    Indy = input.positions(j,1) + (1:input.Ny);
    Indx = input.positions(j,2) + (1:input.Nx);
    
    embededProbeSupport = zeros(size(input.sample_plane));
    embededProbeSupport(Indy,Indx) = probeSupport;
    shiftedProbeSupports(:,:,j) = embededProbeSupport;
    
    objectSupport = objectSupport + embededProbeSupport;
    objectSupport = objectSupport > 0.01;
end
probes = shiftedProbeSupports;

%% Apply the selected blocking strategy.
switch input.blocking_scheme
    case 'one'
        placed  = ones(input.N_pie,1);
        inBlock = ones(input.N_pie,1);
    case 'single_view'
        placed  = ones(input.N_pie,1);
        inBlock = 1:input.N_pie;
    case 'greedy'
        % Place the patterns into blocks (using as many as required) using
        % a greedy strategy.
        b = 1;
        while(sum(placed) < input.N_pie)
            thisBlock = zeros(size(input.sample_plane));
            for p=1:input.N_pie
                if placed(p) == 0
                    if ~(sum(sum(thisBlock+ probes(:,:,p) > ones(size(input.sample_plane)))))
                        thisBlock = thisBlock + probes(:,:,p);
                        placed(p) = 1;
                        inBlock(p) = b;
                    end
                end
            end
            b = b + 1;
        end
        
    case 'divide'
        % Divides the object support into an array with 'rows' rows and
        % 'cols' columns. Each block contains all patterns whose position
        % is in a given cell.
        %rows = 2;
        %cols = 2;
        
        positions = input.positions;
        theMax = max(positions) + 1;        
        rowLen = theMax(1) / rows;
        colLen = theMax(2) / cols;
        
        for p=1:input.N_pie
            for r=1:rows
                for c=1:cols
                    b = c + (r-1)*cols;
                    topIndex = positions(p,:) + 1;
                    if (placed(p)==0) && (topIndex(1)>=round((r-1)*rowLen)+1) && (topIndex(1)<=round(r*rowLen)) && (topIndex(2)>=round((c-1)*colLen)+1) && (topIndex(2)<=round(c*colLen))
                        inBlock(p) = b;
                        placed(p) = 1;
                    end
                end
            end
        end
        
    case 'split'
        % Divides the object support into an array with 'rows' rows and
        % 'cols' columns. Each block contains at most one pattern from each
        % cell. 
        %rows = 2;
        %cols = 2;
        
        positions = input.positions;
        theMax = max(positions) + 1;
        rowLen = theMax(1) / rows;
        colLen = theMax(2) / cols;
        
        b = 1;
        while (sum(placed)<input.N_pie)
            for r=1:rows
                for c=1:cols
                    found = 0;
                    p = 1;
                    while (found==0);
                        topIndex = positions(p,:) + 1;
                        if (placed(p)==0) && (topIndex(1)>=round((r-1)*rowLen)+1) && (topIndex(1)<=round(r*rowLen)) && (topIndex(2)>=round((c-1)*colLen)+1) && (topIndex(2)<=round(c*colLen))
                            placed(p) = 1;
                            inBlock(p) = b;
                            found = 1;
                        else
                            if (p==input.N_pie)
                                found = 1;
                            else
                                p = p + 1;
                            end
                        end
                    end
                end
            end
            b = b + 1;
        end
    
    case 'divide_sequential'
        thisInput = input;
        thisInput.blocking_scheme = 'divide';
        divideInBlock = GetPtychographyBlocks(thisInput);
        
        b = 1;
        numBlocks = max(divideInBlock);
        for p=1:numBlocks
            inThisP = (divideInBlock==p);
            v = (1:input.N_pie);
            thisP   = v(inThisP);
            for pp=thisP
                inBlock(pp) = b;
                placed(pp)  = 1;
                b = b + 1;
            end
        end
        
    otherwise
        error('Error: "blocking_strategy" was not recognized.')
end

%% Plot the blocks: a visual check.
if input.block_verbose
    figure
    for p=1:max(inBlock)
        current = zeros(size(input.sample_plane));
        for j=1:input.N_pie
            if inBlock(j) == p
                current = current + probes(:,:,j);
            end
        end
        current = (current>0);
        
        imagesc(objectSupport + 2*current);
        title(strcat('Block number: ',num2str(p),', Block size:',num2str(sum(inBlock==p)),[]));
        pause(0.01);
    end
end
        
%% An error check: Are all patterns in blocks?
if sum(placed) < input.N_pie
    error(strcat('Error: ',num2str(input.N_pie - sum(placed)),' patterns were not placed!'))
end    

end

