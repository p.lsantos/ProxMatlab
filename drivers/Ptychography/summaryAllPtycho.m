function summaryAllPtycho()


%%
% Abbreviations:
%  NN = no noise
%  P = poisson noise
%  R = stricted pupil

close all;
clc
%addpath('drivers/Ptychography');
addpath('Ptychography_data/Robin');

resultsDir = '../../OutputData/';
NNlogs = dir([resultsDir '*noNoiseLog.txt']);
NNlogs = {NNlogs.name};
Plogs = dir([resultsDir '*poisson*Log.txt']);
Plogs = {Plogs.name};
Rlogs = dir([resultsDir '*restrictedPupilLog.txt']);
Rlogs = {Rlogs.name};

resultsSubDir = dir([resultsDir]);
isSub = [resultsSubDir(:).isdir];
resultsSubDir = resultsSubDir(isSub);
resultsSubDir = {resultsSubDir.name};
resultsSubDir(ismember(resultsSubDir,{'.','..'})) = [];



%load([resultsDir '/Ptychography_in_PHeBIE_noNoise_run1.input.mat']);
%trueObject = input.sample_plane;
%trueProbe  = input.probe;

%s(resultsDir)

%%
%% No-noise summary
%%
%etTexTable()


%%
%% Poisson noise summary
%%



%%
%% Restricted Pupil summary
%%


end


function printTexTableStart()
  disp('\begin{table}[htbp]')
  disp('  \caption{Average (Worst) Results for noiseless simulated data}')
  disp('  \vspace{5pt}')
  disp('  \begin{tabular}{lcccccc} \hline')
  disp('    Algorithm                   & $F(u^{300})$      & $\|u^{300}-u^{299}\|^2$ & RMS-Object       & RMS-Probe        & R-factor         & Time (s)          \\ \hline')
end  

function printTexTableLine()
  disp('123');
end

function printTexTableEnd()
  disp('   \end{tabular}')
  disp('  }\\')
  disp('\end{table}')
end






%\begin{table}[htbp]
%\caption{Average (Worst) Results for noiseless simulated data}\label{t:gaensNoNoise}
%\vspace{5pt}
%\resizebox{\textwidth}{!}{
%\begin{tabular}{lcccccc} \hline
%  Algorithm                   & $F(u^{300})$      & $\|u^{300}-u^{299}\|^2$ & RMS-Object       & RMS-Probe        & R-factor         & Time (s)          \\ \hline
% PHeBIE                       &   99.63 (126.64)  &  0.5931 (0.9383)        & 0.0410 (0.0461)  & 0.0155 (0.0222)  & 0.0131 (0.0154)  & 2291.50 (2318.63) \\
% Madien \& Rodenburg          &  948.13 (1499.11) &  5.5163 (8.4885)        & 0.0542 (0.0590)  & 0.0952 (0.1714)  & 0.0350 (0.0419)  & 2285.53 (2291.47) \\
% Thibault {\em et al.}        & 4258.59 (4429.31) & 15.3217 (16.4272)       & 0.0517 (0.0629)  & 0.0140 (0.0171)  & 0.0221 (0.0231)  & 1874.60 (1879.61) \\
% Thibault {\em et al.} AP     &   75.25 (81.22)   &  0.3331 (0.5912)        & 0.0425 (0.0474)  & 0.0095 (0.0151)  & 0.0107 (0.0112)  & 1991.67 (1916.11) \\ \hline
%\end{tabular}
%}\\
%\end{table}
