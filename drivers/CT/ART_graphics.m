%                      ART_graphics.m
%                  written on 29. Mai, 2012 by
%                        Russell Luke
%                  Universität Göttingen
%
% DESCRIPTION:  Script driver for viewing results from projection
%               algorithms on various toy problems
%
% INPUT:  
%              method_input/output = data structures
%
% OUTPUT:       graphics
% USAGE: ART_graphics(method_input,method_output)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function success = ART_graphics(method_input, method_output)
              
method=method_input.method;
beta0 = method_input.beta_0;
beta_max = method_input.beta_max;
u_0 = method_input.u_0(:,1);
N=sqrt(method_input.Ny);
u = reshape(method_output.u1,N, N);
u2 = reshape(method_output.u2,N,N);
iter = method_output.stats.iter;
change = method_output.stats.change;
if(any(strcmp('time', fieldnames(method_output.stats))))
    time = method_output.stats.time;
else
    time=999
end


figure(900);

    subplot(2,2,1); imagesc((u)); colormap gray; axis equal tight; colorbar; title('best approximation - physical domain'); drawnow; 
    subplot(2,2,2); imagesc((u2)); colormap gray; axis equal tight; colorbar; title('best approximation - data constraint'); drawnow; %caxis([4.85,5.35]);
    label = [ 'iteration', ', time = ',num2str(time), 's'];
    subplot(2,2,3);   semilogy(change),xlabel(label),ylabel(['log of iterate difference'])
    label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
    title(label)
    if(any(strcmp('diagnostic', fieldnames(method_input))))        
        gap  = method_output.stats.gap;    
        label = [ 'iteration', ', time = ',num2str(time), 's'];
        subplot(2,2,4);   semilogy(gap),xlabel(label),ylabel(['log of gap distance'])
        label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
        title(label)
    end


success = 1;
return
