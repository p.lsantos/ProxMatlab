%  hyperplane projection subroutine of the
%  ART method  for tomographic recostruction
%  of a density profile. For use with the
%  RCAAR algorithim.
% 
%  Last Modified: Matthew Tam
%   University of Newcastle
%      5th November 2013

function v = P_sequential_hyperplane_RCAAR(input,u0)

[m,n]=size(input.A);
K=input.proj_iter;

a=input.A(K,:);
aaT=(a*a')+1e-20;
b=input.b(K)/aaT;
v=a*u0/aaT;
v=u0+a'*(b-v);


clear u0 b input
return