%  hyperplane projection subroutine of the
%  Cimmino ART method  
%  for tomographic recostruction of a 
%  density profile.
% 
%  Russell Luke
%  Universitaet Goettingen
%  May 29, 2012
%
function u0 = P_block_sequential_hyperplane(input,u0)

% compute the sequential projections on the blocks
% this could easily be parallelized since the blocks are
% independent.  I only do one cycle of this, but should 
% iterate until 'convergence'
for j=1:input.product_space_dimension-1
  for k=input.block_map(j):input.block_map(j+1)-1  
      a=input.A(k,:);
      aaT=(a*a')+1e-20;
      b=input.b(k)/aaT;
      v=a*u0(:,j)/aaT;
      u0(:,j)=u0(:,j)+a'*(b-v);
  end
end
for k=input.block_map(input.product_space_dimension):input.full_product_space_dimension  
      a=input.A(k,:);
      aaT=(a*a')+1e-20;
      b=input.b(k)/aaT;
      v=a*u0(:,end)/aaT;
      u0(:,end)=u0(:,end)+a'*(b-v);
end

% should probably iterate the above proceedure until 'convergence',
% but one step is close enough if the blocks consist of orthogonal 
% elemenets...

clear v b aaT input
return