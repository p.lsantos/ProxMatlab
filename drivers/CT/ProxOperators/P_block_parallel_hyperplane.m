%  hyperplane projection subroutine of the
%  Cimmino ART method  
%  for tomographic recostruction of a 
%  density profile.
% 
%  Russell Luke
%  Universitaet Goettingen
%  May 29, 2012
%
function v0 = P_block_parallel_hyperplane(input,u0)

v0=zeros(size(u0));
u=zeros(input.Ny,input.full_product_space_dimension); 
% expand the blocks:
for j=1:input.product_space_dimension-1
  u(:,input.block_map(j):input.block_map(j+1)-1)=...
      u0(:,j)*ones(1,input.block_map(j+1)-input.block_map(j));
end
u(:,input.block_map(input.product_space_dimension):end)=...
    u0(:,end)*ones(1,input.full_product_space_dimension-input.block_map(end)+1);

% compute the micro-projections
AAT=diag(input.A*input.A')+1e-20;
b=input.b./AAT;
v=diag(input.A*u)./AAT;
v=u+input.A'.*(ones(input.Ny,1)*(b-v)');

% now average and condense the blocks
for j=1:input.product_space_dimension-1
  v0(:,j)=...
      sum(v(:,input.block_map(j):input.block_map(j+1)-1),2)/(input.block_map(j+1)-input.block_map(j));
end
v0(:,end)=...
      sum(v(:,input.block_map(input.product_space_dimension):end),2)/(length(v)-input.block_map(input.product_space_dimension));
% should probably iterate the above proceedure until 'convergence',
% but one step is close enough if the blocks consist of orthogonal 
% elemenets...

clear u0 u v b AAT input
return