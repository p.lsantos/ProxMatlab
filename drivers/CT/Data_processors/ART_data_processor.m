%                  ART_data_processor.m
%                written on May 27, 2012 by
%                     Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  
%
% INPUT: input = a data structure
% OUTPUT: input = a data structure
%
% USAGE: input = ART_data_processor(input) 
%
% Data loaded:  
% Nonstandard function calls:  Rbin, ZeroPad, Mgen, Resize, OnesPad
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function input = ART_data_processor(input)

addpath('../InputData/CT')
if(isempty(strfind(path,'InputData/CT')))
   disp('******************************************************************')
   disp('* Input data missing.  Please download the CT input data from    *') 
   disp('*    http://vaopt.math.uni-goettingen.de/data/CT.tar.gz          *')
   disp('* Save and unpack the CT.tar.gz datafile in the                  *')
   disp('*    ProxMatlab/InputData/CT subdirectory                        *')
   disp('******************************************************************')
   return
end
data_ball = input.data_ball;

% load toy data
    disp(['Loading data file ART_SheppLogan.mat '])
    load ART_SheppLogan.mat

input.Ny=N^2;
input.inner_dimension=p;
input.outer_dimension=length(theta);
input.Nx=1; % input.inner_dimension*input.outer_dimension;
input.Nz=1;
input.block_step=p; % ceil(input.inner_dimension/N); % p
% the next is a generic scaling
% that removes the dependence of the 
% norms from the problem dimension. 
% More specific normalizations are up to the user. 
input.norm_data = sqrt(input.Ny);

input.A=A;
input.b=b_ex;
if any(strcmp('rescale',fieldnames(input)))
    tmp=(diag(A*A')+1e-20).^(-1);
    input.A=(diag(tmp))*A;
    input.b=b_ex.*tmp;
end
input.product_space_dimension=input.block_step*input.outer_dimension;
input.u_0=zeros(N^2,input.product_space_dimension);
% input.product_space_dimension=length(b_ex);
clear A b_ex x_ex theta 
end

