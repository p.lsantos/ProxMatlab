%                  ART_data_processor.m
%                written on May 27, 2012 by
%                     Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  
%
% INPUT: input = a data structure
% OUTPUT: input = a data structure
%
% USAGE: input = BLOCK_ART_data_processor(input) 
%
% Data loaded:  
% Nonstandard function calls:  Rbin, ZeroPad, Mgen, Resize, OnesPad
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function input = BLOCK_ART_data_processor(input)

% load toy data
addpath('../InputData/CT')
if(isempty(strfind(path,'InputData/CT')))
   disp('******************************************************************')
   disp('* Input data missing.  Please download the CT input data from    *') 
   disp('*    http://vaopt.math.uni-goettingen.de/data/CT.tar.gz          *')
   disp('* Save and unpack the CT.tar.gz datafile in the                  *')
   disp('*    ProxMatlab/InputData/CT subdirectory                        *')
   disp('******************************************************************')
   return
end
if(~strcmp(input.fanbeam,'yes'))
     disp('Loading data file ART_SheppLogan.mat ')
     load ART_SheppLogan.mat
else
     disp('Loading data file ART_SheppLogan_fanbeam.mat ')
     load ART_SheppLogan_fanbeam.mat    
end    

[~, input.Ny]=size(A);
input.Nx=1;
input.Nz=1;
input.inner_dimension=p;
input.outer_dimension=length(theta);
input.full_product_space_dimension=p*input.outer_dimension;
% I rearrange the rows of the matrix A and the corresponding
% measurements b_ex so that the blocks are contiguous.
% The projectors downstream will always presume a contiguous 
% block structure with possibly different block sizes.  How
% the blocks are constructed is completely up to the user and 
% will be defined in the data processor as per this example. 
% For graphics, the user will also have to undo the block 
% packing in a corresponding graphics module.  
% This makes it easier to have blocks of different sizes and 
% different structures since the projections are relatively dumb.
% The block sizes are saved and passed to the projector
% so that the projector knows how to expand the blocks
% for the microprojections, but the projectors won't know the 
% phyical ordering of the data. 

%----------------------------------
%
% Rearrange the data into blocks.
% This is not the only way to do it and 
% in general will be data specific.  Since this
% example is a parallel beam CT scan, I block my data
% into parallel beams (rows of A) at the same angle with enough 
% space between the beams (rows of A) so that no two beams will 
% pass through the same pixel (i.e. so that the grouped rows 
% are orthogonal).  This way the corresponding 
% images WITHIN THE BLOCKS will be orthogonal and hence independent.
%  The blocks will in general be of different sizes.
%----------------------------------

block_step= ceil(input.inner_dimension/N); % p

tmp_A=A;
tmp_b=b_ex;
% block_map keeps track of how many rows are in the
% individual blocks
input.product_space_dimension=block_step*input.outer_dimension;
block_map=zeros(input.product_space_dimension,1);
% the next vector tells us how to rearrange the
% solution x to the reordered problem so that it
% matches the original physical representation
pointer_map=zeros(size(tmp_b));
block_map(1)=1;
for k=1:input.outer_dimension
    block_index_skip=0;
    for j=1:block_step
        block_index=(j:block_step:input.inner_dimension);
        block_size=length(block_index);
        tmp_A((k-1)*input.inner_dimension+block_index_skip+(1:block_size),:)=...
            A((k-1)*input.inner_dimension+block_index,:);
        tmp_b((k-1)*input.inner_dimension+block_index_skip+(1:block_size))=b_ex((k-1)*input.inner_dimension+block_index);
        pointer_map((k-1)*input.inner_dimension+block_index_skip+(1:block_size))=((k-1)*input.inner_dimension+block_index);
        block_index_skip=block_size + block_index_skip;
        block_map((k-1)*block_step+j+1)=block_size+block_map((k-1)*block_step+j);
    end
end
block_map=block_map(1:end-1);
% the next is a generic scaling
% that removes the dependence of the 
% norms from the problem dimension. 
% More specific normalizations are up to the user. 
input.norm_data = sqrt(input.Ny);

input.A=tmp_A;
input.b=tmp_b;
input.block_map=block_map;
input.pointer_map=pointer_map;
input.u_0=zeros(N^2,input.product_space_dimension);
% input.product_space_dimension=length(b_ex);
clear A b x theta 
end

