%                      AART_animation.m
%                  written on May 23, 2012 by 
%                         Russell Luke
%       Inst. Fuer Numerische und Angewandte Mathematik
%                    Universitaet Gottingen
%
%
%
function [input] = ART_animation(input,output)
N=sqrt(input.Ny);
u = reshape(output.u(:,1),N, N);

if input.iter==1
figure(999)
messungen=reshape(input.b,input.inner_dimension,input.outer_dimension);
imagesc(messungen)
end
figure(904)
  % colormap('gray');
  imagesc(u);colormap gray; axis equal tight;
  title(['iteration ',num2str(input.iter)])
  drawnow
end
