%                      Sensor_location.m
%             written on Aug 11, 2016 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen

%
%
% This module sets the projectors based on the problem type
% within the problem family 'Sensor_location'
function [input, output] = Sensor_location(input)

%%===============================================================
%% Rewrite fields in our representation:  this bit needs work to 
%%   standardise, particularly for the object domain constraints
%%===============================================================
if(strcmp(input.instance, 'JWST'))
    %% reshape and rename the data 
    input.data_sq = input.data;
    input.data = input.rt_data;
    tmp=size(input.data);
    if(tmp(1)~=input.Nx)
	input.data_sq=reshape(input.data_sq,input.Nx,input.Ny);
	% the projection algorithms work with the square root of the measurement:
	input.data=reshape(input.data,input.Nx,input.Ny);
    end
    input.norm_data=input.norm_rt_data;

    if(any(strcmp('Nz',fieldnames(input))))
    else
	input.Nz = 1;
    end
    % set the animation program:
    input.animation='Phase_animation';
end


%% If input.formulation is does not exist, i.e. not specified in 
%% the *_in.m file, use the product space as the default.
if(any(strcmp('formulation',fieldnames(input))))
    formulation = input.formulation;
else
    formulation = 'product space';
end


%% Set the projectors and inputs based on the types of constraints and 
%% experiments
if(~strcmp(input.instance, 'JWST'))&&(~strcmp(input.method,'RAAR'))
    input.Proj1 = 'Project_on_diagonal';
    input.Proj2 = 'Project_on_balls'; % 
elseif(~strcmp(input.instance, 'JWST'))&&(strcmp(input.method, 'RAAR'))
    input.Proj1 = 'Project_on_shifted_diagonal';
    input.Proj2 = 'Project_on_spheres'; % 
elseif(strcmp(input.instance, 'JWST'))&&(strcmp(input.method, 'RAAR'))
    input.Proj1 = 'Project_on_JWST_diagonal';
    input.Proj2 = 'Project_on_JWST_spheres'; % 
elseif(strcmp(input.instance, 'JWST'))&&(~strcmp(input.method, 'RAAR'))
    input.Proj1 = 'Project_on_JWST_diagonal';
    input.Proj2 = 'Project_on_JWST_spheres'; % 
end    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  translate variables into more general names
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% nothing necessary

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Set parameters and initialization if use has not
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(~any(strcmp('shift_data',fieldnames(input)))) % used to be called receiver_data
    input.shift_data=0; % default receiver location
else
end

if(~any(strcmp('TOL2',fieldnames(input))))
    input.TOL2 = 1e-15; 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  FOR BENCHMARKING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(input.run_number>1)
    if(strcmp(input.instance, 'JWST'))        
        % initial guess
        rng(5+input.run_number)
        newres = input.Nx;
        % input.u_0 = Xi_A.*exp(1i*2*pi*rand(newres));
        for j=1:input.product_space_dimension
            input.u_0(:,:,j) = input.abs_illumination.*exp(1i*2*pi*rand(newres));
        end        
    else
        % generate new initial guess for the same problem
        rng(5+input.run_number)
        input.x_0 = repmat(2*input.phys_boundary*(rand(1,input.dimension)-0.5),input.sensors,1);
        input.u_0 = input.x_0-input.shift_data; % zeros(size(input.x_0));
        % % input.u_0=(ones(length(input.x_0),1)*input.truth-input.shift_data);
        % % nu_0=sqrt(sum(input.u_0.*input.u_0, 2))*[1,1];
        % % input.u_0=input.u_0./nu_0;
        % % 2*input.phys_boundary*(rand    (sensors,dimension)-0.5);
        input.v_0 = input.u_0; % zeros(size(input.x_0)); % 2*input.phys_boundary*(rand(sensors,dimension)-0.5);
        input.w_0 = input.shift_data - input.x_0; % 2*input.phys_boundary*(rand(sensors,dimension)-0.5);
    end
end

%%===================================================================
%% 
%%   Run the algorithm!!!
%% 
%%===================================================================

% There are two ways to run the algorithms with all the plug-ins to the 
% ProxToolbox features: with and without the algorithm_wrapper.
% The algorithm_wrapper uses a generic call to the algorithm as a 
% fixed point operator.  The is easier for the user to program, but does
% not take advantage of efficiencies of recycling computations.  By 
% default, ProxMatlab works with the algorithm_wrapper
if(nargin(input.method)>1) % running in simplified mode
    output=feval('algorithm_wrapper', input);
else
    output=feval(input.method,input);
end

iter = output.stats.iter;
stats=output.stats;
t=stats.time;
label = ['Took ',num2str(iter),' iterations and ',num2str(t), ' seconds.'];
disp(label)
if strcmp(input.method,'RAAR')
    tmp=min(stats.shadow_change);
else
    tmp=min(stats.change);
end
if(tmp>input.TOL)
    % label = ['Tolerance ',num2str(TOL), ' not achieved by iteration ', num2str(iter(j))];
    label = ['MAXIT=', num2str(input.MAXIT),' exceeded'];
    disp(label)
end
disp(' ')

%%===================================================================
%%
%%  now write output in form that comforms with standard format
%%  assumed in main_ProxToolbox
%%
%%===================================================================
if(strcmp(input.method,'RAAR'))
    output.x=output.u1+input.shift_data;
end
%%===================================================================
%% 
%%   Graphics (could also do this from within main_ProxToolbox)
%% 
%%===================================================================
if(input.graphics==1)
    success = feval(input.graphics_display, input,output);
end
%%=========================================================================
%% 
%%   write results to file (could also do this from within main_ProxToolbox)
%% 
%%=========================================================================

end
