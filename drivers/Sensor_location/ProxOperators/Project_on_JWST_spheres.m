%                 Project_on_JWST_spheres.m
%              written on 8th September 2016 by
%                 Russell Luke (from Haifa!)
%                  University of Gottingen
%
% DESCRIPTION:  The projection of a vector on the product of balls with different radius.
%
%

function uB = Project_on_JWST_spheres(method_input,u)

uB = u;
m=method_input.Ny;
n=method_input.Ny;
p=method_input.Nz;
K=method_input.product_space_dimension;

    TOL2=method_input.TOL2;
    epsilon = method_input.data_ball;
    for j=1:method_input.product_space_dimension-1
        u_sq = u(:,:,j).*feval('conj',u(:,:,j));
        u_abs = sqrt(u_sq);
        tmp = u_sq./method_input.data_sq(:,:,j);
        tmp2=method_input.data_zeros(:,j)~=0;
        tmpI = method_input.data_zeros(tmp2,j);
        tmp(tmpI)=1;
        u_sq(tmpI)=0;
        tmpI= tmp==0;
        tmp(tmpI)=1;
        tmp=log(tmp);
        hu = sum(sum(u_sq.*tmp + method_input.data_sq(:,:,j) - u_sq));
        if(hu>=epsilon+TOL2)
            uB(:,:,j) = feval('MagProj',method_input.data(:,:,j),u(:,:,j));
        else
            % no change
        end
    end
    % now project onto the pupil constraint.
    % this is a qualitative constraint, so no 
    % noise is taken into account.
    uB(:,:,K) = feval('MagProj',method_input.abs_illumination,u(:,:,K));

end

