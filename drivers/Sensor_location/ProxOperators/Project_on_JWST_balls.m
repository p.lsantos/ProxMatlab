%                 Project_on_JWST_balls.m
%              written on 8th September 2016 by
%                 Russell Luke (from Haifa!)
%                  University of Gottingen
%
% DESCRIPTION:  The projection of a vector on the product of balls with different radius.
%
%

function uB = Project_on_JWST_balls(method_input,u)

uB = u;
m=method_input.Ny;
n=method_input.Ny;
p=method_input.Nz;
K=method_input.product_space_dimension;

    TOL2=method_input.TOL2;
    epsilon = method_input.data_ball;
    for j=1:method_input.product_space_dimension-1
        u_sq = u(:,:,j).*feval('conj',u(:,:,j));
        u_abs = sqrt(u_sq);
        tmp = u_sq./method_input.data_sq(:,:,j);
        tmp2=method_input.data_zeros(:,j)~=0;
        tmpI = method_input.data_zeros(tmp2,j);
        tmp(tmpI)=1;
        u_sq(tmpI)=0;
        tmpI= tmp==0;
        tmp(tmpI)=1;
        tmp=log(tmp);
        hu = sum(sum(u_sq.*tmp + method_input.data_sq(:,:,j) - u_sq));
        if(hu>=epsilon+TOL2)
            u_sq = u(:,:,j);  % overload u_sq
            tmp=method_input.data(:,:,j);
            tmpI = find(u_abs>tmp); % find only those indeces where amplitude is greater than measurement
            tmp(tmpI)  = feval('MagProj',tmp(tmpI),u_sq(tmpI));
            uB(:,:,j)=tmp;
        else
            % no change
        end
    end
    % now project onto the pupil constraint.
    % this is a qualitative constraint, so no 
    % noise is taken into account.
    tmp=u(:,:,K);
    tmpI=find(abs(tmp)>method_input.abs_illumination); % find only those indeces where amplitude is greater than measurement
    tmp(tmpI)  = feval('MagProj',method_input.abs_illumination(tmpI),tmp(tmpI));
    uB(:,:,K) = tmp;    

end

