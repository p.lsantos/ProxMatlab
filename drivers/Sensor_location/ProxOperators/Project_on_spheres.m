%                 Project_on_spheres.m
%              written on 8th September 2016 by
%                  Russell Luke (from Haifa!)
%                   University of Gottingen
%
% DESCRIPTION:  The projection of a vector on the product of balls with different radius.
%
%

function uB = Project_on_spheres(method_input,u)

uB = u;
m=method_input.Ny;
n=method_input.Nx;
p=method_input.Nz;
K=method_input.product_space_dimension;

if(m==K)         
        % the following assumes a certain data array structure
	for jj=1:K;
            norm_u=norm(u(jj,:),2);
       	    uB(jj,:) = u(jj,:)*method_input.data(jj)/norm_u;
	end
elseif(n==K)
        % the following assumes a certain data array structure
	for jj=1:K;
            norm_u= norm(u(:,jj),2);
       	    uB(:,jj) = u(:,jj)*method_input.data(jj)/norm_u;
	end
else
	disp('[messasge from Project_on_product_of_balls] All cases not in yet.')
end

end

