%                            IRP.m
%                  written on May 23, 2012 by 
%                         Russell Luke
%       Inst. Fuer Numerische und Angewandte Mathematik
%                    Universitaet Gottingen
%
% DESCRIPTION: This module sets the proximal operators based on the problem type
% within the problem family 'IRP', the Goettingen X-Ray Physics setup of phase
% retrieval.  Not sure if this is being called anymore. 
%
% INPUT: input, a data structure 
%
% OUTPUT:  input:  the expanded data structure passed in to the processor. 
%          output: data structure, results of the algorithm
% 
% USAGE: [input, output] = IRP(input)
%
% PROXTOOLBOX FUNCTION CALLS:  
%           input.Prox* 
%           input.method
%     and
%           input.graphics_display
%
%
function output = IRP(input)

%%===============================================================
%% Rewrite fields in our representation:  this bit needs work to 
%%   standardise, particularly for the object domain constraints
%%===============================================================

%% reshape and rename the data 
input.data_sq = input.data;
input.data = input.rt_data;
tmp=size(input.data);
if(tmp(1)~=input.Nx)
    input.data_sq=reshape(input.data_sq,input.Nx,input.Ny);
    % the projection algorithms work with the square root of the measurement:
    input.data=reshape(input.data,input.Nx,input.Ny);
end
input.norm_data=input.norm_rt_data;

%% Set the projectors and inputs based on the types of constraints and 
%% experiments
if(strcmp(input.constraint,'hybrid'))
    input.Prox1 = 'P_cP'; % This will be problem specific
elseif(strcmp(input.constraint,'support only'))
    input.Prox1 ='P_S';
elseif(strcmp(input.constraint,'real and support'))
    input.Prox1 ='P_S_real';
elseif(strcmp(input.constraint,'nonnegative and support'))
    input.Prox1 ='P_SP';
elseif(strcmp(input.constraint,'amplitude only'))
    input.Prox1 ='P_amp';
elseif(strcmp(input.constraint,'minimum amplitude'))
    input.Prox1 ='P_min_amp';
elseif(strcmp(input.constraint,'sparse'))
    input.Prox1 ='not in yet';    
end

if(strcmp(input.experiment,'single diffraction'))
    if(strcmp(input.noise,'Poisson'))
        % the following projector is specific to the way
        % Hohage and the IRP people arrange their data
        input.Prox2='Approx_P_FreFra_Poisson';
    else
        % the following projector is specific to the way
        % Hohage and the IRP people arrange their data
        input.Prox2='Approx_P_FreFra_Gaussian';
    end
elseif(strcmp(input.experiment,'diversity diffraction')) 
    % So far, only the JWST data is this type of experiment
    % but this can serve as a model. 
    input.Prox2 = 'Approx_P_JWST_Poisson';  
    input.Prox3 = input.Prox1;
    input.Prox1 = 'P_Diag';
elseif(strcmp(input.experiment,'ptychography'))
    input.Prox2 = 'not in yet';
elseif(strcmp(input.experiment,'complex'))
    input.Prox2 = 'not in yet';
end

% input.Prox1_input.F=F;  % is it any more expensive to pass everything
% into the projectors rather than just a selection of data and
% parameters?  If not, and we pass everything anyway, there is no need
% to create a new structure element.

if(isempty(input.product_space_dimension))
    input.product_space_dimension = 1;
end
% if you are only working with two sets but 
% want to do averaged projections
% (= alternating projections on the product space)
% or RAAR on the product space (=swarming), then
% you will want to change product_space_dimension=2
% and adjust your input files and projectors accordingly. 
% you could also do this within the data processor

input.TOL2 = 1e-15; 
u_1 = feval(input.Prox1,input,input.u_0);
u_2 = feval(input.Prox2,input,u_1);
% estimate the gap in the relevant metric
tmp_gap=0;
for j=1:input.product_space_dimension
      % compute (||P_Sx-P_Mx||/normM)^2:
      tmp_gap = tmp_gap+(feval('norm',u_1(:,:,j)-u_2(:,:,j),'fro')/input.norm_rt_data)^2; 
end
gap_0=sqrt(tmp_gap);

% sets the set fattening to be a percentage of the
% initial gap to the unfattened set with 
% respect to the relevant metric (KL or L2), 
% that percentage given by
% input.data_ball input by the user.
input.data_ball=input.data_ball*gap_0;
% the second tolerance relative to the oder of 
% magnitude of the metric
input.TOL2 = input.data_ball*1e-15; 

%%===================================================================
%% 
%%   Run the algorithm!!!
%% 
%%===================================================================

output=feval(input.method,input);

iter = output.stats.iter;
stats=output.stats;
label = ['Took ',num2str(iter),' iterations'];
disp(label)
tmp = min(stats.change);
if(tmp>input.TOL)
    % label = ['Tolerance ',num2str(TOL), ' not achieved by iteration ', num2str(iter(j))];
    label = ['MAXIT=', num2str(input.MAXIT),' exceeded'];
    disp(label)
end
disp(' ')

%%===================================================================
%%
%%  now write output in form that comforms with standard format
%%  assumed in main_ProxToolbox
%%
%%===================================================================

%%===================================================================
%% 
%%   Graphics (could also do this from within main_ProxToolbox)
%% 
%%===================================================================
if(input.graphics==1)
    success = feval(input.graphics_display, input,output);
end
%%=========================================================================
%% 
%%   write results to file (could also do this from within main_ProxToolbox)
%% 
%%=========================================================================

end
