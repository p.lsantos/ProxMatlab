%               Approx_P_RCAAR_JWST_Poisson.m
%                Created by:  Matthew Tam
%                 University of Newcastle
%                 Created: 30th Oct 2013.
%               Last modified: 7th Nov 2013.
%
% Description: Prox mappings for the JWST for use with RCAAR.
%               This is an approximate
%               projector onto a ball around the data determined
%               by the Kullback-Leibler divergence, as appropriate
%               for Poisson noise.  The analysis of such 
%               approximate projections was studied in 
%               D.R.Luke, ``Local Linear Convergence of Approximate 
%               Projections onto Regularized Sets '', Nonlinear Analysis, 
%               75(2012):1531--1546.

% INPUT:  input = a data structure
%         u = the point to be mapped.
% OUTPUT: u = prox mapping of u on set indexed by input.proj_iter.
%
% USAGE: u = Approx_P_RCAAR_JWST_Poisson(input,u) 
%

function u = Approx_P_RCAAR_JWST_Poisson(input,u)

TOL2=input.TOL2;
epsilon = input.data_ball;
proj_iter=input.proj_iter;

if(strcmp(input.formulation,'product space'))
    if(proj_iter==2)
        Prox='Approx_P_JWST_Poisson';
    else %iter_proj==1
        Prox='P_Diag';
    end
    u=feval(Prox,input,u);
elseif((input.product_space_dimension==4)&&strcmp(input.formulation,'sequential'))
    j=proj_iter;
    if(j<input.product_space_dimension)
        U = feval('fft2',input.indicator_ampl.*exp(1i*input.illumination_phase(:,:,j)).*u(:,:));
        U_sq = U.*feval('conj',U);
        tmp = U_sq./input.data_sq(:,:,j);
        tmp2=input.data_zeros(:)~=0;
        tmpI = input.data_zeros(tmp2);
        tmp(tmpI)=1;
        U_sq(tmpI)=0;
        IU= tmp==0;
        tmp(IU)=1;
        tmp=log(tmp);
        hU = sum(sum(U_sq.*tmp + input.data_sq(:,:,j) - U_sq));
        if(hU>=epsilon+TOL2)
            U0 = feval('MagProj',input.data(:,:,j),U);
            u(:,:) = input.indicator_ampl.*exp(-1i*input.illumination_phase(:,:,j)).*feval('ifft2',U0);
        else
            % no change
        end
    else  % this is the case when j is the index of the final constraint.
        % now project onto the pupil constraint.
        % this is a qualitative constraint, so no
        % noise is taken into account.
        %j=input.product_space_dimension;%MKT commented.
        u(:,:) = feval('MagProj',input.abs_illumination,u(:,:));
    end
elseif((input.product_space_dimension==6)&&strcmp(input.formulation,'sequential'))
    j=proj_iter;
    if((mod(j,2)==0))
        j=j/2;
        U = feval('fft2',input.indicator_ampl.*exp(1i*input.illumination_phase(:,:,j)).*u(:,:));
        U_sq = U.*feval('conj',U);
        tmp = U_sq./input.data_sq(:,:,j);
        tmp2=input.data_zeros(:)~=0;
        tmpI = input.data_zeros(tmp2);
        tmp(tmpI)=1;
        U_sq(tmpI)=0;
        IU= tmp==0;
        tmp(IU)=1;
        tmp=log(tmp);
        hU = sum(sum(U_sq.*tmp + input.data_sq(:,:,j) - U_sq));
        if(hU>=epsilon+TOL2)
            U0 = feval('MagProj',input.data(:,:,j),U);
            u(:,:) = input.indicator_ampl.*exp(-1i*input.illumination_phase(:,:,j)).*feval('ifft2',U0);
        else
            % no change
        end
    else  % this is the case when j is the index of the final constraint.
        % now project onto the pupil constraint.
        % this is a qualitative constraint, so no
        % noise is taken into account.
        %j=input.product_space_dimension;%MKT commented.
        u(:,:) = feval('MagProj',input.abs_illumination,u(:,:));
    end
else
    disp('Error: P_RCAAR_select could not find the Prox file.')
end
        

clear Prox

return
