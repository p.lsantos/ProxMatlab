%                      Approx_PM_Poisson.m
%             written on Feb. 18, 2011 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto Fourier
%               magnitude constraints, This is an approximate
%               projector onto a ball around the data determined
%               by the Kullback-Leibler divergence, as appropriate
%               for Poisson noise.  The analysis of such 
%               approximate projections was studied in 
%               D.R.Luke, ``Local Linear Convergence of Approximate 
%               Projections onto Regularized Sets '', Nonlinear Analysis, 
%               75(2012):1531--1546.

%
% INPUT:        input = data structure
%                       .data_ball is the regularization parameter described in 
%                        D. R. Luke, Nonlinear Analysis 75 (2012) 1531–1546.
%               u = function in the physical domain to be projected
%
% OUTPUT:       
%               
% USAGE: u_epsilon = P_M(input,u)
%
% Nonstandard Matlab function calls:  MagProj

function u = Approx_P_JWST_Poisson_b(input,u)

TOL2=input.TOL2;
epsilon = input.data_ball;
for j=1:input.product_space_dimension
    U = feval('fft2',input.indicator_ampl.*exp(1i*input.illumination_phase(:,:,j)).*u(:,:,j));
    U_sq = U.*feval('conj',U);
    tmp = U_sq./input.data_sq(:,:,j);
    tmp2=input.data_zeros(:,j)~=0;
    tmpI = input.data_zeros(tmp2,j);
    tmp(tmpI)=1;
    U_sq(tmpI)=0;
    IU= tmp==0;
    tmp(IU)=1;
    tmp=log(tmp);
    hU = sum(sum(U_sq.*tmp + input.data_sq(:,:,j) - U_sq));
    if(hU>=epsilon+TOL2)
        U0 = feval('MagProj',input.data(:,:,j),U);
        u(:,:,j) = input.indicator_ampl.*exp(-1i*input.illumination_phase(:,:,j)).*feval('ifft2',U0);
    else
        % no change
    end
end
