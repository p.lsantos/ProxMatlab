%                      P_Rank1.m
%             written on Sep 04, 2014 by 
%                   Patrick Neumann
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               the set of matrices of rank 1
%
% INPUT:        input, a data structure with .supp_ampl a vector of indeces of the nonzero elements of the array
%               u = to be specified
%
% OUTPUT:       p_Rank1    = the projection IN THE PHYSICAL (time) DOMAIN
%               
% USAGE: p_Rank1 = P_Rank1(input,u)
%
% Nonstandard Matlab function calls:  

function p_Rank1 = P_Rank1(~,u)

%Compute the singular value decomposition and then use the first left- and
%right singular vectors.
[U,Sigma,V] = svd(u);
p_Rank1 = U(:,1)*Sigma(1,1)*V(:,1)';