%                      P_amp.m
%             written on Feb 3, 2012 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               support constraints
%
% INPUT:        input.abs_illumination = OBJECT DOMAIN CONSTRAINT:  0-1 indicator function
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_amp    = the projection IN THE PHYSICAL (time) DOMAIN
%               
% USAGE: p_amp = P_amp(S,u)
%
% Nonstandard Matlab function calls:  MagProj

function p_amp = P_amp(input,u)

if isfield(input,'supp_phase')
    p_amp = input.abs_illumination;
    p_amp(input.supp_phase) = feval('MagProj',input.abs_illumination(input.supp_phase),u(input.supp_phase));
else 
    p_amp = feval('MagProj',input.abs_illumination,u);
end
