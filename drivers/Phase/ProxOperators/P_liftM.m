%                      P_liftM.m
%             written on Mar 10, 2015 by 
%                   Patrick Neumann
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto Fourier
%               magnitude constraints in a lifted space of matrices
%
% INPUT:         input = a data structure with .data = nonegative real FOURIER DOMAIN CONSTRAINT
%                     .M = nonegative real FOURIER DOMAIN CONSTRAINT
%               u = function in the lifted physical domain to be projected
%
% OUTPUT:       p_M    = the projection IN THE PHYSICAL (time) DOMAIN
%               
% USAGE: p_liftM = P_liftM(input,u)
%
% Nonstandard Matlab function calls:  MagProj

function p_liftM = P_liftM(input, u)
 
[k,l] = size(u);
if k == 32
    u = reshape(u, [k*l,1]);
    u = u*u';
end
% Building the linear map generating the affine subspace
% Careful: When dealing with fft2 of S, then the action is
% kron( F, F)*vec(S) with F as dftmtx.
 [~,n] = size(u);

% a = dftmtx(sqrt(n));
% A = kron(a,a);
% 
A = input.kronA;
 Au = zeros(n,1);
 for i = 1:n
     Au(i) = A(i,:)*u*A(i,:)';
 end



mu = input.rt_data;
mu = reshape(mu, [n,1]);

% Projection
p_liftM = u-1/(n^2)*A'*diag(Au-mu.^2)*A;

% Finding errors...
%m = norm(u-p_liftM);
m1 = norm(u-input.blabla, 'fro');
%m2 = norm(p_liftM-input.blabla, 'fro');
%innerproduct = trace((u-p_liftM)'*(p_liftM-input.blabla));
%lab1 = ['1 Distance to affine subspace:            ', num2str(m)];
 lab2 = [' Distance to true solution:              ', num2str(m1)];
%lab3 = ['3 Distance of projection to true solution:', num2str(m2)];
%lab5 = ['Nonexpansive? (expression must be >0)     ', num2str(m1-m2)];
%lab4 = ['Inner product of vectors:                 ', num2str(innerproduct)];
%disp(lab2)
%disp(lab5)
%disp(lab3)
%disp(lab4)


%%%%%%%%%%%%%%%%%%%%%NEW%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract the vector from the rank one matrix to get a proper image!
u1 = p_liftM;
[~,n] = size(u1);
    %Stability
[values1, ~] = max(real(u1));%find the vector of maximal absolute entries in each column

[value1, index1]  = max(values1); %find the index with the largest absolute entry

uvec11 = u1(:,index1)/value1;
uvec12 = reshape(uvec11, [sqrt(n), sqrt(n)]);
p_liftM = uvec12;
