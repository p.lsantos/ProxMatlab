%                      P_S.m
%             written on May 23, 2002 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               support constraints
%
% INPUT:        input = data structure with .supp_ampl a vector of indeces of the nonzero elements of the array.
%
%               u = array to be projected
%
% OUTPUT:       p_S    = the projection 
%               
% USAGE: p_S = P_S(input,u)
%
% Nonstandard Matlab function calls:  

function p_S = P_S(input,u)

% p_S=zeros(size(u));
% p_S(input.supp_ampl) = u(input.supp_ampl);



    p_S = input.abs_illumination;
    p_S(input.supp_phase) = u(input.supp_phase);

% bla=zeros(size(u));
% bla(input.supp_phase)=1;
% sum(sum(bla))/size(u,1)/size(u,2);
% input.s=floor(sum(sum(bla))*.5);
% reshapedabs=reshape(abs((p_S)-(input.abs_illumination)),size(u,1)*size(u,2),1);
% [tmp,I]=sort(reshapedabs,'descend');
% tmp=reshape(input.abs_illumination,size(u,1)*size(u,2),1);
% p_S=reshape(p_S,size(u,1)*size(u,2),1);
% tmp(I(1:input.s))=p_S(I(1:input.s));
% p_S=reshape(tmp,size(u,1),size(u,2));

end
