%                      Approx_PM_Poisson.m
%             written on Feb. 18, 2011 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto Fourier
%               magnitude constraints. This is an approximate
%               projector onto a ball around the data determined
%               by the Kullback-Leibler divergence, as appropriate
%               for Poisson noise.  The analysis of such 
%               approximate projections was studied in 
%               D.R.Luke, ``Local Linear Convergence of Approximate 
%               Projections onto Regularized Sets '', Nonlinear Analysis, 
%               75(2012):1531--1546.

%
% INPUT:       Func_params = a data structure with 
%                             .data = nonegative real FOURIER DOMAIN CONSTRAINT
%                             .data_sq = the elementwise square of .data
%                             .data_ball is the regularization parameter described in 
%                             D. R. Luke, Nonlinear Analysis 75 (2012) 1531–1546.
%                            .TOL2 is an extra tolerance. 
%               u = function in the physical domain to be projected
%
% OUTPUT:       
%               
% USAGE: u_epsilon = P_M(Func_params,u)
%
% Nonstandard Matlab function calls:  MagProj

function u_epsilon = Approx_PM_Poisson(Func_params,u)

TOL2=Func_params.TOL2;
M=Func_params.data;
b=Func_params.data_sq; % M.*M;
Ib=Func_params.data_zeros; %find(b==0);
epsilon = Func_params.data_ball;
U = feval('fft2',u);
U_sq = U.*feval('conj',U);
tmp = U_sq./b; tmp(Ib)=1;
U_sq(Ib)=0;
IU= tmp==0;
tmp(IU)=1;
tmp=log(tmp);
hU = sum(sum(U_sq.*tmp + b - U_sq));
if(hU>=epsilon+TOL2)
    U0 = feval('MagProj',M,U);
    u_epsilon = feval('ifft2',U0);
else
    u_epsilon = u;
    % hU=hU-epsilon;
end
    
%     U0 = feval('MagProj',M,U);
%     DUU0 = U-U0;
%     DUU0_sq = DUU0.*feval('conj',DUU0);
%     term2 = 2*real(DUU0.*feval('conj',U0));
%     TOL=5e-12;
%     lambda = .5;
%     Ulambda = lambda*U+(1-lambda)*U0;
%     % compute damping parameter
%     Ulambda_sq = Ulambda.*feval('conj',Ulambda);
%     Ulambda_sq(Ib)=0;
%     tmp = Ulambda_sq./b; tmp(Ib)=1;
%     tmp=log(tmp);
%     h = sum(sum(max(0,Ulambda_sq.*tmp + b - Ulambda_sq)));
%     counter=0;
%     % bracketed Newton's method
%     lb=0;
%     ub=1;
%     while (abs(h-epsilon)>=TOL2 && counter<=40)
%         counter=counter+1;
%         if h-epsilon>0
%             ub=lambda;
%         else
%             lb=lambda;
%         end        
%         dUlambda_sq = 2*lambda*DUU0_sq+term2;
%         dh = sum(sum(dUlambda_sq.*tmp));
%         tmp_lambda = min(max(lambda-(h-epsilon)/dh, lb),ub);
%         if(tmp_lambda==lambda)
%             if (ub-lb>=TOL)
%                 lambda=.5*(ub+lb);
%             else
%                 break
%             end
%         else
%             lambda=tmp_lambda;
%         end
%         Ulambda = lambda*U+(1-lambda)*U0;
%         % compute damping parameter
%         Ulambda_sq = Ulambda.*feval('conj',Ulambda);
%         Ulambda_sq(Ib)=0;
%         tmp=Ulambda_sq./b;  tmp(Ib)=1;
%         tmp=log(tmp);
%         h = sum(sum(max(0,Ulambda_sq.*tmp + b - Ulambda_sq)));
%     end
%     counter;
%     u_epsilon=feval('ifft2',Ulambda);
%     h=h-epsilon;
%     % counter=counter
% else
%     u_epsilon=feval('ifft2',U);
%     hU=hU-epsilon;
% end
% 


