%                      P_mean.m
%             written on Mar 15, 2015 by 
%                   Patrick Neumann
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for taking the average of
%               nonnegativity and support constraints and magn. constr. in
%               lifted space.
%
% INPUT:        input, a data structure with .supp_ampl a vector of indeces of the nonzero elements of the array
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_SP    = the projection IN THE PHYSICAL (time) DOMAIN in
%                         the lifted space
%               
% USAGE: p_liftSP = P_liftSP(input,u)
%
% Nonstandard Matlab function calls:

function p_mean_SP = P_mean_SP(input,u)

p_mean_SP = 1/2*feval('P_liftSP', input, u) + 1/2*feval('P_liftM', input, u);