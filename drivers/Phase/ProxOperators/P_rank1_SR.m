%                      P_mean.m
%             written on Mar 15, 2015 by 
%                   Patrick Neumann
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for taking the average of
%               nonnegativity and support constraints and magn. constr. in
%               lifted space.
%
% INPUT:        input, a data structure with .supp_ampl a vector of indeces of the nonzero elements of the array
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_SP    = the projection IN THE PHYSICAL (time) DOMAIN in
%                         the lifted space
%               
% USAGE: p_liftSP = P_liftSP(input,u)
%
% Nonstandard Matlab function calls:

function p_rank1_SR = P_rank1_SR(input,u)

% Check dimensions
[k1,l1] = size(u);
if k1 == 32
    u = reshape(u, [k1*l1,1]);
    u = u*u';
end

% Actual Projection
p_rank1_SR = feval('P_Rank1', input, feval('P_liftSR', input, u));

% Extract the vector from the rank one matrix to get a proper image!
u1 = p_rank1_SR;
[~,n] = size(u1);
    %Stability
[values1, ~] = max(real(u1));%find the vector of maximal absolute entries in each column

[value1, index1]  = max(values1); %find the index with the largest absolute entry

uvec11 = u1(:,index1)/value1;
uvec12 = reshape(uvec11, [sqrt(n), sqrt(n)]);
p_rank1_SR = uvec12;