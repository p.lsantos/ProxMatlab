%                      P_Fresnel.m
%             written on April 29, 2014 by 
%                   Robert Hesse
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto Fresnel
%               magnitude constraints
%
% INPUT:         input = a data structure with .data = nonegative real FOURIER DOMAIN CONSTRAINT
%                     .M = nonegative real FOURIER DOMAIN CONSTRAINT
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_M    = the projection IN THE PHYSICAL (time) DOMAIN
%               phat_M = projection IN THE FOURIER DOMAIN
%               
% USAGE: [p_M,phat_M] = P_M(input,u)
%
% Nonstandard Matlab function calls:  MagProj

function [p_M, rms] = P_Fresnel(input,u)

uhat=prop_nf_pa(u,input.lambda,input.z_eff,input.d1x,input.d2x);
Puhat=feval('MagProj',input.data,uhat);
rms = sum(sum( abs(abs(uhat)-input.data)))/(sum(sum(input.data)));
p_M=prop_nf_pa(Puhat,input.lambda,-input.z_eff,input.d1x,input.d2x);



