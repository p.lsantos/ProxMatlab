function u = P_CDP(input,u)
%                      P_CDP.m
%             written on Sept. 27, 2016 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto Fourier
%               magnitude constraints with nonunitary, but invertible 
%               masks. 
%
% INPUT:        input = data structure
%               u = function in the physical domain to be projected
%
% OUTPUT:       
%               
% USAGE: u = P_CDP(input,u)
%
% Nonstandard Matlab function calls:  MagProj


if(input.Nx==1||input.Ny==1)
    FFT='fft'; IFFT='ifft';
else
    FFT='fft2'; IFFT='ifft2';
end
U = feval(FFT,input.Masks.*u);
U0 = feval('MagProj',input.data,U);
u = feval(IFFT,U0)./input.Masks;

end
