%                Approx_P_FreFra_Poisson.m
%                written around Jan 23, 2012 by 
%                    Robert Hesse and Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto Fourier
%               magnitude constraints.  This is an approximate
%               projector onto a ball around the data determined
%               by the Kullback-Leibler divergence, as appropriate
%               for Poisson noise.  The analysis of such 
%               approximate projections was studied in 
%               D.R.Luke, ``Local Linear Convergence of Approximate 
%               Projections onto Regularized Sets '', Nonlinear Analysis, 
%               75(2012):1531--1546.
%
% INPUT:        input, a data structure
%               f = function in the physical domain to be projected
%
% OUTPUT:       p_M    = the projection IN THE PHYSICAL (time) DOMAIN
%               phat_M = projection IN THE FOURIER DOMAIN
%               
% USAGE: p_M = Approx_P_FreFra_Poisson(M,u)
%
% Nonstandard Matlab function calls:  MagProj

function p_M = Approx_P_FreFra_Poisson(input,f)

% p_M = feval('ifft2',feval('MagProj',M,feval('fft2',u)));

if input.use_farfield_formula
    if  input.fresnel_nr>0       
        fhat = -1i*input.fresnel_nr/(input.Nx^2*2*pi) * ifftshift(fft2(fftshift(f-input.abs_illumination))) ...
         + ifftshift(input.FT_conv_kernel);
    else
        fhat = (-1i/input.Nx^2)*ifftshift(fft2(fftshift(f)));
    end    
else
    if(any(strcmp('beam',fieldnames(input))))
       fhat = feval('ifft2',input.FT_conv_kernel.*feval('fft2',f.*input.beam))/input.magn;
    else
       fhat = feval('ifft2',input.FT_conv_kernel.*feval('fft2',f))/input.magn;
    end
end
fhat_sq = fhat.*feval('conj',fhat);
tmp = fhat_sq./input.data_sq; tmp(input.data_zeros)=1;
fhat_sq(input.data_zeros)=0;
Ifhat= tmp==0;
tmp(Ifhat)=1;
tmp=log(tmp);
hfhat = sum(sum(fhat_sq.*tmp + input.data_sq - fhat_sq));
if(hfhat>=input.data_ball+input.TOL2)
    p_Mhat = feval('MagProj',input.data,fhat);
    if input.use_farfield_formula
        if  input.fresnel_nr>0       
            p_M=1i/input.fresnel_nr*(input.Nx^2*2*pi) *ifftshift(ifft2(fftshift(p_Mhat)));
        else
            p_M=1i*(input.Nx^2) *ifftshift(ifft2(fftshift(p_Mhat)));
        end
    else
        if(any(strcmp('beam',fieldnames(input))))
	   p_M=ifft2(fft2(p_Mhat*input.magn)./input.FT_conv_kernel)./input.beam;
	else
           p_M=ifft2(fft2(p_Mhat*input.magn)./input.FT_conv_kernel);
	end
    end
else
    p_M = f;
end    
