%                     MagProj.m
%             written on July 24, 2001 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection operator onto a magnitude constraint
%
% INPUT:        constr = a nonnegative array that is the magnitude
%                        constraint
%               u = the function to be projected onto constr 
%                   (can be complex)
%
% OUTPUT:       unew = the projection
%
% USAGE: unew = MagProj(constr,u)
%
% Nonstandard Matlab function calls:  

function unew = P_cP(input,u)

unew=input.illumination;
% illabs=sqrt(conj(unew).*unew);
% construct the above in teh data processor
% unew=feval('MagProj',illabs,u);

uabs=sqrt(conj(u).*u);
%unew(input.supp_phase)=u(input.supp_phase);
unew(input.supp_phase)=feval('MagProj',input.abs_illumination(input.supp_phase).*exp(-0.0045*angle(u(input.supp_phase)./input.illumination(input.supp_phase))),u(input.supp_phase));
