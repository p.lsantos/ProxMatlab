%                Approx_P_FreFra_Gaussian.m
%                written around Jan 23, 2012 by 
%                    Robert Hesse and Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto Fourier
%               magnitude constraints. This is an approximate
%               projector onto a ball around the data determined
%               by the Kullback-Leibler divergence, as appropriate
%               for Poisson noise.  The analysis of such 
%               approximate projections was studied in 
%               D.R.Luke, ``Local Linear Convergence of Approximate 
%               Projections onto Regularized Sets '', Nonlinear Analysis, 
%               75(2012):1531--1546.

%
% INPUT:        input, a data structure
%               f = function in the physical domain to be projected
%
% OUTPUT:       p_M    = the projection IN THE PHYSICAL (time) DOMAIN
%               phat_M = projection IN THE FOURIER DOMAIN
%               
% USAGE: p_M = Approx_P_FreFra_Poisson(M,u)
%
% Nonstandard Matlab function calls:  MagProj

function p_M = Approx_P_FreFra_Gaussian(input,f)

% p_M = feval('ifft2',feval('MagProj',M,feval('fft2',u)));

if input.use_farfield_formula
    if  input.fresnel_nr>0       
        fhat = -1i*input.fresnel_nr/(input.Nx^2*2*pi) * ifftshift(fft2(fftshift(f-input.illumination))) ...
         + ifftshift(input.FT_conv_kernel);
    else
        fhat = (-1i/input.Nx^2)*ifftshift(fft2(fftshift(f)));
    end    
else
    if(any(strcmp('beam',fieldnames(input))))
       fhat = feval('ifft2',input.FT_conv_kernel.*feval('fft2',f.*input.beam))/input.magn;
    else
       fhat = feval('ifft2',input.FT_conv_kernel.*feval('fft2',f))/input.magn;
    end
end
% fhat_sq = fhat.*feval('conj',fhat);
% hfhat = feval('norm',fhat_sq-input.data_sq,'fro')
% the above is consistent with the statistics of the 
% actual data which is collected:  the data really is
% the modulus squared of the Fourier transform, corrupted by 
% noise.   But the projections are not consistent with the 
% modulus SQUARED, but rather the modulus.  It is possible (indeed,
% has happend) that hfhat defined above INCREASES from one
% iteration to the next while the GAP calculated according 
% to the modulus decreases monotonically.  This causes problems
% if one wants to introduce a stopping/regularization 
% criterion based on the reduction of the gap between the 
% sets.  There must be something more to this, but for the moment
% I just keep it consistent and work with the set gap.
% Particularly in the case where NO noise is involved, it 
% does not make sense to work with the square of the modulus. 
% For noisy data I conjecture that the difference is unlikely to\
% to be noticed.
% Another rather ad hoc (but still valid) point is that the 
% statistically relevant distance to the measurement data can 
% always be computed after the algorithm has converged to within
% the percentage gap decrease criteria implemented here.  
% Interesting is that, for inconsistent problems, it is 
% impossible to tell the difference between the data gap 
% due to satistical noise, and the data gap due to model 
% inconsistency. 


hfhat = feval('norm',abs(fhat)-input.data,'fro');
if(hfhat>=input.data_ball+input.TOL2)
    p_Mhat = feval('MagProj',input.data,fhat);
    if input.use_farfield_formula
        if  input.fresnel_nr>0       
            p_M=1i/input.fresnel_nr*(input.Nx^2*2*pi) *ifftshift(ifft2(fftshift(p_Mhat)));
        else
            p_M=1i*(input.Nx^2) *ifftshift(ifft2(fftshift(p_Mhat)));
        end
    else
        if(any(strcmp('beam',fieldnames(input))))
	   p_M=ifft2(fft2(p_Mhat*input.magn)./input.FT_conv_kernel)./input.beam;
	else
           p_M=ifft2(fft2(p_Mhat*input.magn)./input.FT_conv_kernel);
	end
    end
else
    p_M = f;
end    
