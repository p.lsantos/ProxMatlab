%                     MagProj.m
%             written on July 24, 2001 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection operator onto a magnitude constraint
%
% INPUT:        constr = a nonnegative array that is the magnitude
%                        constraint
%               u = the function to be projected onto constr 
%                   (can be complex)
%
% OUTPUT:       unew = the projection
%
% USAGE: unew = MagProj(constr,u)
%
% Nonstandard Matlab function calls:  

function unew = P_hybrid(input,u)

unew=input.illumination;
% illabs=sqrt(conj(unew).*unew);
% construct the above in teh data processor
% unew=feval('MagProj',illabs,u);
uabs=sqrt(conj(u).*u);
%unew(input.supp_phase)=u(input.supp_phase);
unew(input.supp_phase)=feval('MagProj',input.abs_illumination(input.supp_phase),u(input.supp_phase));
%unew(input.supp_phase)=u(input.supp_phase)./uabs(input.supp_phase).*input.abs_illumination(input.supp_phase);
unew(input.supp_ampl)=unew(input.supp_ampl)./input.abs_illumination(input.supp_ampl).*uabs(input.supp_ampl);
% 
% bla=zeros(size(u));
% bla(input.supp_phase)=1;
% sum(sum(bla))/size(u,1)/size(u,2);
% input.s=sum(sum(bla))*1;
% reshapedabsangle=reshape(abs((unew)-(input.illumination)),size(u,1)*size(u,2),1);
% [tmp,I]=sort(reshapedabsangle,'descend');
% pU=reshape(input.illumination,size(u,1)*size(u,2),1);
% unew=reshape(unew,size(u,1)*size(u,2),1);
% pU(I(1:input.s))=unew(I(1:input.s));
% unew=reshape(pU,size(u,1),size(u,2));