% global max_warmup_it
global max_it
global beta_0;
global beta_max;
global beta_switch;
    prbl.MAXIT = max_it;
    prbl.TOL = 1e-12;

    %% relaxaton parameters in RAAR, HPR and HAAR
    beta_0 =0.9;                % starting relaxation prameter (only used with
    % HAAR, HPR and RAAR)
    beta_max=0.5;             % maximum relaxation prameter (only used with
    % HAAR, RAAR, and HPR)
    beta_switch=1;           % iteration at which beta moves from beta_0 -> beta_max

% global repetitions
% 
% max_warmup_it = 0
max_it = 1000
% repetitions = 1




input_files={'dicty_hires_AP';...
             'dicty_hires_AP_support_sparse';...
             'dicty_hires_RAAR_amplitude';...
             'dicty_hires_RAAR_support_amplitude';...
             'dicty_hires_RAAR_support_sparse';...
             'dicty_hires_RAAR_support';...
    };
theColour = [[0 0 1]; [1 0 0]; [0 0.75 0]; [0.75 0 0.75]; [0.75 0.75 0]; [0 0.75 0.75]; [0.5 0.5 0.5]];
methodAlg = {'RodenburgPALM' 'PALMPALM' 'ThibaultRAAR_PALM'};
theMethods = {'AP support','AP support sparse','amplitude only' 'support and amplitude' 'support and sparsity' 'support only'};

   
for i=1:size(input_files)
    input_file = input_files{i}
    diary(['../OutputData/' datestr(now,'yymmdd') input_file 'Log.txt'])
    [output,input] = main_ProxToolbox(char(input_file));
    diary off
    rms(i,1:max_it+1)=output.stats.rms;
    change(i,1:max_it)=output.stats.change;
    gap(i,1:max_it)=output.stats.gap;
     if true
    % Save graphics to file.
    figs = get(0,'children');
    directory = ['/scratch/ProxToolbox/OutputData/' datestr(now,'yymmdd') input.problem_family];
    if ~exist(directory,'dir')
        mkdir(directory);
    end
    for i=1:length(figs)
%         figure_filename = [directory '/' input.ptychography_prox 'beta' num2str(input.beta_max) 'figure' num2str(i)];

        figure_filename = [directory '/' input_file 'figure' num2str(i)];
        saveas(figs(i), figure_filename, 'fig');
        saveas(figs(i), figure_filename, 'png');
    end
    end
end
   



%% Make some summary figures figures
resultsDirectory = ['../OutputData/' datestr(now,'yymmdd') 'Phase/'];
summaryDirectory = ['../OutputData/' datestr(now,'yymmdd') 'PhaseSummary/'];
if ~exist(summaryDirectory,'dir')
    mkdir(summaryDirectory);
end


close all;

% RMS-factor
figure(11);
for m=1:size(input_files)
    figure(11);
    hold on;
    plot(rms(m,1:max_it+1),'Color',theColour(m,:));
end
legend(theMethods);
title('RMS-factoragainst Iterations');
xlabel('Iterations');
ylabel('R-factor');
set(gca,'yscale','log');
hold off;
saveas(gca,[summaryDirectory 'RMSProbe'],'fig');
saveas(gca,[summaryDirectory 'RMSProbe'],'png');

% gap-factor
figure(12);
for m=1:size(input_files)
    figure(12);
    hold on;
    plot(gap(m,1:max_it),'Color',theColour(m,:));
end
legend(theMethods);
title('gap Iterations');
xlabel('Iterations');
ylabel('R-factor');
set(gca,'yscale','log');
hold off;
saveas(gca,[summaryDirectory 'gapProbe'],'fig');
saveas(gca,[summaryDirectory 'gapProbe'],'png');

% change-factor
figure(13);
for m=1:size(input_files)
    figure(13);
    hold on;
    plot(change(m,1:max_it),'Color',theColour(m,:));
end
legend(theMethods);
title('change Iterations');
xlabel('Iterations');
ylabel('R-factor');
set(gca,'yscale','log');
hold off;
saveas(gca,[summaryDirectory 'RMSProbe'],'fig');
saveas(gca,[summaryDirectory 'RRSProbe'],'png');