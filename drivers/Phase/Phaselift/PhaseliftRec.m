load Testrun150506output.mat;
u = output.u;
v = output.u1;
w = output.u2;

if rank(u)>1
    [U,Sigma,V] = svd(u);
     u = U(:,1)*Sigma(1,1)*V(:,1)';
end

if rank(v)>1
    [U,Sigma,V] = svd(v);
     v = U(:,1)*Sigma(1,1)*V(:,1)';
end

if rank(w)>1
    [U,Sigma,V] = svd(w);
     w = U(:,1)*Sigma(1,1)*V(:,1)';
end

[~,n] = size(u);
[values1, ~] = max(real(u));%find the vector of maximal absolute entries in each column
[values2, ~] = max(real(v));
[values3, ~] = max(real(w));

[value1, index1]  = max(values1); %find the index with the largest absolute entry
[value2, index2]  = max(values2);
[value3, index3]  = max(values3);

uvec11 = u(:,index1)/value1;
uvec12 = reshape(uvec11, [sqrt(n), sqrt(n)]);
uvec21 = v(:,index2)/value2;
uvec22 = reshape(uvec21, [sqrt(n), sqrt(n)]);
uvec31 = w(:,index3)/value3;
uvec32 = reshape(uvec31, [sqrt(n), sqrt(n)]);

u = uvec12;
u2 = uvec32;

gap = output.stats.gap;
change = output.stats.change;
iter = output.stats.iter;
time = output.stats.time;

figure(900);

    subplot(3,2,1); imagesc(abs(u)); colormap gray; axis equal tight; colorbar; title('best approximation - physical domain'); drawnow; 
    subplot(3,2,2); imagesc(abs(u2)); colormap gray; axis equal tight; colorbar; title('best approximation - Fourier constraint'); drawnow; %caxis([4.85,5.35]);
   % subplot(4,2,3), imagesc(real(u)), colorbar; axis equal tight;
   % xlabel('best approximation - physical constraint')
    %label = ['Algorithm: ',method, ', \beta=',num2str(beta0),' to ',num2str(beta_max)];
    %title(label)
    subplot(3,2,3); imagesc(angle(u)); colormap gray; axis equal tight; colorbar; title('best approx. phase - physical constraint'); drawnow; %caxis([4.85,5.35]); caxis([-0.9 -0.4]);
    %subplot(4,2,7); imagesc(abs(u2)); colormap gray; axis equal tight; colorbar; title('best approximation - Fourier constraint'); drawnow; %caxis([4.85,5.35]);
    subplot(3,2,4); imagesc(angle(u2)); colormap gray; axis equal tight; colorbar; title('best approx. phase - Fourier constraint'); drawnow; %caxis([4.85,5.35]); caxis([-0.9 -0.4]);
    subplot(3,2,5);   semilogy(change),xlabel('iteration'),ylabel(['||x^{2k+2}-x^{2k}||'])
    label = ['Algorithm: lifted AP'];
    title(label)
    subplot(3,2,6);   semilogy(gap),xlabel('iteration'),ylabel(['||x^{2k+1}-x^{2k}||'])
    label = ['time = ',num2str(time)];
    title(label)
  