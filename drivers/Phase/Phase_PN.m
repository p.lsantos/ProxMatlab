%                      Phase_PN.m
%             written on May 23, 2012 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%            Modified by:  Matthew Tam
%             University of Newcastle
%                 30th Oct 2013.
%             Modified by Patrick Neumann sometime around 2014, 
%             Not supported since then... no guarantee this works.
%
%
% This module sets the projectors based on the problem type
% within the problem family 'Phase'
function output = Phase_PN(input)

%%===============================================================
%% Rewrite fields in our representation:  this bit needs work to 
%%   standardise, particularly for the object domain constraints
%%===============================================================

%% reshape and rename the data 
input.data_sq = input.data;
input.data = input.rt_data;
tmp=size(input.data);
if(strcmp(input.experiment,'diffractionHohage'))
    % no reshape in specific cases. Ny,Nx are not consistent anyway it seems.    
elseif(tmp(1)~=input.Nx)
    input.data_sq=reshape(input.data_sq,input.Nx,input.Ny);
    % the projection algorithms work with the square root of the measurement:
    input.data=reshape(input.data,input.Nx,input.Ny);
end
input.norm_data=input.norm_rt_data;

if(any(strcmp('Nz',fieldnames(input))))
else
    input.Nz = 1;
end
% 

%% If method_input.formulation is does not exist, i.e. not specified in 
%% the *_in.m file, use the product space as the default.
if(any(strcmp('formulation',fieldnames(input))))
    formulation = input.formulation;
else
    formulation = 'product space';
end

%% Set the projectors and inputs based on the types of constraints and 
%% experiments
if(strcmp(input.constraint,'hybrid'))
    input.Prox1 = 'P_hybrid'; % This will be problem specific
elseif(strcmp(input.constraint,'support only'))
    input.Prox1 ='P_S';
elseif(strcmp(input.constraint,'real and support'))
    input.Prox1 ='P_S_real';
elseif(strcmp(input.constraint,'nonnegative and support'))
    input.Prox1 ='P_SP';
elseif(strcmp(input.constraint,'amplitude only'))
    input.Prox1 ='P_amp';
elseif(strcmp(input.constraint,'amplitude with support'))
    input.Prox1 ='P_amp_support';
elseif(strcmp(input.constraint,'minimum amplitude'))
    input.Prox1 ='P_min_amp';
elseif(strcmp(input.constraint,'amplitude with support and sparsity'))
    input.Prox1 ='P_amp_support_sparsity';   
elseif(strcmp(input.constraint,'support and sparsity'))
    input.Prox1 ='P_support_sparse';    
elseif(strcmp(input.constraint,'sparse'))
    input.Prox1 ='not in yet';    
elseif(strcmp(input.constraint,'phaselift'))
    input.Prox1 ='P_mean_SP';
elseif(strcmp(input.constraint,'phaselift2'))
    input.Prox1 ='P_liftM';
    input.Prox3 ='Approx_PM_Poisson'; % Patrick: This is just to monitor the change of phases!
end

if(strcmp(input.experiment,'single diffraction'))
    if(strcmp(input.distance,'far field'))
        if(strcmp(input.constraint,'phaselift'))
            input.Prox2 = 'P_Rank1';
        elseif(strcmp(input.constraint,'phaselift2'))
            input.Prox2 = 'P_rank1_SR';
        else
            if(strcmp(input.noise,'Poisson'))
                input.Prox2='Approx_PM_Poisson';
            else
                input.Prox2='Approx_PM_Gaussian';
            end
        end
    else
        input.Prox2='P_Fresnel';
    end
% The following selects the projectors for diversity diffraction not
% performed in the product space. So far only used for RCAAR.
elseif(strcmp(input.experiment,'diffractionHohage'))
    if(strcmp(input.noise,'Poisson'))
        input.Prox2='P_Fresnel';
    else
        input.Prox2='P_FreFra';
    end
elseif(strcmp(input.experiment,'diversity diffraction')&&strcmp(formulation,'sequential'))
    input.Prox2 = 'Approx_P_RCAAR_JWST_Poisson';
    input.Prox1 = input.Prox2;
elseif(strcmp(input.experiment,'diversity diffraction')) 
    % So far, only the JWST data is this type of experiment
    % but this can serve as a model. 
    input.Prox2 = 'Approx_P_JWST_Poisson';  
    input.Prox3 = input.Prox1;
    input.Prox1 = 'P_Diag';
elseif(strcmp(input.experiment,'ptychography'))
    input.Prox2 = 'not in yet';
elseif(strcmp(input.experiment,'complex'))
    input.Prox2 = 'not in yet';
elseif(strcmp(input.constraint,'phaselift'))
    input.Prox2 ='P_PL_lowrank';
end

% input.Prox1_input.F=F;  % is it any more expensive to pass everything
% into the projectors rather than just a selection of data and
% parameters?  If not, and we pass everything anyway, there is no need
% to create a new structure element.

if(isempty(input.product_space_dimension))
    input.product_space_dimension = 1;
end

% set the animation program:
input.animation='Phase_animation';
%
% if you are only working with two sets but
% want to do averaged projections
% (= alternating projections on the product space)
% or RAAR on the product space (=swarming), then
% you will want to change product_space_dimension=2
% and adjust your input files and projectors accordingly. 
% you could also do this within the data processor

input.TOL2 = 1e-15; 

% To estimate the gap in the sequential formulation, we build the
% appropriate point in the product space. This allows for code reuse.
% Note for sequential diversity diffraction, input.Prox1 is the "RCAAR"
% version of the function.
if(strcmp(formulation,'sequential'))
    for j=1:input.product_space_dimension
        input.proj_iter=j;
        u_1(:,:,j)=feval(input.Prox1,input,input.u_0);
        input.proj_iter=mod(j,input.product_space_dimension)+1;
        u_2(:,:,j)=feval(input.Prox1,input,input.u_0);
    end;
else %i.e. formulation=='product space'
    u_1 = feval(input.Prox1,input,input.u_0);
    u_2 = feval(input.Prox2,input,u_1);
end

% estimate the gap in the relevant metric
tmp_gap=0;
for j=1:input.product_space_dimension
      % compute (||P_Sx-P_Mx||/normM)^2:
  %    tmp_gap = tmp_gap+(feval('norm',u_1(:,:,j)-u_2(:,:,j),'fro')/input.norm_rt_data)^2; 
end
gap_0=sqrt(tmp_gap);

% sets the set fattening to be a percentage of the
% initial gap to the unfattened set with 
% respect to the relevant metric (KL or L2), 
% that percentage given by
% input.data_ball input by the user.
input.data_ball=input.data_ball*gap_0;
% the second tolerance relative to the oder of 
% magnitude of the metric
input.TOL2 = input.data_ball*1e-15; 

%%===================================================================
%% 
%%   Run the algorithm!!!
%% 
%%===================================================================

output=feval(input.method,input);

iter = output.stats.iter;
stats=output.stats;
label = ['Took ',num2str(iter),' iterations'];
disp(label)
tmp = min(stats.change);
if(tmp>input.TOL)
    % label = ['Tolerance ',num2str(TOL), ' not achieved by iteration ', num2str(iter(j))];
    label = ['MAXIT=', num2str(input.MAXIT),' exceeded'];
    disp(label)
end
disp(' ')

%%===================================================================
%%
%%  now write output in form that comforms with standard format
%%  assumed in main_ProxToolbox
%%
%%===================================================================

%%===================================================================
%% 
%%   Graphics (could also do this from within main_ProxToolbox)
%% 
%%===================================================================
if(input.graphics==1)
    success = feval(input.graphics_display, input,output);
end
%%=========================================================================
%% 
%%   write results to file (could also do this from within main_ProxToolbox)
%% 
%%=========================================================================

end
