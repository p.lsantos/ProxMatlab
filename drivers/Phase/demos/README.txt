These demos run mostly in the simple mode, which uses ``algorithm_wrapper.m" in the Algorithms subdirectory.  More advanced
methods, like Quasi-Newton accelerated alternating projections (QNAP) and the PHeBIE algorithms run in expert mode 
which does not use the algorithm wrapper (and is less user friendly).  

