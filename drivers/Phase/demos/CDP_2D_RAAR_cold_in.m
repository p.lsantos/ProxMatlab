function input = CDP_2D_RAAR_cold_in(input)
%                         CDP_in
%              written on September 21, 2016 by
%                        Russell Luke
%                 University of Goettingen
%
% DESCRIPTION:  input file for ProxToolbox 
% implementation of the Wirtinger Flow (WF) algorithm presented in the paper 
% "Phase Retrieval via Wirtinger Flow: Theory and Algorithms" 
% by E. J. Candes, X. Li, and M. Soltanolkotabi

%% We start very general.
%%
%% What type of problem is being solved?  Classification 
%% is according to the geometry:  Affine, Cone, Convex, 
%% Phase, Affine-sparsity, Nonlinear-sparsity, Sudoku

input.problem_family = 'Phase';
input.expert=false;

%%==========================================
%% Problem parameters
%%==========================================
%% What is the name of the data file?
input.data_filename = 'CDP_processor';


%% What type of object are we working with?
%% Options are: 'phase', 'real', 'nonnegative', 'complex'
input.object = 'complex';

%% What type of constraints do we have?
%% Options are: 'support only', 'real and support', 'nonnegative and support',
%%              'amplitude only', 'sparse real', 'sparse complex', and 'hybrid'
input.constraint = 'hybrid';

%% What type of measurements are we working with?
%% Options are: 'single diffraction', 'JWST', 'CDP', 
%%              'ptychography', and 'complex'
input.experiment='CDP';


% The input data are coded diffraction patterns about a random complex
% valued image. 

%% Make image
input.Ny = 128;
input.Nx = 256; % 1 for 1D signals 
input.Nz=1;  % images are 1 or 2D

%% Make masks and linear sampling operators
input.product_space_dimension = 10;  % 6 for 1D signals % Number of masks  

%% Are the measurements in the far field or near field?
%% Options are: 'far field' or 'near field', 
input.distance = 'far field';

% Number of power iterations for initialization
input.warmup_iter=0;

%%==========================================
%%  Algorithm parameters
%%==========================================
%% Now set some algorithm parameters that the user should be 
%% able to control (without too much damage)

%% Algorithm:
input.method = 'RAAR';%'Accelerated_AP_product_space';  
input.numruns=100; % the only time this parameter will
% be different than 1 is when we are
% benchmarking...not something a normal user
% would be doing.
input.keep_log=1;

%% The following are parameters specific to RAAR, HPR, and HAAR that the 
%% user should be able to set/modify.  Surely
%% there will be other algorithm specific parameters that a user might 
%% want to play with.  Don't know how best 
%% to do this.  Thinking of a GUI interface, we could hard code all the 
%%  parameters the user might encounter and have the menu options change
%% depending on the value of the input.method field. 
%% do different things depending on the chosen algorithm:


    %% maximum number of iterations and tolerances
    input.MAXIT = 1000;
    input.TOL = 1e-8;

    %% relaxaton parameters in RAAR, HPR and HAAR
    input.beta_0 = 0.75;                % starting relaxation prameter (only used with
    % HAAR, HPR and RAAR)
    input.beta_max =0.75;             % maximum relaxation prameter (only used with
    % HAAR, RAAR, and HPR)
    input.beta_switch = 20;           % iteration at which beta moves from beta_0 -> beta_max

    %% parameter for the data regularization 
    %% need to discuss how/whether the user should
    %% put in information about the noise
    input.data_ball = 1e-15;  
    % the above is the percentage of the gap
    % between the measured data and the
    % initial guess satisfying the
    % qualitative constraints.  For a number 
    % very close to one, the gap is not expected 
    % to improve much.  For a number closer to 0
    % the gap is expected to improve a lot.  
    % Ultimately the size of the gap depends
    % on the inconsistency of the measurement model 
    % with the qualitative constraints.

%%==========================================
%% parameters for plotting and diagnostics
%%==========================================
input.diagnostic = true; % to turn this off, just comment out
input.verbose = 0; % options are 0 or 1
input.graphics = 0; % whether or not to display figures, options are 0 or 1.
                   % default is 1.
input.anim = 0;  % whether or not to disaply ``real time" reconstructions
                % options are 0=no, 1=yes, 2=make a movie
                % default is 1.
input.graphics_display = 'Phase_graphics'; % unless specified, a default 
                            % plotting subroutine will generate 
                            % the graphics.  Otherwise, the user
                            % can write their own plotting subroutine

%%======================================================================
%%  Technical/software specific parameters
%%======================================================================
%% Given the parameter values above, the following technical/algorithmic
%% parameters are automatically set.  The user does not need to know 
%% about these details, and so probably these parameters should be set in 
%% a module one level below this one.  


end
