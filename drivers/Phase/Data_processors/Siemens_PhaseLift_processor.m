%                  Siemens_PhaseLift_processor.m
% Written without documentation by Patrick Neumann sometime in 2014.
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  
%
% INPUT: input = a data structure
% OUTPUT: input = a data structure
%
% USAGE: input = Siemens_processor(input) 
%
% Data loaded: Siemens_star_200px.mat
% ProxToolbox function calls:  
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data reader/processor

function method_input = Siemens_PhaseLift_processor(method_input)

data_ball = method_input.data_ball;

noise = method_input.noise;
snr = method_input.data_ball;

[fid,message]=fopen('Siemens_star_200px.mat','r','l');
if (fid<0)
   disp('******************************************************************')
   disp('* Input data missing.  Please download the phase input data from *') 
   disp('*    http://vaopt.math.uni-goettingen.de/data/Phase.tar.gz       *')
   disp('* Save and unpack the Phase.tar.gz datafile in the               *')
   disp('*    ProxMatlab/InputData subdirectory                           *')
   disp('******************************************************************')
else
   disp(['Loading data file: Siemens_star_200px.mat'])
   load -ascii Siemens_star_200px.mat
   S = Siemens_star_200px;
   clear Siemens_star_200px
   fclose(fid);
end
% For Phaselift only! Shrinkage of the image to 200/rat by 200/rat px
%         rat         = 8;
%         pixelsnew   = round(200/rat);
%         S           = Resize(S, pixelsnew, pixelsnew);
%         S           = round(S);
 
        
S=ZeroPad(S);
[m,n]=size(S);
method_input.Nx=n;
method_input.Ny=m;

% For Phaselift only! reshape oconstraintf the image to a one-column-vector.
       method_input.blabla = reshape(S, m*n,1)*reshape(S,m*n,1)';

if(strcmp(method_input.object,'real')||strcmp(method_input.object,'nonnegative'))
    M=abs(fft2(S));
    % For Phaselift only!
            A = dftmtx(n);
    %        M = abs(A*S*A);
    % method_input.data_ball=method_input.Nx*method_input.Ny*data_ball;
    method_input.rt_data=M;
    % standard for the main program is that
    % the data field is the magnitude SQUARED
    % in Luke.m this is changed to the magnitude.
    method_input.data=M.^2;
    method_input.norm_rt_data=norm(S,'fro');
    method_input.norm_data=method_input.norm_rt_data^2;
    method_input.data_zeros = find(M==0);
    % generating of bad data
    %    Stmp=zeros(size(S));
    %    Stmp((m/2-m/4):(m/2+m/4),(n/2-n/4):(n/2+n/4))=S((m/2-m/4):(m/2+m/4),(n/2-n/4):(n/2+n/4));
    %    S=Stmp;
    method_input.supp_ampl = find(S~=0);
    % use the abs_illumination field to represent the
    % support constraint.
    method_input.abs_illumination = method_input.norm_rt_data*S/(norm(S,'fro'));
    method_input.abs_illumination = method_input.abs_illumination/norm(method_input.abs_illumination,'fro')*method_input.norm_rt_data(1);
    method_input.supp_phase = find(ones(m,n));
    method_input.illumination_phase = find(ones(m,n));
elseif(strcmp(method_input.object,'complex'))
    % put some phase across S
    points = method_input.Nx;
    method_input.norm_rt_data=norm(S,'fro');
    method_input.norm_data=method_input.norm_rt_data^2;
    % use the abs_illumination field to represent the
    % support constraint.
    method_input.abs_illumination = method_input.norm_rt_data*S/(norm(S,'fro'));
    % input.abs_illumination = input.abs_illumination/norm(input.abs_illumination,'fro')*input.norm_rt_data(1);
    G=zeros(size(S));% Gaussian(points,10,[points/2+1,points/2+1]);
    W=S*exp(1i*2*pi*G);
    M=abs(fft2(W));
    % method_input.data_ball=method_input.Nx*method_input.Ny*data_ball;
    method_input.rt_data=M;
    % standard for the main program is that
    % the data field is the magnitude SQUARED
    % in Luke.m this is changed to the magnitude.
    method_input.data=M.^M;
    method_input.data_zeros = find(M==0);
    method_input.supp_ampl = find(S~=0);    
elseif(strcmp(method_input.object,'phase'))
    % put some phase across S
    points = method_input.Nx;
    % use the abs_illumination field to represent the
    % support constraint.
    method_input.abs_illumination = ones(size(S));  
    G=Gaussian(points,10,[points/2+1,points/2+1]);
    W=exp((1i*2*pi)*S.*G);
    method_input.norm_rt_data=norm(W,'fro');
    M=abs(fft2(W));
    % method_input.data_ball=method_input.Nx*method_input.Ny*data_ball;
    method_input.rt_data=M;
    method_input.norm_data=method_input.norm_rt_data^2;
    % standard for the main program is that
    % the data field is the magnitude SQUARED
    % in Luke.m this is changed to the magnitude.
    method_input.data=M.^M;
    method_input.data_zeros = find(M==0);
    method_input.supp_ampl = find(W~=0);    
    method_input.supp_phase = S;
    method_input.illumination_phase = S;    
end
% initial guess
 method_input.u_0 = ifft2(M.*exp(2*pi*1i*rand(m,n)));
% method_input.u_0 = ifft2(M.*exp(2*pi*1i*Gaussian(N,N/2,[N/2+1,N/2+1]))).*S;
% method_input.u_0 = method_input.u_0/norm(method_input.u_0,'fro')*method_input.abs_illumination; 

% Phaselift only!
 A = kron(A,A);
% 
% 
 method_input.kronA = A;
% 
 %u_0 = S;
% u_0 = ifft2(M.*exp(2*pi*1i*rand(m,n)));
% u_0 = reshape(u_0, [n*m,1]);
% u_0 = u_0*u_0';
% method_input.u_0 = u_0;

method_input.product_space_dimension = 1;
end
