function input = JWST_sequential_weighted_data_processor(input)
%              JWST_RCAAR_weighted_data_processor.m
%               Modified by:  Matthew Tam
%                University of Newcastle
%                    3rd November 2013.
%
%             Original file: JWST_data_processor.m
%                written on May 27, 2012 by
%                     Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%  
%                modified on Oct 28, 2013 by
%                       Matthew Tam
%                 University of Newcastle
%
% DESCRIPTION:  
%
% INPUT: input = a data structure
% OUTPUT: input = a data structure
%
% USAGE: input = JWST_data_processor(input) 
%
% Data loaded:  
% Nonstandard function calls:  Rbin, ZeroPad, Mgen, Resize, OnesPad
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data reader/processor


addpath('../InputData/Phase')
if(isempty(strfind(path,'InputData/Phase')))
   disp('******************************************************************')
   disp('* Input data missing.  Please download the phase input data from *') 
   disp('*    http://vaopt.math.uni-goettingen.de/data/Phase.tar.gz       *')
   disp('* Save and unpack the Phase.tar.gz datafile in the               *')
   disp('*    ProxMatlab/InputData/Phase subdirectory                     *')
   disp('******************************************************************')
   return
end
data_ball = input.data_ball;

newres = input.Nx;
noise = input.noise;
snr = input.data_ball;

[fid,message]=fopen('pupil.pmod','r','l');
if (fid<0)
   disp('******************************************************************')
   disp('* Input data missing.  Please download the phase input data from *') 
   disp('*    http://vaopt.math.uni-goettingen.de/data/Phase.tar.gz       *')
   disp('* Save and unpack the Phase.tar.gz datafile in the               *')
   disp('*    ProxMatlab/InputData/Phase subdirectory                     *')
   disp('******************************************************************')
else
  Xi_A=fread(fid,[512,512],'float');
  fclose(fid);
end

diversity=3;

[fid,message]=fopen('phase_p37.pmod','r','l');
if (fid<0)
  disp(message)
else
  temp1=fread(fid,[512,512],'float');
  fclose(fid);
end

[fid,message]=fopen('phase_m37.pmod','r','l');
if (fid<0)
  disp(message)
else
  temp2=fread(fid,[512,512],'float');
  fclose(fid);
end

defocus=(temp1-temp2)/2;
theta=(temp1+temp2)/2;
if newres~=512
  defocus=feval('Resize',defocus,newres,newres);
  theta=feval('Resize',theta,newres,newres);
  Xi_A=feval('Resize',Xi_A,newres,newres);
end
aberration(:,:,1)=zeros(newres,newres);  % Note this order!!!!!
aberration(:,:,2)=defocus;  % Note this order!!!!!
aberration(:,:,3)=-defocus;  % Note this order!!!!!

true_object = Xi_A.*exp(1i*(theta));
k=zeros(newres,newres,diversity);
rt_k=zeros(newres,newres,diversity);
for j=1:diversity
  Pj=Xi_A.*exp(1i*(aberration(:,:,j)+theta));
  k(:,:,j)=(abs(feval('FFT',Pj))).^2;  
end

clear Pj temp1 temp2 defocus 

epsilon=0;
norm_rt_data=sqrt(sum(sum(Xi_A)));
data_zeros=zeros(newres^2,3);
for i=1:diversity
  rt_k(:,:,i)=(k(:,:,i).^(.5)); % *newres ?
  % normalize the data so that the corresponding 
  % pupil function has amplitude 1 everywhere
  rt_k(:,:,i) = rt_k(:,:,i)/norm(IFFT(rt_k(:,:,i)),'fro')*norm_rt_data;
  k(:,:,i) = rt_k(:,:,i).^2;
  if(strcmp(noise,'Poisson'))
      % Add Poisson noise according to Ning Lei:
      % f = alpha*4*4*pi/q/q*abs(cos(qx(iX))*sin(qy(iY))*(sin(q)/q-cos(q)));
      %        f2 = PoissonRan(f*f*2)/alpha/alpha/2;   % 2 is for I(-q)=I(q)
      %	sigma(iX, iY, iZ) = f/sqrt(2)/alpha/alpha/abs(ff)/abs(ff);
      % July 15, 2010:  Since this is meant to model photon counts (which
      % are integers) we add noise and then round down
      k(:,:,i)=k(:,:,i)/snr;
      for ii=1:newres
          for jj=1:newres
              k(ii,jj,i)= PoissonRan(k(ii,jj,i))*snr;
          end
      end
      k(:,:,i)=round(k(:,:,i));
      rt_k(:,:,i)=(k(:,:,i).^(.5)); 
  end
  % fftshift and scale the data for use with fft:
  rt_k(:,:,i) = fftshift(rt_k(:,:,i))*newres;
  k(:,:,i) = fftshift(k(:,:,i))*newres;
  temp = find(rt_k(:,:,i)==0);
  data_zeros(1:length(temp),i) = temp;
end
input.rt_data = rt_k;
input.data = k;
input.norm_rt_data=norm_rt_data; %  this is correct since
                                 % is is calculated in the 
                                 % object domain
input.data_zeros=data_zeros;
input.supp_ampl = find(Xi_A~=0);
input.indicator_ampl = zeros(size(Xi_A));
input.indicator_ampl(input.supp_ampl) = 1;

input.product_space_dimension = diversity*2;

input.abs_illumination = Xi_A;
% Normalize the illumination so that it has the 
% same energy as the data.
input.abs_illumination = input.abs_illumination/norm(input.abs_illumination,'fro')*input.norm_rt_data(1); 
input.illumination_phase = aberration;

% initial guess
% input.u_0 = Xi_A.*exp(1i*2*pi*rand(newres));

% We initilize u_0 with appropriate dimension for the formulation.
% If we are not using 'product space' then we use
% input.product_space_dimension as the number of constraints.
if(strcmp(input.formulation,'sequential'))
    input.u_0(:,:) = input.abs_illumination.*exp(1i*2*pi*rand(newres));
else %i.e. (strcmp(input.formulation,'product space'))
    for j=1:input.product_space_dimension
        input.u_0(:,:,j) = input.abs_illumination.*exp(1i*2*pi*rand(newres)); 
    end
end
input.truth = true_object;
input.truth_dim = size(true_object);
input.norm_truth=norm(true_object,'fro');


%% Makes a call to Thorsten's Operator file.  Probably should
%% be in the main_HohageLuke.m master file and here, 
%% but I needed to do it this way to avoid 
%% problems with the initialization.
% [input, input_ref] = feval(input.OperatorName,input); 


clear I Y k rt_k Xi_A temp aberration theta
end



