function method_input = dictyM103_stx6_600frames(method_input)
%%  Parameters of the forward problem
method_input.OperatorName = 'FresnelPropExpAmpl';
method_input.syntheticdata_flag = false;

data_file = method_input.data_filename;
data_ball = method_input.data_ball;

disp(['Loading data file: ', data_file])

%load intensity
load dicty_hires;
data = sqrt(I_in);     %diffraction pattern (image) on detector


figure(1);
imagesc((data));colorbar;
% 
% %%  Parameters of the forward problem
% method_input.OperatorName = 'FresnelPropExpAmpl';
% method_input.syntheticdata_flag = false;



%data = DPmed; %diffraction pattern with 3x3 median filter
method_input.d1x=option.dx;
method_input.d2x=option.dy;
method_input.Nx =size(data,2);
method_input.Ny = size(data,1);
method_input.lambda=option.lambda;
method_input.data= (data);

method_input.rt_data = sqrt(method_input.data); 
method_input.norm_rt_data = norm(method_input.rt_data,'fro');
method_input.norm_data=method_input.norm_rt_data^2;

method_input.abs_illumination=ones(size(data));
% 
% method_input.Nx =size(data,2);
% method_input.Ny = size(data,1);

method_input.supp_phase = find(supp_in>0);
% method_input.supp_ampl = find(supp_in>0);

method_input.z_eff=option.zx;% distance object plane to observation plane [m]
method_input.u_0=ones(size(data));
end
