% data reader/processor

function method_input = SLM_processor(method_input)

data_ball = method_input.data_ball;

noise = method_input.noise;
snr = method_input.data_ball;

% disp(['Loading data file: pattern2.bmp'])
% S=-(double(imread('pattern2.bmp','BMP'))-1);
disp(['Loading data file: Siemens_star_200px.mat'])
    load -ascii Siemens_star_200px.mat
    S = Siemens_star_200px;
    clear Siemens_star_200px
M=ones(size(S));
S=ZeroPad(S);
M=fftshift(ZeroPad(M));
[m,N]=size(M);
S=Resize(S,m,N);
% N = length(M);
method_input.Nx=N;
method_input.Ny=m;


% method_input.data_ball=method_input.Nx*method_input.Ny*data_ball;
method_input.rt_data=M;
% standard for the main program is that 
% the data field is the magnitude SQUARED
% in Luke.m this is changed to the magnitude.
method_input.data=M.^M; 
method_input.norm_rt_data=norm(M,'fro');
method_input.data_zeros = find(M==0);
method_input.supp_ampl = find(S~=0);

% use the abs_illumination field to represent the 
% support constraint.
method_input.abs_illumination = method_input.norm_rt_data*S/(N*norm(S,'fro'));  
% input.abs_illumination = input.abs_illumination/norm(input.abs_illumination,'fro')*input.norm_rt_data(1); 

% initial guess
% method_input.u_0 = ifft2(10*M*exp(2*pi*1i*rand(N)));
% put some phase across S
% use the abs_illumination field to represent the
% support constraint.
G=Gaussian(N,10,[N/2+1,N/2+1]);
method_input.u_0=(fft2(M*exp((1i*2*pi)*G)));
% method_input.u_0 = ifft2(M.*exp(2*pi*1i*Gaussian(N,N/2,[N/2+1,N/2+1]))).*S;
% method_input.u_0 = method_input.u_0/norm(method_input.u_0,'fro')*method_input.abs_illumination; 

method_input.product_space_dimension = 1;

end



