function method_input = Goettingen_data_processor(method_input)
%%  Parameters of the forward problem

addpath('../InputData/Phase')
[fid,~]=fopen('cell256.mat');
if (fid<0)
   disp('******************************************************************')
   disp('* Input data missing.  Please download the phase input data from *') 
   disp('*    http://vaopt.math.uni-goettingen.de/data/Phase.tar.gz       *')
   disp('* Save and unpack the Phase.tar.gz datafile in the               *')
   disp('*    ProxMatlab/InputData/Phase subdirectory                     *')
   disp('******************************************************************')
else
end
fclose(fid);

experiment = method_input.experiment;

if(strcmp(experiment,'CDI')==1)
    
    disp('Loading data file CDI_intensity')
    load CDI_intensity
    % diffraction pattern
    dp = intensity.img;
    orig_res = length(dp); % actual data size
    step_up = ceil(log2(method_input.Nx)-log2(orig_res));
    workres = 2^(step_up)*2^(floor(log2(orig_res)));%desired array size

    N=workres;
    %% center data and use region of interest around center
    %central pixel
    %find max data value
    mx = max(dp(:));
    tmp = dp==mx;
    [Xi,Xj] = find(tmp);

    %get desired roi:
    %necessary conversion
    Di = N/2-(Xi-1);
    Dj = N/2-(Xj-1);

    % roi around central pixel
    Ni = 2*(Xi+Di*(Di<0)-1);
    Nj = 2*(Xj+Dj*(Dj<0)-1);

    tmp = zeros(N);
    tmp((Di*(Di>0)+1):(Di*(Di>0)+Ni),(Dj*(Dj>0)+1):(Dj*(Dj>0)+Nj)) =...
     dp((Xi-Ni/2):(Xi+Ni/2-1),(Xj-Nj/2):(Xj+Nj/2-1));
    M=(fftshift(tmp)).^(.5);
    % M=tmp.^(.5);
    %% define support
    DX = ceil(N/7);
    S = zeros(N);
    S(N/4+1+DX:3/4*N-1-DX,N/4+1+DX:3/4*N-1-DX) = 1;

    method_input.magn=1; % magnification factor
    if  method_input.fresnel_nr*method_input.magn <=  2*pi*sqrt(method_input.Nx*method_input.Ny)
        method_input.use_farfield_formula = 1;
        fprintf('Using farfield formula.\n');
    else
        method_input.use_farfield_formula = 0;
        fprintf('Using near field formula.\n');
    end
    

    % method_input.data_ball=method_input.Nx*method_input.Ny*data_ball;
    method_input.rt_data=M;
    % standard for the main program is that 
    % the data field is the magnitude SQUARED
    method_input.data=M.^M; 
    method_input.norm_rt_data=norm(ifft2(M),'fro');
    method_input.data_zeros = find(M==0);
    method_input.support_idx = find(S~=0);
    method_input.product_space_dimension = 1;

    % use the abs_illumination field to represent the 
    % support constraint.
    method_input.abs_illumination = S;  
    method_input.supp_phase = [];

    % initial guess
    method_input.u_0 = S.*rand(N);
    method_input.u_0 = method_input.u_0/norm (method_input.u_0,'fro')*method_input.norm_rt_data; 

    figure(1);
    subplot(1,2,1), 
       imagesc(log10(dp));colorbar;title('Far field data')
    subplot(1,2,2), 
      imagesc(method_input.abs_illumination); title('Support constraint')

elseif(strcmp(experiment, 'Near_field_cell_syn'))
  disp('Loading data file: cell256.mat')
  obj = load('cell256.mat');
  true_object = exp(1i*obj.pattern); % phase object
    
  [res,~]= size(true_object);
  method_input.Nx=res;
  method_input.Ny=res;
  method_input.use_farfield_formula=false;
  method_input.magn=1; % magnification factor 
  method_input.fresnel_nr = 1*2*pi*res; % Fresnel number
  [Y,X] = meshgrid((-res:2:res-1)/(2*res),(-res:2:res-1)/(2*res));
  % Fresnel propagator:
  %   the Fresnel propogator includes all relevant physical parameters
  %   so that the numerics expert need not be concerned with this
  method_input.FT_conv_kernel = fftshift(exp(-1i*(2*pi*res)^2/(2*method_input.fresnel_nr)* (X.^2 + Y.^2)));

  % simulate the hologram:
  prop_data = ifft2(method_input.FT_conv_kernel .* fft2(true_object));
  method_input.data = mypoissonrnd(method_input.snr*abs(prop_data).^2)/method_input.snr;
  method_input.data_zeros = find(method_input.data==0);
  method_input.rt_data = sqrt(method_input.data);
  method_input.norm_rt_data=sqrt(sum(sum(method_input.data)));

  % estimate the support constraint by a blurring operation.  
  %  In practice, the physicist would simply draw an envelope of
  % the hologram via visual inspection, but we do this automatically 
  % using the true object.  This leads to systemmatic errors in the 
  % reconstruction, which a methematician does not care about.
  g=Gaussian(256,[.1,.1],[128, 128]);
  blurr=fftshift(real(ifft2(fft2(obj.pattern).*fft2(g))));
  method_input.support_idx = find(blurr>10);
  method_input.indicator_ampl = double(blurr > 10);
  method_input.truth = true_object;
  method_input.truth_dim = size(true_object);
  method_input.norm_truth=norm(true_object,'fro');
  % initial guess:
  method_input.u_0=method_input.indicator_ampl;


  figure(1)
  subplot(2,2,1), 
  imagesc(real(true_object));
  title('True Object')
  subplot(2,2,2), 
  imagesc(method_input.rt_data); title('Near field data')
  subplot(2,2,3), 
  imagesc(method_input.indicator_ampl); title('Support constraint')
 
  clear obj 

elseif(strcmp(experiment,'dict')==1)
    % This demo does not seem to work.  I think the magnification factor is
    % wrong
    disp('Loading data file dict')
    load dict
    % dp=ZeroPad(dp);
    % supp=ZeroPad(supp);
    [Ny,Nx]=size(dp);
    method_input.Nx=Nx; method_input.Ny=Ny;
    % % Johann
    %     z01 = 5E-3;
    %     z12 = 5.17;
    %     M = 1+z12/z01;
    %     %
    %     % % wavelength
    %     E = 13;
    %     lambda = 12.3984/E*1E-10;
    %     %
    %     % % effective propagation distance
    %     z_eff = z12/M;
    %
    % % effective pixelsize:
    % d1x =20E-6/M;
    % d1y = 20E-6/M;
    
    % dicties
    
    z_eff = 0.0088;                % distance object plane to observation plane [m]
    lambda = 7.0846e-11;           % x-ray wavelength [m]
    d1x = 1.5672e-07;       % pixel size in object plane [m]
    d1y = 1.5672e-07;
    
    x02 = floor(Nx/2)+1;
    y02 = floor(Ny/2)+1;
    x01 = x02;
    y01 = y02;
    
    
    % create Coordinate systems %
    % Get Frequency coordinates
    xd = ((1:Nx)-x01)/Nx/d1x;
    yd = ((1:Ny)-y01)/Ny/d1y;
    [Fx,Fy] = meshgrid(xd,-yd);
    method_input.magn=1; % magnification factor

    clear xd yd;
    
    % calculate necessary propagators
    method_input.use_farfield_formula=false;
    kappa = 2*pi/lambda*sqrt(1-lambda^2*(Fx.^2+Fy.^2));
    % Fresnel propagator
    method_input.FT_conv_kernel = fftshift(exp(-sqrt(-1)*kappa*abs(z_eff)));
    method_input.fresnel_nr = 1*2*pi*Nx; % Fresnel number
    
    method_input.data = dp;
    M=sqrt(dp);       
    method_input.data_zeros = find(M==0);
    method_input.rt_data = M;
    method_input.norm_rt_data=sqrt(sum(sum(method_input.data)));
    method_input.product_space_dimension = 1;

    % support constraint
    method_input.support_idx = find(supp~=0);
    method_input.abs_illumination = supp;  
    method_input.supp_phase = [];
    method_input.supp_ampl = supp;
    method_input.indicator_ampl = double(supp > 0);

    % initial guess
    temp=M;
        
    u_0= ifft2(fft2(temp)./method_input.FT_conv_kernel);

    method_input.u_0 = u_0./(abs(u_0)); % P_ph(S,u_0);
    figure(1);
    subplot(2,2,1)
    imagesc(log10(dp));colorbar; title('Near field observation')
    subplot(2,2,2)
    imagesc(method_input.indicator_ampl);colorbar; title('support constraint')
    subplot(2,2,3)
    imagesc(real(method_input.u_0));colorbar; title('initial guess')
    

    % u_0 = S.*feval('Mgen',workres,workres,variance,variance/2,seed+1021);
    % u_0 = u_0/norm(u_0,'fro')*norm(M,'fro')/workres^(4); %/sqrt(workres);
elseif(strcmp(experiment,'Krueger')==1)
    % Sample: NTT-AT, ATN/XREESCO-50HC
    % 500 nm thick Ta structure: amplitude transmission 0.9644,
    % phase shift 0.4rad at E = 17.5 keV
    % parameters see below

    % empty waveguide (WG) beam
    load 'WG_beam.mat'

    % hologram
    disp('Loading data hologram_not-normalized.mat')
    load 'hologram_not-normalized.mat'
    I_exp(isnan(I_exp)>0) = 1;


    % The following is NOT used because it is not clear how the ``normalized" hologram 
    % is obtained.  I suspect it is obtained by simply dividing out the empty beam
    % data in the obervation plane.  But this is incorrect.  Despite the 
    % efforts of the Hohage team to get around this error by ad hoc regularization, 
    % we take the following exact approach:  back propagate the empty beam, and 
    % divide this from the unknown object in the object plane.  This approach is true 
    % to the imaging model and does not introduce any approximations. It does, however, 
    % expose the fact that we do not know the phase of the beam.   
    % clear I_exp
    % load 'data/hologram.mat'
    %% single image reconstruction

    i = sqrt(-1);

    % total number of elements
    N = numel(I_exp);

    %number of pixels
    [Ny,Nx] = size(I_exp);
    method_input.Ny=Ny; method_input.Nx=Nx;

    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Experimental parameters
    %%%%%%%%%%%%%%%%%%%%%%%%%%

    % energy in keV
    E = 17.5;

    % wavelength [m]
    lambda = 12.398/E*1E-10;
    k = 2*pi/lambda;

    % distance source-sample [m]
    z1 = 7.48e-3;

    % distance sample-detector [m]
    z2 = 3.09;

    % effective distance of detector
    z_eff = z1*z2/(z1+z2);

    % effective magnification factor
    M = (z1+z2)/z1;

    %pixel size in detector plane
    dx_det = 55e-6;
    dy_det = 55e-6;

    % magnified coordinate system in detector plane
    % [X_det,Y_det] = meshgrid(dx_det*[0:1:Nx-1],dy_det*[0:1:Ny-1]);

    % demagnifiedpixel size in sample plane
    dx = dx_det/M;
    dy = dy_det/M;

    % coordinate system in sample plane
    % [X,Y] = meshgrid(dx*((1:1:Nx)-floor(Nx/2)-1),dy*((1:1:Ny)-floor(Ny/2)-1));

    % magnified coordinate system in detector plane
    % [X_det,Y_det] = meshgrid(dx_det*((1:1:Nx)-floor(Nx/2)-1),dy_det*((1:1:Ny)-floor(Ny/2)-1));

    % grid conversion in q-space
    dqx = 2*pi/(Nx*dx);
    dqy = 2*pi/(Ny*dy);

    [Qx,Qy] = meshgrid(dqx*((1:1:Nx)-floor(Nx/2)-1),dqy*((1:1:Ny)-floor(Ny/2)-1));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Prepare data from ProxToolbox:
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Fresnel propagator:
    method_input.use_farfield_formula=false;
    method_input.Nx=Nx;
    method_input.Ny=Ny;
    method_input.fresnel_nr = 1*2*pi*Nx; % Fresnel number
    method_input.magn=1; % magnification factor (should this be M above, or 
    % is it okay since the demagnified pixel size is used?)

    kappa = sqrt(k^2-(Qx.^2+Qy.^2));
    method_input.FT_conv_kernel = fftshift(exp(-1i*kappa*z_eff));
    method_input.beam = abs(ifft2(fft2(sqrt(WG*7.210116465530644e-04))./method_input.FT_conv_kernel));  
    % the scaling factor of the waveguide array above was 
    % reverse engineered from the scaling that was apparently
    % used in the preparation of the data in the file hologram.mat
    % I don't want to use this hologram as the data, preferring instead
    % to keep the experimental data as close to the actual observation
    % as possible.  Also, I am disagreeing with what my colleagues in 
    % physics think is the correct way to compensate for a structured 
    % empty beam.  According to my reverse engineering, it appears that 
    % they have divided the EMPTY BEAM MEASUREMENT WG from the object
    % IN THE OBJECT PLANE.  It is true that you need to do the empty beam 
    % correction in the object plane, though not with the empty beam, but 
    % rather the reverse propagated empty beam, as I have done above. The
    % reason being that the empty beam is measured in the measurement plane, 
    % so it does not make sense to divide the object&beam in the object 
    % plane by the empty beam in the measurement plane -  you should divide
    % by the empty beam propagated back to the object plane.  The difficulty in 
    % this is that we do not know the phase of the empty beam.  The above 
    % formulation sets the phase of the beam to zero at the object plane. 
    % The beam is saved as the square root of the empty beam to conform with 
    % the projeciton operations working on the amplitude instead of the
    % magnitude.			

    % problem data:
    method_input.data = I_exp;
    method_input.rt_data = sqrt(I_exp);
    method_input.data_zeros = find(method_input.data==0);
    method_input.norm_rt_data=sqrt(sum(sum(method_input.data)));
    % use the abs_illumination field to represent the 
    % support constraint.
    method_input.abs_illumination = ones(size(I_exp));  
    method_input.support_idx = find(method_input.abs_illumination~=0);
    method_input.product_space_dimension = 1;
    method_input.supp_phase = [];

    % start values
    method_input.u_0=ifft2(fft2(method_input.rt_data*method_input.magn)./method_input.FT_conv_kernel)./method_input.beam;


    figure(1);
    subplot(2,2,1)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,method_input.beam);
    axis equal;
    axis tight;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    colormap(gray); colorbar; title('empty beam - detector plane')

    subplot(2,2,2)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,method_input.data);
    axis equal;
    axis tight;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    colormap(gray); colorbar; title('uncorrected near field observation')


    subplot(2,2,3)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,(abs(method_input.u_0)));
    axis image; colormap(gray); colorbar;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    title('initial guess amplitude');

    % % pattern
    subplot(2,2,4);
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,(angle(method_input.u_0))');
    axis image; colormap(gray); colorbar;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    title('initial guess phase');
    caxis([-0.9 -0.4]);

elseif(strcmp(experiment,'dictyM103_stx6_600frames')==1)
    % project stx6 
    % folder_base: 'Z:\analysis\petraIII\run7\framework\dicty_M103'
    % DRL 07.06.2017:  I don't think the parameter values are correct. 
    % hologram
    disp('Loading data dictyM103_stx6_600frames')
    load 'dictyM103_stx6_600frames'


    %% single image reconstruction

    i = sqrt(-1);

    % total number of elements
    N = numel(DP);

    %number of pixels
    [Ny,Nx] = size(DP);
    method_input.Ny=Ny; method_input.Nx=Nx;

    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Experimental parameters
    %%%%%%%%%%%%%%%%%%%%%%%%%%

    % energy in keV
    E = P.exp.E;

    % wavelength [m]
    lambda = P.exp.lambda;
    k = P.exp.k;

    % effective distance of detector
    z_eff = P.exp.z_eff;

    % effective magnification factor
    M = P.exp.M;

    %pixel size in detector plane
    dx_det = P.det.dx;
    dy_det = P.det.dy;

    % magnified coordinate system in detector plane
    % [X_det,Y_det] = meshgrid(dx_det*[0:1:Nx-1],dy_det*[0:1:Ny-1]);

    % demagnifiedpixel size in sample plane
    dx = dx_det/M;
    dy = dy_det/M;

    % coordinate system in sample plane
    % [X,Y] = meshgrid(dx*((1:1:Nx)-floor(Nx/2)-1),dy*((1:1:Ny)-floor(Ny/2)-1));

    % magnified coordinate system in detector plane
    % [X_det,Y_det] = meshgrid(dx_det*((1:1:Nx)-floor(Nx/2)-1),dy_det*((1:1:Ny)-floor(Ny/2)-1));

    % grid conversion in q-space
    dqx = 2*pi/(Nx*dx);
    dqy = 2*pi/(Ny*dy);

    [Qx,Qy] = meshgrid(dqx*((1:1:Nx)-floor(Nx/2)-1),dqy*((1:1:Ny)-floor(Ny/2)-1));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Prepare data from ProxToolbox:
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Fresnel propagator:
    method_input.use_farfield_formula=false;
    method_input.Nx=Nx;
    method_input.Ny=Ny;
    method_input.fresnel_nr = 2*pi * (dx*dy*Nx*Ny)/(z_eff*lambda);% 1*2*pi*Nx; % Fresnel number
    method_input.magn=1; % magnification factor (should this be M above, or 
    % is it okay since the demagnified pixel size is used?)

    kappa = sqrt(k^2-(Qx.^2+Qy.^2));
    method_input.FT_conv_kernel = fftshift(exp(-1i*kappa*z_eff));
    % problem data:
    method_input.data = DP;
    method_input.rt_data = sqrt(DP);
    method_input.data_zeros = find(method_input.data==0);
    method_input.norm_rt_data=sqrt(sum(sum(method_input.data)));
    % use the abs_illumination field to represent the 
    % support constraint.
    method_input.abs_illumination = ones(size(DP));  
    method_input.support_idx = find(method_input.abs_illumination~=0);% find(supp~=0);
    method_input.product_space_dimension = 1;
    method_input.supp_phase = [];

    % start values
    method_input.u_0=ifft2(fft2(method_input.rt_data*method_input.magn)./method_input.FT_conv_kernel);


    figure(1);
    subplot(2,2,1)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,method_input.data);
    axis equal;
    axis tight;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    colormap(gray); colorbar; title('near field observation')
    subplot(2,2,2)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,method_input.abs_illumination);
    axis equal;
    axis tight;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    colormap(gray); colorbar; title('support constraint')


    subplot(2,2,3)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,(abs(method_input.u_0)));
    axis image; colormap(gray); colorbar;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    title('initial guess amplitude');

    % % pattern
    subplot(2,2,4);
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,(angle(method_input.u_0)));
    axis image; colormap(gray); colorbar;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    title('initial guess phase');
    caxis([-0.9 -0.4]);
elseif(strcmp(experiment,'xenopus')==1)
    % Measuremnt of a tadpole head with the laboratory source JULIA 
    % Don't know that the parameters are correct (DRL 07.06/2017)
    % hologram
    disp('Loading data data_xenopus')
    load 'data_xenopus'


    %% single image reconstruction

    i = sqrt(-1);

    % total number of elements
    N = numel(P.I_exp);

    %number of pixels
    [Ny,Nx] = size(P.I_exp);
    method_input.Ny=Ny; method_input.Nx=Nx;

    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Experimental parameters
    %%%%%%%%%%%%%%%%%%%%%%%%%%

    % energy in keV
    E = P.E;

    % wavelength [m]
    lambda = P.lambda;
    k = 2*pi/lambda;

    % effective distance of detector
    z_eff = P.z_eff;

    % effective magnification factor
    M = P.M;

    %pixel size in detector plane
    dx_det = P.dx;
    dy_det = P.dx;

    % magnified coordinate system in detector plane
    % [X_det,Y_det] = meshgrid(dx_det*[0:1:Nx-1],dy_det*[0:1:Ny-1]);

    % demagnifiedpixel size in sample plane
    dx = dx_det/M;
    dy = dy_det/M;

    % coordinate system in sample plane
    % [X,Y] = meshgrid(dx*((1:1:Nx)-floor(Nx/2)-1),dy*((1:1:Ny)-floor(Ny/2)-1));

    % magnified coordinate system in detector plane
    % [X_det,Y_det] = meshgrid(dx_det*((1:1:Nx)-floor(Nx/2)-1),dy_det*((1:1:Ny)-floor(Ny/2)-1));

    % grid conversion in q-space
    dqx = 2*pi/(Nx*dx);
    dqy = 2*pi/(Ny*dy);

    [Qx,Qy] = meshgrid(dqx*((1:1:Nx)-floor(Nx/2)-1),dqy*((1:1:Ny)-floor(Ny/2)-1));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Prepare data from ProxToolbox:
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Fresnel propagator:
    method_input.use_farfield_formula=false;
    method_input.Nx=Nx;
    method_input.Ny=Ny;
    method_input.fresnel_nr = P.fresnelnumber; % Fresnel number
    method_input.magn=1; % magnification factor (should this be M above, or 
    % is it okay since the demagnified pixel size is used?)

    kappa = sqrt(k^2-(Qx.^2+Qy.^2));
    method_input.FT_conv_kernel = fftshift(exp(-1i*kappa*z_eff));
    % problem data:
    method_input.data = P.I_exp;
    method_input.rt_data = sqrt(P.I_exp);
    method_input.data_zeros = find(method_input.data==0);
    method_input.norm_rt_data=sqrt(sum(sum(method_input.data)));
    % use the abs_illumination field to represent the 
    % support constraint.
    method_input.abs_illumination = ones(size(P.I_exp));  
    method_input.support_idx = find(P.I_exp~=0);
    method_input.product_space_dimension = 1;
    method_input.supp_phase = [];

    % start values
    method_input.u_0=ifft2(fft2(method_input.rt_data*method_input.magn)./method_input.FT_conv_kernel);


    figure(1);
    subplot(2,2,1)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,method_input.data);
    axis equal;
    axis tight;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    colormap(gray); colorbar; title('near field observation')
    subplot(2,2,2)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,method_input.abs_illumination);
    axis equal;
    axis tight;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    colormap(gray); colorbar; title('support constraint')


    subplot(2,2,3)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,(abs(method_input.u_0)));
    axis image; colormap(gray); colorbar;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    title('initial guess amplitude');

    % % pattern
    subplot(2,2,4);
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,(angle(method_input.u_0)));
    axis image; colormap(gray); colorbar;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    title('initial guess phase');
    caxis([-0.9 -0.4]);
elseif(strcmp(experiment,'living_worm')==1)
    % Measuremnt of a tadpole head with the laboratory source JULIA 

    % hologram
    disp('Loading data data_lving_worm')
    load 'data_living_worm'


    %% single image reconstruction

    i = sqrt(-1);

    % total number of elements
    N = numel(P.I_exp);

    %number of pixels
    [Ny,Nx] = size(P.I_exp);
    method_input.Ny=Ny; method_input.Nx=Nx;

    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Experimental parameters
    %%%%%%%%%%%%%%%%%%%%%%%%%%

    % energy in keV
    E = P.E;

    % wavelength [m]
    lambda = P.lambda;
    k = 2*pi/lambda;

    % effective distance of detector
    z_eff = P.z_eff;

    % effective magnification factor
    M = P.M;

    %pixel size in detector plane
    dx_det = P.dx;
    dy_det = P.dx;

    % magnified coordinate system in detector plane
    % [X_det,Y_det] = meshgrid(dx_det*[0:1:Nx-1],dy_det*[0:1:Ny-1]);

    % demagnifiedpixel size in sample plane
    dx = dx_det/M;
    dy = dy_det/M;

    % coordinate system in sample plane
    % [X,Y] = meshgrid(dx*((1:1:Nx)-floor(Nx/2)-1),dy*((1:1:Ny)-floor(Ny/2)-1));

    % magnified coordinate system in detector plane
    % [X_det,Y_det] = meshgrid(dx_det*((1:1:Nx)-floor(Nx/2)-1),dy_det*((1:1:Ny)-floor(Ny/2)-1));

    % grid conversion in q-space
    dqx = 2*pi/(Nx*dx);
    dqy = 2*pi/(Ny*dy);

    [Qx,Qy] = meshgrid(dqx*((1:1:Nx)-floor(Nx/2)-1),dqy*((1:1:Ny)-floor(Ny/2)-1));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Prepare data from ProxToolbox:
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Fresnel propagator:
    method_input.use_farfield_formula=false;
    method_input.Nx=Nx;
    method_input.Ny=Ny;
    method_input.fresnel_nr = P.fresnelnumber; % Fresnel number
    method_input.magn=1; % magnification factor (should this be M above, or 
    % is it okay since the demagnified pixel size is used?)

    kappa = sqrt(k^2-(Qx.^2+Qy.^2));
    method_input.FT_conv_kernel = fftshift(exp(-1i*kappa*z_eff));
     % problem data:
    method_input.data = P.I_exp;
    method_input.rt_data = sqrt(P.I_exp);
    method_input.data_zeros = find(method_input.data==0);
    method_input.norm_rt_data=sqrt(sum(sum(method_input.data)));
    % use the abs_illumination field to represent the 
    % support constraint.
    method_input.abs_illumination = ones(size(P.I_exp));  
    method_input.support_idx = find(P.supp~=0);
    method_input.product_space_dimension = 1;
    method_input.supp_phase = [];

    % start values
    method_input.u_0=ifft2(fft2(method_input.rt_data*method_input.magn)./method_input.FT_conv_kernel);


    figure(1);
    subplot(2,2,1)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,method_input.data);
    axis equal;
    axis tight;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    colormap(gray); colorbar; title('near field observation')
    subplot(2,2,2)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,P.supp);
    axis equal;
    axis tight;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    colormap(gray); colorbar; title('support constraint')


    subplot(2,2,3)
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,(abs(method_input.u_0)));
    axis image; colormap(gray); colorbar;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    title('initial guess amplitude');

    % % pattern
    subplot(2,2,4);
    imagesc(Nx*dx*linspace(0,1,Ny)*1E6,Nx*dx*linspace(0,1,Nx)*1E6,(angle(method_input.u_0)));
    axis image; colormap(gray); colorbar;
    xlabel('x [\mum]');
    ylabel('y [\mum]');
    title('initial guess phase');
    caxis([-0.9 -0.4]);


end
