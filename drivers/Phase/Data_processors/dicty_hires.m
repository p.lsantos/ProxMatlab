function [prbl,reg] = dicty_hires(prbl)
%%  Parameters of the forward problem
prbl.OperatorName = 'FresnelPropExpAmpl';
prbl.syntheticdata_flag = false;


%load intensity
load dicty_hires;
data = I_in;     %diffraction pattern (image) on detector

%data = DPmed; %diffraction pattern with 3x3 median filter

prbl.data= data;
prbl.Nx =size(data,2);
prbl.Ny = size(data,1);


%supp=supp2; %use tight support
supp =supp_in; %use loose support

% define support

%load supp_for_data_without_outliers.mat;
N=prbl.Nx;
xn = [-N:2:N-1]/N;
N=prbl.Ny;
yn = [-N:2:N-1]/N;
[X,Y]=meshgrid(yn,xn);
prbl.supp_phase = find(supp>0);
prbl.supp_ampl = [];%find(supp_ampl>0);
%prbl.supp_ampl = prbl.supp_phase;%find(supp_ampl>0);

% The following 4 variables are included for convenience if experimental
% data are used. They are used only for the computation of the Fresnel
% number and the magnification factor. 
% If simulated data are used, the Fresnel number may be provided directly.
% In this case the magnification number may always be set to 1 since the
% case magn=M can be reduced to the case magn=1 by replacing fresnel_nr by
% M*fresnel_nr and intensity by intensity/M.
%
Gamma = option.zx;                % distance object plane to observation plane [m]
%R = 5e-3;                 % distance source to observation plane [m]
lambda = option.lambda;           % x-ray wavelength [m]
p_eff = option.dx; %effective pixel size
prbl.x = [0:prbl.Nx-1]*p_eff;
prbl.y = [0:prbl.Ny-1]*p_eff;
%prbl.fresnel_nr = 2*pi * (p_eff*p_eff*prbl.Nx*prbl.Ny)/(Gamma*lambda)
prbl.fresnel_nr = 2*pi*[(p_eff*prbl.Nx)^2, (p_eff*prbl.Ny)^2]/(Gamma*lambda);
%prbl.fresnel_nr=2*pi*P.fresnelnumber;
prbl.magn = 1;
prbl.intensity = 1;   
prbl.sSobo = 0.25;
%prbl.init_guess = zeros(size(prbl.supp_phase));
%prbl.phase_init_guess = [];

prbl.noiselevel = 0.01;

prbl.Nphase = length(prbl.supp_phase);

prbl.plotWhat.n1 = 1;
prbl.plotWhat.n2 = 3;
prbl.plotWhat.plots = 'PYy';

%%    parameters of the regularization method
% regularization method
reg.method = 'IRNM_KL'; 
% 'Landweber'; %'Newton_CG'; %'IRGNM_CG' %'IRNM_KL_CG'
% for further parameters see file 'complete_itreg_par.m'!
reg.inner_solver_name = 'Chambolle_Pock2_BS'; %'Chambolle_Pock2_BS';%'SQP' ;
% 'CP', CP2', 'KL_HS', BA_C' 'Chambolle_Pock2_BS', 'Chambolle_Pock_BS'
reg.alpha0 = 1; %0.5e-5;
% decreasing step for the regulation parameter (IRNM)
reg.alpha_step = 2/3;



%reg.rho = 1.65e-1;

reg.tau = 145.36;
reg.N_max_it = 45;
reg.N_CG = 100;
reg.inner_params.offset0 = 5;

reg.verbose = 2;
reg.inner_params.verbose = 1;
reg.inner_params.update_ratio = 0.0001;%0.0001; 

if strcmp(reg.inner_solver_name, 'CP') | strcmp(reg.inner_solver_name, 'CP2')

   reg.inner_params.theta = 1;
   reg.inner_params.N_CP = 400;
   reg.inner_params.sigma1 = 0.05; 
   reg.inner_params.sigma2 = 0.01;
   % set proximity maps for the data misfit term...
   reg.inner_params.Proj1 = 'kl_star';%'l2_star'; %'kl_star' % 'kl_shift_star';
   %... and the penalty term
   reg.inner_params.Proj2 = 'Sobo_sqrt'; %l2 , l1 l2_sqrt Sobo_sqrt


end

if strcmp(reg.inner_solver_name, 'Chambolle_Pock_BS') | strcmp(reg.inner_solver_name, 'Chambolle_Pock2_BS')

  reg.inner_params.type_BS_X = 'l_r'; %'H^s_r'; %'l_r';
  reg.inner_params.q = 2;
  reg.inner_params.X_r = 1.5; %1.2; 
  reg.inner_params.sSobo = 0;
  reg.inner_params.BS_X_par = prbl;
  reg.inner_params.BS_X_par.Nphase = length(prbl.supp_phase);
  reg.inner_params.type_BS_Y ='l_r';
  reg.inner_params.Y_r = 2;
  reg.inner_params.theta = 1;
  reg.inner_params.N_CP = 2000;
  reg.inner_params.sigma1 = 0.0001; %0.003; %kl 0.05, 0.5, % 4: 0.25
  reg.inner_params.sigma2 = 20; %0.02; %kl 0.01 0.005 % 4: 20
  % set proximity maps for the data misfit term...
  reg.inner_params.Proj1 = 'kl_star_in_weightedls';% 'kl_star_in_ls'; 'l2_star'; %'kl_star' % 'kl_shift_star'; 
  % normY_ToTheq_star, 'kl_star_in_ls_beta' 'kl_star_in_weightedls';
  %reg.inner_params.Proj1par.weight = 'third_moment';
  %... and the penalty term
  reg.inner_params.Proj2 = 'normX_ToTher'; %l2 , l1 l2_sqrt Sobo_sqrt, normX_ToTher, normlr_ToTher
  reg.inner_params.Proj2par.R_r = 1.5;
  reg.inner_params.Proj2par.posconstr = 0;
  reg.inner_params.gamma = 0.15;
  

end


if strcmp(reg.inner_solver_name, 'DS')  

  reg.inner_params.Proj1 = 'l2_sqrt';
  reg.inner_params.Proj2 = 'l1';
  reg.inner_params.N_DS = 45;  
  reg.N_max_it = 30;
  reg.inner_params.update_ratio = 0.0001;

 
end

end