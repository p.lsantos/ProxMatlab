% data reader/processor

function method_input = sensor_location_processor(method_input)

noise = method_input.noise;

%   disp(['Loading data file: sensor_location_data.mat'])
%   load -ascii sensor_location_data.mat
    rng(3);
    A = method_input.phys_boundary*(rand(method_input.sensors,method_input.dimension)-0.5);
    method_input.ball_radii = method_input.radius_bound*rand(1,method_input.sensors);

% initial quadrupts guess
method_input.x_0 = repmat(method_input.phys_boundary*(rand(1,method_input.dimension)-0.5),method_input.sensors,1);
method_input.u_0 = method_input.phys_boundary*(rand(method_input.sensors,method_input.dimension)-0.5);
method_input.v_0 = method_input.phys_boundary*(rand(method_input.sensors,method_input.dimension)-0.5);
method_input.w_0 = method_input.phys_boundary*(rand(method_input.sensors,method_input.dimension)-0.5);
% method_input.u_0 = ifft2(M.*exp(2*pi*1i*rand(m,n)));
% method_input.u_0 = ifft2(M.*exp(2*pi*1i*Gaussian(N,N/2,[N/2+1,N/2+1]))).*S;
% method_input.u_0 = method_input.u_0/norm(method_input.u_0,'fro')*method_input.abs_illumination; 
method_input.product_space_dimension = 1;

end