%  hyperplane projection subroutine of the
%  ART method  
%  for tomographic recostruction of a 
%  density profile.
% 
%  Russell Luke
%  Universitaet Goettingen
%  May 29, 2012
%
function v = P_sequential_hyperplane_even(input,u0)

[m,n]=size(input.A);
K=mod(2*input.iter,m)+2;
a=input.A(K,:);
aaT=(a*a')+1e-20;
b=input.b(K)/aaT;
v=a*u0/aaT;
v=u0+a'*(b-v);


clear u0 b input
return