%                      P_S.m
%             written on May 23, 2002 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               support constraints
%
% INPUT:        input = data structure with .supp_ampl a vector of indeces of the nonzero elements of the array.
%
%               u = array to be projected
%
% OUTPUT:       p_S    = the projection 
%               
% USAGE: p_S = P_S(input,u)
%
% Nonstandard Matlab function calls:  

function p_S = P_S(input,u)

p_S=zeros(size(u));
p_S(input.support_idx) = u(input.support_idx);


