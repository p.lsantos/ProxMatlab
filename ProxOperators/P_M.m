%                      P_M.m
%             written on May 23, 2002 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto Fourier
%               magnitude constraints
%
% INPUT:         input = a data structure with .data = nonegative real FOURIER DOMAIN CONSTRAINT
%                     .M = nonegative real FOURIER DOMAIN CONSTRAINT
%               u = function in the physical domain to be projected
%
% OUTPUT:       p_M    = the projection IN THE PHYSICAL (time) DOMAIN
%               phat_M = projection IN THE FOURIER DOMAIN
%               
% USAGE: [p_M,phat_M] = P_M(input,u)
%
% Nonstandard Matlab function calls:  MagProj

function p_M = P_M(input,u)


p_M = feval('ifft2',feval('MagProj',input.M,feval('fft2',u)));



