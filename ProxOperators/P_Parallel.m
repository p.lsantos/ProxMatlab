%                      P_Parallel.m
%             written on July 26, 2011 by 
%                   Russell Luke
%             University of Goettingen
%
% DESCRIPTION:  Projection subroutine onto the diagonal
%               of a product space
%
% INPUT:       input = a data structure with fields:
%                 .product_space_dimension; % the size of the product space
%                 .Nx; % the row-dim of the prod. space elements 
%                 .Ny; % the column-dim of the prod. space elements 
%                 .input.projectors; % character column vector 
                               % of projector names
%                 u = array in the domain to be projected
%
% OUTPUT:       u_Diag    = the projection, a product space array
%               
% USAGE: p_parallel = P_Parallel(input,u)
%
% Nonstandard Matlab function calls:  

function u_parallel = P_Parallel(input,u)

K = input.product_space_dimension; % the size of the product space
m = input.Nx; % the row-dim of the prod. space elements 
n = input.Ny; % the column-dim of the prod. space elements 
p = input.Nz; % the depth-dim of the prod. space elements (for 3-D matrices) 
Projectors = input.projectors; % character column vector 
                               % of projector names
if(m==1)
    u_parallel=zeros(K,n);
    for k=1:K
    	u_parallel(k,:) = feval(Projectors(k,:),input,u(k,:));
    end
elseif(n==1)
    u_parallel=zeros(m,K);
    for k=1:K
    	u_parallel(:,k) = feval(Projectors(k,:),input,u(:,k));
    end
elseif(p==1)
    u_parallel=zeros(m,n,K);
    for (k=1:K)
    	u_parallel(:,:,k) = feval(Projectors(k,:),input,u(:,:,k));
    end
else
    u_parallel=zeros(m,n,p,K);
    for (k=1:K)
    	u_parallel(:,:,:,k) = feval(Projectors(k,:),input,u(:,:,:,k));
    end
end
clear u input
return
