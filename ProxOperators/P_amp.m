%                      P_amp.m
%             written on Feb 3, 2012 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Projection subroutine for projecting onto
%               amplitude constraints
%
% INPUT:        input.amplitude =  nonnegative array
%               u = complex-valued array to be projected onto the ampltude constraints
%
% OUTPUT:       p_amp    = the projection 
%               
% USAGE: p_amp = P_amp(S,u)
%
% Nonstandard Matlab function calls:  MagProj

function p_amp = P_amp(input,u)

p_amp = feval('MagProj',input.amplitude,u);

