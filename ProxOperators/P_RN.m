%  dummy projection subroutine for the
%  fast implementation of the Cimmino ART method  
%  for tomographic recostruction of a 
%  density profile.
% 
%  Russell Luke
%  Universitaet Goettingen
%  May 29, 2016
%
function v = P_RN(input,u0)

v=u0;

clear u0 
return