%                      Q_Heau.m
%             written on MAY 31, 2006 by 
%           Russell Luke & Daniel Gempesaw
%             University of Delaware
%
% DESCRIPTION:  Q operator for the Heaugaseau-like algorithm as
%               described in Bauschke Combettes&Luke Jour. Approx. Thry., 2006
%
% INPUT:        x,y,z - all column vectors, not nec. real
%
% OUTPUT:       y_new = Q(x,y,z)
%               
% USAGE: y_new = Q_Heau(x,y,z)
%
% Nonstandard Matlab function calls:  none

function y_new = Q_Heau(x,y,z)

xsi = real((y-z)'*(x-y))+imag((y-z)'*(x-y));
eta = (x-y)'*(x-y);
nu =  (y-z)'*(y-z);
rho = eta*nu-xsi^2;

if((rho<=0)&&(xsi>=0))
    y_new = z;
elseif((rho>0)&&(xsi*nu>=rho))
    y_new = x+(1+xsi/nu)*(z-y);
elseif((rho>0)&&(xsi*nu<rho))
    y_new = y+(nu/rho)*(xsi*(x-y)+nu*(z-y));
end

