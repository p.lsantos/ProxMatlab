function u_Diag = P_Diag(input,u)
%                      P_Diag.m
%             written on July 26, 2011 by 
%                   Russell Luke
%             University of Goettingen
%
% DESCRIPTION:  Projection subroutine onto the diagonal
%               of a product space
%
% INPUT:       input = a data structure with fields:
%                  input.Nx
%                  input.Ny
%                  input.product_space_dimension
%               u = array in the domain to be projected
%
% OUTPUT:       u_Diag    = the projection, an array on the 
%                           diagonal of the product space
%               
% USAGE: p_Diag = P_Diag(input,u)
%
% Nonstandard Matlab function calls:  

K = input.product_space_dimension; % the size of the product space
% [n,m,z]=size(u);
% m = input.Ny; % the row-dim of the prod. space elements 
% n = input.Nx; % the column-dim of the prod. space elements 

if(input.Ny==1)
    tmp = sum(u,1)/K;
    u_Diag = ones(K,1)*tmp;
elseif(input.Nx==1)
    tmp = sum(u,2)/K;
    u_Diag = tmp*ones(1,K);
elseif(input.Nz==1)
    tmp=mean(u, 3);
    u_Diag = repmat(tmp,[1,1,K]);
else
    tmp=mean(u,4);
    u_Diag = repmat(tmp,[1,1,1,K]);
end

clear u input
end

