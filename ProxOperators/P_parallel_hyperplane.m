%  hyperplane projection subroutine of the
%  Cimmino ART method  
%  for tomographic recostruction of a 
%  density profile.
% 
%  Russell Luke
%  Universitaet Goettingen
%  May 29, 2012
%
function v = P_parallel_hyperplane(input,u0)

% AAT=diag(input.A*input.A')+1e-20;
% b=input.b./AAT;
% v=diag(input.A*u0)./AAT;
% v=u0+input.A'.*(ones(input.Nx,1)*(b-v)');
% the above is way slower than the loop below.

[m,n]=size(input.A);
v=zeros(n,m);
for K=1:m
  a=input.A(K,:);
  aaT=(a*a')+1e-20;
  b=input.b(K);
  v_scal=a*u0(:,K);
  v(:,K)=u0(:, K)+a'*((b-v_scal)/aaT);
end

clear u0 b AAT
return