%                      QNAP.m
%             written on May 23, 2017 by
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Averaged prox mappings with a Quasi-Newton acceleration
%
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = QNAP(method_input)
%
% Nonstandard Matlab function calls:  samsara, method_input.Prox1 and .Prox2
%    It is assumed that the Prox1 is a projection onto the diagonal of the
%    product space, P_Diag.  Must have samsara, a reverse communication nonlinear
%    optimization package installed
%
%

function method_output = QNAP(method_input)

% load samsara into the matlab path:
addpath('../../samsara/matlab/')
if(isempty(strfind(path,'samsara/matlab')))
   disp('******************************************************************')
   disp('* SAMSARA package missing.  Please download from                 *') 
   disp('*    http://vaopt.math.uni-goettingen.de/en/software.php         *')
   disp('* Save and unpack the datafile in any subdirectory in your       *')
   disp('* MATLAB path.                                                   *')
   disp('******************************************************************')
   return
end

samsara_data=[];
Prox1 = method_input.Prox1;
Prox2 = method_input.Prox2;

u = method_input.u_0;
[~,~,p,q]=size(u);
MAXIT = method_input.MAXIT;
TOL = method_input.TOL;
iter=1;
method_input.iter=iter;
% preallocate the error monitors:
change=zeros(1,MAXIT);
change(1)=999;
gap=change;
if(any(strcmp('diagnostic', fieldnames(method_input))))
    if(isempty(method_input.anim))
        anim = 0;           % numeric graphics toggle.  anim=1 generates animated picture
        %   of each iterate in succession.
    else
        anim=method_input.anim;
    end
    if(any(strcmp('truth',fieldnames(method_input))))
        Relerrs = change;
    end
    if(anim>=1)
        method_output.u=u;
        method_output=feval(method_input.animation, method_input,method_output);
    end
    
end

normM = method_input.norm_data; %sqrt(sum(sum(method_input.data.*method_input.data)));

tic;
tmp1 = feval(Prox2,method_input,u);
while((iter<MAXIT)&&(change(iter)>=TOL))
    iter=iter+1;
    method_input.iter=iter;
    tmp_u = feval(Prox1,method_input,tmp1);
    % make call to SAMSARA for acceleration step
    % since for the product space Prox1 is the
    % projection onto the diagonal, all arrays in
    % tmp_u are the same, so we can just send one
    % array to samsara.  Have to reshape the complex
    % array as a real-valued vector.  Use the gap as the
    % objective function, and the negative gradient tmp_u- u
    if(p==1)&&(q==1)
        if (isreal(u))
            if(iter>4)
                uold_vec=u(:,1);
                unew_vec=tmp_u(:,1);
                gradfold_vec = gradfnew_vec;
                gradfnew_vec = (u(:,1)- tmp_u(:,1));
                [unew_vec,uold_vec,gap(iter-1),gradfold_vec, change(iter), samsara_data]=...
                    feval('samsara', uold_vec, unew_vec, ...
                    gap(iter-2)*method_input.Nx*method_input.Ny,...
                    gap(iter-1)*method_input.Nx*method_input.Ny,...
                    gradfold_vec, gradfnew_vec, samsara_data);
                for j=1:method_input.product_space_dimension
                    tmp_u(:,j)=unew_vec;
                end
                gap(iter-1)=gap(iter-1)/(method_input.Nx*method_input.Ny);
            else
                unew_vec=tmp_u(:,1);
                uold_vec=u(:,1);
                gradfnew_vec = (u(:,1)- tmp_u(:,1));
            end
        else
            if iter>3
                uold_vec=reshape([real(u(:,1)), imag(u(:,1))], ...
                    method_input.Ny*method_input.Nx*2, 1);
                unew_vec=reshape([real(tmp_u(:,1)), imag(tmp_u(:,1))], ...
                    method_input.Ny*method_input.Nx*2, 1);
                gradfold_vec = gradfnew_vec;
                gradfnew_vec = uold_vec-unew_vec;
                [unew_vec,uold_vec,gap(iter-1),gradfold_vec, change(iter), samsara_data]=...
                    feval('samsara', uold_vec, unew_vec, ...
                    gap(iter-2)*method_input.Nx*method_input.Ny,...
                    gap(iter-1)*method_input.Nx*method_input.Ny,...
                    gradfold_vec, gradfnew_vec, samsara_data);
                % now reshape unew_vec
                tmp_u_vec = unew_vec(1:method_input.Ny*method_input.Nx)+1i*unew_vec(method_input.Ny*method_input.Nx+1:method_input.Ny*method_input.Nx*2);
                for j=1:method_input.product_space_dimension
                    tmp_u(:,j)=tmp_u_vec;
                end
                gap(iter-1)=gap(iter-1)/(method_input.Nx*method_input.Ny);
            else
                uold_vec=reshape([real(u(:,1)), imag(u(:,1))], ...
                    method_input.Ny*method_input.Nx*2, 1);
                unew_vec=reshape([real(tmp_u(:,1)), imag(tmp_u(:,1))], ...
                    method_input.Ny*method_input.Nx*2, 1);
                gradfnew_vec = uold_vec-unew_vec;
            end
        end
    elseif (p~=1)&&(q==1)
        if (isreal(u))
            tmp2 = u(:,:,1);
            uold_vec=reshape(tmp2,method_input.Nx*method_input.Ny,1);
            tmp2 = tmp_u(:,:,1);
            unew_vec = reshape(tmp2,method_input.Nx*method_input.Ny,1);
            if iter<=3
                gradfnew_vec = uold_vec-unew_vec;
            else
                gradfold_vec = gradfnew_vec;
                gradfnew_vec = uold_vec-unew_vec;
                [unew_vec,uold_vec,gap(iter-1),gradfold_vec, change(iter), samsara_data]=...
                    feval('samsara', uold_vec, unew_vec, ...
                    gap(iter-2)*method_input.Nx*method_input.Ny, ...
                    gap(iter-1)*method_input.Nx*method_input.Ny,...
                    gradfold_vec, gradfnew_vec, samsara_data);
                % now reshape and replace u 
                gap(iter-1)=gap(iter-1)/(method_input.Nx*method_input.Ny);
            end
            tmp2=reshape(unew_vec,method_input.Ny,method_input.Nx);
            for j=1:method_input.product_space_dimension
                tmp_u(:,:,j)=tmp2;
            end
        else
            tmp2=[real(u(:,:,1)),imag(u(:,:,1))];
            uold_vec=reshape(tmp2,method_input.Nx*method_input.Ny*2,1);
            tmp2=[real(tmp_u(:,:,1)), imag(tmp_u(:,:,1))];
            unew_vec = reshape(tmp2,method_input.Nx*method_input.Ny*2,1);
            if iter<=3
                gradfnew_vec = uold_vec-unew_vec;
            else
                gradfold_vec = gradfnew_vec;
                gradfnew_vec = uold_vec-unew_vec;
                [unew_vec,uold_vec,gap(iter-1),gradfold_vec, change(iter), samsara_data]=...
                    feval('samsara', uold_vec, unew_vec, ...
                    gap(iter-2)*method_input.Nx*method_input.Ny, ...
                    gap(iter-1)*method_input.Nx*method_input.Ny,...
                    gradfold_vec, gradfnew_vec, samsara_data);
                % now reshape and replace u 
                gap(iter-1)=gap(iter-1)/(method_input.Nx*method_input.Ny);
                tmp2=reshape(unew_vec,method_input.Ny,method_input.Nx*2);
                unew=tmp2(:,1:method_input.Nx) +1i*tmp2(:,method_input.Nx+1:2*method_input.Nx);
                for j=1:method_input.product_space_dimension
                    tmp_u(:,:,j)=unew;
                end
            end
        end
    else % product space of 3D arrays
        disp('Cannot handle 3D arrays on the product space yet')
    end
    % compute the normalized set distance error:
    tmp1 = feval(Prox2,method_input,tmp_u); % Prox2 is assumed here to be the Projection
    % onto the constraints in the product space
    % error(iter)=sum(sum(abs((feval('P_SP',S,tmp2))).*log(abs(tmp2)))); % entropy
    tmp_change=0; tmp_gap=0;
    if(p==1)&&(q==1)
        tmp_change= (feval('norm',u-tmp_u, 'fro')/normM)^2;
        tmp_gap = (feval('norm',tmp1-tmp_u,'fro')/normM)^2;
        if(any(strcmp('diagnostic', fieldnames(method_input))))
            if(any(strcmp('truth',fieldnames(method_input))))
                if method_input.truth_dim(1)==1
                    z=tmp_u(1,:);
                elseif method_input.truth_dim(2)==1
                    z=tmp_u(:,1);
                else
                    z=tmp_u;
                end
                Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*z))) * z, 'fro')/method_input.norm_truth;
            end
        end
    elseif(q==1)
        for j=1:method_input.product_space_dimension
            tmp_change= tmp_change+(feval('norm',u(:,:,j)-tmp_u(:,:,j), 'fro')/normM)^2;
            tmp_gap = tmp_gap+(feval('norm',tmp1(:,:,j)-tmp_u(:,:,j),'fro')/normM)^2;
        end
        if(any(strcmp('truth',fieldnames(method_input))))
            Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*tmp_u(:,:,1)))) * tmp_u(:,:,1), 'fro')/method_input.norm_truth;
        end
    else
        Relerrs(iter)=0;
        for j=1:method_input.product_space_dimension
            for(k=1:method_input.Nz)
                tmp_change= tmp_change+(feval('norm',u(:,:,k,j)-tmp_u(:,:,k,j), 'fro')/normM)^2;
                % compute (||P_Sx-P_Mx||/normM)^2:
                tmp_gap = tmp_gap+(feval('norm',tmp1(:,:,k,j)-tmp_u(:,:,k,j),'fro')/(normM))^2;
            end
            if(any(strcmp('truth',fieldnames(method_input))))&&(j==1)
                Relerrs(iter) = Relerrs(iter)+feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*tmp_u(:,:,:,1)))) * tmp_u(:,:,k,1), 'fro')/method_input.norm_truth;
            end
        end
        
    end
    change(iter)=sqrt(tmp_change);
    gap(iter) = sqrt(tmp_gap);
    u=tmp_u;
    
    if(any(strcmp('diagnostic', fieldnames(method_input))))
        % graphics
        if((anim>=1)&&(mod(iter,2)==0))
            method_output.u=tmp1;
            method_output=feval(method_input.animation, method_input,method_output);
        end
    end
end
method_output.stats.time = toc;

tmp = feval(Prox1,method_input,u);
method_output.u=tmp;

tmp2 = feval(Prox2,method_input,u);
if(method_input.Nx==1)
    method_output.u1 = tmp(:,1);
    method_output.u2 = tmp2(:,1);
elseif(method_input.Ny==1)
    method_output.u1 = tmp(1,:);
    method_output.u2 = tmp2(1,:);
elseif(method_input.Nz==1)
    method_output.u1 = tmp(:,:,1);
    method_output.u2 = tmp2(:,:,1);
else
    method_output.u1 = tmp;
    method_output.u2 = tmp2;
end
change=change(2:iter);
method_output.stats.iter = iter-1;
method_output.stats.change = change;
if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=gap(2:iter);
    method_output.stats.gap = gap;
    
    if(any(strcmp('truth',fieldnames(method_input))))
        method_output.stats.Relerrs=Relerrs;
    end
end

