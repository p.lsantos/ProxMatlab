%                    RAAR.m
%             written on Aug.18 , 2017 by
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%
% DESCRIPTION:  User-friendly version of the Relaxed Averaged Alternating Reflection algorithm.  
% 		For background see:
%                D.R.Luke, Inverse Problems 21:37-50(2005)
%                D.R. Luke, SIAM J. Opt. 19(2): 714--739 (2008).
%
%
% INPUT:  u, an array, and method_input, a data structure
%
% OUTPUT: method_input, the appended data structure with 
%               unew, an array
%
% USAGE: [unew, method_input] = RAAR(u,method_input)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2

function [unew, method_input] = RAAR(u, method_input)

beta0 = method_input.beta_0;
beta_max = method_input.beta_max;
beta_switch=method_input.beta_switch; 
iter = method_input.iter;
tmp1 = 2*feval(method_input.Prox2,method_input,u)-u;
tmp2=feval(method_input.Prox1,method_input,tmp1);
% update
beta = exp((-iter/beta_switch).^3)*beta0+(1-exp((-iter/beta_switch).^3))*beta_max; % unrelaxes as the
unew = (beta*(2*tmp2-tmp1) + (1-beta)*tmp1 + u)/2;
