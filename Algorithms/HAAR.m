%                      HAAR.m
%             written on May 31, 2006 by
%          Russell Luke & Daniel Gempesaw
%               University of Delaware
%
% DESCRIPTION:  Heaugazeau-variant of RAAR, as proposed in 
%   H. H. Bauschke, P. L. Combettes and D. R. Luke,
%   `` A Strongly Convergent Reflection Method for Finding the 
%    Projection onto the Intersection of Two Closed Convex 
%    Sets in a Hilbert Space"  
%    Journal of Approximation Theory 141(1):63-69 (2006).
% 
%  The fixed point of the algorithm is in fact the PROJECTION
%  onto the intersection.  
%
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = HAAR(method_input)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2, RAAR, Q_Heau

function method_output = HAAR(method_input)


Prox1 = method_input.Prox1;
Prox2 = method_input.Prox2;
T='RAAR_expert';
T_input = method_input;
T_input.MAXIT=1;
T_input.beta_max=.1;
T_input.beta_0=.1;
T_input.anim=0;
y0 = method_input.u_0;
MAXIT = method_input.MAXIT;
TOL = method_input.TOL;
TOL2 = method_input.TOL2;

% preallocate the error monitors:
mu0 = method_input.beta_0; 
mu_max = method_input.beta_max;

u = method_input.u_0;
[m,n,p,q]=size(y0);
iter=1;
method_input.iter=iter;
change=zeros(1,MAXIT);
change(1)=999;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  set up diagnostic arrays
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=change;
    shadow_change=change;
    if(any(strcmp('truth',fieldnames(method_input))))
        Relerrs = change;
    end
    if(~any(strcmp('anim',fieldnames(method_input))))
        anim = 0;           % numeric graphics toggle.  anim=1 generates animated picture
        %   of each iterate in succession.
    else
        anim=method_input.anim;
        if anim==2
            % preallocate an array to store the movie frames
            method_output.mov(floor(MAXIT/2)) = struct('cdata',[],'colormap',[]);
        end
    end
    if(anim>=1)
        method_output.u=u;
        method_output=feval(method_input.animation, method_input,method_output);
    end
end
normM = method_input.norm_data; % normM = sqrt(sum(sum(method_input.data.*method_input.data)));
y=y0;
if(p==1)&&(q==1)&&(n~=1)&&(m~=1)
    y0=feval('reshape',y,n*m,1);
elseif(p>1)&&(q==1)
    y0=zeros(n*m*p,1,1);
    for j=1:p
        ytmp=feval('reshape',y(:,:,j),n*m,1);
        y0(n*m*(j-1)+1:m*n*j)=ytmp;
    end
elseif(q>1)
    disp('not ready for 4D arrays')
    return
end

shadow = feval(Prox2,method_input,u);
mu=mu0;
while((iter<MAXIT)&&(change(iter)>=TOL))
  % mu = exp((-iter/7).^3)*mu0+(1-exp((-iter/7).^3))*mu_max; % unrelaxes as the
                                                                % iteration
                                                                % proceed.
  iter = iter+1;
  method_input.iter=iter;

  % next iterate
  % Ty= RAARy with beta=1 and MAXIT=1;
  T_output = feval(T,T_input);
  Ty = T_output.u;
  if(p==1)&&(q==1)&&(n~=1)&&(m~=1)
    Ty=feval('reshape',Ty,n*m,1);
    tmp_y=feval('reshape',y,n*m,1);
  elseif(p>1)&&(q==1)
    tmp_y=zeros(n*m*p,1,1);
    tmp_Ty=tmp_y;
    for j=1:p
        ytmp=feval('reshape',y(:,:,j),n*m,1);
        tmp_y(n*m*(j-1)+1:m*n*j)=ytmp;
        ytmp=feval('reshape',Ty(:,:,j),n*m,1);
        tmp_Ty(n*m*(j-1)+1:m*n*j)=ytmp;
    end
    Ty=tmp_Ty;
  elseif(q>1)
    disp('not ready for 4D arrays')
  end
  y_new=feval('Q_Heau',y0,tmp_y,(1-mu)*tmp_y+mu*Ty);

  if(p==1)&&(q==1)&&(n~=1)&&(m~=1)
    y_new=feval('reshape',y_new,m,n);
  elseif(p>1)&&(q==1)
    tmpy=zeros(m,n,p);
    for j=1:p
        tmpy(:,:,j)=feval('reshape',y_new(n*m*(j-1)+1:m*n*j),m,n);
    end
    y_new=tmpy;
  elseif(q>1)
    disp('not ready for 4D arrays')
  end

  if(any(strcmp('diagnostic', fieldnames(method_input))))
      % the next prox operations only gets used in the computation of
      % the size of the gap.  This extra step is not
      % required in alternating projections, which makes RAAR
      % and special cases thereof more expensive to monitor.
      % compute the normalized change in successive iterates:
      tmp = feval(Prox2,method_input,y_new);
      tmp2 = feval(Prox1,method_input,tmp);
  end

  % compute the normalized change in successive iterates: 
  % change(iter) = sum(sum((feval('P_M',M,u)-tmp).^2))/normM;
  % what to do if u is a 3D matrix?
  tmp_change=0; tmp_gap=0; tmp_shadow=0;
  if(p==1)&&(q==1)
      tmp_change= (feval('norm',y_new-y, 'fro')/normM)^2;
      if(any(strcmp('diagnostic', fieldnames(method_input))))
          % For Douglas-Rachford,in general it is appropriate to monitor the
          % SHADOWS of the iterates, since in the convex case these converge
          % even for beta=1.
          % (see Bauschke-Combettes-Luke, J. Approx. Theory, 2004)
          tmp_shadow = (feval('norm',tmp-shadow,'fro')/normM)^2;
          tmp_gap = (feval('norm',tmp-tmp2,'fro')/normM)^2;
          if(any(strcmp('truth',fieldnames(method_input))))
              if method_input.truth_dim(1)==1
                  z=tmp(1,:);
              elseif method_input.truth_dim(2)==1
                  z=tmp(:,1);
              else
                  z=tmp;
              end
              Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*z))) * z, 'fro')/method_input.norm_truth;
          end
      end
  elseif(q==1)
      for j=1:method_input.product_space_dimension
          tmp_change= tmp_change+(feval('norm',y_new(:,:,j)-y(:,:,j), 'fro')/normM)^2;
          if(any(strcmp('diagnostic', fieldnames(method_input))))              
              % compute (||P_SP_Mx-P_Mx||/normM)^2:
              tmp_gap = tmp_gap+(feval('norm',tmp(:,:,j)-tmp2(:,:,j),'fro')/normM)^2;
              tmp_shadow = tmp_shadow+(feval('norm',tmp(:,:,j)-shadow(:,:,j),'fro')/normM)^2;
          end
      end
      if(any(strcmp('diagnostic', fieldnames(method_input))))
          if(any(strcmp('truth',fieldnames(method_input))))
              z=tmp(:,:,1);
              Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*z))) * z, 'fro')/method_input.norm_truth;
          end
      end
  end
  change(iter)=sqrt(tmp_change);
  if(any(strcmp('diagnostic', fieldnames(method_input))))      
      gap(iter) = sqrt(tmp_gap);
      shadow_change(iter) = sqrt(tmp_shadow);% this is the Euclidean norm of the gap to
      % the unregularized set.  To monitor the Euclidean norm of the gap to the
      % regularized set is expensive to calculate, so we use this surrogate.
      % Since the stopping criteria is on the change in the iterates, this
      % does not matter.
      % graphics
      if((anim>=1)&&(mod(iter,2)==0))
          method_output.u=y_new;
          method_output=feval(method_input.animation, method_input,method_output);
      end
  end
  % update
  y=y_new;
  T_input.u_0=y;
  if(any(strcmp('diagnostic', fieldnames(method_input))))
      % For Douglas-Rachford,in general it is appropriate to monitor the
      % SHADOWS of the iterates, since in the convex case these converge
      % even for beta=1.
      % (see Bauschke-Combettes-Luke, J. Approx. Theory, 2004)
      shadow=tmp;
  end
end
u = tmp2;
tmp1 = feval(Prox1,method_input,u);
method_output.u=tmp1;
tmp2 = feval(Prox2,method_input,u);
if(method_input.Nx==1)
    method_output.u1 = tmp(:,1);
    method_output.u2 = tmp2(:,1);     
elseif(method_input.Ny==1)
    method_output.u1 = tmp(1,:);
    method_output.u2 = tmp2(1,:);     
elseif(method_input.Nz==1)
    method_output.u1 = tmp(:,:,1);
    method_output.u2 = tmp2(:,:,1); 
else
    method_output.u1 = tmp;
    method_output.u2 = tmp2;     
end
method_input.data_ball=tmp2;
change=change(2:iter);

method_output.stats.iter = iter-1;
method_output.stats.change = change;

if(any(strcmp('diagnostic', fieldnames(method_input))))    
    gap=gap(2:iter);
    shadow_change=shadow_change(2:iter);    
    method_output.stats.gap = gap;
    method_output.stats.shadow_change = shadow_change;
    if(any(strcmp('truth',fieldnames(method_input))))
        method_output.stats.Relerrs=Relerrs;
    end
end
