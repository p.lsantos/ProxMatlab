%                ADMMPlus.m
%            written on September 08, 2016 by 
%           Russell Luke (from Haifa)
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  ADMM with modification
%
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = ADMMPlus(method_input)
%
% Nonstandaard Matlab function calls:  method_input.Prox1 and .Prox2


function method_output = ADMMPlus(method_input)

Prox1 = method_input.Prox1;
Prox2 = method_input.Prox2;

if(any(strcmp('truth',fieldnames(method_input))))
    x_true = method_input.truth;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% insert defaults and initialization if user has not
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(~any(strcmp('beta_0',fieldnames(method_input))))
    beta0=2;  % default steplength
    beta_max=2;
    beta_switch = 1;
else 
    % stepsize=method_input.stepsize;
    beta0 = method_input.beta_0;
    beta_max = method_input.beta_max;
end
if(~any(strcmp('shift_data',fieldnames(method_input))))
    A=0; % default reciever location
else
    A = method_input.shift_data;
end

if(~any(strcmp('x_0',fieldnames(method_input)))) % variable naming issues here:  sometimes the variable is called x, sometimes u...
    u = method_input.u_0;  % assumes that user has initialized with a u_0 variable
    x = u+A;
    v = u; % zeros(size(method_input.x_0)); % 2*method_input.phys_boundary*(rand(method_input.sensors,method_input.dimension)-0.5);
    w = A - x; % 2*method_input.phys_boundary*(rand(method_input.sensors,method_input.dimension)-0.5);
elseif(~any(strcmp('u_0',fieldnames(method_input)))) % variable naming issues here:  sometimes the variable is called x, sometimes u...
    x = method_input.x_0;  % assumes that user has initialized with x_0 variable
    u = x-A; % zeros(size(method_input.x_0)); 
	% 2*method_input.phys_boundary*(rand    (method_input.sensors,method_input.dimension)-0.5);
    v = u; % zeros(size(method_input.x_0)); % 2*method_input.phys_boundary*(rand(method_input.sensors,method_input.dimension)-0.5);
    w = A - x; % 2*method_input.phys_boundary*(rand(method_input.sensors,method_input.dimension)-0.5);
else
    x = method_input.x_0;
    u = method_input.u_0;
    v = method_input.v_0;
    w = method_input.w_0;
end

[~,~,p,q]=size(x);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   run algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MAXIT = method_input.MAXIT;
TOL = method_input.TOL;
normM = method_input.norm_data; % normM = sqrt(sum(sum(method_input.data.*method_input.data)));
iter=1;
method_input.iter = iter;
change = zeros(1,MAXIT);
change(1) = 999;
if(any(strcmp('diagnostic', fieldnames(method_input))))
    % preallocate the error monitors:
    gap = change;
    shadow_change=change;
    if(~any(strcmp('anim',fieldnames(method_input))))
        anim = 0;           % numeric graphics toggle.  anim=1 generates animated picture
        % of each iterate in succession.
    else
        anim = method_input.anim;
    end
    if(anim>=1)
        method_output.u = u;
        method_output = feval(method_input.animation,method_input,method_output);
    end
end

tic
while((iter<MAXIT)&&(change(iter)>=TOL))
  iter=iter+1;
  beta = exp((-iter/method_input.beta_switch).^3)*beta0+(1-exp((-iter/method_input.beta_switch).^3))*beta_max; % unrelaxes as the

  method_input.iter = iter;
  tmp_x = feval(Prox1,method_input,A+v);
  tmp_u = feval(Prox2,method_input,v-w/beta);
  tmp_v = tmp_u + (tmp_x-A+w)/beta;
  tmp_w = A-tmp_x;
    
  % compute the normalized change in successive iterates: 
  % change(iter) = sum(sum((feval('P_M',M,u)-tmp).^2))/normM;
  tmp_change=0; tmp_gap=0; tmp_shadow=0;
  if(p==1)
      tmp_change= (feval('norm',[x,u,v,w]-[tmp_x,tmp_u,tmp_v,tmp_w], 'fro')/normM)^2;
      if(any(strcmp('diagnostic', fieldnames(method_input))))
          tmp_shadow = (feval('norm',u-tmp_u,'fro')/normM)^2;
          tmp_gap = (feval('norm',tmp_x-tmp_u,'fro')/normM)^2;
      end
  elseif(p>1)&&(q==1)
      for j=1:method_input.product_space_dimension
          tmp_change= tmp_change+(feval('norm',[x(:,:,j), u(:,:,j), v(:,:,j), w(:,:,j)]-[tmp_x(:,:,j), tmp_u(:,:,j), tmp_v(:,:,j), tmp_w(:,:,j)], 'fro')/normM)^2;
          if(any(strcmp('diagnostic', fieldnames(method_input))))
              % compute (||P_SP_Mx-P_Mx||/normM)^2:
              tmp_gap = tmp_gap+(feval('norm',tmp_x(:,:,j)-tmp_u(:,:,j),'fro')/normM)^2;
              tmp_shadow = tmp_shadow+(feval('norm',tmp_u(:,:,j)-u(:,:,j),'fro')/normM)^2;
          end
      end
  elseif(p>1)&&(q>1)
      for j=1:method_input.product_space_dimension
          for(k=1:method_input.Nz)
              tmp_change= tmp_change+(feval('norm',x(:,:,k,j)-tmp_x(:,:,k,j), 'fro')/normM)^2;
              if(any(strcmp('diagnostic', fieldnames(method_input))))
                  % compute (||P_Sx-P_Mx||/normM)^2:
                  tmp_gap = tmp_gap+(feval('norm',tmp_x(:,:,k,j)-tmp_u(:,:,k,j),'fro')/(normM))^2;
                  tmp_shadow = tmp_shadow+(feval('norm',tmp_u(:,:,k,j)-u(:,:,k,j),'fro')/(normM))^2;
              end
          end
      end
  end
  change(iter)=sqrt(tmp_change);
  if(any(strcmp('diagnostic', fieldnames(method_input))))
      gap(iter) = sqrt(tmp_gap);
      shadow_change(iter) = sqrt(tmp_shadow);% this is the Euclidean norm of the gap to
      % the unregularized set.  To monitor the Euclidean norm of the gap to the
      % regularized set is expensive to calculate, so we use this surrogate.
      % Since the stopping criteria is on the change in the iterates, this
      % does not matter.
      % update
      
      % graphics
      if((anim>=1)&&(mod(iter,2)==0))
          method_output.u=tmp_u;
          method_output=feval(method_input.animation, method_input,method_output);
      end
  end
  x=tmp_x;u=tmp_u;v=tmp_v;w=tmp_w;

end
method_output.stats.time=toc;
change=change(2:iter);
method_output.stats.iter = iter-1;
method_output.stats.change = change;
if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=gap(2:iter);
    shadow_change=shadow_change(2:iter);
    method_output.stats.gap = gap;
    method_output.stats.shadow_change=shadow_change;
end

if(method_input.Nz*method_input.product_space_dimension==1)
    method_output.x = x;
elseif(method_input.Nz==1)
    method_output.x = x(:,:,1);
else
    method_output.x = x(:,:,:,1);
end
method_output.u = u;
tmp = x;
tmp2 = x;

if(method_input.Nx==1)
    method_output.u1 = tmp(:,1);
    method_output.u2 = tmp2(:,1);     
elseif(method_input.Ny==1)
    method_output.u1 = tmp(1,:);
    method_output.u2 = tmp2(1,:);     
elseif(method_input.Nz==1)
    method_output.u1 = tmp(:,:,1);
    method_output.u2 = tmp2(:,:,1); 
else
    method_output.u1 = tmp;
    method_output.u2 = tmp2;     
end



