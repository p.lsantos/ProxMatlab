function u = Wirttinger_grad(input,u)
%% Make image
n1 = input.Ny;
n2 = input.Nx;
x = randn(n1,n2) + 1i*randn(n1,n2);
input.truth=x;

%% Make masks and linear sampling operators

L = input.Nz;                  % Number of masks  
Masks = zeros(n1,n2,L);  % Storage for L masks, each of dim n1 x n2

% Sample phases: each symbol in alphabet {1, -1, i , -i} has equal prob. 
for ll = 1:L, Masks(:,:,ll) = randsrc(n1,n2,[1i -1i 1 -1]); end

% Sample magnitudes and make masks 
temp = rand(size(Masks));
input.Masks = Masks .* ( (temp <= 0.2)*sqrt(3) + (temp > 0.2)/sqrt(2) );

% Make linear operators; A is forward map and At its scaled adjoint (At(Y)*numel(Y) is the adjoint) 
A = @(I)  fft2(conj(Masks) .* reshape(repmat(I,[1 L]), size(I,1), size(I,2), L));  % Input is n1 x n2 image, output is n1 x n2 x L array
At = @(Y) mean(Masks .* ifft2(Y), 3);                                              % Input is n1 x n2 X L array, output is n1 x n2 image

% Data 
Y = abs(A(x)).^2;  
input.data=Y;

%% Initialization

npower_iter = input.warmup_iter;                          % Number of power iterations 
z0 = randn(n1,n2); z0 = z0/norm(z0,'fro'); % Initial guess 
tic                                        % Power iterations 
for tt = 1:npower_iter 
    z0 = At(Y.*A(z0)); z0 = z0/norm(z0,'fro');
end
Times = toc; 

normest = sqrt(sum(Y(:))/numel(Y)); % Estimate norm to scale eigenvector 
input.norm_rt_data=normest;
 
z = normest * z0;                   % Apply scaling 
input.u_0=reshape(repmat(z,[1 L]), size(I,1), size(I,2), L);
Relerrs = norm(x - exp(-1i*angle(trace(x'*z))) * z, 'fro')/norm(x,'fro'); % Initial rel. error

%% Loop

T = 500;                            % Max number of iterations
tau0 = 330;                         % Time constant for step size
mu = @(t) min(1-exp(-t/tau0), 0.4); % Schedule for step size

tic
for t = 1:T,
    Bz = A(z);
    C  = (abs(Bz).^2-Y) .* Bz;
    grad = At(C);                    % Wirtinger gradient
    z = z - mu(t)/normest^2 * grad;  % Gradient update 
    
    Relerrs = [Relerrs, norm(x - exp(-1i*angle(trace(x'*z))) * z, 'fro')/norm(x,'fro')]; 
    Times  = [Times, toc] ;   
end
fprintf('All done!\n')

%%  Check results


