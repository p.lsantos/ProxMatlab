%                       RAAR_PHeBIE.m
%            written on 22nd February 2014 by 
%                     Matthew Tam
%                    CARMA Centre
%                 University of Newcastle
%              last modified 8th March 2014
%
% DESCRIPTION: To run the algorithm of Thibault et al. we would normal use
% the RAAR algorithim. However so that it can be compared with PHeBIE, etc,
% this algorithm file records comparable error statistics. 
%
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = RAAR_PHeBIE(method_input)
%
% Nonstandard Matlab function calls:  Prox1, Prox2, ..., Proxm


function method_output = RAAR_PHeBIE(method_input)

if length(method_input.ProxSequence)~=2
    error('Error: RAAR_PHeBIE requires precisely two prox-operators (Prox1 & Prox2).')
end
if ~isfield(method_input,'RAARwith')
    warning('Warning: "RAARwith" not specified... performing reflections w.r.t. entire varible.')
end

u     = method_input.u_0;
MAXIT = method_input.MAXIT;
TOL   = method_input.TOL;

if(isempty(method_input.anim))
    anim = 0;           % numeric graphics toggle.
else
    anim = method_input.anim;
end

iter = 1;
method_input.iter = iter;

% Should we compute errors? When solving sub-problems, we mostly do not for
% the purpose of saving time.
if(~isfield(method_input,'diagnostic') || isempty(method_input.diagnostic)...
        || ~method_input.diagnostic)
    ignore_error = true;
else
    ignore_error = false;
end
    
if ~ignore_error
    % Preallocate the error monitors:
    change    = zeros(1,MAXIT);
    change(1) = 999;
    gap       = change;
    if isfield(method_input,'error_type') && strcmp(method_input.error_type,'custom')
        numCustomErrors = size(computeErrors(u,u,method_input,u),2);
        customError = zeros(MAXIT,numCustomErrors);
        for i=1:numCustomErrors
            customError(1,i) = 999;
        end
    end
    
    if(anim>=1)
        if(anim==2)
            set(904,'DoubleBuffer','on');
            set(gca,...
                'NextPlot','replace','Visible','off')
            method_input.mov = avifile('RAARmovie.avi');
        end
        method_output.u = u;
        method_output.change = change;
        method_output = feval(method_input.animation, method_input,method_output);
    end
end




%% The main loop.
tmp_u1 = feval(method_input.Prox1,method_input,u);
while( (iter<=MAXIT) && ((ignore_error) || (change(iter)>=TOL)) )
  if method_input.verbose==1
      iter = iter+1
  else
      iter = iter+1;
  end
  method_input.iter=iter;

  %Some remark on how this code works....
  %As we do not want to perform RAAR on all the varibles of u, the RAARwith
  %gives a cell-array of string of the fields of u on which RAAR is to be
  %performed. For example, in Thibault et al's method RAARwith = {'phi'}.
  %This mean that reflections etc are performed on phi, but not on x and y
  %(the probe and object).
  
  %Relaxation parameter.
  beta = exp((-iter/method_input.beta_switch).^3)*method_input.beta_0+(1-exp((-iter/method_input.beta_switch).^3))*method_input.beta_max;

  %tmp_u3 is the reflection of u on the first set. (i.e. R_A(u)
  tmp_u3 = tmp_u1;
  for f=method_input.RAARwith
      tmp_u3.(char(f)) = 2*tmp_u1.(char(f)) - u.(char(f));
  end
  
  %tmp_u is the new iterate. ie. tmp_u = (u+R_BR_A(u)/2) is the unrelaxed version.
  tmp_u4 = feval(method_input.Prox2,method_input,tmp_u3);
  tmp_u  = tmp_u4;
  for f=method_input.RAARwith
      tmp_u.(char(f)) = (beta*(2*tmp_u4.(char(f)) - tmp_u3.(char(f))) + (1-beta)*tmp_u3.(char(f)) + u.(char(f))) / 2;
  end
  
  %tmp_u2 is the new shadow
  tmp_u2 = feval(method_input.Prox1,method_input,tmp_u);
  
  change(iter) =  feval('change',tmp_u1,tmp_u2,method_input);
  if ~ignore_error
      % Compute statistics.
      %change(iter) = feval('objective',tmp_u2,method_input); %value of objective function.
      gap(iter)    = 0;
      if isfield(method_input,'error_type') && strcmp(method_input.error_type,'custom')
          % Use a custom method to compute the errors. The method should be
          % implemented in the iteration class data structure and named
          % ``computeErrors". We the following specification:
          %     'otherErrors', a vector containing other errors of intrest.
          otherErrors = feval('computeErrors',tmp_u1,tmp_u2,method_input,tmp_u4);
          customError(iter,:) = otherErrors;
      end
      
      % Plot animation.
      if((anim>=1)&&(mod(iter,2)==0))
          method_output.u = tmp_u2;
          method_output.change = change;
          method_output=feval(method_input.animation, method_input,method_output);
      end
  end

  
  % Update with the current iterate and shadows.
  u      = tmp_u;
  tmp_u1 = tmp_u2;
  
  %[change(iter) customError(iter,5) customError(iter,3) ]
  %customError(iter,1)
  
  

  
end
method_output.u = u;

%% Clear variable local to the main algorithm loop.
clear method_input.ProxSequenceIter; 


%% Collect the output of the algorithm.
method_output.u_final = tmp_u1;

tmp = u;
if(method_input.Nx==1)
    method_output.u1 = tmp(:,1);
elseif(method_input.Ny==1)
    method_output.u1 = tmp(1,:);
else
    method_output.u1 = tmp(:,:,1);
end

change = change(2:iter);
method_output.stats.iter   = iter-1;
method_output.stats.change = change;
if ~ignore_error
    gap    = gap(2:iter);    
    method_output.stats.gap    = gap;
    
    if isfield(method_input,'error_type') && strcmp(method_input.error_type,'custom')
        method_output.stats.customError = customError(2:iter,:);
    end
    
    if(anim==2)
        method_input.mov = close(method_input.mov);
    end
end


