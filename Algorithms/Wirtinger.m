%                      Wirtinger.m
%             written on June 29, 2017 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Wirtinger flow algorithm as implemented 
%               by E. J. Candes, X. Li, and M. Soltanolkotabi
%               "Phase Retrieval via Wirtinger Flow: Theory and Algorithms" 
%               adapted for ProxToolbox
%
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = Wirttinger(method_input)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2


function method_output = Wirtinger(method_input)

%% Make image
n1 = method_input.Ny;
n2 = method_input.Nx;
L = method_input.product_space_dimension;        % Number of masks  

if(any(strcmp('truth',fieldnames(method_input))))
    x=method_input.truth;
    normM=method_input.norm_truth;
end
u = method_input.u_0;

[~,~,p,q]=size(u);

if(strcmp(method_input.experiment,'JWST'))
    Masks=zeros(n1,n2,L);
    for j=1:L
        Masks(:,:,j) = method_input.indicator_ampl.*exp(1i*method_input.illumination_phase(:,:,j));
    end
    % Masks(:,:,L)=method_input.abs_illumination;
    Y=method_input.data_sq;
elseif(strcmp(method_input.experiment,'CDP'))
    Masks = method_input.Masks; % Not sure why, but I store the 
                            % Masks conjugate to Candes and Soltanolkotabi.
    Y=method_input.data_sq;
else
    Y=method_input.data_sq;
    normM=method_input.norm_rt_data;
    Masks(1,1,1:L)=1/normM;

end
normest = method_input.norm_data; %sqrt(sum(sum(method_input.data.*method_input.data)));

% for monitoring the gap between the implicit constraints (the Fourier data
% and the diagonal of the product space).  These are only calculated when 
% the algorithm is run in diagnostic mode, i.e. it will not effect 
% cpu times if you run in ``industrial" mode without the diagnostics.
Prox1 = method_input.Prox1;
Prox2 = method_input.Prox2;

% Saving the conjugate of the mask saves on computing the conjugate
% every time the mapping A (below) is applied.

if(n2==1)
    % Make linear operators; A is forward map and At its scaled adjoint (At(Y)*numel(Y) is the adjoint)
    A = @(I)  fft(Masks .* I);  % Input is n x 1 signal, output is n x L array
    At = @(Y) repmat(mean(conj(Masks) .* ifft(Y), 2),[1 L]); % Input is n x L array, output is n x 1 signal
elseif(n1==1)
    % Make linear operators; A is forward map and At its scaled adjoint (At(Y)*numel(Y) is the adjoint)
    A = @(I)  fft(Masks .* I);  % Input is 1 x n signal, output is L x n array
    At = @(Y) repmat(mean(conj(Masks) .* ifft(Y), 1),[L 1]);  % Input is L x n array, output is 1 x n signal
else
    A = @(I)  fft2(Masks .* I);  % Input is n1 x n2 x 1 image, output is n1 x n2 x L array
    At = @(Y) reshape(repmat(mean(conj(Masks) .* ifft2(Y), 3),[1 L]), n1, n2, L); % Input is n1 x n2 X L array, output is n1 x n2 x L image
end

% %%%%%%%%%%%%%%%%%%%%%%%%%
% % initialization
% %%%%%%%%%%%%%%%%%%%%%%%%%%
% % Number of power iterations for initialization
% if(~any(strcmp('warmup_iter',fieldnames(method_input))))
%     method_input.warmup_iter=50;
% end
% npower_iter = method_input.warmup_iter;                          % Number of power iterations 
% z0 = u;
% tic                                        % Power iterations 
% for tt = 1:npower_iter 
%     z0 = At(Y.*A(z0)); z0 = z0/norm(z0(:,:,1),'fro');
% end
% Times = toc; 
% 
% u = normest * z0;                   % Apply scaling 
% % if(n2==1)
% %     Relerrs = norm(x - exp(-1i*angle(trace(x'*z))) * z, 'fro')/norm(x,'fro');
% %     method_input.u_0 = repmat(z,1,L);
% % elseif(n1==1)
% %     Relerrs = norm(x - exp(-1i*angle(trace(z'*x))) * z, 'fro')/norm(x,'fro');
% %     method_input.u_0 = repmat(z,L,1);
% % else
% %     Relerrs = norm(x - exp(-1i*angle(trace(x'*z))) * z, 'fro')/norm(x,'fro');
% %     method_input.u_0=reshape(repmat(z,[1 L]), size(z,1), size(z,2), L);
% % end
% % fprintf('Relative error after initialization: %f\n', Relerrs)
%  fprintf('Run time of initialization: %.2f\n', Times)
%  fprintf('\n')
% %%%%%%%%%%%%%%%%%%%%%%
% %  end initialization
% %%%%%%%%%%%%%%%%%%%%%%

MAXIT = method_input.MAXIT;
TOL = method_input.TOL;
iter=1;
method_input.iter=iter;
% preallocate the error monitors:
change=zeros(1,MAXIT);
change(1)=999;
if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=change;
    if(isempty(method_input.anim))
        anim = 0;           % numeric graphics toggle.  anim=1 generates animated picture
        %   of each iterate in succession.
    else
        anim=method_input.anim;
    end
    if(any(strcmp('truth',fieldnames(method_input))))
        Relerrs = change;
        Times=change;
    end
    if(anim>=1)
        method_output.u=u;
        method_output=feval(method_input.animation, method_input,method_output);
    end
    
end

tau0 = 330;                         % Time constant for step size
mu = @(t) min(1-exp(-t/tau0), 0.4); % Schedule for step size

tic;
while((iter<MAXIT)&&(change(iter)>=TOL))
    iter=iter+1;
    method_input.iter=iter;
    Bz = A(u);
    C  = (abs(Bz).^2-Y) .* Bz;
    grad = At(C);                    % Wirtinger gradient
    step = mu(iter)/normest^2 * grad;
    u = u - step;  % Gradient update
    
    % compute the normalized set distance error:
    % error(iter)=sum(sum(abs((feval('P_SP',S,tmp2))).*log(abs(tmp2)))); % entropy
    tmp_change=0; tmp_gap=0;
    if(p==1)&&(q==1)
        tmp_change= (feval('norm',step, 'fro')/normM)^2;
        if(any(strcmp('diagnostic', fieldnames(method_input))))
            Times(iter)=toc;
            tmp_u = feval(Prox1,method_input,u);
            tmp1 = feval(Prox2,method_input,tmp_u);
            tmp_gap = (feval('norm',tmp1-tmp_u,'fro')/normM)^2;
            if(any(strcmp('truth',fieldnames(method_input))))
                if method_input.truth_dim(1)==1
                    z=u(1,:);
                elseif method_input.truth_dim(2)==1
                    z=u(:,1);
                else
                    z=u(:,:,1);
                end
                Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*z))) * z, 'fro')/method_input.norm_truth;
            end
        end
    elseif(q==1)
        if(any(strcmp('diagnostic', fieldnames(method_input))))
            Times(iter)=toc;
            tmp_u = feval(Prox1,method_input,u);
            tmp1 = feval(Prox2,method_input,tmp_u);
        end
        tmp_change= L*(feval('norm',step(:,:,1), 'fro')/normM)^2;
        if(any(strcmp('diagnostic', fieldnames(method_input))))
            for j=1:method_input.product_space_dimension
                % compute (||P_SP_Mx-P_Mx||/normM)^2:
                tmp_gap = tmp_gap+(feval('norm',tmp1(:,:,j)-tmp_u(:,:,j),'fro')/normM)^2;
                if(any(strcmp('truth',fieldnames(method_input))))
                    Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*tmp_u(:,:,1)))) * tmp_u(:,:,1), 'fro')/method_input.norm_truth;
                end
                % graphics
                if((anim>=1)&&(mod(iter,2)==0))
                    method_output.u=tmp1;
                    method_output=feval(method_input.animation, method_input,method_output);
                end
            end
        end
    else
        if(any(strcmp('diagnostic', fieldnames(method_input))))
            Times(iter)=toc;
            tmp_u = feval(Prox1,method_input,u);
            tmp1 = feval(Prox2,method_input,tmp_u);
        end
        for j=1:method_input.product_space_dimension
            for(k=1:method_input.Nz)
                tmp_change= tmp_change+(feval('norm',step(:,:,k,j), 'fro')/normM)^2;
                if(any(strcmp('diagnostic', fieldnames(method_input))))
                    % compute (||P_Sx-P_Mx||/normM)^2:
                    tmp_gap = tmp_gap+(feval('norm',tmp1(:,:,k,j)-tmp_u(:,:,k,j),'fro')/(normM))^2;
                    if(any(strcmp('truth',fieldnames(method_input))))
                        Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*tmp_u(:,:,:,1)))) * tmp_u(:,:,:,1), 'fro')/method_input.norm_truth;
                    end
                    % graphics
                    if((anim>=1)&&(mod(iter,2)==0))
                        method_output.u=tmp1;
                        method_output=feval(method_input.animation, method_input,method_output);
                    end
                end
            end
        end
        
    end
    gap(iter) = sqrt(tmp_gap);
    change(iter)=sqrt(tmp_change);
end
method_output.stats.time = toc;

tmp = feval(Prox1,method_input,u);
method_output.u=tmp;

tmp2 = feval(Prox2,method_input,u);
if(method_input.Nx==1)
    method_output.u1 = tmp(:,1);
    method_output.u2 = tmp2(:,1);
elseif(method_input.Ny==1)
    method_output.u1 = tmp(1,:);
    method_output.u2 = tmp2(1,:);
elseif(method_input.Nz==1)
    method_output.u1 = tmp(:,:,1);
    method_output.u2 = tmp2(:,:,1);
else
    method_output.u1 = tmp;
    method_output.u2 = tmp2;
end
change=change(2:iter);
method_output.stats.iter = iter-1;
method_output.stats.change = change;
if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=gap(2:iter);
    method_output.stats.gap = gap;
    
    if(any(strcmp('truth',fieldnames(method_input))))
        method_output.stats.Relerrs=Relerrs;
    end
end

