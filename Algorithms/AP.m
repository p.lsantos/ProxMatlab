%                      AP.m
%             written on Aug.18, 2017 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  User-friendly version of Alternating prox mappings 
%
% INPUT:  u, an array, and method_input, a data structure
%
% OUTPUT: method_input, the appended data structure with 
%               u = the algorithm fixed point
% USAGE: [unew, method_input]= AP(u, method_input)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2


function [unew, method_input] = AP(u, method_input)

tmp_u = feval(method_input.Prox2,method_input,u);
unew = feval(method_input.Prox1,method_input,tmp_u);

