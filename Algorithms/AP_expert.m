%                      AP_expert.m
%             written on May 23, 2011 by 
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Gottingen
%
% DESCRIPTION:  Non-user-friendly version of the method of alternating prox mappings 
%
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = AP_expert(method_input)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2


function method_output = AP_expert(method_input)

Prox1 = method_input.Prox1;
Prox2 = method_input.Prox2;

u = method_input.u_0;
[~,~,p,q]=size(u);
MAXIT = method_input.MAXIT;
TOL = method_input.TOL;
iter=1;
method_input.iter=iter;
% preallocate the error monitors:
change=zeros(1,MAXIT);
change(1)=999;
if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=change;
    if(isempty(method_input.anim))
        anim = 0;           % numeric graphics toggle.  anim=1 generates animated picture
        %   of each iterate in succession.
    else
        anim=method_input.anim;
    end
    if(any(strcmp('truth',fieldnames(method_input))))
    Relerrs = change;
    end
    if(anim>=1)
        method_output.u=u;
        method_output=feval(method_input.animation, method_input,method_output);
    end
    
end

normM = method_input.norm_data; %sqrt(sum(sum(method_input.data.*method_input.data)));

tic;
tmp1 = feval(Prox2,method_input,u);
while((iter<MAXIT)&&(change(iter)>=TOL))
    iter=iter+1;
    method_input.iter=iter;
    tmp_u = feval(Prox1,method_input,tmp1);
    tmp1 = feval(Prox2,method_input,tmp_u);
    % compute the normalized set distance error:
    % error(iter)=sum(sum(abs((feval('P_SP',S,tmp2))).*log(abs(tmp2)))); % entropy
    tmp_change=0; tmp_gap=0;
    if(p==1)&&(q==1)
        tmp_change= (feval('norm',u-tmp_u, 'fro')/normM)^2;
        if(any(strcmp('diagnostic', fieldnames(method_input))))
            tmp_gap = (feval('norm',tmp1-tmp_u,'fro')/normM)^2;
            if(any(strcmp('truth',fieldnames(method_input))))
                if method_input.truth_dim(1)==1
                    z=tmp_u(1,:);
                elseif method_input.truth_dim(2)==1
                    z=tmp_u(:,1);
                else
                    z=tmp_u;
                end
                Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*z))) * z, 'fro')/method_input.norm_truth;
            end
        end
    elseif(q==1)
        for j=1:method_input.product_space_dimension
            tmp_change= tmp_change+(feval('norm',u(:,:,j)-tmp_u(:,:,j), 'fro')/normM)^2;
            if(any(strcmp('diagnostic', fieldnames(method_input))))
                % compute (||P_SP_Mx-P_Mx||/normM)^2:
                tmp_gap = tmp_gap+(feval('norm',tmp1(:,:,j)-tmp_u(:,:,j),'fro')/normM)^2;
            end
        end
        if(any(strcmp('truth',fieldnames(method_input))))
            Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*tmp_u(:,:,1)))) * tmp_u(:,:,1), 'fro')/method_input.norm_truth;
        end
    else 
        Relerrs(iter)=0;
        for j=1:method_input.product_space_dimension
            for(k=1:method_input.Nz)
                tmp_change= tmp_change+(feval('norm',u(:,:,k,j)-tmp_u(:,:,k,j), 'fro')/normM)^2;
                if(any(strcmp('diagnostic', fieldnames(method_input))))
                    % compute (||P_Sx-P_Mx||/normM)^2:
                    tmp_gap = tmp_gap+(feval('norm',tmp1(:,:,k,j)-tmp_u(:,:,k,j),'fro')/(normM))^2;
                end
            end
            if(any(strcmp('truth',fieldnames(method_input))))&&(j==1)
                Relerrs(iter) = Relerrs(iter)+feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*tmp_u(:,:,:,1)))) * tmp_u(:,:,k,1), 'fro')/method_input.norm_truth;
            end
        end
    end
    change(iter)=sqrt(tmp_change);
    if(any(strcmp('diagnostic', fieldnames(method_input))))
        gap(iter) = sqrt(tmp_gap);
        % graphics
        if((anim>=1)&&(mod(iter,2)==0))
            method_output.u=tmp1;
            method_output=feval(method_input.animation, method_input,method_output);
        end
    end
    
    % update
    u=tmp_u;
end
method_output.stats.time = toc;
tmp = feval(Prox1,method_input,u);
method_output.u=tmp;
tmp2 = feval(Prox2,method_input,u);
if(method_input.Nx==1)
    method_output.u1 = tmp(:,1);
    method_output.u2 = tmp2(:,1);     
elseif(method_input.Ny==1)
    method_output.u1 = tmp(1,:);
    method_output.u2 = tmp2(1,:);     
elseif(method_input.Nz==1)
    method_output.u1 = tmp(:,:,1);
    method_output.u2 = tmp2(:,:,1); 
else
    method_output.u1 = tmp;
    method_output.u2 = tmp2;         
end
change=change(2:iter);
method_output.stats.iter = iter-1;
method_output.stats.change = change;
if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=gap(2:iter);
    method_output.stats.gap = gap;
    
    if(any(strcmp('truth',fieldnames(method_input))))
        method_output.stats.Relerrs=Relerrs;
    end    
end

