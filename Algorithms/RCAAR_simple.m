%                        RCAAR.m
%                      Matthew Tam
%                 University of Newcastle
%                 Created: 30th Oct 2013.
%               Last modified: 6th Nov 2013.
%
%                Original file: RAAR.m
%             written on May 23, 2003 by
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%               Revised Jan.17, 2005,
%                    Russell Luke
%               University of Delaware
%
% DESCRIPTION:  Relaxed Averaged Alternating Reflection algorithm.  
% 		For background see:
%
%
%
%
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = RCAAR(method_input)
%
% Nonstandard Matlab function calls:

function method_output = RCAAR_simple(u,method_input)

% We do not set Prox1/Prox2 as other algorithm do. Instead use the
% following as a wrapper for Prox1/Prox2. P_RCAAR_select select the
% Prox operator from those specified in method_input.Prox1 (or .Prox2) and
% the method_input.Prox_iter variable.
Prox='P_cyclic_select';

u = method_input.u_0;
iter = method_input.iter;

% If method_input.formulation does not exist, i.e. not specified in 
% the *_in.m file, use the product space as the default.
if(any(strcmp('formulation',fieldnames(method_input))))
    formulation = method_input.formulation;
else
    formulation = 'product space';
end

beta0 = method_input.beta_0;
beta_max = method_input.beta_max;

if(any(strcmp('storeiter',fieldnames(method_input))))
    store_iter=method_input.storeiter;
else
    store_iter=1;
end
normM = method_input.norm_data; % normM = sqrt(sum(sum(method_input.data.*method_input.data)));

% It is convienient to sort the last iterate from each cycle (and the last
% prox operations). We use these for computing the change and the gap.
if(strcmp(formulation,'product space'))
    prox_iter_mod = 2;
    method_input.proj_iter = 2;
    last_u    = zeros([size(u) 2]);
    new_u     = -last_u;
    prox_u    = last_u;
    tmp1      = feval(Prox,method_input,u);
    prox_u(:,:,:,method_input.proj_iter) = tmp1;
elseif(strcmp(formulation,'sequential'))
    prox_iter_mod = method_input.product_space_dimension;
    method_input.proj_iter = method_input.product_space_dimension;
    tmp1      = feval(Prox,method_input,u);
    if(store_iter==1)
        last_u  = zeros([size(u) method_input.product_space_dimension]);
        prox_u = last_u;
        prox_u(:,:,method_input.proj_iter) = tmp1;
        new_u  = last_u;
    end
else
    disp('Error: Invalid formulation. Options are "product space" or "sequential".')
end

tmp1 = 2*tmp1-u;
method_input.proj_iter=mod(method_input.proj_iter,prox_iter_mod)+1;
tmp2=fevalProx,method_input,tmp1); % tmp3
% update
beta = exp((-iter/beta_switch).^3)*beta0+(1-exp((-iter/beta_switch).^3))*beta_max; % unrelaxes as the
unew = (beta*(2*tmp2-tmp1) + (1-beta)*tmp1 + u)/2; %tmp_u



  method_input.proj_iter=mod(method_input.proj_iter,prox_iter_mod)+1;
  tmp3=feval(Prox,method_input,tmp1); % tmp1 = R_{C_i}(u), tmp3=P_{C_i+1}R_{C_i}(u)
  tmp_u = (beta*(2*tmp3-tmp1) + (1-beta)*tmp1 + u)/2;
  tmp2 = feval(Prox,method_input,tmp_u);
  
  j=method_input.proj_iter;
  if(strcmp(formulation,'product space'))
      new_u(:,:,:,j)  = tmp_u;
      prox_u(:,:,:,j) = tmp2;
  elseif((store_iter==1)&&strcmp(formulation,'sequential'))
      new_u(:,:,j)  = tmp_u;
      prox_u(:,:,j) = tmp2;
      if(any(strcmp('diagnostic', fieldnames(method_input))))    
          if(any(strcmp('truth',fieldnames(method_input))))
             Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*tmp3))) * tmp3, 'fro')/method_input.norm_truth;
          end
      end
  elseif((store_iter==0)&&strcmp(formulation,'sequential'))
      %do nothing (used for error checking)
      %new_u  = tmp_u;
      %prox_u = tmp2;
  else
      disp('Error: Could not store iteration.')
  end

  
  % compute the normalized set distance error:
  % error(iter) = Error in Fourier domain:
  % error(iter)=sum(sum(abs((feval('P_SP',S,tmp2))).*log(abs(tmp2)))); % entropy

  % the next prox mapping only gets used in the computation of 
  % the size of the gap.  This extra step is not 
  % required in alternating projections, which makes RAAR
  % and special cases thereof more expensive to monitor.
  % tmp3 = feval(Prox1,method_input,tmp2); %MKT not needed for RCAAR
  % compute the normalized change in successive iterates: 
  % change(iter) = sum(sum((feval('P_M',M,u)-tmp).^2))/normM;
  % what to do if u is a 3D matrix?
  tmp_change=0; tmp_gap=0;
% %   if(method_input.Ny==1)||(method_input.Nx==1)
% %       tmp_change= (feval('norm',u-tmp_u, 'fro')/normM)^2; %%hmmm?
% %       tmp_gap = (feval('norm',tmp3-tmp2,'fro')/normM)^2;
% %   else
% %       for j=1:method_input.product_space_dimension
% %           tmp_change= tmp_change+(feval('norm',u(:,:,j)-tmp_u(:,:,j), 'fro')/normM)^2;
% %           %compute (||P_SP_Mx-P_Mx||/normM)^2:
% %           tmp_gap = tmp_gap+(feval('norm',tmp3(:,:,j)-tmp2(:,:,j),'fro')/normM)^2;
% %       end
% %   end
  if(strcmp(formulation,'product space'))
    for j=1:method_input.product_space_dimension
        for k=1:2
            tmp_change=max(tmp_change,(feval('norm',last_u(:,:,j,k)-new_u(:,:,j,k), 'fro')/normM)^2);
            if(any(strcmp('diagnostic', fieldnames(method_input))))
                tmp_gap=max(tmp_gap,(feval('norm',prox_u(:,:,j,k)-prox_u(:,:,mod(j,method_input.product_space_dimension)+1,k),'fro')/normM)^2);
            end
        end
    end
  elseif((store_iter==1)&&strcmp(formulation,'sequential'))
    for j=1:method_input.product_space_dimension
        tmp_change=max(tmp_change,(feval('norm',last_u(:,:,j)-new_u(:,:,j), 'fro')/normM)^2);
        if(any(strcmp('diagnostic', fieldnames(method_input))))
            tmp_gap=max(tmp_gap,(feval('norm',prox_u(:,:,j)-prox_u(:,:,mod(j,method_input.product_space_dimension)+1),'fro')/normM)^2);
        end
    end
  elseif((store_iter==0)&&strcmp(formulation,'sequential'))
      tmp_change=(feval('norm',tmp_u-u,'fro')/normM)^2;
      if(any(strcmp('diagnostic', fieldnames(method_input))))
          tmp_gap=(feval('norm',tmp3-tmp2,'fro')/normM)^2;
      end
  else
      disp('Error: Could not compute change or gap.')
  end

  change(iter)=sqrt(tmp_change);
  if(any(strcmp('diagnostic', fieldnames(method_input))))
      gap(iter) = sqrt(tmp_gap);
      % When using a sequential formulation, we only want to update the
      % animation after each iteration cycle, else the picture is unstable.
      if((anim>=1)&&(mod(iter,2)==0))
          if(strcmp(formulation,'sequential'))
              method_output.u=last_u;
          else
              method_output.u=u; %chooses every second iteration.
          end
          method_output=feval(method_input.animation, method_input,method_output);
      end
  end

  % update
  u=tmp_u;
  tmp1 = (2*tmp2-tmp_u);
  if((store_iter==1)&&strcmp(formulation,'sequential'))
      last_u(:,:,method_input.proj_iter)=new_u(:,:,method_input.proj_iter);
  elseif((store_iter==0)&&strcmp(formulation,'sequential'))
      %last_u=new_u;
      %do nothing
  else %strmp(formulation,'product space')
      last_u(:,:,:,method_input.proj_iter)=new_u(:,:,:,method_input.proj_iter);
  end
end
%u=tmp2;

%If the sequential formulation is used we build up a product space point,
%to allow for code reuse.
if((store_iter==1)&&strcmp(formulation,'sequential'))
    tmp  = prox_u;
    for j=1:size(tmp,3)-1
        tmp2(:,:,j)=tmp(:,:,j+1);
    end
    tmp2(:,:,size(tmp,3))=tmp(:,:,1);
elseif((store_iter==0)&&strcmp(formulation,'sequential'))
    tmp = tmp3;
    %tmp2 = tmp2;
else
    tmp = tmp2;
    %method_input.proj_iter=mod(method_input.proj_iter,prox_iter_mod)+1;
    tmp2 = tmp3;
end

method_output.u=tmp;

if(method_input.Nx==1)
    method_output.u1 = tmp(:,1);
    method_output.u2 = tmp2(:,1);     
elseif(method_input.Ny==1)
    method_output.u1 = tmp(1,:);
    method_output.u2 = tmp2(1,:);     
else
    method_output.u1 = tmp(:,:,1);
    method_output.u2 = tmp2(:,:,1); 
end
method_input.data_ball=tmp2;
change=change(2:iter);
method_output.stats.iter = iter-1;
method_output.stats.change = change;

if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=gap(2:iter);
    
    method_output.stats.gap = gap;
    if(any(strcmp('truth',fieldnames(method_input))))
        method_output.stats.Relerrs=Relerrs;
    end
end