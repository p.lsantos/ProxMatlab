%                       PHeBIE.m
%            written on 16th January 2014 by 
%                     Matthew Tam
%                    CARMA Centre
%               University of Newcastle
%          last modified on 10th February 2014
%
% DESCRIPTION:  Proximal Heterogenious Block 
%              Implicit-Explicit (PHeBIE) minimzation
% INPUT:  method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = PHeBIE(method_input)
%
% Nonstandard Matlab function calls:  Prox1, Prox2, ..., Proxm


function method_output = PHeBIE(method_input)

u     = method_input.u_0;
MAXIT = method_input.MAXIT;
TOL   = method_input.TOL;

if(isempty(method_input.anim))
    anim = 0;           % numeric graphics toggle.
else
    anim = method_input.anim;
end

iter = 1;
method_input.iter = iter;

% Should we compute errors? When solving sub-problems, we mostly do not for
% the purpose of saving time.
if(~isfield(method_input,'diagnostic') || isempty(method_input.diagnostic)...
        || ~method_input.diagnostic)
    ignore_error = true;
else
    ignore_error = false;
end

if ~ignore_error
    % Preallocate the error monitors:
    change    = zeros(1,MAXIT);
    change(1) = 999;
    gap       = change;
    if isfield(method_input,'error_type') && strcmp(method_input.error_type,'custom')
        numCustomErrors = size(computeErrors(u,u,method_input),2);
        customError = zeros(MAXIT,numCustomErrors);
        for i=1:numCustomErrors
            customError(1,i) = 999;
        end
    end
    
    if(anim>=1)
        method_output.u = u;
        method_output.change = change;
        success = feval(method_input.animation, method_input,method_output);
    end
end

tmp_u = u;

%% The main loop.
while( (iter<=MAXIT) && ((ignore_error) || (change(iter)>=TOL)) )
    if method_input.verbose==1
        iter = iter+1
    else
        iter = iter+1;
    end
    method_input.iter=iter;
    
    % Loop through the update steps.
    for s=1:length(method_input.ProxSequence)
        method_input.ProxSequenceIter = s;
        Prox  = eval(strcat('method_input.Prox',num2str(method_input.ProxSequence(s))));
        tmp_u = feval(Prox, method_input, tmp_u);
    end
    
    change(iter) =  feval('change',u,tmp_u,method_input);
    
    if ~ignore_error
        % Compute statistics.
        %change(iter) = feval('objective',tmp_u,method_input); %value of objective function.
        gap(iter) = 0;
        if isfield(method_input,'error_type') && strcmp(method_input.error_type,'custom')
            % Use a custom method to compute the errors. The method should be
            % implemented in the iteration class data structure and named
            % ``computeErrors". We the following specification:
            %     'otherErrors', a vector containing other errors of intrest.
            otherErrors = feval('computeErrors',u,tmp_u,method_input);
            customError(iter,:) = otherErrors;
        end
        
        
        
        % Plot animation.
        if((anim>=1)&&(mod(iter,2)==0))
            method_output.u = tmp_u;
            method_output.change = change;
            success=feval(method_input.animation, method_input,method_output);
        end
    end
    
    % Update with the current iterate.
    u=tmp_u;
    
    %[change(iter) TOL customError(iter,5)]
    
    
    
    
end
method_output.u = u;

%% Clear variable local to the main algorithm loop.
clear method_input.ProxSequenceIter;


%% Collect the output of the algorithm.
% method_output.u_final = u;
method_output.u=u;

tmp = u;
if(method_input.Nx==1)
    method_output.u1 = tmp(:,1);
elseif(method_input.Ny==1)
    method_output.u1 = tmp(1,:);
else
    method_output.u1 = tmp(:,:,1);
end

method_output.stats.iter   = iter-1;
change = change(2:iter);
method_output.stats.change = change;
if ~ignore_error
    gap    = gap(2:iter);
    method_output.stats.gap    = gap;
    
    if isfield(method_input,'error_type') && strcmp(method_input.error_type,'custom')
        method_output.stats.customError = customError(2:iter,:);
    end
    
    if(anim==2)
        method_input.mov = close(method_input.mov);
    end
end

