%                      HPR_expert.m
%             written on May 23, 2002 by
%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%               Revised Jan.17, 2005,
%                    Russell Luke
%               University of Delaware
%
% DESCRIPTION:  non-user-friendly version of the Heinz-Patrick-Russell algorithm.  
%               See Bauschke,Combettes&Luke, Journal of the Optical 
%   		    Society of America A, 20(6):1025-1034 (2003)
%
%
% INPUT: method_input, a data structure
%
% OUTPUT: method_output, a data structure with 
%               u = the algorithm fixed point
%               stats = [iter, change, gap]  where
%                     gap    = squared gap distance normalized by
%                              the magnitude constraint.
%                     change = the percentage change in the norm
%                              squared difference in the iterates
%                     iter   = the number of iterations the algorithm
%                              performed
%
% USAGE: method_output = HPR_expert(method_input)
%
% Nonstandard Matlab function calls:  method_input.Prox1 and .Prox2

function method_output = HPR_expert(method_input)

Prox1 = method_input.Prox1;
Prox2 = method_input.Prox2;
u = method_input.u_0;
[~,~,p,q]=size(u);
MAXIT = method_input.MAXIT;
TOL = method_input.TOL;
TOL2 = method_input.TOL2;
beta0 = method_input.beta_0;
beta_max = method_input.beta_max;
 
iter=1;
method_input.iter=iter;
% preallocate the error monitors:
change=zeros(1,MAXIT);
change(1)=999;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  set up diagnostic arrays
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=change;
    shadow_change=change;
    if(any(strcmp('truth',fieldnames(method_input))))
        Relerrs = change;
    end
    if(~any(strcmp('anim',fieldnames(method_input))))
        anim = 0;           % numeric graphics toggle.  anim=1 generates animated picture
        %   of each iterate in succession.
    else
        anim=method_input.anim;
        if anim==2
            % preallocate an array to store the movie frames
            method_output.mov(floor(MAXIT/2)) = struct('cdata',[],'colormap',[]);
        end
    end
    if(anim>=1)
        method_output.u=u;
        method_output=feval(method_input.animation, method_input,method_output);
    end
end
normM = method_input.norm_data; % normM = sqrt(sum(sum(method_input.data.*method_input.data)));
tic;

tmp = feval(Prox2,method_input,u);
tmp1 = 2*feval(Prox2,method_input,u)-u;
shadow = feval(Prox1,method_input,u);
beta=beta0;
while((iter<MAXIT+1)&&(change(iter)>=TOL))
    beta = exp((-iter/method_input.beta_switch).^3)*beta0+(1-exp((-iter/method_input.beta_switch).^3))*beta_max; % unrelaxes as the
    % iterations proceed.
    iter = iter+1;
    method_input.iter=iter;
    % label = ['iter = ',num2str(iter)];
    % disp(label)
    
    % next iterate
    % tmp_u = (feval('R_S',S,feval('R_M',M,u))+u)/2;  % (R_S(R_M(u))+u)/2
    tmp2=2*feval(Prox2,method_input,u)- u +(beta-1)*tmp;
    tmp_u = (2*feval(Prox1,method_input,tmp2)-tmp2+u +(1-beta)*tmp)/2;
    tmp = feval(Prox2,method_input,tmp_u);

    if(any(strcmp('diagnostic', fieldnames(method_input))))
      % the next prox operation only gets used in the computation of
      % the size of the gap.  This extra step is not
      % required in alternating projections, which makes RAAR
      % and special cases thereof more expensive to monitor.
      tmp3 = feval(Prox1,method_input,tmp);
    end

    % compute the normalized change in successive iterates:
    % change(iter) = sum(sum((feval('P_M',M,u)-tmp).^2))/normM;
    % For Douglas-Rachford,in general it is appropriate to monitor the
    % SHADOWS of the iterates, since in the convex case these converge
    % even for beta=1.
    % (see Bauschke-Combettes-Luke, J. Approx. Theory, 2004)
    % what to do if u is a 3D matrix?
    tmp_change=0; tmp_gap=0; tmp_shadow=0;
    if(p==1)&&(q==1)
        tmp_change= (feval('norm',u-tmp_u, 'fro')/normM)^2;
        if(any(strcmp('diagnostic', fieldnames(method_input))))
            tmp_shadow = (feval('norm',tmp3-shadow,'fro')/normM)^2;
            tmp_gap = (feval('norm',tmp3-tmp,'fro')/normM)^2;
            if(any(strcmp('truth',fieldnames(method_input))))
                if method_input.truth_dim(1)==1
                    z=tmp3(1,:);
                elseif method_input.truth_dim(2)==1
                    z=tmp3(:,1);
                else
                    z=tmp3;
                end
                Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*z))) * z, 'fro')/method_input.norm_truth;
            end
        end
    elseif(q==1)
        for j=1:method_input.product_space_dimension
            tmp_change= tmp_change+(feval('norm',u(:,:,j)-tmp_u(:,:,j), 'fro')/normM)^2;
            if(any(strcmp('diagnostic', fieldnames(method_input))))
                % compute (||P_SP_Mx-P_Mx||/normM)^2:
                tmp_gap = tmp_gap+(feval('norm',tmp3(:,:,j)-tmp(:,:,j),'fro')/normM)^2;
                tmp_shadow = tmp_shadow+(feval('norm',tmp3(:,:,j)-shadow(:,:,j),'fro')/normM)^2;
            end
        end
        if(any(strcmp('diagnostic', fieldnames(method_input))))
            if(any(strcmp('truth',fieldnames(method_input))))
                z=tmp3(:,:,1);
                Relerrs(iter) = feval('norm',method_input.truth - exp(-1i*angle(trace(method_input.truth'*z))) * z, 'fro')/method_input.norm_truth;
            end
        end
    else
        for j=1:method_input.product_space_dimension
            for(k=1:method_input.Nz)
                tmp_change= tmp_change+(feval('norm',u(:,:,k,j)-tmp_u(:,:,k,j), 'fro')/normM)^2;
                if(any(strcmp('diagnostic', fieldnames(method_input))))
                    % compute (||P_Sx-P_Mx||/normM)^2:
                    tmp_gap = tmp_gap+(feval('norm',tmp3(:,:,k,j)-tmp(:,:,k,j),'fro')/(normM))^2;
                    tmp_shadow = tmp_shadow+(feval('norm',tmp3(:,:,k,j)-shadow(:,:,k,j),'fro')/(normM))^2;
                end
            end
        end
        
    end
    change(iter)=sqrt(tmp_change);
    if(any(strcmp('diagnostic', fieldnames(method_input))))
        
        gap(iter) = sqrt(tmp_gap);
        shadow_change(iter) = sqrt(tmp_shadow);% this is the Euclidean norm of the gap to
        % the unregularized set.  To monitor the Euclidean norm of the gap to the
        % regularized set is expensive to calculate, so we use this surrogate.
        % Since the stopping criteria is on the change in the iterates, this
        % does not matter.
        
        
        % graphics
        if((anim>=1)&&(mod(iter,2)==0))
            method_output.u=tmp_u;
            method_output=feval(method_input.animation, method_input,method_output);
        end
    end
    % update
    u=tmp_u;
    shadow=tmp3;
end
method_output.stats.time = toc;

tmp = feval(Prox1,method_input,u);
method_output.u=tmp;

tmp2 = feval(Prox2,method_input,u);
if(method_input.Nx==1)
    method_output.u1 = tmp(:,1);
    method_output.u2 = tmp2(:,1);     
elseif(method_input.Ny==1)
    method_output.u1 = tmp(1,:);
    method_output.u2 = tmp2(1,:);     
elseif(method_input.Nz==1)
    method_output.u1 = tmp(:,:,1);
    method_output.u2 = tmp2(:,:,1); 
else
    method_output.u1 = tmp;
    method_output.u2 = tmp2;     

end
method_input.data_ball=tmp2;
change=change(2:iter);
method_output.stats.iter = iter-1;
method_output.stats.change = change;

if(any(strcmp('diagnostic', fieldnames(method_input))))
    gap=gap(2:iter);
    shadow_change=shadow_change(2:iter);
    
    method_output.stats.gap = gap;
    method_output.stats.shadow_change = shadow_change;
    if(any(strcmp('truth',fieldnames(method_input))))
        method_output.stats.Relerrs=Relerrs;
    end
end

