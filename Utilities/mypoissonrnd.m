function n = mypoissonrnd(lambda);
L = exp(-lambda);
n = zeros(size(lambda));
k=0;
maxit = 2000;
p = ones(size(lambda));

ind = find(p>L & lambda<200);
large_ind = find(lambda>=200);

while (~isempty(ind) & k<=maxit)
    p(ind) = p(ind).*rand(size(ind));
    n= n + (p>L);
    ind = find(p>L);
    k=k+1;
end

n(large_ind) = round(lambda(large_ind)+ sqrt(lambda(large_ind)).*randn(size(large_ind)));
neg_ind = find(n<=0);
n(neg_ind) = 0;
end
