%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                    
%                             ZeroPad.m
%                            written by
%                           Russell Luke
%                      Nachwuchsforschergruppe
%                   Inst. fuer Num. u. Angew. Math
%                       Universitaet Goettingen
%                          August 12, 2001
% DESCRIPTION: Function for zero padding an array preparatory to
% using the FFT.  Automatically resizes the array to the next
% largest diad  by adding zeros symmetrically to the array and again
% symmetrically doubles the size of the array.  Takes one or two
% dimensional arrays.
%
% INPUT:
%              m = data array
%
% OUTPUT:      padded_m = the zero padded array ready for use with
%                         the FFT.
%
% USAGE:       padded_m = ZeroPad(m)
% Non-standard function calls: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function padded_m = ZeroPad(m)

dim = size(m);
[major,orient] = max(dim);
minor = min(dim);
tmp = log2(major+1);
n = ceil(tmp);
tmp = round(2^n-(2^tmp-1));
if(mod(tmp,2)==1)
  leftpad =  (tmp+1)/2;
  rightpad = (tmp-1)/2;
else
  leftpad =  tmp/2;
  rightpad = leftpad;
end 

if(orient==2)
  tmp = zeros(minor,leftpad);
  padded_m = [tmp,m];
  tmp = zeros(minor,rightpad);
  padded_m = [padded_m,tmp];    
else
  tmp = zeros(leftpad,minor);
  padded_m = [tmp;m];
  tmp = zeros(rightpad,minor);
  padded_m = [padded_m;tmp];
end
major = 2^n;

if(minor~=1)
  tmp = log2(minor);
  tmp = round(2^n-2^tmp);
  if(mod(tmp,2)==1)
    leftpad =  (tmp+1)/2;
    rightpad = (tmp-1)/2;
  else
    leftpad =  tmp/2;
    rightpad = leftpad;
  end 
  if(orient==2)
    tmp = zeros(leftpad,major);
    padded_m = [tmp;padded_m];
    tmp = zeros(rightpad,major);
    padded_m = [padded_m;tmp];    
  else
    tmp = zeros(major,leftpad);
    padded_m = [tmp,padded_m];
    tmp = zeros(major,rightpad);
    padded_m = [padded_m,tmp];
  end  
end



