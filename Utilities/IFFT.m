function f=InvFFT(F)
% Inverse Fourier transform processor.  Takes data in the fourier domain
% that has been properly normalized and arranged (i.e. negative intensities
% resulting from Matlab's fft algorithm flipped to the propper sign) by the
% companion routine FFT.
% First we fftshift the data, then we take the negative sign of every other
% mode in each direction (x and y), and finally we
% multiply the bad boy by (sqrt(res))^2 to get the correct intensity scaling.


[ydim,xdim]=size(F);
res=max(xdim,ydim);
F=feval('fftshift',F);

if (xdim == ydim)
  F(2:2:res,:)=-F(2:2:res,:);
  F(:,2:2:res)=-F(:,2:2:res);
  F=res*F;
  f=feval('ifft2',F);
else
  F(2:2:res)=-F(2:2:res);
  F=sqrt(res)*F;
  f=feval('ifft',F);
end
end
