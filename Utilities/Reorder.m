%                          Reorder.m
%                   written on ??, 200? by 
%                        Roland Potthast
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  Reorders a 2x2 image
% INPUT: A, an image to be reordered
% OUTPUT: B, the reordered image
% USAGE: B=Reorder(A)
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function A=Reorder(B)
%
% Reorders a matrix
%

n1=size(B,1);
n2=size(B,2);


for j=1:n1

  A(n1+1-j,:)=B(j,:);
  
end

