%                written on January 17, 2005 by
%                        Russell Luke
%                   University of Delaware
%
% WBIN:    .m-file for writing binary files from matlab.
% USAGE:   Wbin(data,'filename','permission','format','precision','info')
% INPUTS:  'filename' must specify the entire directory path
%          [size] must be in matrix form
% OUTPUTS: Writes data to 'filename', returns info about
%          written data, and creates a companion .info file
%          with information about the size and class of the data.

function info=Wbin(data,file,permis,form,precis,info)

if(isreal(data))
   file=[file,'.bin'];
	[fid,message]=fopen(file,permis,form);
	if (fid<0)
	  disp(message)
	else
	  COUNT=fwrite(fid,data,precis)
	  fclose(fid);
	end

	if(~strcmp(info,'noinfo'))
	  diary([file,'.info'])
	  info=whos('data')
	  disp(['written as real ',precis])
	  diary off
	end
else
   file=[file,'.bin'];
	[fid,message]=fopen(file,permis,form);
	if (fid<0)
	  disp(message)
	else
	  COUNT=fwrite(fid,real(data),precis)
	  COUNT=fwrite(fid,imag(data),precis)
	  fclose(fid);
	end

	if(~strcmp(info,'noinfo'))
	  diary([file,'.info'])
	  info=whos('data')
	  disp(['written as complex ',precis])
	  diary off
	end
end
