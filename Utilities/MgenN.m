% Function MgenN:
% Usage: A = Mgen(k,n,v,m,seed)
% Inputs: rows, k; columns, n; variance, v; mean, m; seed. seed.
% Output: Random matrix with normally distributed (mean m, variance v) iid elements.

function A = MgenN(k,n,v,m,seed);

randn('state',seed);
A = v*(randn(k,n))+m;

