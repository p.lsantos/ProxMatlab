%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
% Function Mgen:
% Usage: A = Mgen(k,n,s,c)
% Inputs: rows, k; columns, n; scaling, s; center, c; seed, seed.
% Output: Random matrix with elements uniformly distributed on [c-s, c+s].

function A = Mgen(k,n,s,c,seed);

% rand('state',sum(100*clock));
rand('state',seed);
A = s*(rand(k,n)-.5*ones(k,n))+c;



