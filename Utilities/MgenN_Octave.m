% Function MgenN_Octave:
% Usage: A = MgenN_Octave(k,n,s,c)
% Inputs: rows, k; columns, n; scaling, s; center, c; seed, seed.
% Output: Random matrix with elements uniformly distributed on [c-s, c+s].

function A = MgenN_Octave(k,n,s,c,seed);

% rand('state',sum(100*clock));
randn('seed',seed);
A = s*(randn(k,n))+c;


%                   Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
