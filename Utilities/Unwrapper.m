function tnew=Unwrapper(Xi_A,theta)

pi2=2*pi;
thresh=pi;
theta=Xi_A.*theta;
res=length(Xi_A)+2;
% I zero pad the region of interest a little to stabilize things
temp=zeros(res,res); % used to be color
temp(2:res-1,2:res-1)=full(Xi_A);
Xi_A=temp;
temp(2:res-1,2:res-1)=full(theta);
theta=temp;
J=find(Xi_A'~=0);
istart=ceil(J(1)/res);
istop=ceil(J(length(J))/res);
Maxit=40;
Iter=0;
for i=istart:istop
  J=find(Xi_A(i,:)~=0);
  if ~isempty(J)
    lJ=length(J);
    I2=J(2:lJ)-J(1:lJ-1);
    I2=find(I2>1);
    I2=[0,I2,lJ];
    lI2=length(I2);
    for i2=2:lI2
      jstart=J(I2(i2-1)+1);
      jstop=J(I2(i2));
      lI=1;
      Iter=0;
      jump=theta(i,jstart+1:jstop)-theta(i,jstart:jstop-1);
      I=find(abs(jump)>=thresh);
      lI=length(I);
      jump=sign(jump(I))*pi2;
      I=jstart-1+I;  %
      while (Iter<Maxit & lI~=0)
	if (lI>0)
	  j=1;
	  while (j>=1 & j<=lI-1)
	    theta(i,I(j)+1:I(j+1))=theta(i,I(j)+1:I(j+1))-jump(j);
	    if (jump(j)+jump(j+1)==0)
	      j=j+2;
	    else
	      j=j+1;
	    end
	  end
	  if (lI==j)
	    theta(i,I(lI)+1:jstop)=theta(i,I(lI)+1:jstop)-jump(lI);
	  end
	end
	jump=theta(i,jstart+1:jstop)-theta(i,jstart:jstop-1);
	I=find(abs(jump)>=thresh);
	lI=length(I);
	jump=sign(jump(I))*pi2;
	I=jstart-1+I;  %
	Iter=Iter+1;
      end
      Jref=find(Xi_A(i-1,jstart:jstop)~=0);
      if (~isempty(Jref))
	temp=ceil(length(Jref)/2);
	jump=theta(i-1,jstart-1+Jref(temp))-theta(i,jstart-1+Jref(temp));
	jump=round(jump/pi2);
	if (jump~=0)
	  theta(i,jstart:jstop)=theta(i,jstart:jstop)+jump*pi2;
	end
      end
    end
  else
  end
end

tnew=theta(2:res-1,2:res-1);
