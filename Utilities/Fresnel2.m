function y = Fresnel2(A)

[Ny,Nx] = size(A);

% % Johann
% z01 = 5E-3;
% z12 = 5.17;
% M = 1+z12/z01;
% 
% % wavelength
% E = 13;
% lambda = 12.3984/E*1E-10;           
% 
% % effective propagation distance
% z_eff = z12/M;               
% 
% % effective pixelsize:
% d1x =20E-6/M;
% d1y = 20E-6/M;

% dicties

z_eff = 0.0088;                % distance object plane to observation plane [m]
lambda = 7.0846e-11;           % x-ray wavelength [m]
d1x = 1.5672e-07;       % pixel size in object plane [m]
d1y = 1.5672e-07;

x02 = floor(Nx/2)+1;
y02 = floor(Ny/2)+1;
x01 = x02;
y01 = y02;


% create Coordinate systems %
% Get Frequency coordinates
xd = ((1:Nx)-x01)/Nx/d1x;
yd = ((1:Ny)-y01)/Ny/d1y;
[Fx,Fy] = meshgrid(xd,-yd);
clear xd yd;

%calculate necessary propagators
kappa = 2*pi/lambda*sqrt(1-lambda^2*(Fx.^2+Fy.^2));
%forward
H = exp(+sqrt(-1)*kappa*abs(z_eff));

% propagate from sample plane to detector plane
E_out =(fft2(A));
E_out = circshift(circshift(E_out,[y01-1 x01-1]).*H,-[y01-1 x01-1]);
y = (ifft2(E_out));


end