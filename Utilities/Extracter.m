%                          Extracter.m
%                   written on Aug 20, 2002 by 
%                           Russell Luke
%        Inst. Fuer Numerische und Angewandte Mathematik
%                    Universitaet Goettingen
%
% DESCRIPTION:  Extracts a 2x2 image from a zero field
% INPUT:  u, an array with an imbedded image
%         l, left pixel adjustment
%         r, right pixel adjustment
%         up, upper pixel adjustment
%         dn, lower pixel adjustment
% OUTPUT: u, the extracted image
% USAGE:  U=Extracter(u,l,r,up,dn)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function u=Extracter(u,l,r,up,dn)

tmp=Laplacian(u,[1,1]);
tmp = (max(1e10-1e9,min(1e10,abs(tmp)))-(1e10-1e9))/1e9;
tmpx = abs(tmp(64,2:128)-tmp(64,1:127));
west = min(find(tmpx==1))+l;
east = max(find(tmpx==1))+r;
tmpy1 = abs(tmp(2:128,64-1)-tmp(1:127,64-1));
north = min(find(tmpy1==1))+up;
south = max(find(tmpy1==1))+dn;
u=u(north:south, west:east);
