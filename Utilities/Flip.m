%                          Flip.m
%                written on Aug 20, 2002 by 
%                        Russell Luke
%   Inst. Fuer Numerische und Angewandte Mathematik
%                Universitaet Goettingen
%
% DESCRIPTION:  Flips a 2x2 image
% INPUT: A, an image to be flipped
% OUTPUT: B, the flipped image
% USAGE: B=Flip(A)
%
% NONSTANDARD FUNCTION CALLS:  Reorder.m
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function B=Flip(A)


B = Reorder(Reorder(A).')';
