%						Rbin.m
%      written January 23, 2005
%            by Russell Luke
%        University of Delaware
%
% RBIN: .m-file for reading binary files into matlab.
% USAGE: data=Rbin('filename','permission','format',[size],'precision','type')
% INPUTS:  'filename' must specify the entire directory path
%          [size] must be in matrix form, and 'type' is either 'real' or not
% OUTPUTS: reads data from 'filename'

function data=Rbin(file,permis,form,size,precis,type)
if(strcmp(type,'real'))
  [fid,message]=fopen(file,permis,form);
  if (fid<0)
    disp(message)
  else
    data=fread(fid,size,precis);
    fclose(fid);
  end
else
  [fid,message]=fopen(file,permis,form);
  if (fid<0)
    disp(message)
  else
    data=fread(fid,size,precis);
    data=data+1i*fread(fid,size,precis);	 
    fclose(fid);
  end
end
