function F=FastFT(f)
% Fourier transform processor.   FFT's data in the physical domain and
% corrects for the peculiarities of matlab's fft algorithm.
% First we fft the data, then we take the negative sign of every other
% mode in each direction (x and y), then we
% divide the bad boy by (sqrt(res))^2 to get the correct intensity scaling,
% finally we fftshift everything.

[ydim,xdim]=size(f);
res=max(xdim,ydim);

if (xdim == ydim)
  F=feval('fft2',f);
  F=F/(res);
  F(2:2:res,:)=-F(2:2:res,:);
  F(:,2:2:res)=-F(:,2:2:res);
else
  F=feval('fft',f);
  F(2:2:res)=-F(2:2:res);
  F=F/sqrt(res);
end

F=feval('fftshift',F);
end
