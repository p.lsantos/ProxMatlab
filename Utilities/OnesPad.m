%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                    
%                             OnesPad.m
%                            written by
%                           Russell Luke
%                   Inst. fuer Num. u. Angew. Math
%                       Universitaet Goettingen
%                          Oct 12, 2011
% DESCRIPTION: Function for padding an array with ones.
% Automatically resizes the array to the next
% largest diad  by adding zeros symmetrically to the array and again
% symmetrically doubles the size of the array.  Takes one or two
% dimensional arrays.
%
% INPUT:
%              m = data array
%
% OUTPUT:      padded_m = the ones padded array
%
% USAGE:       padded_m = OnesPad(m)
% Non-standard function calls: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function padded_m = OnesPad(m)

dim = size(m);
[major,orient] = max(dim);
minor = min(dim);
tmp = log2(major+1);
n = ceil(tmp);
tmp = round(2^n-(2^tmp-1));
if(mod(tmp,2)==1)
  leftpad =  (tmp+1)/2;
  rightpad = (tmp-1)/2;
else
  leftpad =  tmp/2;
  rightpad = leftpad;
end 

if(orient==2)
  tmp = ones(minor,leftpad);
  padded_m = [tmp,m];
  tmp = ones(minor,rightpad);
  padded_m = [padded_m,tmp];    
else
  tmp = ones(leftpad,minor);
  padded_m = [tmp;m];
  tmp = ones(rightpad,minor);
  padded_m = [padded_m;tmp];
end
major = 2^n;

if(minor~=1)
  tmp = log2(minor);
  tmp = round(2^n-2^tmp);
  if(mod(tmp,2)==1)
    leftpad =  (tmp+1)/2;
    rightpad = (tmp-1)/2;
  else
    leftpad =  tmp/2;
    rightpad = leftpad;
  end 
  if(orient==2)
    tmp = ones(leftpad,major);
    padded_m = [tmp;padded_m];
    tmp = ones(rightpad,major);
    padded_m = [padded_m;tmp];    
  else
    tmp = ones(major,leftpad);
    padded_m = [tmp,padded_m];
    tmp = ones(major,rightpad);
    padded_m = [padded_m,tmp];
  end  
end



