\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{fullpage}
\usepackage{listings}
\usepackage{graphicx}

\lstset{language=Matlab,basicstyle=\ttfamily}

\title{PHeBIE Documentation}
\author{Matthew K. Tam}

\begin{document}
\maketitle

\begin{abstract}
 This documentation explains how to run the \textsc{Matlab} implementation of {\nobreak PHeBIE} so that the results in ``\emph{Proximal Heterogeneous Block Input-Output Method and application to Blind Ptychographic Diffraction Imaging}" can be reproduced.
\end{abstract}

\section{How to Reproduce the Results (Short Version)}
To reproduce the results the two command must be run. The first command runs the experiments themselves:
\begin{lstlisting}
 >> cd ProxToolbox_v2.0/drivers
 >> runAllPtycho
\end{lstlisting}
This command saves the output data in the \texttt{OutputData} directory. Using these files, the second command produces the tables in Section~\ref{results} in the paper. They are included here in Section~\ref{results}.
\begin{lstlisting}
 >> cd Ptychography
 >> bash table.sh
\end{lstlisting}


\section{How to Reproduce the Results (Long Version)}
The basic idea to run code within the ProxToolbox is the call the function
\begin{lstlisting}
 >> main_ProxToolbox(<input_file>)
\end{lstlisting}
The argument \texttt{<input\_file>} is the filename (without extension) of the \emph{input file} which is located in \texttt{drivers/Ptychography}. For a sample input file, see
 \vspace{-1ex}\begin{center}\texttt{drivers/Ptychography/Ptychography\_in.m}\end{center}
All the input files for this experiment are contained in the folder
 \vspace{-1ex}\begin{center}\texttt{drivers/Ptychography/ptychography\_experiment\_input\_files\_v3}\end{center}
To run these input file one must first add the directory to the \textsc{Matlab} path using:
\begin{lstlisting}
 >> addpath('Ptychography/ptychography_experiment_input_files_v3');
\end{lstlisting}
Furthermore, the following command will run scripts which execute all the input files for each of the experiments:
\begin{lstlisting}
 >> run_Ptychography_noNoise;
 >> run_Ptychography_poisson;
 >> run_Ptychography_restrictedPupil;
\end{lstlisting}


%\newpage

\section{Code Structure}
In this section with give a summary of the function of each file. Further details can be found with the header of these files.
\subsection{The cPtychography Class}
The basic data structure for the ptychography problem is the \texttt{cPtychography} class which is located in \texttt{drivers/Ptychography/cPtychography.m}. It works as follows:
\begin{itemize}
 \item  The class is initialized by calling
  \begin{lstlisting}
   >> u = cPtychography(<phi>,<object>,<probe>);
  \end{lstlisting}
  \item The variables are then accessed by calling \texttt{u.phi}, \texttt{u.object}, and \texttt{u.probe}.
  \item When a PALM-type algorithm is used, the \emph{objective function} is implemented within this class, and its value is given by
   \begin{lstlisting}
    >> objFunVal = objective(<cPtychography>,method_input);
   \end{lstlisting}
   where \texttt{method\_input} is the \emph{problem family} strut given by the input file.
  \item The various errors (RMS-errors, R-factor, change, etc) are  similarly implemented within this class.
\end{itemize}

\subsection{Summary of Other Code}
\noindent\texttt{ProxToolbox\_v2.0/Algorithms:}\vspace{-1ex}
\begin{itemize}
 \item \texttt{PALM.m} -- implementation of the PALM algorithm. Similar to \texttt{AP.m} but allows for an objective functions and more than two \emph{prox-operators}.
 \item \texttt{RAAR\_PALM.m} -- implementation of the Douglas--Rachford algorithm as if it were PALM (i.e., has an objective functions, etc). Used mainly for Thibault's method.
\end{itemize}
\noindent\texttt{ProxToolbox\_v2.0/drivers/Ptychography/ProxOperators:}\vspace{-1ex}
\begin{itemize}
 \item \texttt{P\_ptychography\_PHeBIE\_probe.m} -- Performs the PHeBIE probe-update to a instances of \texttt{cPtychography}.
 \item \texttt{P\_ptychography\_PHeBIE\_object.m} -- Performs the PHeBIE object-update to a instances of \texttt{cPtychography}.
 \item \texttt{P\_ptychography\_PHeBIE\_phi.m} -- Performs the PHeBIE phi-update to a instances of \texttt{cPtychography} using \texttt{MagProj.m}.
instances of \texttt{cPtychography}.
 \item \texttt{P\_ptychography\_PHeBIE\_phi\_regularized.m} -- Performs the regularized PHeBIE phi-update to a instances of \texttt{cPtychography} using \texttt{MagProj.m}.
 \item \texttt{P\_ptychography\_Rodenburg.m} -- Performs one update of each of the variables (phi, object, probe) to an instance of \texttt{cPtychography} using Madien and Rodenburg's method.
 \item \texttt{P\_ptychography\_Rodenburg\_probe\_fixed.m} -- Performs one update of each of the variables, excluding the probe, to an instance of \texttt{cPtychography} using Madien and Rodenburg's method. Use for the warm-up phase of the algorithm.
 \item \texttt{P\_ptychography\_Thibault\_F.m} -- Performs the phi-update to an instances of \texttt{cPtychography} for Thibault's method.
 \item \texttt{P\_ptychography\_Thibault\_O.m} -- Performs the object-update to an instances of \texttt{cPtychography} for Thibault's method (probe fixed). Use in the warm-up phase of the algorithm.
 \item \texttt{P\_ptychography\_Thibault\_OP.m} -- Performs the (object,probe)-update to an instances of \texttt{cPtychography} for Thibault's method.
\end{itemize}
\noindent\texttt{ProxToolbox\_v2.0/drivers/Ptychography/}\vspace{-1ex}
\begin{itemize}
 \item \texttt{Ptychography.m} -- This file contains initialization for the ptychography run. The user need not touch this, as all parameters are set in the input file.
\end{itemize}
\noindent\texttt{ProxToolbox\_v2.0/drivers/Ptychography/Ptychography\_data}\vspace{-1ex}
\begin{itemize}
 \item \texttt{Ptychography\_data\_processor.m} -- Takes raw data from within \texttt{InputData} and processes it into the form used by the rest of the ProxToolbox.
 \item \texttt{InputData} -- the directory containing the raw data to be reconstructed.
\end{itemize}


\newpage
\section{Results}\label{results}
\begin{table}[htbp]
 \caption{Average (Worst) Results for noiseless simulated data.}
 \vspace{5pt}
 \resizebox{\textwidth}{!}{
  \begin{tabular}{lcccccc} \hline
   Algorithm                   & $ F(u^{300})$      & $\|u^{300}-u^{299}\|^2$ & RMS-Object       & RMS-Probe        & R-factor         & Time (s)          \\ \hline
PHeBIE 	&  99.6318 (126.641590) 	&  0.593135 (0.938263) 	&  0.0410154 (0.046059) 	&  0.0155114 (0.022207) 	&  0.0131046 (0.015402) 	&  913.746 (925.850000) \\
PHeBIE (pointwise) 	&  70.7578 (77.165558) 	&  0.220963 (0.352208) 	&  0.0423406 (0.047057) 	&  0.0081282 (0.015362) 	&  0.010106 (0.010802) 	&  636.74 (652.170000) \\
Rodenburg \& Madien 	&  948.134 (1499.112771) 	&  5.51636 (8.488472) 	&  0.0541556 (0.059036) 	&  0.0951512 (0.171364) 	&  0.035019 (0.041945) 	&  1178.21 (1198.720000) \\
Thibault {\em et al} with AP 	&  91.2847 (142.790706) 	&  0.449708 (1.479907) 	&  0.0426212 (0.048176) 	&  0.0085614 (0.016298) 	&  0.011396 (0.014362) 	&  904.098 (920.220000) \\
Thibault {\em et al} 	&  4347.08 (4554.284684) 	&  28.8622 (34.442153) 	&  0.0515276 (0.064176) 	&  0.0240348 (0.037833) 	&  0.024387 (0.026408) 	&  875.94 (887.760000) \\
  \hline
  \end{tabular}
 }\\
\end{table}




\begin{table}[htbp]
 \caption{Average (Worst) Results for simulated data with Poisson noise.}
 \vspace{5pt}
 \resizebox{\textwidth}{!}{
  \begin{tabular}{lcccccc} \hline
   Algorithm                   & $ F(u^{300})$      & $\|u^{300}-u^{299}\|^2$ & RMS-Object       & RMS-Probe        & R-factor         & Time (s)          \\ \hline
PHeBIE 	&  1.44145e+07 (69222218.500499) 	&  4.15036 (14.082309) 	&  0.192788 (0.683974) 	&  0.189575 (0.708388) 	&  0.349915 (1.269794) 	&  899.302 (933.540000) \\
PHeBIE (pointwise) 	&  1.43643e+07 (68971648.765392) 	&  521.945 (2600.968901) 	&  0.280702 (0.993979) 	&  0.253737 (0.974556) 	&  0.400051 (1.513539) 	&  685.672 (714.210000) \\
Rodenburg \& Madien	&  67894.2 (314136.721878) 	&  14633.8 (61868.374313) &  0.265394 (0.999571) 	&  0.320546 (0.950676) 	&  0.382718 (1.281439) 	&  1168.36 (1177.710000) \\
Thibault {\em et al} with AP	&  1.4384e+07 (69070268.113760) 	&  30.4566 (143.800879) 	&  0.271003 (0.988666) 	&  0.233222 (0.893987) 	&  0.40385 (1.532388) 	&  915.6 (1058.900000) \\
Thibault {\em et al}	&  1.45199e+07 (69688092.797128) 	&  247.313 (976.203934) 	&  0.247628 (0.999996) 	&  0.0699934 (0.249758) 	&  0.174844 (0.568648) 	&  868.074 (892.190000) \\
  \hline
  \end{tabular}
 }\\
\end{table}




\begin{table}[htbp]
 \caption{Average (Worst) Results for noiseless simulated data with over-restrictive pupil constraint.}
 \vspace{5pt}
 \resizebox{\textwidth}{!}{
  \begin{tabular}{lcccccc} \hline
   Algorithm                   & $ F(u^{300})$      & $\|u^{300}-u^{299}\|^2$ & RMS-Object       & RMS-Probe        & R-factor         & Time (s)          \\ \hline
PHeBIE 	&  25653.2 (25656.118308) 	&  0.147375 (0.168246) 	&  0.0443148 (0.050070) 	&  0.0492462 (0.049411) 	&  0.293575 (0.293666) 	&  959.26 (1108.530000) \\
PHeBIE (pointwise) 	&  25653.8 (25660.133696) 	&  0.0621856 (0.085180) 	&  0.031421 (0.035558) 	&  0.0496418 (0.049935) 	&  0.293603 (0.293717) 	&  632.614 (645.090000) \\
Rodenburg \& Madien	&  3987.67 (4413.767532) 	&  6.29209 (16.305502) 	&  0.068898 (0.075991) 	&  0.0549872 (0.057044) 	&  0.283375 (0.283948) 	&  1190.19 (1309.230000) \\
Thibault {\em et al} with AP 	&  25644.3 (25649.881144) 	&  5.68678 (5.694388) 	&  0.935542 (0.935558) 	&  0.0492624 (0.049360) 	&  0.293505 (0.293595) 	&  865.082 (986.840000) \\
Thibault {\em et al} 	&  306602 (378554.381771) 	&  219.211 (267.930274) 	&  0.943222 (0.943665) 	&  0.189808 (0.243533) 	&  0.363432 (0.391603) 	&  897.666 (962.450000) \\
  \hline
  \end{tabular}
 }\\
\end{table}



\end{document}